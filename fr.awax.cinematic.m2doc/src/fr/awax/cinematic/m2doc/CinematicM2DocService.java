package fr.awax.cinematic.m2doc;

import org.eclipse.emf.ecore.EObject;
import org.obeonetwork.dsl.cinematic.flow.FlowFactory;
import org.obeonetwork.dsl.cinematic.flow.ViewState;
import org.obeonetwork.dsl.cinematic.view.ViewContainer;

public class CinematicM2DocService {
   
   /**
    * Exemple de service Java M2Doc.
    * @param pSelf Le contexte (ex : CinematicRoot).
    * @param pMessage Le message à ajouter.
    * @return Le chaîne désirée.
    */
   public static String helloWorld (EObject pSelf, String pMessage) {
      return "hello world : " + pMessage;
   }

//   /**
//    * Exemple pour instancier un ViewState avec une instance de ViewContainer.
//    * @param pViewContainer Le ViewContainer à ajouter.
//    * @return L'instance de ViewState avec le 'pViewContainer'.
//    */
//   public ViewState getDefaultViewState (ViewContainer pViewContainer) {
//      ViewState v = FlowFactory.eINSTANCE.createViewState();
//      v.getViewContainers().add(pViewContainer);
//
//      return v;
//   }
}
