package fr.awax.acceleo.generator1.ui.popupMenus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.acceleo.engine.utils.AcceleoLaunchingUtil;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.business.api.session.Session;

import fr.awax.acceleo.common.conf.AllPropAbstract;
import fr.awax.acceleo.common.dsl.Dsl_Enum;
import fr.awax.acceleo.common.engine.ui.AcceleoGeneratePluginAbstract;
import fr.awax.acceleo.generator1.main.MainGenGenerator1;

public class AcceleoGeneratePluginGenerator1 extends AcceleoGeneratePluginAbstract {

   @Override
   public void unzipAsserts (String pTargetAbsolutePath, AllPropAbstract pAllProp) {
//    String path = "";
//    super.doUnzipAsserts(pAllProp, path);
    throw new UnsupportedOperationException("A implémenter");
   }

   @Override
   public void runGenerationAcceleo (Dsl_Enum pDsl_Enum, Session pSession, String pTargetAbsolutePath, IProgressMonitor pMonitor, EObject pModelRoot) throws IOException {
      super.runGenerationAcceleo_root(pDsl_Enum, pSession, pTargetAbsolutePath, pMonitor, pModelRoot);
      MainGenGenerator1 gen0 = new MainGenGenerator1(pModelRoot, getAbsolutePathRoot(), new ArrayList<Object>());
//      MainGenGenerator1 gen0 = new MainGenGenerator1(pModelRoot, new File(pTargetAbsolutePath), new ArrayList<Object>());
      // Mémoriser le générateur lancé
      super.addGeneratorRunning(gen0);
      String generationID = AcceleoLaunchingUtil.computeUIProjectID(pDsl_Enum.getNom(), "fr.awax.acceleo.generator1.MainGenGenerator1", pSession.getSessionResource().getURI().toString(), pTargetAbsolutePath, new ArrayList<String>());
//      gen0.clear(); // NE PAS FAIRE LE CLEAR A CE NIVEAU (fait en automatique à la fin de la génération)
      gen0.setGenerationID(generationID);
      gen0.doGenerate(BasicMonitor.toMonitor(pMonitor));
   }

}
