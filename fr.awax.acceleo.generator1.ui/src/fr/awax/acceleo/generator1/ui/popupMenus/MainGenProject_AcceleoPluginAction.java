package fr.awax.acceleo.generator1.ui.popupMenus;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.engine.TrtPostGeneration;
import fr.awax.acceleo.common.engine.TrtPostGenerationEnum;
import fr.awax.acceleo.common.engine.ui.AcceleoGeneratePluginProjectAbstract;
import fr.awax.acceleo.generator1.conf.AllPropGenerator1;
import fr.awax.acceleo.generator1.main.MainGenGenerator1;
import fr.awax.acceleo.generator1.ui.Activator;

/**
 * Classe sollicitée à un évènement clic sur à la racine d'un 'Project'.
 * cf. plugin.xml
 */
public class MainGenProject_AcceleoPluginAction extends AcceleoGeneratePluginProjectAbstract {

   @Override
   public Object execute (final ExecutionEvent event) throws ExecutionException {
      String pluginId = Activator.PLUGIN_ID;
      EngineHelper vEngineHelper = EngineHelper.getInstance(pluginId);
      // Mémoriser l'identifiant du générateur
      vEngineHelper.setPluginId(pluginId);
      // Obtenir les propriétés pour le plugin de génération
      AllPropGenerator1 allProp = AllPropGenerator1.getInstance(pluginId);
      // Mémoriser les Properties de l'application
      vEngineHelper.setAllProperties(allProp);
      
      AcceleoGeneratePluginGenerator1 vAcceleoGeneratePlugin = new AcceleoGeneratePluginGenerator1();

      // Définir le traitement post-génération
      TrtPostGeneration vTrtPostGeneration = new TrtPostGeneration(TrtPostGenerationEnum.deleteEmptyLines);
      vTrtPostGeneration.addExtensionFile(".htm");
      vTrtPostGeneration.addExtensionFile(".html");
      vTrtPostGeneration.addXPropElem(allProp.propSpecif().path_generator1);

      return super.execute_root(allProp, vAcceleoGeneratePlugin, event, "Gen " + MainGenGenerator1.class.getName(), "Generation " + vEngineHelper.getPluginId(), null, vTrtPostGeneration);
   }
}
