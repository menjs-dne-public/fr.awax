package fr.awax.acceleo.ihm.rgaa_html.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.obeonetwork.dsl.cinematic.view.AbstractViewElement;
import org.obeonetwork.dsl.cinematic.view.ViewContainer;
import org.obeonetwork.dsl.cinematic.view.ViewElement;
import org.obeonetwork.dsl.environment.Annotation;
import org.obeonetwork.dsl.environment.MetaData;
import org.obeonetwork.dsl.environment.ObeoDSMObject;

import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.exception.ModelisationRuntimeException;
import fr.awax.acceleo.common.only_export.SceAbstractInstance;
import fr.awax.acceleo.common.only_export.dsl.cinematic.SceCommonDslCinematic;
import fr.awax.acceleo.common.util.MetadataUtils;

public class MetadataDslCinematic extends SceAbstractInstance {
//   static final String KEY_DATA = "DATA";
   /** Pour indiquer un Group de composants - Ex : Group de boutons Radio */
   static final String KEY_GROUP = "GROUP";
   /** Pour indiquer le nombre de lignes. */
   static final String KEY_NB_ROWS = "NB_ROWS";
   /** Nombre de FakeData par défaut : "0" désactivation. */
   static final int KEY_NB_LINES_FAKE_DATA = 0;
   static final String KEY_MENU_GAUCHE = "MENU_GAUCHE";

   /** Variable METADATA pour désactiver l'affichage de la première ligne de titres des colonnes d'une table. */
   public static final String cMetaVar_LINE_TITLE = "LINE_TITLE";
   /** Variable METADATA pour savoir si un composant est désactivé. */
   public static final String cMetaVar_COUNTABLE = "COUNTABLE";
   /** Variable METADATA pour indiquer si un composant doit être généré. */
   public static final String cMetaVar_GENERATED = "GENERATED";
   /** Variable METADATA pour savoir si un composant est désactivé. */
   public static final String cMetaVar_ComponentDisabled = "DISABLED";
   /** Variable METADATA pour indiquer si un composant est ENABLED. */
   public static final String cMetaVar_ENABLED = "ENABLED";
   /** Variable METADATA pour indiquer la taille de la fonte. */
   public static final String cMetaVar_FONT_SIZE = "FONT_SIZE";
   /** Variable METADATA pour indiquer la CLASS CSS. */
   public static final String cMetaVar_CSS_CLASS = "CSS_CLASS";
   /** Permet de définir le nombre de colonnes. */
   private static final String cMetaVar_NB_COLS = "NB_COLS";
   /** Permet de définir le nombre de lignes. */
   private static final String cMetaVar_NB_ROWS = "NB_ROWS";
   /** Variable METADATA pour indiquer si la CheckBox est coché. */
   public static final String cMetaVar_CHECKED = "CHECKED";
   /** Valeur METADATA "true". */
   public static final String cMetaVal_TRUE = "TRUE";

   private final String KEY_DOCUMENTATION_LINK = "DocumentationLink - ";

   private MetadataDslCinematic() {
      super();
   }

   private SceCommonDslCinematic _SceCommonDslCinematic;
   private MetadataDslCinematic(String pPluginId) {
      super(pPluginId);
      _SceCommonDslCinematic = SceCommonDslCinematic.getInstance(pPluginId);
   }

   private static Map<String, MetadataDslCinematic> _MapInstances = new LinkedHashMap<>();

   public static MetadataDslCinematic getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static MetadataDslCinematic getInstance (String pPluginId) {
      MetadataDslCinematic instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new MetadataDslCinematic(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      // TODO Auto-generated method stub
      
   }

   public List<Annotation> getRelatedDocuments (ObeoDSMObject obj) {
      List<Annotation> annotations = new ArrayList<>();
      if (obj != null && obj.getMetadatas() != null) {
         for (MetaData metadata : obj.getMetadatas().getMetadatas()) {
            if (metadata instanceof Annotation) {
               Annotation annotation = (Annotation) metadata;
               if (annotation.getTitle() != null && annotation.getTitle().startsWith(KEY_DOCUMENTATION_LINK)) {
                  annotations.add(annotation);
               }
            }
         }
      }
      return annotations;
   }

   /**
    * Obtenir la taille pour le champs à partir du contenu dans la METADATA "DATA".
    * @param pComposant Le composant à regarder.
    * @return La taille par défaut (si pas de METADATA), ou la taille du texte contenu dans "DATA".
    */
   public int getSizeOfDATA (ViewElement pComposant) {
      int vReturn;
//      String meta_DATA = MetadataUtils.getMetadataBody(pComposant, KEY_DATA);
      String meta_DATA = pComposant.getExample();
      // SI pas de METADATA "DATA"
      if (meta_DATA == null) {
         vReturn = 8;
      }
      else {
         vReturn = meta_DATA.length() + 1;
      }
      return vReturn;
   }

   public String getGROUP (AbstractViewElement pComposant) {
      String vReturn = MetadataUtils.getMetadataBody(pComposant, KEY_GROUP);
      return vReturn;
   }

   public int sizeList (ViewElement pElement) {
      int vReturn;

      // SI pas de nombre de lignes spécifié en 'KEY_NB_ROWS'
      Integer nbRows = getNbRows(pElement);
      if (nbRows == null) {
//         String fakeDataBrute = MetadataUtils.getMetadata(pElement, KEY_DATA);
         String fakeDataBrute = pElement.getExample();
         int size = getFakeData(pElement, null, fakeDataBrute).size();
         if (size == 0) {
            vReturn = 2;
         }
         else {
            vReturn = size;
         }
      }
      else {
         vReturn = nbRows;
      }

      return vReturn;
   }
   
   public List<String> getFakeDataOnly (ViewElement pElement, Integer pNbFakeData) {
      return getFakeData(pElement, pNbFakeData, false);
   }
   public List<String> getFakeData (ViewElement pElement, Integer pNbFakeData) {
      return getFakeData(pElement, pNbFakeData, true);
   }
   public List<String> getFakeData (ViewElement pElement, Integer pNbFakeData, boolean pIsLabelDefault) {
      List<String> vReturn;
//      String fakeDataBrute = MetadataUtils.getMetadata(pElement, KEY_DATA);
      String fakeDataBrute = pElement.getExample();
      // SI pas de FakeData
      if (fakeDataBrute == null) {
         if (pIsLabelDefault) {
            vReturn = new ArrayList<>();
            vReturn.add(pElement.getLabel());
         }
         else {
            vReturn = null;
         }
      }
      else {
         fakeDataBrute = fakeDataBrute.trim();
         // SI pas de FakeData
         if ("".equals(fakeDataBrute)) {
            if (pIsLabelDefault) {
               vReturn = new ArrayList<>();
               vReturn.add(pElement.getLabel());
            }
            else {
               vReturn = null;
            }
         }
         else {
            vReturn = getFakeData(pElement, pNbFakeData, fakeDataBrute);
         }
      }
      return vReturn;
   }

   public List<String> getFakeData (ViewElement pElement, Integer pNbFakeData, String pFakeDataBrute) {
      List<List<String>> data = new ArrayList<>();

      Integer nbRowsInMetadata = getNbRows(pElement);

      int nbRows;
      if (pNbFakeData == null) {
         nbRows = (nbRowsInMetadata != null ? nbRowsInMetadata : KEY_NB_LINES_FAKE_DATA);
      }
      else {
         nbRows = (nbRowsInMetadata != null ? nbRowsInMetadata : pNbFakeData);
      }

      if (pFakeDataBrute != null) {
         // Fake data have been set in the model
         // We just have to format them
         // format is aaa | bbb | ccc on multiple lines
         String[] lines = pFakeDataBrute.split("[\\r\\n]+");
         for (String line : lines) {
            String[] values = line.split("\\|");
            data.add(new ArrayList<>(Arrays.asList(values)));
         }
      }
      else {
         // No data set in model, we use the children to generate some
         if (pElement instanceof ViewContainer && !((ViewContainer) pElement).getOwnedElements().isEmpty()) {
            List<String> row = new ArrayList<>();
            for (AbstractViewElement child : ((ViewContainer) pElement).getOwnedElements()) {
               if (child.getLabel() != null) {
                  row.add(child.getLabel());
               }
               else {
                  row.add(child.getName());
               }
            }
            for (int i = 0; i < nbRows; i++) {
               data.add(row);
            }
         }
         else {
            if (nbRows == 0) {
               data.add(Arrays.asList(pElement.getLabel()));
            }
            else {
               // No children
               for (int i = 0; i < nbRows; i++) {
                  data.add(Arrays.asList("value " + (i + 1)));
               }
            }
         }
      }

      // ensure each row has the right number of elements
      if (pElement instanceof ViewContainer && !((ViewContainer) pElement).getOwnedElements().isEmpty()) {
         int size = ((ViewContainer) pElement).getOwnedElements().size();
         for (List<String> dataRow : data) {
            while (dataRow.size() < size) {
               dataRow.add("");
            }
            while (dataRow.size() > size) {
               dataRow.remove(dataRow.size() - 1);
            }
         }
      }

      // transform back into a list of strings with | separator
      // because List of List of strings are not supported in Ocl
      // we encode potential use of pipes in values too
      List<String> result = new ArrayList<>();
      for (List<String> row : data) {
         String stringWithSeparator = "";

         for (String cell : row) {
            String value = cell.replace("|", "&#124;");
            if ("".equals(value)) {
               value = " ";
            }
            stringWithSeparator += "|" + value;
         }

         // remove first pipe
         if (stringWithSeparator.length() > 0) {
            stringWithSeparator = stringWithSeparator.substring(1);
         }

         // remove end EOL
         if (stringWithSeparator.endsWith("\n")) {
            stringWithSeparator = stringWithSeparator.substring(0, stringWithSeparator.length()-"\n".length());
         }
         result.add(stringWithSeparator);
      }

      return result;
   }

   public Integer getNbRows (AbstractViewElement pElement) {
      Integer vReturn;
      String nbAsString = MetadataUtils.getMetadata(pElement, KEY_NB_ROWS);
      try {
         vReturn = new Integer(nbAsString);
      }
      catch (NumberFormatException e) {
         // Not a number, return null
         vReturn = null;
      }
      return vReturn;
   }

   public String getGenerated (AbstractViewElement pElement) {
      String metadataName = cMetaVar_GENERATED;
      String vReturn = getMetadataValue(pElement, metadataName, "TRUE");
      return vReturn;
   }
   
   public String getEnabled (AbstractViewElement pElement) {
      String metadataName = cMetaVar_ENABLED;
      String vReturn = getMetadataValue(pElement, metadataName, "TRUE");
      return vReturn;
   }

   public String getFontSize (AbstractViewElement pElement, String pDefaultValue) {
      String metadataName = cMetaVar_FONT_SIZE;
      String vReturn = getMetadataValue(pElement, metadataName, pDefaultValue);
      return vReturn;
   }
   
   public String getCssClass (AbstractViewElement pElement, String pDefaultValue) {
      String metadataName = cMetaVar_CSS_CLASS;
      String vReturn = getMetadataValue(pElement, metadataName, pDefaultValue);
      return vReturn;
   }
   
   public String getNbCols (AbstractViewElement pElement, String pDefaultValue) {
      String metadataName = cMetaVar_NB_COLS;
      String vReturn = getMetadataValue(pElement, metadataName, pDefaultValue);
      return vReturn;
   }
   
   public String getNbRows (AbstractViewElement pElement, String pDefaultValue) {
      String metadataName = cMetaVar_NB_ROWS;
      String vReturn = getMetadataValue(pElement, metadataName, pDefaultValue);
      return vReturn;
   }

   private String getMetadataValue (AbstractViewElement pElement, String metadataName, String pDefaultValue) {
      String vReturn;
      String metadataValue = MetadataUtils.getMetadata(pElement, metadataName);
      if (metadataValue == null) {
         if (pDefaultValue == null || "".equals(pDefaultValue)) {
            throw new ModelisationRuntimeException("Le composant \"" + pElement.getName() + "\" ne possède pas de valeur par défaut ni de METADATA", "Indiquer une METADATA sur \"" + metadataName + "\" sur le composant \"" + pElement.getName() + "\"");
         }
         else {
            vReturn = pDefaultValue;
         }
      }
      else {
         vReturn = metadataValue;
      }
      return vReturn;
   }

   /**
    * Savoir si la CheckBox est cochée.
    * @return 'true' si elle est cochée.
    */
   public boolean isCheckBoxChecked (ViewElement pViewElement) {
      boolean vReturn;

      String example = pViewElement.getExample();
//      String vMetaVar_CHECKED = MetadataUtils.getMetadata(pViewElement, cMetaVar_CHECKED);
      if (example == null || "".equals(example)) {
         vReturn = false;
      }
      else {
         if (cMetaVar_CHECKED.equalsIgnoreCase(example) || cMetaVal_TRUE.equalsIgnoreCase(example)) {
            vReturn = true;
         }
         else {
            vReturn = false;
         }
      }

      return vReturn;
   }

   /**
    * Savoir si un élément est désactivé.
    * @param pObeoDSMObject L'élément à regarder.
    * @return 'true' s'il est désactivé
    */
   public boolean isDisabled_sce (ObeoDSMObject pObeoDSMObject) {
      boolean vReturn;

      String value = MetadataUtils.getMetadata(pObeoDSMObject, cMetaVar_ComponentDisabled);
      // Si pas de METADATA
      if (value == null) {
         vReturn = false;
      }
      else {
         if (value.trim().toUpperCase().equals(cMetaVal_TRUE)) {
            vReturn = true;
         }
         else {
            vReturn = false;
         }

      }

      return vReturn;
   }

   public boolean isMetadata_LINE_TITLE_sce (ViewContainer pViewContainer) {
      boolean vReturn;

      String value = MetadataUtils.getMetadata(pViewContainer, cMetaVar_LINE_TITLE);
      if ("FALSE".equalsIgnoreCase(value)) {
         vReturn = false;
      }
      else {
         vReturn = true;
      }

      return vReturn;
   }

}
