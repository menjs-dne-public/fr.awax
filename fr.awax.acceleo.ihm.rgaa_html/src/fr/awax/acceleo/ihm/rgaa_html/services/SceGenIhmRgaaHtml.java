package fr.awax.acceleo.ihm.rgaa_html.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.obeonetwork.dsl.cinematic.CinematicElement;
import org.obeonetwork.dsl.cinematic.CinematicRoot;
import org.obeonetwork.dsl.cinematic.NamedElement;
import org.obeonetwork.dsl.cinematic.flow.ActionState;
import org.obeonetwork.dsl.cinematic.flow.Flow;
import org.obeonetwork.dsl.cinematic.flow.FlowFactory;
import org.obeonetwork.dsl.cinematic.flow.FlowState;
import org.obeonetwork.dsl.cinematic.flow.ViewState;
import org.obeonetwork.dsl.cinematic.toolkits.Widget;
import org.obeonetwork.dsl.cinematic.view.AbstractViewElement;
import org.obeonetwork.dsl.cinematic.view.Layout;
import org.obeonetwork.dsl.cinematic.view.ViewContainer;
import org.obeonetwork.dsl.cinematic.view.ViewContainerReference;
import org.obeonetwork.dsl.cinematic.view.ViewElement;
import org.obeonetwork.dsl.cinematic.view.ViewFactory;
import org.obeonetwork.dsl.environment.Attribute;
import org.obeonetwork.dsl.environment.ObeoDSMObject;
import org.obeonetwork.dsl.environment.Reference;
import org.obeonetwork.dsl.requirement.Category;
import org.obeonetwork.dsl.requirement.Repository;
import org.obeonetwork.dsl.requirement.Requirement;
import org.obeonetwork.dsl.soa.Component;
import org.obeonetwork.dsl.soa.Operation;
import org.obeonetwork.dsl.soa.Parameter;

import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.exception.GenerationRuntimeException;
import fr.awax.acceleo.common.exception.ModelisationRuntimeException;
import fr.awax.acceleo.common.only_export.SceAbstractInstance;
import fr.awax.acceleo.common.only_export.ScePathInstance;
import fr.awax.acceleo.common.only_export.dsl.cinematic.ComplexiteWidget_Enum;
import fr.awax.acceleo.common.only_export.dsl.cinematic.SceCommonDslCinematic;
import fr.awax.acceleo.common.only_export.dsl.requirement.SceCommonDslRequirement;
import fr.awax.acceleo.common.util.DirectoryUtils;
import fr.awax.acceleo.common.util.MetadataUtils;
import fr.awax.acceleo.common.util.StringUtils;
import fr.awax.acceleo.ihm.rgaa_htmlconf.AllPropGenIhmHtmlRgaa;

public class SceGenIhmRgaaHtml extends SceAbstractInstance {
   /** Le répertoire racine contenant les écrans. */
   private static final String cPath_Pages = "pages/";
   /** Le répertoire racine contenant les services. */
   private static final String cPath_Services = "services/";
   
   /** Le préfixe pour une ligne de FakeData qui doit être SELECTED. */
   private static final String cPreffixDataSelected = "--->";

   private ScePathInstance _ScePathInstance;
   private SceCommonDslRequirement _SceCommonDslRequirement;
   private SceCommonDslCinematic _SceCommonDslCinematic;
   private MetadataDslCinematic _MetadataDslCinematic;

   public SceGenIhmRgaaHtml() {
      super();
   }

   public SceGenIhmRgaaHtml(String pPluginId) {
      super(pPluginId);
//      _ScePathInstance = ScePathInstance.getInstance(pPluginId);
//      _SceCommonDslRequirement = SceCommonDslRequirement.getInstance(pPluginId);
//      _SceCommonDslCinematic = SceCommonDslCinematic.getInstance(pPluginId);
//      _MetadataDslCinematic = MetadataDslCinematic.getInstance(pPluginId);
   }

   private static Map<String, SceGenIhmRgaaHtml> _MapInstances = new LinkedHashMap<>();

   public static SceGenIhmRgaaHtml getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static SceGenIhmRgaaHtml getInstance (String pPluginId) {
      SceGenIhmRgaaHtml instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new SceGenIhmRgaaHtml(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      _MapInstances.clear();
      _ListProfilGDA.clear();
      _Map_rg_nextFakeData.clear();
      _MapAbsolutePackageOfViewContainer.clear();
      getScePathInstance().clear();
      getSceCommonDslCinematic().clear();
   }

   public void clearDeep () {
      clear();
      _MapFlowViewState.clear();
      _MapFlowViewStateReverse.clear();
   }

   public AllPropGenIhmHtmlRgaa getAllProp () {
      return (AllPropGenIhmHtmlRgaa) super.getAllProp();
   }

   private ScePathInstance getScePathInstance () {
      if (_ScePathInstance == null) {
         _ScePathInstance = ScePathInstance.getInstance(getPluginId());
      }
      return _ScePathInstance;
   }

   private SceCommonDslRequirement getSceCommonDslRequirement () {
      if (_SceCommonDslRequirement == null) {
         _SceCommonDslRequirement = SceCommonDslRequirement.getInstance(getPluginId());
      }
      return _SceCommonDslRequirement;
   }

   private SceCommonDslCinematic getSceCommonDslCinematic () {
      if (_SceCommonDslCinematic == null) {
         _SceCommonDslCinematic = SceCommonDslCinematic.getInstance(getPluginId());
      }
      return _SceCommonDslCinematic;
   }

   private MetadataDslCinematic getMetadataDslCinematic () {
      if (_MetadataDslCinematic == null) {
         _MetadataDslCinematic = MetadataDslCinematic.getInstance(getPluginId());
      }
      return _MetadataDslCinematic;
   }
   
   /**
    * Faire un RAZ pour démarrer la génération d'un ViewContainer.
    */
   public void raz_fileViewContainerHtml_sce () {
      clear();
   }

   /**
    * Obtenir la taille de la font.
    * 
    * @param pElement Le widget contenant la METADATA.
    * @param pDefautFontSize La taille par défaut.
    * @return La taille spécifiée ou la 'pDefautFontSize' si non indiquée.
    */
   public String getMetadata_FONT_SIZE_sce (AbstractViewElement pElement, String pDefautFontSize) {
      return getMetadataDslCinematic().getFontSize(pElement, pDefautFontSize);
   }

   /**
    * Obtenir la CSS à appliquer sur le Widget.
    * 
    * @param pElement Le widget contenant la METADATA.
    * @param pDefaultCssClass La classe CSS à appliquer sur pas de METADATA
    *           sépcifié.
    * @return La CSS désirée.
    */
   public String getMetadata_CSS_CLASS_sce (AbstractViewElement pElement, String pDefaultCssClass) {
      return getMetadataDslCinematic().getCssClass(pElement, pDefaultCssClass);
   }

   public String getMetadata_NB_COLS_sce (AbstractViewElement pElement) {
      return getMetadataDslCinematic().getNbCols(pElement, "50");
//      String vReturn;
//      
//      String value = MetadataUtils.getMetadata(pElement, KEY_NB_COLS);
//      if (value == null) {
//         vReturn = "50";
//      }
//      else {
//         vReturn = value;
//      }
//      
//      return vReturn;
   }
   
   public String getMetadata_NB_ROWS_sce (AbstractViewElement pElement) {
      return getMetadataDslCinematic().getNbRows(pElement, "2");
//      String vReturn;
//
//      String value = MetadataUtils.getMetadata(pElement, KEY_NB_ROWS);
//      if (value == null) {
//         vReturn = "3";
//      }
//      else {
//         vReturn = value;
//      }
//      
//      return vReturn;
   }
   
   public final String get_path_maquette_html_sce () {
      String vReturn = getAllProp().propCommon().dir_generation_projet_root.getValue().toString() + "/"
            + getAllProp().propSpecif().path_maquette_html.getValue().toString() + "/";
      vReturn = DirectoryUtils.uniformPath(vReturn);
      return getScePathInstance().doRelativePath(null, vReturn);
   }

   /** La racine du modèle SOA. */
   private org.obeonetwork.dsl.soa.System _RootSoa;

   public void doMemModelSoa_sce (org.obeonetwork.dsl.soa.System pRootSoa) {
      _RootSoa = pRootSoa;
   }

   /**
    * La Map contenant les écrans (= Pages) du modèle. Rappel : 1 ViewState doit
    * avoir 1 et 1 seul ViewContainer (de type Page)
    */
//   private Map<String, ViewState> _MapViewState = new HashMap<>();
//   private Map<ViewState, String> _MapViewStateReverse = new HashMap<>();
   private Map<Flow, Map<String, ViewState>> _MapFlowViewState = new HashMap<>();
   private Map<Flow, Map<ViewState, String>> _MapFlowViewStateReverse = new HashMap<>();

   /**
    * Mémoriser les éléments du modèle Cinematic..
    * 
    * @param pRoot La racine du modèle Requirement.
    */
   public void doMemModelCinematic_sce (CinematicRoot pRoot) {
      List<Flow> listFlow = getListFlow_sce(pRoot);
      for (Flow vFlow : listFlow) {
         Map<String, ViewState> vMapViewState = _MapFlowViewState.get(vFlow);
         Map<ViewState, String> vMapViewStateReverse = _MapFlowViewStateReverse.get(vFlow);
         // SI pas encore mis en cache
         if (vMapViewState == null) {
            cachedViewContainer(pRoot);
            cachedViewState(pRoot, vFlow);
         }
      }
   }

   private void cachedViewState (CinematicRoot pRoot, Flow vFlow) {
      Map<String, ViewState> vMapViewState;
      Map<ViewState, String> vMapViewStateReverse;
      // Instancier le cache du Flow
      vMapViewState = new HashMap<>();
      _MapFlowViewState.put(vFlow, vMapViewState);
      // Instancier le cache reverse du Flow
      vMapViewStateReverse = new HashMap<>();
      _MapFlowViewStateReverse.put(vFlow, vMapViewStateReverse);

      List<ViewState> listViewState = getListViewState_sce(pRoot, vFlow);
      String name;
      // Parcourir les ViewState
      for (ViewState vViewState : listViewState) {
//         // Obtenir le ViewContainer associé au ViewState
//         ViewContainer vViewContainer = getViewContainer_sce(vFlow, vViewState);
         // Le nom de l'écran en absolu est porté par le ViewState
         name = getAbsoluteNameViewState(vViewState);
//			name = getNamePage(vViewState);
         ViewState instance = vMapViewState.get(name);
         // SI jamais rencontré
         if (instance == null) {
            // Mémoriser le ViewState dans les 2 sens
            vMapViewState.put(name, vViewState);
            vMapViewStateReverse.put(vViewState, name);
         }
//         else {
//            throw new ModelisationRuntimeException("Le nom de l'écran doit être unique : " + name,
//                  "Renommer l'écran qui a été trouvé au moins 2 fois : " + name);
//         }
      }
   }

   /**
    * Mise en cache des 'ViewContainer'.
    * 
    * @param pRoot La racine du modèle Cinematic.
    * @param pFlow Le Flow courant.
    */
   private void cachedViewContainer (CinematicRoot pRoot) {
      // Parcourir les 'ViewContainer' à la racine
      for (ViewContainer vViewContainer : pRoot.getViewContainers()) {
         cachedViewContainer(pRoot, vViewContainer);
      }

      // Parcourir les sous packages
      for (org.obeonetwork.dsl.cinematic.Package vPackage : pRoot.getSubPackages()) {
         cachedPackage(pRoot, vPackage, vPackage.getName());
      }
   }

   /** La Map mémorisant le nom de package en absolu pour chaque ViewContainer. */
   private Map<ViewContainer, String> _MapAbsolutePackageOfViewContainer = new LinkedHashMap<>();

   private void cachedPackage (CinematicRoot pRoot, org.obeonetwork.dsl.cinematic.Package pPackage, String pAbsolutePackage) {
      // Parcourir les 'ViewContainer' à la racine du package
      for (ViewContainer vViewContainer : pPackage.getViewContainers()) {
         // Mémoriser le chemin en absolu de 'vViewContainer'
         _MapAbsolutePackageOfViewContainer.put(vViewContainer, pAbsolutePackage);
         cachedViewContainer(pRoot, vViewContainer);
      }

      // Parcourir récursivement les sous packages
      for (org.obeonetwork.dsl.cinematic.Package vPackage : pPackage.getSubPackages()) {
         cachedPackage(pRoot, vPackage, pAbsolutePackage + "." + vPackage.getName());
      }
   }

   private void cachedViewContainer (CinematicRoot pRoot, ViewContainer pViewContainer) {
      // Parcourir les widgets du conteneur
      for (ViewElement vViewElement : pViewContainer.getViewElements()) {
         // SI c'est un Radio bouton
         if (vViewElement.getWidget().getName().equalsIgnoreCase(ComplexiteWidget_Enum.widgetRadio.getNom())) {
            cachedRadioGroup(vViewElement);
         }
      }

      // Parcourir récursivement les sous conteneur
      for (ViewContainer vViewContainer : pViewContainer.getViewContainers()) {
         cachedViewContainer(pRoot, vViewContainer);
      }
   }

   /** La Map pour mettre en cache des conteneurs virtuels. */
   private Map<String, Layout> _MapViewContainerVirtual = new HashMap<>();

   /**
    * Mise en cache du 'RadioGroup' dans le cas d'une METADATA 'GROUP' sur le
    * bouton Radio.
    * 
    * @param pViewElement Le widget à regarder.
    */
   private void cachedRadioGroup (ViewElement pViewElement) {
      // SI METADATA "GROUP" du le bouton radio
      if (isRadioGroupVirtual_sce(pViewElement)) {
         // Obtenir le nom du groupe de bouton radio
         String groupName = getMetadataDslCinematic().getGROUP(pViewElement);

         Layout vLayoutViewContainer = _MapViewContainerVirtual.get(groupName);
         // SI pas encore rencontré
         if (vLayoutViewContainer == null) {
            ViewContainer vViewContainer = ViewFactory.eINSTANCE.createViewContainer();
            // Donner un nom au conteneur 'RadioGroup'
            vViewContainer.setName("grp_" + groupName);
            vViewContainer.setLabel("");
            // Typer le coneneur en 'widgetRadioGroup'
            Widget vWidget = org.obeonetwork.dsl.cinematic.toolkits.ToolkitsFactory.eINSTANCE.createWidget();
            vWidget.setName(ComplexiteWidget_Enum.widgetRadioGroup.getNom());
            vViewContainer.setWidget(vWidget);
            // Wrapper le conteneur dans un Layout
            vLayoutViewContainer = wrapWithLayout_sce(vViewContainer);
            // Mémoriser
            _MapViewContainerVirtual.put(groupName, vLayoutViewContainer);
         }
         // Wrapper le widget 'Radio' dans un Layout
         Layout vLayoutViewElement = wrapWithLayout_sce(pViewElement);
         // Ajouter le layout du widget au layout du conteneur
         vLayoutViewContainer.getOwnedLayouts().add(vLayoutViewElement);
      }
   }

   /**
    * Obtenir le nom d'un écran en absolu.
    * 
    * @param pViewState L'instance ViewState contenant le ViewConteneur.
    * @return Le nom en absolu (ex : "enseignant.titulaire.EcrSaisirEnseignantTitulair").
    */
   private String getAbsoluteNameViewState (ViewState pViewState) {
      ViewContainer viewContainer = pViewState.getViewContainers().get(0);
      String absolutePackage = _MapAbsolutePackageOfViewContainer.get(viewContainer);
      String vReturn;
      if (absolutePackage == null) {
         // Pas de package défini : on met "root" en package racine
         vReturn = "root." + getScreenName(pViewState);
      }
      else {
         vReturn = absolutePackage + "." + getScreenName(pViewState);
      }
      return vReturn;
   }

   public String getScreenName_sce (ViewState pViewState, ViewContainer pViewContainer, Requirement pProfilGDA) {
      // TODO : Gérer le 'pProfilGDA'
      return getScreenName(pViewState);
   }

   /**
    * Obtenir le nom d'un écran.
    * 
    * @param pViewState L'instance ViewState contenant le ViewConteneur.
    * @return Le nom en absolu (ex : "EcrSaisirEnseignantTitulair").
    */
   private String getScreenName (ViewState pViewState) {
      // On retourne le nom du ViewState afin de pouvoir associer un ViewContainer à plusieurs ViewState
      String vReturn = pViewState.getName();

      if ("".equals(vReturn)) {
         throw new ModelisationRuntimeException("La ViewState doit avoir un nom, le technicalId étant : " + pViewState.getTechnicalid(),
               "Remplisser la propriété 'name' du ViewState");
      }
//      if (pViewState.getViewContainers().size() == 1) {
//         ViewContainer viewContainer = pViewState.getViewContainers().get(0);
//         vReturn = viewContainer.getName();
//      }
//      else {
//         throw new ModelisationRuntimeException("La ViewState doit avoir un ViewContainer (et un seul) : " + pViewState.getName(),
//               "Ajouter au ViewState '" + pViewState.getName() + "' un ViewContainer de type " + ComplexiteWidget_Enum.widgetPage.getNom());
//      }
      return vReturn;
   }

   /**
    * Mémoriser les exigences.
    * 
    * @param pRepository La racine du modèle Requirement.
    * @see #cPreffixProfilGDA
    */
   public void doMemModelRequirement_sce (Repository pRepository) {
      // Parcourir les catégories d'exigences
      for (Category vCategory : pRepository.getOwnedCategories()) {
         // SI la 'Category' représente une liste de "profil GDA"
         if (vCategory.getId().startsWith(cPreffixProfilGDA)) {
            // Parcourir les 'Requirement' de la 'Category'
            for (Requirement vRequirement : vCategory.getOwnedRequirements()) {
               // Mémoriser le "Profil GDA"
               _ListProfilGDA.add(vRequirement);
            }
         }
      }
   }

   /** Le cache de la liste des 'Flow'. */
   private List<Flow> _ListFLow = null;

   /**
    * Obtenir la liste des Flows du modèle. Si vide : en créé un par défaut.
    * 
    * @param pRoot La racine du modèle.
    * @return La liste des 'Flow' (au moins 1 élément garanti).
    */
   public List<Flow> getListFlow_sce (CinematicRoot pRoot) {
      List<Flow> vReturn;

      if (_ListFLow == null) {
         if (isFlowEmpty(pRoot.getFlows())) {
            vReturn = new ArrayList<>();
            // Instancier un Flow
            Flow vFlow = FlowFactory.eINSTANCE.createFlow();
            vFlow.setName("defaultFlow");
            vReturn.add(vFlow);
         }
         else {
            vReturn = pRoot.getFlows();
         }
         _ListFLow = vReturn;
      }
      else {
         vReturn = _ListFLow;
      }

      return vReturn;
   }

   private boolean isFlowEmpty (EList<Flow> pListFlow) {
      boolean vReturn;

      if (pListFlow == null) {
         vReturn = true;
      }
      else if (pListFlow.size() == 0) {
         vReturn = true;
      }
      else if (pListFlow.size() == 1) {
         // SI uniquement le InitialState
         if (pListFlow.get(0).getStates().size() == 1) {
            vReturn = true;
         }
         else {
            vReturn = false;
         }
      }
      else {
         vReturn = false;
      }
      return vReturn;
   }

   public List<ActionState> getListActionState_sce (CinematicRoot pRoot, Flow pFlow) {
      List<ActionState> vReturn = new ArrayList<>();

      // SI pas de Flow : uniquement des écrans (= ViewContainer)
      if (pFlow == null) {
         // RAS
//         // Instancier un Flow
//         Flow vFlow = FlowFactory.eINSTANCE.createFlow();
//         createActionStateForFlow(pRoot, vReturn, vFlow);
      }
      else {
         ActionState vActionState;
         // SI le flow est vide
         if (pFlow.getStates().size() == 0) {
            // RAS
//            createActionStateForFlow(pRoot, vReturn, pFlow);
         }
         else {
            for (FlowState vFlowState : pFlow.getStates()) {
               // SI c'est un ActionState
               if (vFlowState instanceof ActionState) {
                  vActionState = (ActionState) vFlowState;
                  vReturn.add(vActionState);
               }
            }
         }
      }

      return vReturn;
   }

   /**
    * Permet d'obtenir une liste de 'ViewState' :
    * - Soit depuis un Flow spécifié : pFlow (= cinematic des écrans)
    * - Soit en instanciant 1 ViewState par ViewContainer (= uniquement des Mockups d'écrans)
    * 
    * @param pRoot La racine de Cinematic.
    * @param pFlow Le Flow (si null : uniquement Mockup).
    * @return La liste désirée.
    */
   public List<ViewState> getListViewState_sce (CinematicRoot pRoot, Flow pFlow) {
      List<ViewState> vReturn = new ArrayList<>();

      // SI pas de Flow : uniquement des écrans (= ViewContainer)
      if (pFlow == null) {
         // Instancier un Flow
         Flow vFlow = FlowFactory.eINSTANCE.createFlow();
         createViewStateForFlow(pRoot, vReturn, vFlow);
      }
      else {
         ViewState vViewState;
         // SI le flow est vide
         if (pFlow.getStates().size() == 0) {
            createViewStateForFlow(pRoot, vReturn, pFlow);
         }
         else {
            for (FlowState vFlowState : pFlow.getStates()) {
               // SI c'est un ViewState
               if (vFlowState instanceof ViewState) {
                  vViewState = (ViewState) vFlowState;
                  vReturn.add(vViewState);
               }
            }
         }
      }

      return vReturn;
   }

   private void createViewStateForFlow (CinematicRoot pRoot, List<ViewState> vReturn, Flow vFlow) {
      List<ViewContainer> listViewContainer = pRoot.getViewContainers();
      // SI pas de ViewContainer à la racine
      if (listViewContainer == null || listViewContainer.isEmpty()) {
         List<org.obeonetwork.dsl.cinematic.Package> listSubPackage = pRoot.getSubPackages();
         // SI pas de package défini
         if (listSubPackage == null || listSubPackage.isEmpty()) {
            // RAS : on retourne la liste vide
         }
         else {
            getListViewState_intern(vReturn, pRoot, vFlow, listSubPackage, "");
         }
      }
      else {
         // Associer un ViewState à chaque ViewContainer de la racine
         associateViewStateForViewContainer(vReturn, vFlow, listViewContainer, null);
      }
   }

   private void getListViewState_intern (List<ViewState> pReturn, CinematicRoot pRoot, Flow pFlow,
         List<org.obeonetwork.dsl.cinematic.Package> pListPackage, String pPackageName) {
      List<org.obeonetwork.dsl.cinematic.Package> vSubPackage;
      String packageName;
      for (org.obeonetwork.dsl.cinematic.Package vPackage : pListPackage) {
         vSubPackage = vPackage.getSubPackages();
         packageName = pPackageName + "." + vPackage.getName();
         // Associer un ViewState à chaque ViewContainer du Package
         associateViewStateForViewContainer(pReturn, pFlow, vPackage.getViewContainers(), packageName);
         if (vSubPackage != null) {
            getListViewState_intern(pReturn, pRoot, pFlow, vSubPackage, packageName);
         }
      }
   }

   /**
    * Associer un VIewState à chaque ViewContainer 'pListViewContainer'.
    * 
    * @param pReturn (Out)(*) La liste résultat.
    * @param pListViewContainer La liste de ViewContainer à associer.
    * @param pPackageName Le nom du package courant.
    */
   private void associateViewStateForViewContainer (List<ViewState> pReturn, Flow pFlow,
         List<ViewContainer> pListViewContainer, String pPackageName) {
      ViewState vViewState;
      for (ViewContainer vViewContainer : pListViewContainer) {
         // Instancier un ViewState avec une instance de ViewContainer
         vViewState = FlowFactory.eINSTANCE.createViewState();
         String name;
         if (pPackageName == null) {
            name = vViewContainer.getName();
         }
         else {
            name = sanitizedPackageName(pPackageName + "." + vViewContainer.getName());
         }
         vViewState.setName(name);
         vViewState.getViewContainers().add(vViewContainer);
         // Ajouter le ViewState au Flow
         pFlow.getStates().add(vViewState);
         pReturn.add(vViewState);
      }
   }

   /**
    * Netoyer le nom d'un package d'écran.
    * 
    * @param pPackageName Le nom d'origine.
    * @return Le nom désiré.
    */
   private String sanitizedPackageName (String pPackageName) {
      String vReturn = pPackageName;
      if (pPackageName.startsWith(".")) {
         vReturn = pPackageName.substring(".".length(), pPackageName.length());
      }
      return vReturn;
   }

   /**
    * Obtenir le ViewContainer associé au ViewState.
    * 
    * @param pViewState Le ViewState a regarder.
    * @return Le ViewContainer désiré ou une exception si plusieurs.
    */
   public ViewContainer getViewContainer_sce (Flow pFlow, ViewState pViewState) {
      ViewContainer vReturn;

      List<ViewContainer> listViewContainer = pViewState.getViewContainers();
      // SI aucun ViewContainer associé au ViewState
      if (listViewContainer == null || listViewContainer.size() == 0) {
         throw new ModelisationRuntimeException("La ViewState doit avoir un ViewContainer : " + pViewState.getName(),
               "Ajouter au ViewState '" + pViewState.getName() + "' un ViewContainer de type "
                     + ComplexiteWidget_Enum.widgetPage.getNom());
      }
      else {
         if (listViewContainer.size() == 1) {
            vReturn = listViewContainer.get(0);
         }
         else {
            vReturn = getViewContainer_intern(listViewContainer, ComplexiteWidget_Enum.widgetPage.getNom());
            if (vReturn == null) {
               throw new ModelisationRuntimeException(
                     "La ViewState doit avoir un seul ViewContainer : " + pViewState.getName(),
                     "Sur le ViewState '" + pViewState.getName() + "', laisser uniquement un ViewContainer de type "
                           + ComplexiteWidget_Enum.widgetPage.getNom());
            }
         }
      }

      return vReturn;
   }

   private ViewContainer getViewContainer_intern (List<ViewContainer> pListViewContainer, String pWidgetName) {
      ViewContainer vReturn = null;
      for (ViewContainer viewContainer : pListViewContainer) {
         String widgetName = viewContainer.getWidget().getName();
         if (pWidgetName.equals(widgetName)) {
            vReturn = viewContainer;
            break;
         }
      }
      return vReturn;
   }
   
   public String getFileName_ViewActionHtml_sce (Flow pFlow, ActionState pActionState) {
      return pActionState.getName();
   }

   public String getPackageName_ViewActionHtml_sce (Flow pFlow, ActionState pActionState) {
      String vReturn;
      if (pActionState == null) {
         vReturn = null;
      }
      else {
         vReturn = cPath_Services;
      }

      return vReturn;
   }

   public String getFileName_ViewContainerHtml_sce (Flow pFlow, ViewState pViewState) {
      String vReturn;

      if (pFlow == null) {
         throw new GenerationRuntimeException("Erreur application : pFlow est null", "Revoir l'algo");
      }

      Map<ViewState, String> vMapViewStateReverse = _MapFlowViewStateReverse.get(pFlow);
      // Ex : "enseignant.titulaire.EcrSaisirEnseignantTitulaire"
      String fullName = vMapViewStateReverse.get(pViewState);
      if (fullName == null) {
         throw new IllegalStateException(
               "Le ViewContainer n'a pas été trouvé dans _MapViewContainerReverse : " + pViewState.getName());
      }
      else {
         // Astuce pour se placer dans un contexte de path pour prendre le nom de l'écran
         // (= répertoire final)
         // Ex : "enseignant/titulaire/EcrSaisirEnseignantTitulaire/"
         fullName = StringUtils.replace(fullName, ".", "/") + "/";
         // Ex : "EcrSaisirEnseignantTitulaire"
         vReturn = DirectoryUtils.getLastDirectory(fullName);
      }

      return vReturn;
   }

   public String getPackageName_FlowStateHtml_sce (Flow pFlow, FlowState pFlowState) {
      String vReturn;
      if (pFlowState == null) {
         vReturn = null;
      }
      // SI transition vers un écran
      else if (pFlowState instanceof ViewState) {
         ViewState vViewState = (ViewState) pFlowState;
         vReturn = getPackageName_ViewContainerHtml_sce(pFlow, vViewState);
      }
      // SI transition vers un service
      else if (pFlowState instanceof ActionState) {
         ActionState vActionState = (ActionState) pFlowState;
         vReturn = getPackageName_ViewActionHtml_sce(pFlow, vActionState);
      }
//      else if (pViewState instanceof ViewAction) {
//         ViewAction vViewAction = (ViewAction) pViewState;
//         vReturn = getPackageName_ViewContainerHtml_sce(pFlow, vViewAction);
//      }
      else {
         throw new IllegalArgumentException("Erreur, cas non prévu pour pViewState=" + pFlowState.getTechnicalid());
      }

      return vReturn;
   }

   public String getPackageName_ViewContainerHtml_sce (Flow pFlow, ViewState pViewState) {
      String vReturn = cPath_Pages;

      if (pFlow == null) {
         throw new GenerationRuntimeException("Erreur application : pFlow est null", "Revoir l'algo");
      }

      Map<ViewState, String> vMapViewStateReverse = _MapFlowViewStateReverse.get(pFlow);
//      ViewContainer vViewContainer = getViewContainer_sce(pViewState)

      // Obtenir le nom complet de l'écran
      // Ex : "enseignant.titulaire.EcrSaisirEnseignantTitulaire"
      String fullName = vMapViewStateReverse.get(pViewState);
      if (fullName == null) {
         throw new IllegalStateException(
               "Le ViewContainer n'a pas été trouvé dans _MapViewContainerReverse : " + pViewState.getName());
      }
      else {
         // Ex : "EcrSaisirEnseignantTitulaire"
         String name = getFileName_ViewContainerHtml_sce(pFlow, pViewState);
         // Ex : "enseignant/titulaire/EcrSaisirEnseignantTitulaire/"
         fullName = StringUtils.replace(fullName, ".", "/") + "/";
         int rang = fullName.indexOf(name);
         // Ex : "enseignant/titulaire"
         String pathDroit = fullName.substring(0, rang);

         vReturn = vReturn + pathDroit;
      }

      return vReturn;
   }

   /**
    * Obtenir la liste des profils Gestion Droits d'Accès (= GDA) sur l'écran.
    * 
    * @return La liste d'exigences représentant un "profil GDA".
    * @see #cPreffixProfilGDA
    */
   public List<Requirement> getListProfilGdaOfScreen_sce (ViewState pViewState) {
      List<Requirement> vReturn = new ArrayList<>();
      List<Requirement> vListAllRequirementsOfViewState = getSceCommonDslCinematic()
            .getAllRequirementsOfViewState(pViewState);
      // Filtrer sur les exigences qui sont de type "Profil GDA"
      for (Requirement requirement : vListAllRequirementsOfViewState) {
         // SI c'est de type 'Profil GDA'
         if (_ListProfilGDA.contains(requirement)) {
            vReturn.add(requirement);
         }
      }

      return vReturn;
   }

   public ViewContainer getPage_sce (ViewState pViewState) {
      return getSceCommonDslCinematic().getPage_sce(pViewState);
   }

   public static String getHtmlChecked_sce (ViewElement pViewElement) {
      String vReturn;
      boolean isChecked = isCheckBoxChecked(pViewElement);
      if (isChecked) {
         vReturn = "CHECKED";
      }
      else {
         vReturn = "";
      }

      return vReturn;
   }

   /**
    * Savoir si la CheckBox est cochée.
    * 
    * @return 'true' si elle est cochée.
    */
   public static boolean isCheckBoxChecked (ViewElement pViewElement) {
      boolean vReturn;

      String vMetaVar_CHECKED = MetadataUtils.getMetadata(pViewElement, MetadataDslCinematic.cMetaVar_CHECKED);
      if (vMetaVar_CHECKED != null) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }

      return vReturn;
   }

   /**
    * Permet de formater le nom afin qu'il soit compatible avec un fichier.
    * 
    * @param pNameBrut LE nom de fichier brut (ex : "monService()").
    * @return Le nom désiré (ex : "monService").
    */
   public String getNameForFile_sce (String pNameBrut) {
      String vReturn = pNameBrut;

      vReturn = StringUtils.replace(vReturn, "(", "");
      vReturn = StringUtils.replace(vReturn, ")", "");
      vReturn = StringUtils.replace(vReturn, "\\", "_");
      vReturn = StringUtils.replace(vReturn, "/", "_");

      return vReturn;
   }

   /**
    * Obtenir la liste des exigences sur un élement de modélisation DSM (une
    * opération SOA).
    * 
    * @param pElement L'élément d'accrochage.
    * @return La liste d'exigence(s) accroché sur l'élément.
    */
   public List<Requirement> getListRequirement_sce (ObeoDSMObject pElement) {
      return getSceCommonDslRequirement().getListRequirement_sce(pElement);
   }

   /**
    * Obtenir toutes les exigences accrochées à un élément de Cinematic.
    * 
    * @param element L'élément (ex : un Panel, un widget, ...)
    * @return La liste désirée.
    */
   public List<Requirement> getRequirements_sce (CinematicElement element) {
      return getSceCommonDslRequirement().getListRequirement_sce(element);
   }

   public List<Requirement> getRequirements_sce (Reference element) {
      return getSceCommonDslRequirement().getListRequirement_sce(element);
   }

   public List<Requirement> getRequirements_sce (Attribute element) {
      return getSceCommonDslRequirement().getListRequirement_sce(element);
   }

   public List<Requirement> getRequirements_sce (Operation element) {
      return getSceCommonDslRequirement().getListRequirement_sce(element);
   }

   public List<Requirement> getRequirements_sce (Parameter element) {
      return getSceCommonDslRequirement().getListRequirement_sce(element);
   }

   public List<Requirement> getRequirements_sce (ObeoDSMObject element) {
      return getSceCommonDslRequirement().getListRequirement_sce(element);
   }

//   /**
//    * Obtenir la taille pour le champs à partir du contenu dans la METADATA "DATA".
//    * @param pComposant Le composant à regarder.
//    * @return La taille par défaut (si pas de METADATA), ou la taille du texte contenu dans "DATA".
//    */
//   public int getSizeOfDATA_sce (ViewElement pComposant) {
//      return getMetadataDslCinematic().getSizeOfDATA(pComposant);
//   }

   /**
    * Obtenir les données "Fake" mises sur le composant, et si aucune, prend le label.
    * @param pElement Le composant.
    * @return La liste des données désirées.
    */
   public List<String> getFakeData_sce (ViewElement pElement) {
      return getMetadataDslCinematic().getFakeData(pElement, null);
   }
   /**
    * Obtenir les données "Fake" mises sur le composant, et si aucune, retourne 'null'.
    * @param pElement Le composant.
    * @return La liste des données désirées.
    */
   public List<String> getFakeDataOnly_sce (ViewElement pElement) {
      return getMetadataDslCinematic().getFakeDataOnly(pElement, null);
   }

   private Map<ViewElement, Integer> _Map_rg_nextFakeData = new HashMap<>();

   /**
    * Obtenir la FakeData suivante associée au widget.
    * 
    * @param pElement Le widget à regarder.
    * @return La FakeData désirée.
    */
   public String nextFakeData_sce (ViewElement pElement) {
      return nextFakeData_sce(pElement, true);
   }
   public String nextFakeDataOnly_sce (ViewElement pElement) {
      return nextFakeData_sce(pElement, false);
   }
   private String nextFakeData_sce (ViewElement pElement, boolean pIsLabelDefault) {
      String vReturn;
      List<String> vListFakeData = getMetadataDslCinematic().getFakeData(pElement, null, pIsLabelDefault);
      List<String> vListFakeDataOnly = getMetadataDslCinematic().getFakeData(pElement, null, false);
      // Obtenir le rang
      Integer rg_nextFakeData = getRangFakeData_sce(pElement);

      if ((vListFakeData != null) && (rg_nextFakeData != null) && (rg_nextFakeData < vListFakeData.size())) {
         vReturn = vListFakeData.get(rg_nextFakeData);
      }
      else if ((vListFakeData == null) && (false == pIsLabelDefault)) {
         vReturn = null;
      }
      // SI pas de FakeData et l'on retourne le Label
      else if ((vListFakeDataOnly == null) && (vListFakeData.size() == 1) && (rg_nextFakeData == 0) && (pIsLabelDefault)) {
         vReturn = pElement.getLabel();
      }
      else {
//         vReturn = pElement.getLabel();
         vReturn = null;
      }

      // Incrémenter et mémoriser
      rg_nextFakeData++;
      _Map_rg_nextFakeData.put(pElement, rg_nextFakeData);

      return vReturn;
   }

   /**
    * Obtenir le rang de la FakeData courante associé au widget.
    * 
    * @param pElement Le widget à regarder.
    * @return Le rang de la FakeData courante.
    */
   public Integer getRangFakeData_sce (ViewElement pElement) {
      // Obtenir le rang
      Integer vReturn = _Map_rg_nextFakeData.get(pElement);
      // SI première fois
      if (vReturn == null) {
         vReturn = 0;
         _Map_rg_nextFakeData.put(pElement, vReturn);
      }

      return vReturn;
   }

   /**
    * Obtenir la liste des FakeData sur le widget.
    * 
    * @param pElement L'élément à regarder.
    * @return La liste désirée.
    */
   public List<String> getListFakeData_sce (AbstractViewElement pElement) {
      List<String> vReturn;
      if (pElement instanceof ViewElement) {
         vReturn = getMetadataDslCinematic().getFakeData((ViewElement) pElement, null);
      }
      else {
         vReturn = Collections.EMPTY_LIST;
      }

      return vReturn;
   }

   /**
    * Parcourir le nombre de FakeData indiquée sur le 1er composant du conteneur.
    * 
    * @param pElement L'élément à regarger.
    * @return La liste avec un compteur pour parcourir les FakeData.
    */
   public List<Integer> getRangFakeData_sce (AbstractViewElement pElement) {
      List<Integer> vReturn = new ArrayList<>();
      List<String> listFakeData = null;
      if (pElement instanceof ViewElement) {
         listFakeData = getMetadataDslCinematic().getFakeData((ViewElement) pElement, null);
      }
      if (pElement instanceof ViewContainer) {
         ViewContainer vContainer = (ViewContainer) pElement;
         for (AbstractViewElement vElement : vContainer.getOwnedElements()) {
            // Prendre le 1er widget du conteneur
            if (vElement instanceof ViewElement) {
               listFakeData = getMetadataDslCinematic().getFakeData((ViewElement) vElement, null);
               break;
            }
         }
      }
      else {
         throw new GenerationRuntimeException("Cas non prévu pour getRangFakeData_sce() avec pElement=" + pElement,
               "Revoir l'algo");
      }

      // Pas d'élément trouvé
      if (listFakeData == null) {
         throw new GenerationRuntimeException(
               "Erreur application - Pas réussi à déterminer la 'listFakeData' pour getRangFakeData_sce() avec pElement="
                     + pElement,
               "Revoir l'algo");
      }
      else {
         int cpt = 1;
         for (String data : listFakeData) {
            vReturn.add(cpt);
            cpt++;
         }
      }

      return vReturn;
   }

   /**
    * Obtenir la FakeData formatée.
    * 
    * @param pFakeData La ligne de données "bidon" courante.
    * @return La donnée formatée.
    */
   public String getFakeDataFmt_sce (String pFakeData) {
      String vReturn;

      // SI la donnée doit être SELECTED
      if (isFakeDataSelected_sce(pFakeData)) {
         vReturn = pFakeData.substring(cPreffixDataSelected.length(), pFakeData.length());
      }
      else {
         vReturn = pFakeData;
      }

      return vReturn;
   }

   /**
    * Savoir si une ligne de "FakeData" doit être sélectionnée.
    * 
    * @param pFakeData La ligne de données "bidon" courante.
    * @return 'true' si la ligne doit être sélectionnée.
    */
   public Boolean isFakeDataSelected_sce (String pFakeData) {
      Boolean vReturn;

      // SI la donnée doit être sélectionnée
      if (pFakeData.startsWith(cPreffixDataSelected)) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }

      return vReturn;
   }

   /**
    * Obtenir le chemin en absolu.
    * 
    * <pre>
    * Le CinematicElement peut-être :
    *     - Soit un 'ViewState' si génération à partir d'un Flow.
    *     - Soit un 'ViewContainer' si génération à partir de Mockup uniquement (maquette sans cinematic).
    * Rappel :
    *     - Un 'ViewContainer' peut-être un écran.
    *     - Un 'ViewState' est un état dans un Flow qui référence un 'ViewContainer'.
    * </pre>
    * 
    * @param pCinematicElement L'élément a regarder.
    * @return La chemin désiré.
    */
   public String getAbsoluteFileElement_sce (CinematicElement pCinematicElement) {
      String vReturn;

      // Convertir en 'NamedElement'
      NamedElement vNamedElement = (NamedElement) pCinematicElement;

      Package vPackage;
      // SI si génération à partir d'un Flow
      if (vNamedElement instanceof ViewState) {
         ViewState vViewState = (ViewState) pCinematicElement;
         // Obtenir le package contenant le 'ViewState'
         vPackage = (Package) vViewState.eContainer();
      }
      // SI génération à partir de Mockup uniquement (maquette sans cinematic)
      else if (vNamedElement instanceof ViewContainer) {
         ViewContainer vViewContainer = (ViewContainer) pCinematicElement;
         // Obtenir le package contenant le 'ViewContainer'
         vPackage = (Package) vViewContainer.eContainer();

      }
      else {
         throw new GenerationRuntimeException(
               "Erreur application : Cas non prévu pour pCinematicElement=" + pCinematicElement,
               "Reprendre l'algo pour prendre en compte ce type");
      }

      if (vPackage == null) {
         vReturn = vNamedElement.getName() + ".html";
      }
      else {
         vReturn = "/" + vPackage.getName() + "/" + vNamedElement.getName() + ".html";
      }
      return vReturn;
   }

   /**
    * Savoir si un widget doit être généré.
    * 
    * @param pElement L'élément a regardé.
    * @return 'true 'si l'élément doit être généré.
    */
   public boolean isElementGenerated_sce (AbstractViewElement pElement) {
      boolean vReturn;
      String enabled = getMetadataDslCinematic().getGenerated(pElement);
      if ("TRUE".equals(enabled)) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }
      return vReturn;
   }

   /**
    * Le preffixe d'une exigence portant sur un "profil GDA", pour faire : -
    * plusieurs écrans dans la maquette HTML - 1 seul XHTML en JSF
    */
   private static final String cPreffixProfilGDA = "ProfilGDA";
   /** La liste des exigences portant sur un "profil GDA". */
   private List<Requirement> _ListProfilGDA = new ArrayList<>();

   public boolean isElementWithProfilGDA_sce (AbstractViewElement pElement, Requirement pProfilGDA) {
      boolean vReturn;

      // SI pas de profil GDA : pas de filtrage
      if (pProfilGDA == null) {
         vReturn = true;
      }
      else {
         boolean vIsAtLeastOneProfilGDA = false;
         boolean vIsFinded = false;
         List<Requirement> vListAllRequirementsOfViewState = getRequirements_sce(pElement);
         // Parcourir les 'Requirement' du composant
         for (Requirement requirement : vListAllRequirementsOfViewState) {
            // SI l'élément possède au moins une restriction de "profil GDA"
            if (_ListProfilGDA.contains(requirement)) {
               vIsAtLeastOneProfilGDA = true;
            }
            // SI le profil GDA a été trouvé
            if (requirement.equals(pProfilGDA)) {
               vIsFinded = true;
            }
         }

         // SI au moins une restriction GDA
         if (vIsAtLeastOneProfilGDA) {
            if (vIsFinded) {
               vReturn = true;
            }
            else {
               vReturn = false;
            }
         }
         else {
            vReturn = true;
         }
      }

      return vReturn;
   }

   public String getRelatifPathBack_sce (Flow pFlow, ActionState pActionState) {
      String vReturn;

      // Ex : "enseignant.titulaire.EcrSaisieEnseignant"
      String name = getPackageName_ViewActionHtml_sce(pFlow, pActionState);
      // "." --> "/"
      name = StringUtils.replace(name, ".", "/");
      // Obtenir uniquement le path
      String path = DirectoryUtils.getAbsolutePath(name);
      // Obtenir la path inverse en relatif
      vReturn = DirectoryUtils.getRelatifPathBack(path);

      return vReturn;
   }

   public String getRelatifPathBack_sce (Flow pFlow, ViewState pViewState) {
      String vReturn;

      // Ex : "enseignant.titulaire.EcrSaisieEnseignant"
      String name = getPackageName_ViewContainerHtml_sce(pFlow, pViewState);
      // "." --> "/"
      name = StringUtils.replace(name, ".", "/");
      // Obtenir uniquement le path
      String path = DirectoryUtils.getAbsolutePath(name);
      // Obtenir la path inverse en relatif
      vReturn = DirectoryUtils.getRelatifPathBack(path);

      return vReturn;
   }

   public String getRelatifPathBack_sce (Flow pFlow, ViewState pViewState, AbstractViewElement pViewElement) {
      String vReturn;

//      // Obtenir l'écran de type 'Page' associé au widget 
//      ViewContainer vViewContainer = SceCommonDslCinematic.getScreenPage(pViewElement);
      // Le nom de l'écran en absolu est porté par le ViewState
      // Ex : "enseignant.titulaire.EcrSaisieEnseignant"
      String name = getAbsoluteNameViewState(pViewState);
      // "." --> "/"
      name = StringUtils.replace(name, ".", "/");
      // Obtenir uniquement le path
      // Ex : "pages/enseignant/titulaire/EcrSaisieEnseignant"
      String path = cPath_Pages + DirectoryUtils.getAbsolutePath(name);
      // Obtenir la path inverse en relatif
      vReturn = DirectoryUtils.getRelatifPathBack(path);

      return vReturn;
   }

   /**
    * @param pMasque Le masque (ex :
    *           "[getListUrlRelatifPath('<a href="{$relatifPath}" class=
    *           "lienCheminNav"> / {$packageName}</a>')->sep(' ')/]").
    * @param pFlow
    * @param pViewState
    * @return La liste désirée.
    */
   public List<String> getListUrlRelatifPath_sce (String pMasque, Flow pFlow, ViewState pViewState) {
      // Ex : "enseignant.titulaire.EcrSaisieEnseignant"
      String name = getPackageName_ViewContainerHtml_sce(pFlow, pViewState);
      // "." --> "/"
      name = StringUtils.replace(name, ".", "/");
      List<String> vReturn = getListUrlRelatifPah_intern(pMasque, name);

      return vReturn;
   }
   public List<String> getListUrlRelatifPath_sce (String pMasque, Flow pFlow, ActionState pActionState) {
      // Ex : "createEnseignant"
      String name = getPackageName_ViewActionHtml_sce(pFlow, pActionState);
      // "." --> "/"
      name = StringUtils.replace(name, ".", "/");
      List<String> vReturn = getListUrlRelatifPah_intern(pMasque, name);

      return vReturn;
   }

   private List<String> getListUrlRelatifPah_intern (String pMasque, String name) {
      List<String> vReturn = new ArrayList<>();
      List<String> listDirectory = DirectoryUtils.getListDirectory(name);
      String relatifPath = "../";
      String packageName;
      int rang = 1;
      int nbDirectory = listDirectory.size();
      for (String directory : listDirectory) {
         // On passe le premier répertoire
         if (rang >= 2) {
            // pMasque = Ex : "<a href="{$relatifPath}" class="lienCheminNav"> /
            // {$packageName}</a>"
            String valueCour = pMasque;
//            relatifPath = DirectoryUtils.getRelatifPathBack(directory);
            relatifPath = getPointPoint(nbDirectory - rang);
            packageName = DirectoryUtils.getLastDirectory(directory);
            valueCour = StringUtils.replace(valueCour, "{$relatifPath}", relatifPath);
            valueCour = StringUtils.replace(valueCour, "{$packageName}", packageName);
            // Ajouter le valeur
            vReturn.add(valueCour);
         }
         rang++;
      }
      return vReturn;
   }

   private String getPointPoint (int pNbPointPoint) {
      String vReturn;
      if (pNbPointPoint == 0) {
         vReturn = ".";
      }
      else {
         vReturn = "";
         for (int i = 0; i < pNbPointPoint; i++) {
            vReturn = vReturn + "../";
         }
      }
      return vReturn;
   }

   public String getLastPackage_sce (Flow pFlow, ViewState pViewState) {
      String vReturn;

      // Ex : "enseignant.titulaire.EcrSaisieEnseignant"
      String name = getPackageName_ViewContainerHtml_sce(pFlow, pViewState);
      // "." --> "/"
      name = StringUtils.replace(name, ".", "/");
      // Obtenir la dernier "répertoire"
      vReturn = DirectoryUtils.getLastDirectory(name);

      return vReturn;
   }

   public String getParamTagFmt_ENABLED_sce (AbstractViewElement pElement) {
      String vReturn;
      String enabled = getMetadataDslCinematic().getEnabled(pElement);
      if ("TRUE".equals(enabled)) {
         vReturn = "";
      }
      else {
         vReturn = "disabled";
      }
      return vReturn;
   }

   /**
    * Obtenir un Layout qui encapsule l'élément.
    * 
    * @param pElement L'élément à encapsuler.
    * @return L'instance désirée.
    */
   public Layout wrapWithLayout_sce (AbstractViewElement pElement) {
      // Instancier un Flow
      Layout vLayout = ViewFactory.eINSTANCE.createLayout();
      vLayout.setViewElement(pElement);

      return vLayout;
   }

   /**
    * Obtenir un RadioGroup qui encapsule l'élément.
    * 
    * @param pElement Le widget à regarder.
    * @return L'instance désiré.
    */
   public Layout wrapWithRadioGroup_sce (AbstractViewElement pElement) {
      Layout vReturn;

      // SI METADATA "GROUP" du le bouton radio
      if (isRadioGroupVirtual_sce(pElement)) {
         // Obtenir le nom du groupe de bouton radio
         String groupName = getMetadataDslCinematic().getGROUP(pElement);

         Layout vLayoutViewContainer = _MapViewContainerVirtual.get(groupName);
         if (vLayoutViewContainer == null) {
            vReturn = null;
         }
         else {
            vReturn = vLayoutViewContainer;
         }
      }
      else {
         vReturn = null;
      }

      return vReturn;
   }

   /**
    * Savoir si le widget 'Radio' possède une METADATA 'GROUP'.
    * 
    * @param pElement Le widget à regarder.
    * @return 'true' s'il possède la METADATA 'GROUP'.
    */
   public boolean isRadioGroupVirtual_sce (AbstractViewElement pElement) {
      boolean vReturn;
      String value = getMetadataDslCinematic().getGROUP(pElement);
      if (value == null) {
         vReturn = false;
      }
      else {
         vReturn = true;
      }
      return vReturn;
   }

   /**
    * Obtenir le code HTML associé à l'état de la CheckBox.
    * 
    * @param pElement La checkBox.
    * @return Le code HTML désiré.
    */
   public String htmlChecked_sce (ViewElement pElement) {
      String vReturn;
      boolean isChecked = getMetadataDslCinematic().isCheckBoxChecked(pElement);
      if (isChecked) {
         vReturn = "CHECKED";
      }
      else {
         vReturn = "";
      }
      return vReturn;
   }

   /**
    * Obtenir le lien HRef spécifié dans la champs 'Example'.
    * 
    * @param pElement L'élément à regarder.
    * @return Le lie HRef désiré.
    */
   public String getHRefExample_sce (ViewElement pElement) {
      String vReturn;

      // SI aucune valeur
      if ((pElement.getExample() == null) || ("".equals(pElement.getExample()))) {
         vReturn = "*** Aucune valeur de spécifiée dans le champs 'Example' du composant '" + pElement.getName()
               + "'";
      }
      else {
         vReturn = pElement.getExample();
      }

      return vReturn;
   }

   /**
    * Obtenir le composant principal du modèle SOA.
    * @return L'instance désirée.
    */
   public Component getMainComponentSoa_sce () {
      Component vReturn;

      if (_RootSoa == null) {
         vReturn = null;
      }
      else {
         List<Component> listComponent = _RootSoa.getOwnedComponents();

         if (listComponent.size() == 0) {
            vReturn = null;
         }
         else if (listComponent.size() == 1) {
            vReturn = listComponent.get(0);
         }
         else {
            throw new ModelisationRuntimeException("Problème pour déterminer la racine SOA", "Il faut aucune ou une seule racine");
         }
      }

      return vReturn;
   }
   
   /**
    * Retourne le nombre de lignes du composant 'Table'.
    * @param pLayout Le layout représentant le composant 'Table' à regarder.
    * @return Le nombre de lignes.
    */
   public int getNbLignes_Table_sce (Layout pLayout) {
      int vReturn = getSceCommonDslCinematic().getNbLignesLayout(pLayout);
      
      return vReturn;
   }
}
