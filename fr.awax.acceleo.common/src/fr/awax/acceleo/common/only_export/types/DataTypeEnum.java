package fr.awax.acceleo.common.only_export.types;

import org.obeonetwork.dsl.entity.Entity;
import org.obeonetwork.dsl.environment.DTO;
import org.obeonetwork.dsl.environment.Enumeration;
import org.obeonetwork.dsl.environment.ObeoDSMObject;
import org.obeonetwork.dsl.environment.PrimitiveType;
import org.obeonetwork.dsl.environment.StructuredType;

/**
 * Le type de données du POJO : Entity ou DTO (= VO).
 * @author MinEdu
 */
public enum DataTypeEnum {
   inconnu("inconnu"),
   /** Les données modélisées dans Entity. */
   pojoEntityModel("Entity"),
   pojoVoUnPourUn("Vo"),
   conceptFonctionnel("Concept"), // null : car Enum, et donc pas généré en 'Type Java'
   /** Les données modélisées dans SOA-DTO. */
   pojoDtoModel("Dto"),
   /** Un DTO From Entity */
   pojoDtoFromEntityModel("DtoFromEntity"),
   /** Enumération. */
   enumeration("Enum"),
   primitifType("primitif");

   private String _Suffixe;

   private DataTypeEnum(String pSuffixe) {
      _Suffixe = pSuffixe;
   }

   public String getSuffixe () {
      return _Suffixe;
   }

   /**
    * Obtenir la valeur en fonction du type passé en paramètre.
    * @param pStructuredType Le type à regarder (Entity ou DTO).
    * @return La valeur adéquate.
    */
   public static DataTypeEnum getValueWithStructuredType (StructuredType pStructuredType) {
      DataTypeEnum vReturn;
      if (pStructuredType instanceof Entity) {
         vReturn = pojoEntityModel;
      }
      else if (pStructuredType instanceof DTO) {
         vReturn = pojoDtoModel;
      }
      else {
         throw new IllegalArgumentException("Cas non prévu pour pStructuredType=" + pStructuredType.getName());
      }
      return vReturn;
   }

//   /**
//    * Savoir précisément le type de l'élément.
//    * @param pObeoDSMObject L'instance a regarder.
//    * @return La valeur de l'énumération associée.
//    */
//   public static DataTypeEnum getDataTypeEnum(ObeoDSMObject pObeoDSMObject) {
//      DataTypeEnum vDataTypeEnum;
//      if (pObeoDSMObject == null) {
//         vDataTypeEnum = null;
//      }
//      else {
//         if (pObeoDSMObject instanceof Entity) {
//            vDataTypeEnum = DataTypeEnum.pojoEntityModel;
//         }
//         else if (pObeoDSMObject instanceof DTO) {
//            DTO vDTO = (DTO) pObeoDSMObject;
//            boolean isDtoFromEntity = NomageConvention.isDtoFromEntity_sceRoot(vDTO);
//            if (isDtoFromEntity) {
//               vDataTypeEnum = DataTypeEnum.pojoDtoFromEntityModel;
//            }
//            else {
//               vDataTypeEnum = DataTypeEnum.pojoDtoModel;
//            }
//         }
//         else if (pObeoDSMObject instanceof PrimitiveType) {
//            vDataTypeEnum = DataTypeEnum.primitifType;
//         }
//         else if (pObeoDSMObject instanceof Enumeration) {
//            vDataTypeEnum = DataTypeEnum.enumeration;
//         }
//         else {
//            throw new IllegalArgumentException("Erreur - Cas non prévu pour pObeoDSMObject=" + pObeoDSMObject);
//         }
//      }
//      return vDataTypeEnum;
//   }
}
