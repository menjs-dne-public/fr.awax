package fr.awax.acceleo.common.only_export.dsl.cinematic;

/**
 * Les valeurs de complexités pour chaque widget.
 * Cf. "SIRHEN.cinematic_toolkits"
 * @author MinDef
 */
public enum ComplexiteWidget_Enum {
   widgetButton("Button", 4, false, "btn"),
   widgetCheckboxWithoutLabel("CheckboxWithoutLabel", 1, false, "chk"),
   widgetCheckbox("Checkbox", 1, false, "chk"),
   // 0 : Pour un 'CheckboxGroup', la complexité réside dans le nombre de Checkbox qu'il contient 
   widgetCheckboxGroup("CheckboxGroup", 0, true, "cgp"),
   widgetCombo("Combo", 4, false, "cmb"),
   // 0 : Pour un 'Form', la complexité réside dans les widgets qu'il contient
   widgetForm("Form", 0, true, "frm"),
   widgetFormRow("FormRow", 0, true, "frw"),
   widgetFormImage("FormImage", 1, false, "fimg"),
   // 0 : Pour une 'Image' : car juste affichage
   widgetImage("Image", 0, false, "img"),
   widgetLabel("Label", 1, false, "lbl"),
   widgetText("Text", 2, false, "txt"),
   widgetLink("Link", 2, false, "lnk"),
   widgetList("List", 4, false, "lst"),
   widgetPassword("Password", 0, false, "pwd"),
   // 0 : Pour un 'Page', la complexité réside dans les widgets qu'il contient
   widgetPage("Page", 0, true, "pge"),
   // 0 : Pour un 'HomePage', la complexité réside dans les widgets qu'il contient
   widgetPageHome("HomePage", 0, true, "pup"),
   // 0 : Pour un 'PagePopup', la complexité réside dans les widgets qu'il contient
   widgetPagePopup("PagePopup", 0, true, "pup"),
   // 0 : Pour un 'Panel', la complexité réside dans les widgets qu'il contient
   widgetPanel("Panel", 0, true, "pnl"),
   // 0 : Pour un 'RadioGroup', la complexité réside dans le nombre de Checkbox qu'il contient 
   widgetRadioGroup("RadioGroup", 0, true, "rgp"),
   widgetRadio("Radio", 1, false, "rdb"),
   // 0 : Pour un 'TabGroup', la complexité réside dans le nombre de Checkbox qu'il contient 
   widgetTabGroup("TabGroup", 0, true, "tgp"),
   // 0 : Pour un 'Tab', la complexité réside dans le nombre de Checkbox qu'il contient 
   widgetTab("Tab", 0, true, "ong"),
   widgetTable("Table", 0, true, "tbl"),
   widgetTableHeader("TableHeader", 0, true, "thr"),
   widgetTableRow("TableRow", 2, true, "trw"),
   widgetColumnLabel("ColumnLabel", 2, true, "col"),
   widgetTextarea("Textarea", 2, false, "txa"),
   widgetBreak("Break", 0, false, "brk"),
   widgetBlank("Blank", 0, false, "blk");

   private String _Nom;
   private int _NbPf;
   private boolean _IsConteneur;
   private String _Prefixe;

   /**
    * Constructeur.
    * @param pNom Le nom de la complexité.
    * @param p_nbPf Le nombre de points de fonctions.
    */
   ComplexiteWidget_Enum(final String pNom, final int p_nbPf, final boolean pIsConteneur, final String pPrefixe) {
      _Nom = pNom;
      _NbPf = p_nbPf;
      _IsConteneur = pIsConteneur;
      _Prefixe = pPrefixe;
   }

   /**
    * Le nom.
    * @return Le nom.
    */
   public String getNom () {
      return _Nom;
   }

   /**
    * Obtenir le nom de points de fonctions.
    * @return Le nb de PF.
    */
   public int getNbPf () {
      return _NbPf;
   }

   public boolean isConteneur () {
      return _IsConteneur;
   }

   public String getPrefixe () {
      return _Prefixe;
   }

   public void setPrefixe (String pPrefixe) {
      _Prefixe = pPrefixe;
   }

   /**
    * Obtenir la valeur de complexité à partir du nom du type de composant.
    * @param pNom Le nom (ex : "button").
    * @return La valeur ou 'null' si pas trouvé.
    */
   public static ComplexiteWidget_Enum getValueByName (String pNom) {
      ComplexiteWidget_Enum vReturn = null;

      if (pNom == null) {
         vReturn = null;
//         throw new IllegalArgumentException("Le nom est obligatoire pour getValueByName()");
      }
      else {
         ComplexiteWidget_Enum[] vAllValues = ComplexiteWidget_Enum.values();
         for (ComplexiteWidget_Enum vComplexiteWidget_Enum : vAllValues) {
            // SI le nom correspond
            if (vComplexiteWidget_Enum.getNom().toUpperCase().equals(pNom.toUpperCase())) {
               vReturn = vComplexiteWidget_Enum;
               break;
            }
         }

         // SI pas trouvé
         if (vReturn == null) {
            throw new IllegalArgumentException("Erreur application : cas non prévu pour le nom de widget spécifié : " + pNom);
         }
      }

      return vReturn;
   }

}
