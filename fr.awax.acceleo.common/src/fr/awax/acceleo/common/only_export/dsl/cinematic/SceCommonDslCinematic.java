package fr.awax.acceleo.common.only_export.dsl.cinematic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.obeonetwork.dsl.environment.Type;
import org.obeonetwork.dsl.cinematic.CinematicElement;
import org.obeonetwork.dsl.cinematic.CinematicRoot;
import org.obeonetwork.dsl.cinematic.Event;
import org.obeonetwork.dsl.cinematic.flow.ActionState;
import org.obeonetwork.dsl.cinematic.flow.DecisionState;
import org.obeonetwork.dsl.cinematic.flow.FinalState;
import org.obeonetwork.dsl.cinematic.flow.Flow;
import org.obeonetwork.dsl.cinematic.flow.FlowAction;
import org.obeonetwork.dsl.cinematic.flow.FlowFactory;
import org.obeonetwork.dsl.cinematic.flow.FlowState;
import org.obeonetwork.dsl.cinematic.flow.InitialState;
import org.obeonetwork.dsl.cinematic.flow.SubflowState;
import org.obeonetwork.dsl.cinematic.flow.Transition;
import org.obeonetwork.dsl.cinematic.flow.ViewState;
import org.obeonetwork.dsl.cinematic.toolkits.Widget;
import org.obeonetwork.dsl.cinematic.view.AbstractViewElement;
import org.obeonetwork.dsl.cinematic.view.Layout;
import org.obeonetwork.dsl.cinematic.view.ViewContainer;
import org.obeonetwork.dsl.cinematic.view.ViewElement;
import org.obeonetwork.dsl.cinematic.view.ViewEvent;
import org.obeonetwork.dsl.cinematic.view.ViewFactory;
import org.obeonetwork.dsl.environment.Action;
import org.obeonetwork.dsl.environment.MultiplicityKind;
import org.obeonetwork.dsl.environment.PrimitiveType;
import org.obeonetwork.dsl.environment.StructuredType;
import org.obeonetwork.dsl.requirement.Requirement;
import org.obeonetwork.dsl.soa.Operation;
import org.obeonetwork.dsl.soa.Parameter;

import fr.awax.acceleo.common.data.XBoolean;
import fr.awax.acceleo.common.dsl.Dsl_Enum;
import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.engine.error.ErrorControler;
import fr.awax.acceleo.common.exception.GenerationException;
import fr.awax.acceleo.common.exception.GenerationRuntimeException;
import fr.awax.acceleo.common.exception.ModelisationRuntimeException;
import fr.awax.acceleo.common.norme.StringNorme;
import fr.awax.acceleo.common.only_export.SceAbstractInstance;
import fr.awax.acceleo.common.only_export.dsl.requirement.SceCommonDslRequirement;
import fr.awax.acceleo.common.util.DirectoryUtils;
import fr.awax.acceleo.common.util.ListUtils;
import fr.awax.acceleo.common.util.MetadataUtils;
import fr.awax.acceleo.common.util.StringUtils;
import fr.awax.acceleo.common.util.UuidUtils;

public class SceCommonDslCinematic extends SceAbstractInstance {
   /** Pour la traçabilité. */
   private static final Dsl_Enum DSL = Dsl_Enum.DslCinematic;

   /** Préfixe d'un nom de paramètre d'opération (ex : "p_bareme"). */
   private static final String cPreffixeParam = "";
//   private static final String cPreffixeParam = "p_";
   private static final String cAucuneDescription = "** Non renseigné dans le modèle **";

   /** Pour avoir un 'null' non filtré par Acceleo. */
   private static AbstractViewElement cNullWidget;
   private static final String cNullWidgetLib = "NullWidget";

   /** Le gestionnaire permettant de mémoriser les erreurs. */
   private ErrorControler _ErrorControler;

   /** Le type "Page" associé à un 'ViewState'. */
   private static final String cType_PageOfViewState = ComplexiteWidget_Enum.widgetPage.getNom();
   private static final String cType_PanelOfViewState = ComplexiteWidget_Enum.widgetPanel.getNom();

   private SceCommonDslRequirement _SceCommonDslRequirement;

   public SceCommonDslCinematic() {
      super();
   }

   private SceCommonDslCinematic(String pPluginId) {
      super(pPluginId);
      _ErrorControler = ErrorControler.getInstance(pPluginId);
      _SceCommonDslRequirement = SceCommonDslRequirement.getInstance(getPluginId());
   }

   private static Map<String, SceCommonDslCinematic> _MapInstances = new LinkedHashMap<>();

   public static SceCommonDslCinematic getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static SceCommonDslCinematic getInstance (String pPluginId) {
      SceCommonDslCinematic instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new SceCommonDslCinematic(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      // RAS
   }

   /**
    * Obtenir toutes les exigences d'un écran.
    * 
    * @param pViewState L'écran.
    * @return La liste de exigences de l'écran.
    */
   public List<Requirement> getAllRequirementsOfViewState (ViewState pViewState) {
      List<Requirement> vReturn = new ArrayList<>();
      // Parcourir les conteneurs de l'écran
      for (ViewContainer vViewContainer : pViewState.getViewContainers()) {
         getAllRequirementsOfViewState_sceIntern(vViewContainer, vReturn);
      }

      return vReturn;
   }

   private void getAllRequirementsOfViewState_sceIntern (ViewContainer pViewContainer, List<Requirement> pReturn) {
      // Parcourir les widgets du conteneur
      for (ViewElement vViewElement : pViewContainer.getViewElements()) {
         List<Requirement> listRequirementOfViewElement = _SceCommonDslRequirement.getRequirements_sce(vViewElement);
         if ((listRequirementOfViewElement != null) && (false == listRequirementOfViewElement.isEmpty())) {
            pReturn.addAll(listRequirementOfViewElement);
         }
      }

      // Parcourir les conteneurs
      for (ViewContainer vViewContainer : pViewContainer.getViewContainers()) {
         List<Requirement> listRequirementOfViewContainer = _SceCommonDslRequirement
               .getRequirements_sce(vViewContainer);
         if ((listRequirementOfViewContainer != null) && (false == listRequirementOfViewContainer.isEmpty())) {
            pReturn.addAll(listRequirementOfViewContainer);
         }

         // Parcourir récursivement le conteneur
         getAllRequirementsOfViewState_sceIntern(vViewContainer, pReturn);
      }
   }

   /**
    * Savoir si le modèle Cinematic possède un FLow.
    * 
    * @param pRoot La racine du modèle Cinematic.
    * @return 'true' si le modèle Cinematic possède au moins un 'Flow'.
    */
   public boolean isFlow_sce (CinematicRoot pRoot) {
      boolean vReturn;

      if (pRoot.getFlows() == null || pRoot.getFlows().size() == 0) {
         vReturn = false;
      }
      else {
         vReturn = true;
      }

      return vReturn;
   }

   /**
    * Obtenir la 'Page' associée au ViewState (= l'écran). En OCL :
    * pViewState.viewContainers->select(isPage())->asSequence()->first() A SAVOIR :
    * La requête OCL ne passait pas en mode PlugIn déployé
    * 
    * @param pViewState L'écran dont on veut connaître la page associée.
    * @return La page qui contient l'écran.
    */
   public ViewContainer getPage_sce (ViewState pViewState) {
      ViewContainer vReturn = null;
      if (pViewState != null) {
         // Parcourir les conteneurs associés au 'pViewState'
         for (ViewContainer viewContainer : pViewState.getViewContainers()) {
            if (isPage_sce(viewContainer)) {
               vReturn = viewContainer;
               break;
            }
         }
      }
      return vReturn;
   }

   /**
    * Savoir si un ViewState est une 'Page' classique.
    * 
    * @param pViewState Le ViewState.
    * @return 'true' Si c'est une page classique.
    */
   public boolean isPage_sce (ViewState pViewState) {
      boolean vReturn = false;
      List<ViewContainer> listContainer = pViewState.getViewContainers();
      // Parcourir les 'ViewContainer' de pViewState
      for (ViewContainer viewContainer : listContainer) {
         // SI c'est une page classique
         if (isPage_sce(viewContainer)) {
            vReturn = true;
            break;
         }
      }
      return vReturn;
   }

   /**
    * Savoir si le composant est une 'Page' classique.
    * 
    * @param pComposant Le composant à regarder.
    * @return 'true' Si c'est une page classique.
    */
   public boolean isPage_sce (AbstractViewElement pComposant) {
      return isWidgetTypeOf(pComposant, cType_PageOfViewState, cType_PanelOfViewState);
   }

   public boolean isWidgetTypeOf_sce (AbstractViewElement pComposant, String pTypeExpected) {
      return isWidgetTypeOf(pComposant, pTypeExpected);
   }

   public boolean isWidgetTypeOf_sce (ViewState pComposant, String pTypeExpected) {
      return isWidgetTypeOf(pComposant, pTypeExpected);
   }

   /**
    * Savoir si un widget est du type 'pTypeExpected'.
    * 
    * @param pComposant Le composant à regarder.
    * @param pTabTypeExpected Les types attendus (ex : "Page", "Panel").
    * @return 'true' si c'est composant est du type attendu.
    */
   private boolean isWidgetTypeOf (ViewState pComposant, String... pTabTypeExpected) {
      boolean vReturn = false;
      if (pComposant != null) {
         // Parcourir les types associés à l'écran (ex : "PagePopup")
         for (AbstractViewElement element : pComposant.getViewContainers()) {
            // Si le composant est du type attendu
            if ((isWidgetTypeOf(element, pTabTypeExpected))) {
               vReturn = true;
               break;
            }
         }
      }
      return vReturn;
   }

   private boolean isWidgetTypeOf (AbstractViewElement pComposant, String... pTabTypeExpected) {
      boolean vReturn = false;
      if ((pComposant != null) && (pComposant.getWidget() != null)) {
         if ((pComposant.getWidget().getName() == null)) {
            _ErrorControler.writeErrorGeneration(
                  "Le composant  \"" + pComposant.getName() + "\" n'a pas de nom de widget associé",
                  "Vérifier que le Toolkit est pris en compte dans le projet",
                  "Ouvrir le .cinematic avec un éditeur de texte et remplacer 'pathmap://cinematic/toolkits/NAME_PROJECT#' par 'toolkits/NAME_PROJECT.cinematic-toolkits#'",
                  "Ouvrir le .aird avec un éditeur de texte et remplacer 'pathmap://cinematic/toolkits/NAME_PROJECT' par 'toolkits/NAME_PROJECT.cinematic-toolkits'");
         }
         else {
            String[] vTabTypePage = { ComplexiteWidget_Enum.widgetPage.getNom() };
            // SI le composant est du type attendu
            if (isTypeExpected(pComposant.getWidget().getName(), pTabTypeExpected)) {
               vReturn = true;
            }
            // SI le composant est une 'Page' avec une METADATA
            else if (isTypeExpected(pComposant.getWidget().getName(), vTabTypePage)) {
               for (String typeExpected : pTabTypeExpected) {
                  if (MetadataUtils.isMetadataTitle(pComposant, typeExpected.toUpperCase())) {
                     vReturn = true;
                     break;
                  }
               }
            }
         }
      }
      return vReturn;
   }

   /**
    * Permet de savoir si le 'pWidget' fait partie des Widgets attendus.
    * 
    * @param pWidget Le widget a regarder.
    * @param pTabTypeExpected Le tableau des wisgets attendus.
    * @return 'true' s'il fait parti des widgets attendus.
    */
   private boolean isTypeExpected (String pWidgetName, String[] pTabTypeExpected) {
      boolean vReturn = false;
      if (pTabTypeExpected != null) {
         for (int i = 0; ((i < pTabTypeExpected.length) && (vReturn == false)); i++) {
            String vTypeExpected = pTabTypeExpected[i];
            if (pWidgetName.equalsIgnoreCase(vTypeExpected)) {
               vReturn = true;
            }
         }
      }
      return vReturn;
   }

   /**
    * Obtenir les éléments de la page.
    * 
    * @param pPage La page à regarder.
    * @return La liste désirée.
    */
   public List<AbstractViewElement> getOwnedElements_sce (ViewContainer pPage) {
//      List<AbstractViewElement> listElement = pPage.getOwnedElements();
      Layout vLayout = pPage.getLayout();
      List<AbstractViewElement> listElement = new ArrayList<>();
      // Obtenir le 'ViewElement'
      AbstractViewElement vAbstractViewElement = vLayout.getViewElement();
      // Ajouter le 'ViewElement' racine
      listElement.add(vAbstractViewElement);
      // Parcourir le Layout
      initOwnedLayouts_intern(listElement, vLayout);

      return listElement;
   }

   private void initOwnedLayouts_intern (List<AbstractViewElement> pReturn, Layout pLayout) {
      // Ajouter un 'Panel'
      ViewContainer vViewContainer = ViewFactory.eINSTANCE.createViewContainer();
      vViewContainer.setName("CRLF");
      for (Layout vLayout : pLayout.getOwnedLayouts()) {
         // Ajouter le 'ViewElement'
         AbstractViewElement vViewElement = vLayout.getViewElement();
         if (vViewElement != null) {
            pReturn.add(vViewElement);
            initOwnedLayouts_intern(pReturn, vLayout);
         }
      }

   }

   /**
    * Return all next states from a flowState using an event of the specified on
    * the viewElement
    * 
    * @param flowState
    * @param viewElement
    * @param eventType
    * @return
    */
   public List<FlowState> getListNextStates_sce (FlowState flowState, ViewElement viewElement) {
      return getListNextStates(flowState, viewElement, null);
   }

   public List<FlowState> getListNextStates (FlowState pFlowState, ViewElement pViewElement, String pEventType) {
      List<FlowState> vReturn = new ArrayList<>();
      if (pViewElement == null) {
         // Si pas de filtrage sur l'EventType (ex : "OnClick")
         if (pEventType == null) {
            vReturn.addAll(getNextStates_sce(pFlowState, null));
         }
         else {
            _ErrorControler.writeErrorGeneration(
                  "Erreur pour définir le prochaine State : Pas de viewElement sur flowState="
                        + pFlowState.getTechnicalid() + " et eventType non null (=" + pEventType + ")");
         }
      }
      else {
         // SI aucune transition sur le widget
         if (pViewElement.getEvents().size() == 0) {
            // Ajouter uniquement un état Final pour rentrer dans la boucle et faire le
            // if...else
            vReturn.add(FlowFactory.eINSTANCE.createFinalState());
         }
         else {
            // Retrieve the events of the right type
            for (ViewEvent event : pViewElement.getEvents()) {
               // Si pas de filtrage sur l'EventType (ex : "OnClick")
               if (pEventType == null) {
                  vReturn.addAll(getNextStates_sce(pFlowState, event));
               }
               else if (event.getType() != null && pEventType.equals(event.getType().getName())) {
                  vReturn.addAll(getNextStates_sce(pFlowState, event));
               }
            }
         }
      }
      return vReturn;
   }

   public Boolean isNextState_sce (FlowState flowState, ViewElement viewElement) {
      Boolean vReturn;

      List<FlowState> list = getListNextStates_sce(flowState, viewElement);
      if (list == null || list.size() == 0) {
         vReturn = false;
      }
      else {
         vReturn = true;
      }
      return vReturn;
   }

   /**
    * Return a collection of {@link ActionState} and/or {@link ViewState}
    * associated with the given {@link FlowState}.
    * 
    * @param flowState the given {@link FlowState}.
    * @param event the given {@link Event}.
    * @return a collection of {@link FlowState}.
    */
   public List<FlowState> getNextStates_sce (FlowState flowState, Event event) {
      List<FlowState> nexts = new ArrayList<>();
      if (flowState instanceof SubflowState) {
         Flow flow = ((SubflowState) flowState).getSubflow();
         if (flow != null) {
            InitialState initialState = getInitialState(flow);
            nexts.addAll(getListNextStates_sce(initialState, null));
         }

      }
      else if (flowState instanceof ViewState || flowState instanceof ActionState
            || flowState instanceof InitialState || flowState instanceof DecisionState) {
         Flow flow = getFlow(flowState);
         if (flow != null) {
            for (Transition transition : flow.getTransitions()) {
               FlowState from = transition.getFrom();
               FlowState to = transition.getTo();
               Collection<Event> on = transition.getOn();
               if (from != null && to != null && from.equals(flowState) && (on.contains(event) || event == null)) {
                  if (to instanceof ViewState) {
                     nexts.add(to);
                  }
                  else if (to instanceof ActionState) {
                     nexts.add(to);
                  }
                  else if (to instanceof SubflowState) {
                     nexts.addAll(getListNextStates_sce(to, null));
                  }
                  else if (to instanceof DecisionState) {
                     nexts.addAll(getListNextStates_sce(to, null));
                  }
                  else if (to instanceof InitialState) {
                     nexts.add(to);
                  }
                  else if (to instanceof FinalState) {
                     List<SubflowState> subflows = getAllSubFlowStates(flow);
                     List<Transition> transitions = getAllTransitions(flow);
                     for (SubflowState subflowState : subflows) {
                        if (flow.equals(subflowState.getSubflow())) {
                           for (Transition transitionTmp : transitions) {
                              FlowState fromTmp = transitionTmp.getFrom();
                              FlowState toTmp = transitionTmp.getTo();
                              if (from != null && to != null && fromTmp.equals(subflowState)) {
                                 nexts.add(toTmp);
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
      return nexts;
   }

   private InitialState getInitialState (Flow flow) {
      for (FlowState flowState : flow.getStates()) {
         if (flowState instanceof InitialState) {
            return (InitialState) flowState;
         }
      }
      return null;
   }

   /**
    * Obtenir le Flow contenant le FlowState.
    * 
    * @param pFlowState Le FlowState a regarder.
    * @return Le Flow désiré.
    */
   public Flow getFlow_sce (FlowState pFlowState) {
      return getFlow(pFlowState);
   }

   private Flow getFlow (EObject object) {
      EObject container = object.eContainer();
      if (container instanceof Flow) {
         return (Flow) container;
      }
      return null;
   }

   private List<SubflowState> getAllSubFlowStates (EObject object) {
      List<SubflowState> subflows = new ArrayList<SubflowState>();
      CinematicRoot root = getCinematicRoot(object);
      for (TreeIterator<EObject> iterAssoc = root.eAllContents(); iterAssoc.hasNext();) {
         Object obj = iterAssoc.next();
         if (obj instanceof SubflowState) {
            subflows.add((SubflowState) obj);
         }
      }
      return subflows;
   }

   private List<Transition> getAllTransitions (EObject object) {
      List<Transition> transitions = new ArrayList<Transition>();
      CinematicRoot root = getCinematicRoot(object);
      for (TreeIterator<EObject> iterAssoc = root.eAllContents(); iterAssoc.hasNext();) {
         Object obj = iterAssoc.next();
         if (obj instanceof Transition) {
            transitions.add((Transition) obj);
         }
      }
      return transitions;
   }

   private CinematicRoot getCinematicRoot (EObject object) {
      EObject container = EcoreUtil.getRootContainer(object);
      if (container instanceof CinematicRoot) {
         return (CinematicRoot) container;
      }
      return null;
   }

   /**
    * Permet de savoir si un composant est coché "Required" (ou pas).
    * 
    * @param pElement L'élément à regarder.
    * @return 'true' s'il est coché 'Required'.
    */
   public boolean isRequired_sce (AbstractViewElement pElement) {
      boolean vReturn;

      if (pElement instanceof ViewContainer) {
         vReturn = false;
      }
      else if (pElement instanceof ViewElement) {
         ViewElement vElement = (ViewElement) pElement;
         // Si champs oligatoire
         if (vElement.isRequired()) {
            vReturn = true;
         }
         else {
            vReturn = false;
         }
      }
      else {
         throw new UnsupportedOperationException("Cas non prévu pour un un pElement de type" + pElement.getClass());
      }

      return vReturn;
   }

   public static String getUUID (CinematicElement element) {
//    return EcoreUtil.generateUUID();
      // Obtenir une instance unique prévisible pour les tests macros.
      return UuidUtils.getUniqueIdentifierFromInstance();
   }

   /**
    * Convertir un Flow en path vers l'écran.
    * 
    * @param pViewElement Le composant sur lequel on déclenche la
    *           transition.
    * @param pFlowState Le Flow a regarder.
    * @param pPackageName_ViewContainerHtml Le nom du package.
    * @return Le path désiré.
    */
   public String convertFlow2Path_sce (ViewElement pViewElement, FlowState pFlowState, String pPackageFileSource,
         String pPackageFileDest) {
      String vReturn;

      if (pFlowState == null) {
         vReturn = null;
      }
      else {
         // Obtenir le delta pour le path en relatif
         String delta = DirectoryUtils.getDeltaRelatifPath(pPackageFileSource, pPackageFileDest);

         // SI c'est une transition vers un écran
         if (pFlowState instanceof ViewState) {
            ViewState vViewState = (ViewState) pFlowState;
            Flow vFlow = getFlow(pFlowState);
            vReturn = delta + vViewState.getName();
         }
         // SI c'est une transition vers un appel de service SOA
         else if (pFlowState instanceof ActionState) {
            ActionState vActionState = (ActionState) pFlowState;
            vReturn = delta + vActionState.getName();
         }
         else {
            vReturn = "convertFlow2Path_sce() : Cas non prévu - " + pViewElement.getName()
                  + " transition NON déterminée avec pFlowState=" + pFlowState.getTechnicalid();
            throw new GenerationRuntimeException(vReturn, "Revoir l'algo");
         }
      }

      vReturn = DirectoryUtils.sanitizedPath(vReturn);

      return vReturn;
   }

   /**
    * Obtenir la liste des opérations associée à l'ActionState.
    * 
    * @param pActionState L'ActionState à regarder.
    * @return La liste désirée.
    */
   public List<Operation> getListOperation_sce (ActionState pActionState) {
      List<Operation> vReturn = new ArrayList<>();

      List<FlowAction> listAction = pActionState.getActions();
      for (FlowAction flowAction : listAction) {
         List<Action> listOperation = flowAction.getOperations();
         for (Action action : listOperation) {
            // SI c'est une 'Operation'
            if (action instanceof Operation) {
               Operation vOperation = (Operation) action;
               vReturn.add(vOperation);
            }
         }
      }

      return vReturn;
   }

   public String getOperationSignatureHtml_sce (Operation pOperation) {
      String vReturn = getOperationSignature(pOperation);
      vReturn = StringUtils.replace(vReturn, "<", "&lt;");
      vReturn = StringUtils.replace(vReturn, ">", "&gt;");
      return vReturn;
   }

   private String getOperationSignature (Operation pOperation) {
      String vReturn;
      try {
         List<XDataType> listXStructuredType = getListXStructuredType(pOperation.getInput());
         String formatListParameterAndType = ListUtils.formatList(
               "${elem.getString_Type()} " + cPreffixeParam + "${elem.getName()}", ", ", listXStructuredType);
         vReturn = getNameOfOperation(pOperation) + "(" + formatListParameterAndType + ")"
               + getReturnOfOperation(pOperation);
      }
      catch (Exception vErr) {
         throw new ModelisationRuntimeException("Problème dans getOperationSignature_sce() : " + vErr.getMessage(),
               vErr);
      }
      return vReturn;
   }

   private String getReturnOfOperation (Operation pOperation) throws ModelisationRuntimeException, GenerationException {
      String vReturn;
      List<Parameter> listOutput = pOperation.getOutput();
      // SI procédure : void
      if (listOutput.size() == 0) {
         vReturn = "";
      }
      // Si fonction
      else if (listOutput.size() == 1) {
         Parameter parameterReturn = listOutput.get(0);
         XDataType vXDataType = new XDataType(parameterReturn, parameterReturn.getType());
         String returnName = parameterReturn.getName();
         if (returnName == null) {
            returnName = "resultat";
         }
         vReturn = " : " + vXDataType.getString_Type();
      }
      // SI plus d'un return : impossible en Java
      else {
         throw new ModelisationRuntimeException(
               "Erreur dans le modèle SOA AVEC l'opération \"" + pOperation.getName() + "\" : plusieurs return",
               "Conserver au maximum un seul return (= output)");
      }
      return vReturn;
   }

   private String getNameOfOperation (Operation pOperation) {
      StringNorme vStringNorme = new StringNorme(pOperation.getName());
      return vStringNorme.toConvJavaMethod();
//      return NomageConvention.applyCamelCaseLowerFirst_sce(pOperation.getName());
   }

   private List<XDataType> getListXStructuredType (List<Parameter> pListParameter) {
      List<XDataType> vReturn = new ArrayList<>();
      for (Parameter parameter : pListParameter) {
         // Encapsuler le 'Parameter' afin de pouvoir centraliser le formatage du type
         // (@see #getString_Type())
         vReturn.add(new XDataType(parameter, parameter.getType()));
      }
      return vReturn;
   }

   public static String getOperationDescriptionHtml_sce (Operation pOperation) {
      XBoolean vIsEmpty = new XBoolean();
      String description = getDescription(pOperation.getDescription(), "operation " + pOperation.getName(), vIsEmpty);
      // SI aucune description
      if (vIsEmpty.getValue() == true) {
         description = "<span style=\"color: red;\">" + description + "</span>";
      }

      return description;
   }

   /**
    * Obtenir la valeur et savoir si la description d'un élement est vide.
    * 
    * @param pDescription La description à regarder.
    * @param pElemName L'élément sur lequel porte la description.
    * @param pOutIsEmpty (OUT) 'true' Si la description est vide - 'false' sinon.
    * @return La description, où une chaîne indiquant que la description est vide.
    */
   private static String getDescription (String pDescription, String pElemName, XBoolean pOutIsEmpty) {
      String vReturn;

      if (pDescription == null) {
         pOutIsEmpty.setValue(true);
         vReturn = "Description de \"" + pElemName + "\" : " + cAucuneDescription;
      }
      else {
         pOutIsEmpty.setValue(false);
         vReturn = pDescription;
      }

      return vReturn;
   }

   public static String getOperationReturnDescriptionHtml_sce (Operation pOperation) {
      XBoolean vIsEmpty = new XBoolean();
      String description;
      // SI l'opération possède 1 return (= fonction)
      if ((pOperation.getOutput() != null) && (pOperation.getOutput().size() == 1)) {
         description = getDescription(pOperation.getOutput().get(0).getDescription(),
               "retour operation " + pOperation.getName(), vIsEmpty);
         // SI aucune description
         if (vIsEmpty.getValue() == true) {
            description = "<span style=\"color: red;\">" + description + "</span>";
         }
      }
      else {
         description = "";
      }

      return description;
   }

   public boolean isOperationHasParameterOut_sce (Operation pOperation) {
      boolean vReturn = isAtLeatOneParameter(pOperation.getOutput());
      return vReturn;
   }

   public boolean isOperationHasParameterIn_sce (Operation pOperation) {
      boolean vReturn = isAtLeatOneParameter(pOperation.getInput());

      return vReturn;
   }

   private boolean isAtLeatOneParameter (List<Parameter> pListParameter) {
      boolean vReturn;
      // SI au moins un paramètre à la méthode
      if (pListParameter.size() > 0) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }
      return vReturn;
   }

   public String getParameterDescriptionHtml_sce (Parameter pParameter) {
      XBoolean vIsEmpty = new XBoolean();
      String description = getDescription(pParameter.getDescription(), "paramètre " + pParameter.getName(), vIsEmpty);
      // SI aucune description
      if (vIsEmpty.getValue() == true) {
         description = "<span style=\"color: red;\">" + description + "</span>";
      }

      return description;
   }

   public String getParameterNameHtml_sce (Parameter pParameter) {
      XBoolean vIsEmpty = new XBoolean();
      String description = getName(pParameter.getName(), vIsEmpty);
      // SI aucune description
      if (vIsEmpty.getValue() == true) {
         description = "<span style=\"color: red;\">" + description + "</span>";
      }

      return description;
   }

   public String getParameterTypeNameHtml_sce (Parameter pParameter) {
      String vReturn;
      try {
         XBoolean vIsEmpty = new XBoolean();
         vReturn = getTypeName(pParameter.getType(), "paramètre " + pParameter.getName(), vIsEmpty);
         // SI aucune description
         if (vIsEmpty.getValue() == true) {
            vReturn = "<span style=\"color: red;\">" + vReturn + "</span>";
         }
         else {
            vReturn = StringUtils.replace(vReturn, "<", "&lt;");
            vReturn = StringUtils.replace(vReturn, ">", "&gt;");
         }
      }
      catch (Exception vErr) {
         vReturn = "Problème lors de l'appel de la méthode getParameterTypeNameHtml_sce() : " + vErr.getMessage();
         throw new GenerationRuntimeException(vReturn, vErr);
      }

      return vReturn;
   }

   private String getTypeName (Type pType, String pElemName, XBoolean pOutIsEmpty)
         throws GenerationException, ModelisationRuntimeException {
      String vReturn;

      StringNorme vStringNorme = new StringNorme(pType.getName());
      if (pType == null) {
         pOutIsEmpty.setValue(true);
         vReturn = "Type " + pElemName + " : " + cAucuneDescription;
      }
      else {
         pOutIsEmpty.setValue(false);
         // SI c'est une entité
         if (pType instanceof StructuredType) {
            StructuredType vStructuredType = (StructuredType) pType;
            XDataType vXDataType = new XDataType(vStructuredType);

            // SI c'est une Map
            if (vXDataType.isMap()) {
               vReturn = vXDataType.getString_Type();
            }
            else {
               vReturn = vStringNorme.toConvJavaAttribute();
//               vReturn = NomageConvention.applyCamelCase_sce(pType.getName());
            }
         }
         else {
            vReturn = vStringNorme.toConvJavaAttribute();
//            vReturn = NomageConvention.applyCamelCase_sce(pType.getName());
         }
      }

      return vReturn;
   }

   private String getName (String pElemName, XBoolean pOutIsEmpty) {
      String vReturn;

      if (pElemName == null) {
         pOutIsEmpty.setValue(true);
         vReturn = "Aucun nom : " + cAucuneDescription;
      }
      else {
         pOutIsEmpty.setValue(false);
         vReturn = pElemName;
      }

      return vReturn;
   }

   /**
    * Obtenir le lien "a href..." correctement renseigné.
    * 
    * @param pParameter Le paramètre.
    * @param pLibelle Le libellé.
    * @return Le lien HREF - sauf si type primitif --> uniquement le libellé SANS
    *         lien.
    * @throws ModelisationException
    * @throws GenerationException
    */
   public String getLinkHRef_sce (Parameter pParameter, String pLibelle)
         throws GenerationException, ModelisationRuntimeException {
      Type vType = pParameter.getType();
      String libelle = pLibelle;
      if (pLibelle == null) {
         libelle = getParameterTypeNameHtml_sce(pParameter);
      }

      String vTypeFmt;
      // SI c'est un type primitif
      if (vType instanceof PrimitiveType) {
         vTypeFmt = pLibelle;
      }
      else {
         Type type = pParameter.getType();
         // SI c'est une entité
         if (type instanceof StructuredType) {
            StructuredType vStructuredType = (StructuredType) type;
            XDataType vXDataType = new XDataType(vStructuredType);

            // SI c'est une Map
            if (vXDataType.isMap()) {
               List<StructuredType> listTypeMap = vXDataType.getMapElem_StructuredType();
               StructuredType typeKey = listTypeMap.get(0);
               StructuredType typeValue = listTypeMap.get(1);
               vTypeFmt = "Map<" + getLinkHRef_sceIntern(typeKey, typeKey.getName()) + ", "
                     + getLinkHRef_sceIntern(typeValue, typeValue.getName()) + ">";
            }
            else {
               vTypeFmt = getLinkHRef_sceIntern(type, libelle);
            }
         }
         else {
            vTypeFmt = getLinkHRef_sceIntern(type, libelle);
         }
      }

      XBoolean vIsObligatoire = new XBoolean();
      // Obtenir la chaîne représentant la cardinalité formatée
      String vReturn = getMultiplicityFmt(pParameter.getMultiplicity(), "", vType, vTypeFmt, vIsObligatoire);

      return vReturn;
   }

   private String getLinkHRef_sceIntern (Type pType, String pLibelle) {
      String vReturn = "<a href=\"#" + getLinkName_sce(pType) + "\" target=\"_self\">" + pLibelle + "</a>";
//      String vReturn = "<a href=\"#" + getLinkName_sce(pType) + "\" target=\"_blank\">" + pLibelle + "</a>";
      return vReturn;
   }

   /**
    * Obtenir le nom du lien HRef.
    * 
    * @param pParameter Le type.
    * @return Le nom du lien.
    */
   public String getLinkName_sce (Parameter pParameter) {
      String vReturn;
      try {
         vReturn = getLinkName_sceIntern(pParameter.getType());
      }
      catch (Exception vErr) {
         vReturn = "Problème lors de l'appel de la méthode getLinkName_sce() : " + vErr.getMessage();
         throw new GenerationRuntimeException(vReturn, vErr);
      }
      return vReturn;
   }

   public String getLinkName_sce (Type pType) {
      String vReturn;
      try {
         vReturn = getLinkName_sceIntern(pType);
      }
      catch (Exception vErr) {
         vReturn = "Problème lors de l'appel de la méthode getLinkName_sce() : " + vErr.getMessage();
         throw new GenerationRuntimeException(vReturn, vErr);
      }
      return vReturn;
   }

   /**
    * La Map contenant tous les type AVEC une chaîne unique pour un lien HRef
    * <name, link>.
    */
   private Map<String, String> _MapAllType = new HashMap<>();

   public String getLinkName_sceIntern (Type pType) throws GenerationException {
      String vReturn = _MapAllType.get(pType.getName());

      if (vReturn == null) {
         vReturn = "lnk" + pType.getTechnicalid();
         _MapAllType.put(pType.getName(), vReturn);
      }

      return vReturn;
   }

   /**
    * Obtenir la multiplicité formatée.
    * 
    * @param pMultiplicity La multiplicité.
    * @param pStartOfFmt Le début de la chaîne (ex : "Référence ").
    * @param pType Le type concerné.
    * @return La chaîne désirée (ex : "Référence un seul type "Personne"
    *         (obligatoire)".
    * @throws GenerationException
    * @throws ModelisationException
    */
   private String getMultiplicityFmt (MultiplicityKind pMultiplicity, String pStartOfFmt, Type pType, String pTypeName,
         XBoolean pIsObligatoire) throws GenerationException, ModelisationRuntimeException {
      String vReturn;
      // 0..1
      if (pMultiplicity == MultiplicityKind.ZERO_ONE_LITERAL) {
         pIsObligatoire.setValue(false);
         vReturn = pStartOfFmt + "aucun ou un seul type \"" + pTypeName + "\"";
      }
      // 1
      else if (pMultiplicity == MultiplicityKind.ONE_LITERAL) {
         pIsObligatoire.setValue(true);
         vReturn = pStartOfFmt + "un seul type \"" + pTypeName + "\" (obligatoire)";
      }
      // 0..*
      else if (pMultiplicity == MultiplicityKind.ZERO_STAR_LITERAL) {
         pIsObligatoire.setValue(false);
         vReturn = pStartOfFmt + "aucun ou plusieurs type \"" + pTypeName + "\"";
      }
      // 1..*
      else if (pMultiplicity == MultiplicityKind.ONE_STAR_LITERAL) {
         pIsObligatoire.setValue(true);
         vReturn = pStartOfFmt + "un ou plusieurs type \"" + pTypeName + "\" (obligatoire)";
      }
      // Cas non prévu
      else {
         throw new GenerationException("Erreur application - Cas non prévu pour la mutiplicité (" + pMultiplicity
               + ") de " + pType.getName(), "Revoir la structure de cas");
      }
      return vReturn;
   }

   /**
    * Obtenir l'écran (= Page) dans lequel se trouve le Widget.
    * 
    * @param pViewElement Le widget à regarder.
    * @return L'écran conteneur.
    */
   public static ViewContainer getScreenPage (ViewElement pViewElement) {
      ViewContainer vReturn;
      ViewContainer vViewContainer = null;
      CinematicElement vContainerCour = (CinematicElement) pViewElement.eContainer();
      do {
         // SI c'est un ViewContainer
         if (vContainerCour instanceof ViewContainer) {
            vViewContainer = (ViewContainer) vContainerCour;
            // SI c'est un écran (= Page)
            if (vViewContainer.getWidget().getName() != ComplexiteWidget_Enum.widgetPage.getNom()) {
               break;
            }
         }
         // Passer au conteneur au-dessus
         vContainerCour = (CinematicElement) vContainerCour.eContainer();
      } while (vContainerCour != null);

      vReturn = (ViewContainer) vViewContainer;

      return vReturn;
   }

   /**
    * Retourne le nombre de lignes du Layout.
    * 
    * @param pLayout Le layout à regarder.
    * @return Le nombre de lignes.
    */
   public int getNbLignesLayout (Layout pLayout) {
      int vReturn = 1;

      List<Layout> listLayout = pLayout.getOwnedLayouts();
      // Parcourir les sous-Layout
      for (Layout layout : listLayout) {
         // SI c'est un VirtualLayout
         if (layout.getViewElement() == null) {
            vReturn = getNbLignesLayout(layout);
            vReturn = vReturn + 1;
         }
         else {
            // RAS : on sort de la récursivité
         }
      }

      return vReturn;
   }

   /**
    * Obtenir le Layout découpé en Grid (= Table avec des cellules).
    * 
    * @param pLayout Le layout à regarder.
    * @return La liste représentant le Grid.
    */
   public List<List<AbstractViewElement>> getLayoutByGrid_sce (Layout pLayout) {
      List<List<AbstractViewElement>> vReturn = new ArrayList<>();

// FIXME : Bidouille faute de temps, mais compte tenu de la volumétrie du modèle écran, 
// ça fait le job pour l'instant

      // Initialiser la valeur 'Null' pour Acceleo
      initNullWidget();

      // Déterminer les positions X de chaque colonne
      List<Integer> listXColonnes = getNbColonnes(pLayout);

      // Remplir les cases
      getLayoutByGrid_intern(pLayout, listXColonnes, new ArrayList<>(), 0, true, vReturn);

      // Compléter d'éventuelle fin de lignes afin d'avoir une matrice (= table)
      completeEolTable(listXColonnes.size(), vReturn);

      // Inverser la liste
      Collections.reverse(vReturn);

      return vReturn;
   }

   /**
    * Initialiser le 'null' afin qu'Acceleo ne filtre pas.
    */
   private void initNullWidget () {
      cNullWidget = ViewFactory.eINSTANCE.createViewElement();
      cNullWidget.setName(cNullWidgetLib);
   }

   private void completeEolTable (int pNbCol, List<List<AbstractViewElement>> pReturn) {
      List<Integer> listRang = new ArrayList<>();
      int rangCour = 0;
      for (List<AbstractViewElement> listCour : pReturn) {
         if (isListNull(listCour)) {
            listRang.add(rangCour);
         }
         else {
            int ecart = pNbCol - listCour.size();
            // SI pas de composants sur la fin de la ligne
            if (ecart > 0) {
               for (int i = 0; i < ecart; i++) {
                  // On complète avec des 'null'
                  listCour.add(cNullWidget);
               }
            }
         }
         rangCour++;
      }

      // Supprimer les liste avec 'null' partout
      for (int rang : listRang) {
         pReturn.remove(rang);
      }
   }

   private boolean isListNull (List<AbstractViewElement> pList) {
      boolean vReturn = true;

      for (Object elem : pList) {
         if (elem != null && (false == cNullWidget.equals(elem))) {
            vReturn = false;
            break;
         }
      }
      return vReturn;
   }

   /**
    * Permet de savoir si le widget est 'null'.
    * 
    * @param pWidget Le widget à regarder.
    * @return 'true' si le widget est 'null'.
    */
   public boolean isNullWidget_sce (AbstractViewElement pWidget) {
      boolean vReturn;
      if ((pWidget == null) || (cNullWidget.equals(pWidget))) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }

      return vReturn;
   }

   private void getLayoutByGrid_intern (Layout pLayout, List<Integer> pListXColonnes,
         List<AbstractViewElement> pLigneCour, int pNumLigne, boolean pIsRoot,
         List<List<AbstractViewElement>> pReturn) {
      List<Layout> listLayout = pLayout.getOwnedLayouts();

      List<AbstractViewElement> ligneNext;

      // Ajouter la ligne courante
      pReturn.add(pNumLigne, pLigneCour);

      int rangColCour = 0;
      // Parcourir les sous-Layout
      for (Layout layout : listLayout) {
         // SI c'est un VirtualLayout
         AbstractViewElement viewElement = layout.getViewElement();
         if (viewElement == null) {
            rangColCour = 0;
            ligneNext = new ArrayList<>();
//System.err.println("DEBUG - viewElement=null");
            // Appel récursif
            getLayoutByGrid_intern(layout, pListXColonnes, ligneNext, pNumLigne + 1, false, pReturn);
         }
         else {
//System.err.println("DEBUG - viewElement=" + viewElement.getName());
            // SI un seul composant sur la ligne
            if (rangColCour == 0 && pIsRoot) {
               ligneNext = new ArrayList<>();
               // Ajouter la ligne courante
               pReturn.add(pNumLigne, ligneNext);
//               // Appel récursif
//               getLayoutByGrid_intern(layout, pListXColonnes, ligneNext, pNumLigne+1, false, pReturn);
            }
//            else {
            // Savoir dans quelle colonne se trouve le composant (contenu dans 'layout')
            int numColonne = getColonneNum(layout.getX(), pListXColonnes);
            // SI aucun trou dans le positionnement
            if (rangColCour == numColonne) {
               pLigneCour.add(viewElement);
            }
            else {
               int ecart = numColonne - rangColCour;
               for (int i = 0; i < ecart; i++) {
                  pLigneCour.add(cNullWidget);
               }
               rangColCour = numColonne;
               // Ajouter l'élément
               pLigneCour.add(viewElement);
            }
            rangColCour++;
//            }
         }

      } // FIN for (Layout layout : listLayout)
   }

   /**
    * Permet de savoir le No de la colonne par rapport à sa position X.
    * 
    * @param pX La position du ViewElement à regarder.
    * @param pListXColonnes
    * @return
    */
   private int getColonneNum (int pX, List<Integer> pListXColonnes) {
      int vReturn = -1;

      int rang = 0;
      int margeErreur = 10;
      for (Integer vX : pListXColonnes) {
         if ((pX == vX) || ((pX >= vX - margeErreur) && (pX <= vX + margeErreur))) {
            vReturn = rang;
            break;
         }
         rang++;
      }

      return vReturn;
   }

   /**
    * Obtenir le nombre de colonnes = au nombre de ViewElement dans le 1er
    * VirtualLayout (= sous-Layout).
    * 
    * @param pLayout Le Layout principal (ex : une Table).
    * @return Le nombre de colonnes.
    */
   List<Integer> getNbColonnes (Layout pLayout) {
      return getNbColonnes_intern(pLayout);
   }

   private List<Integer> getNbColonnes_intern (Layout pLayout) {
      List<Integer> vReturn = new ArrayList<>();

      List<Layout> listLayout = pLayout.getOwnedLayouts();
      int nivLayout = 1;
      // Parcourir les sous-Layout
      for (Layout layout : listLayout) {
         // SI c'est un VirtualLayout
         if (layout.getViewElement() == null) {
            vReturn = getNbColonnes_intern(layout);
            nivLayout++;
         }
         else {
            // SI uniquement 1er layout
            if (nivLayout <= 1) {
               // Ajouter la position en X (= la colonne)
               vReturn.add(layout.getX());
            }
         }
      }

      return vReturn;
   }

   /** Cache pour le taborder des composants d'un écran. */
   private static Map<ViewContainer, Map<ViewElement, Integer>> _MapWidgetTabOrder = new HashMap<>();

   public int getTabOrder_sce (ViewContainer pScreen, ViewElement pViewElement) {
      int vReturn;

      Map<ViewElement, Integer> mapWidgetTabOrder = _MapWidgetTabOrder.get(pScreen);
      // SI écran pas encore rencontrée
      if (mapWidgetTabOrder == null) {
         List<ViewElement> listWidgetTabOrder = getListWidgetTabOrder(pScreen);
         // List --> Map
         mapWidgetTabOrder = convertTabOrderList2Map(listWidgetTabOrder);
         // Mémoriser
         _MapWidgetTabOrder.put(pScreen, mapWidgetTabOrder);
      }

      if (pViewElement == null) {
//			vReturn = -1;
         throw new IllegalArgumentException("Problème dans l'écran '" + pScreen.getName() + "' car pViewElement=" + pViewElement);
      }
      else {
         if (mapWidgetTabOrder == null) {
            vReturn = -1;
         }
         else {
            // Obtenir le taborder
            vReturn = mapWidgetTabOrder.get(pViewElement);
         }
      }

      return vReturn;
   }

   /**
    * Convertir la List en Map afin d'optimiser le traitement.
    * 
    * @param pListWidgetTabOrderOfScreen La liste d'origine.
    * @return La Map avec le couple (composant, taborder).
    */
   private Map<ViewElement, Integer> convertTabOrderList2Map (List<ViewElement> pListWidgetTabOrderOfScreen) {
      Map<ViewElement, Integer> vReturn = new HashMap<>();

      int rang = 0;
      for (ViewElement viewElement : pListWidgetTabOrderOfScreen) {
         vReturn.put(viewElement, rang);
         rang++;
      }
      return vReturn;
   }

   /**
    * Obtenir la liste des composants classés dans l'ordre du tabOrder.
    * 
    * @param pScreen L'écran à regarder.
    * @return La liste désirée.
    */
   public List<ViewElement> getListWidgetTabOrder (ViewContainer pScreen) {
      List<ViewElement> vReturn = new ArrayList<>();

      getListWidgetTabOrder_intern((ViewContainer) pScreen, vReturn);

//      // SI c'est un conteneur
//      if (pScreen instanceof ViewContainer) {
//         ViewContainer vScreen = (ViewContainer) pScreen;
//
//         // SI c'est un conteneur
//         if (pScreen instanceof ViewContainer) {
//            getListWidgetTabOrder_intern((ViewContainer) pScreen, vReturn);
//         }
//         else {
//            throw new IllegalArgumentException("Erreur - Argument invalide 'pScreen' doit être un conteneur");
//         }
//      }
//      else {
//         throw new IllegalArgumentException("L'argument doit être un conteneur pScreen=" + pScreen.getName());
//      }

      return vReturn;
   }

   private void getListWidgetTabOrder_intern (ViewContainer pContainer, List<ViewElement> pReturn) {
      List<AbstractViewElement> listWidget = pContainer.getOwnedElements();
      for (AbstractViewElement elem : listWidget) {
         // SI c'est un conteneur
         if (elem instanceof ViewContainer) {
            ViewContainer container = (ViewContainer) elem;
            getListWidgetTabOrder_intern(container, pReturn);
         }
         else if (elem instanceof ViewElement) {
            ViewElement widget = (ViewElement) elem;
            // Ajouter l'élément
            pReturn.add(widget);
         }
         else {
            throw new IllegalArgumentException("Erreur - Cas non prévu pour elem=" + elem);
         }
      }
   }
}
