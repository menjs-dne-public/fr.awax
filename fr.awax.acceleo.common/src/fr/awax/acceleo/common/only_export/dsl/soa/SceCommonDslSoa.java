package fr.awax.acceleo.common.only_export.dsl.soa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.ecore.util.EcoreUtil.UsageCrossReferencer;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.obeonetwork.dsl.cinematic.CinematicElement;
import org.obeonetwork.dsl.environment.Attribute;
import org.obeonetwork.dsl.environment.Enumeration;
import org.obeonetwork.dsl.environment.ObeoDSMObject;
import org.obeonetwork.dsl.environment.Reference;
import org.obeonetwork.dsl.environment.StructuredType;
import org.obeonetwork.dsl.environment.Type;
import org.obeonetwork.dsl.requirement.Requirement;
import org.obeonetwork.dsl.requirement.RequirementPackage;
import org.obeonetwork.dsl.soa.Component;
import org.obeonetwork.dsl.soa.Operation;
import org.obeonetwork.dsl.soa.Parameter;

import fr.awax.acceleo.common.data.XBoolean;
import fr.awax.acceleo.common.dsl.Dsl_Enum;
import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.exception.GenerationRuntimeException;
import fr.awax.acceleo.common.only_export.SceAbstractInstance;

public class SceCommonDslSoa extends SceAbstractInstance {
   private static final String cAucuneDescription = "** Non renseigné dans le modèle **";

   /** Pour la traçabilité. */
   private static final Dsl_Enum DSL = Dsl_Enum.DslSoaServices;

   public SceCommonDslSoa() {
      super();
   }

   private SceCommonDslSoa(String pPluginId) {
      super(pPluginId);
   }

   private static Map<String, SceCommonDslSoa> _MapInstances = new LinkedHashMap<>();

   public static SceCommonDslSoa getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static SceCommonDslSoa getInstance (String pPluginId) {
      SceCommonDslSoa instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new SceCommonDslSoa(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      // RAS
   }

   /**
    * Obtenir la liste des types complexes utilisés par l'opération.
    * @param pOperation L'opération à regarder.
    * @return La liste désirée.
    */
   public List<StructuredType> getListStructuredTypeOfOperation_sce (Operation pOperation) {
      List<StructuredType> vReturn = new ArrayList<>();

      // Obtenir les paramètres IN
      for (Parameter vParameter : pOperation.getInput()) {
         if (isStructuredType(vParameter)) {
            vReturn.add((StructuredType) vParameter.getType());
         }
      }

      // Obtenir les paramètres OUT
      for (Parameter vParameter : pOperation.getOutput()) {
         if (isStructuredType(vParameter)) {
            vReturn.add((StructuredType) vParameter.getType());
         }
      }

      return vReturn;
   }

   private boolean isStructuredType (Parameter vParameter) {
      boolean vReturn;

      if (vParameter.getType() instanceof org.obeonetwork.dsl.environment.StructuredType) {
         vReturn = true;
      }
      else if (vParameter.getType() instanceof org.obeonetwork.dsl.environment.PrimitiveType) {
         vReturn = false;
      }
      else {
         vReturn = false;
      }
      return vReturn;
   }

}
