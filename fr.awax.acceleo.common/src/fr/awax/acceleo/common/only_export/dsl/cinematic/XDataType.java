package fr.awax.acceleo.common.only_export.dsl.cinematic;

import java.util.List;

import org.obeonetwork.dsl.environment.Enumeration;
import org.obeonetwork.dsl.environment.MultiplicityKind;
import org.obeonetwork.dsl.environment.PrimitiveType;
import org.obeonetwork.dsl.environment.StructuredType;
import org.obeonetwork.dsl.environment.Type;
import org.obeonetwork.dsl.soa.Parameter;

import fr.awax.acceleo.common.exception.GenerationException;
import fr.awax.acceleo.common.exception.ModelisationRuntimeException;
import fr.awax.acceleo.common.norme.StringNorme;
import fr.awax.acceleo.common.only_export.types.DataTypeEnum;

/**
 * Permet d'encapsuler un 'StructuredType' du DSL SOA-DTO ou Entity.
 */
public class XDataType {
   /**
    * Identifiant pour définir une Map avec un DTO.
    * Ex : Map<String, Personne> --> "_Map_Personne" avec 2 associatedType : "String" et "Personne"
    */
   public static final String cIdMapType = "_Map_";

   private Parameter _Parameter;
   private Type _Type;

   public XDataType(Parameter pParameter, Type pType) {
      _Parameter = pParameter;
      _Type = pType;
   }

   public XDataType(StructuredType pStructuredType) {
      this(null, pStructuredType);
   }

   private void setType (Type pType) {
      _Type = pType;
   }

   public Type getType () {
      return _Type;
   }

   /**
    * Obtenir le concept associé au type du paramètre.
    * @return
    * @throws ModelisationRuntimeException
    * @throws GenerationException
    */
   public StructuredType getConceptAssociatedType () throws ModelisationRuntimeException, GenerationException {
      StructuredType vReturn;
      String parameterName = _Type.getName();
      StructuredType vStructuredType = getStructuredType();
      if (vStructuredType != null) {
         // Obtenir les AssociatedTypes (ex : 'UnePersonne') au type (ex : 'Personne')
         List<StructuredType> listAssociatedTypes = vStructuredType.getAssociatedTypes();
         if (listAssociatedTypes.size() == 0) {
            vReturn = null;
         }
         else if (listAssociatedTypes.size() == 1) {
            vReturn = listAssociatedTypes.get(0);
         }
         else if (listAssociatedTypes.size() == 2) {
            // SI c'est une Map modélisation dans un DTO
            // Ex : Map<String, Personne> --> "_Map_Personne" avec 2 associatedType : "String" et "Personne"
            if (parameterName.startsWith(cIdMapType)) {
               // Pas de concept associé
               vReturn = null;
            }
            else {
               throw new ModelisationRuntimeException("Erreur dans la modélisation : 2 concepts associés à un DTO impossible si c'est pour définir une Map", "Utiliser un seul AssociatedTypes ou préfixé le DTO de " + cIdMapType + " pour utiliser 2 AssociatedTypes");
            }
         }
         else {
            throw new ModelisationRuntimeException("Erreur dans la modélisation : " + listAssociatedTypes.size() + " concepts associés à un DTO impossible ", "Utiliser un seul AssociatedTypes");
         }
      }
      else {
         vReturn = null;
      }

      return vReturn;
   }

   public String getName () throws GenerationException {
      String vReturn = getParameter().getName();
      return vReturn;
   }

   public String getDescription () throws GenerationException {
      String vReturn = getParameter().getDescription();
      return vReturn;
   }

   /**
    * Obtenir les types figurant dans la Map <TypeClé, TypeValeur>.
    * @return La liste des 2 types - 'null' si ce n'est pas une Map.
    * @throws ModelisationRuntimeException
    * @throws GenerationException
    */
   public List<StructuredType> getMapElem_StructuredType () throws ModelisationRuntimeException, GenerationException {
      List<StructuredType> vReturn;
      if (isMap()) {
         StructuredType vStructuredType = getStructuredType();
         vReturn = vStructuredType.getAssociatedTypes();
      }
      else {
         vReturn = null;
      }

      return vReturn;
   }

   public String getString_Type () throws ModelisationRuntimeException, GenerationException {
      String vReturn = "";

      StructuredType vStructuredType = getStructuredType();

      // SI c'est un type complexe (= une classe)
      if (vStructuredType != null) {
         String parameterName = vStructuredType.getName();
         List<StructuredType> listAssociatedTypes = vStructuredType.getAssociatedTypes();
         if (listAssociatedTypes.size() == 1) {
            vReturn = vStructuredType.getName();
         }
         else if (listAssociatedTypes.size() == 2) {
            // SI c'est une Map modélisation dans un DTO
            // Ex : Map<String, Personne> --> "_Map_Personne" avec 2 associatedType : "String" et "Personne"
            if (parameterName.startsWith(cIdMapType)) {
               getMapElem_StructuredType();
               String typeOfMap = "";
               // Formater les types de la Map - Ex : "Map<String, Personne>"
               String cSepVirgule = ",";
               for (StructuredType vStructuredType2 : listAssociatedTypes) {
                  StringNorme vStringNorme = new StringNorme(vStructuredType2.getName());
//                  DataTypeEnum pojodtomodel = DataTypeEnum.getValueWithStructuredType(vStructuredType2);
//                  typeOfMap = typeOfMap + NomageConvention.applyNormeTypeWithProperties(null, vStructuredType2, vStructuredType2.getName(), pojodtomodel) + cSepVirgule;
                  typeOfMap = typeOfMap + vStringNorme.toConvJavaAttribute() + cSepVirgule;
               }
               typeOfMap = typeOfMap.substring(0, typeOfMap.length() - cSepVirgule.length());
//               String masque = "${elem.getName()}";
//               typeOfMap = ListUtils.formatList(masque, ", ", listAssociatedTypes);
               vReturn = "Map<" + typeOfMap + ">";
            }
            else {
               throw new ModelisationRuntimeException("Erreur dans la modélisation : Un DTO préfixé de " + cIdMapType + " est considéré comme une Map", "Ajouter 2 associatedType - Map<String, Personne> --> \"" + parameterName + "\" avec 2 associatedType : \"String\" et \"Personne\"");
            }
         }
         // Cas par défaut
         else {
            vReturn = vStructuredType.getName();
         }
      }
      else {
         vReturn = _Type.getName();
      }

      // Gérer la cardinalité
      // --------------------
      String multiplicityFmt;
      if (_Parameter != null) {
         MultiplicityKind vMultiplicity = _Parameter.getMultiplicity();
         // 1
         if (vMultiplicity.getValue() == MultiplicityKind.ONE) {
            multiplicityFmt = vReturn;
         }
         // 0..1
         else if (vMultiplicity.getValue() == MultiplicityKind.ZERO_ONE) {
            multiplicityFmt = vReturn;
         }
         // 0..*
         else if (vMultiplicity.getValue() == MultiplicityKind.ZERO_STAR) {
            multiplicityFmt = "List<" + vReturn + ">";
         }
         // 1..*
         else if (vMultiplicity.getValue() == MultiplicityKind.ONE_STAR) {
            multiplicityFmt = "List<" + vReturn + ">";
         }
         else {
            throw new GenerationException("Erreur application - Cas non géré vMultiplicity.getLiteral() = " + vMultiplicity.getLiteral(), "Revoir la structure de cas");
         }
         vReturn = multiplicityFmt;
      }

      return vReturn;
   }

   /**
    * Permet de savoir si le type est une Map.
    * @return 'true' si c'est une Map.
    * @throws GenerationException
    */
   public boolean isMap () throws GenerationException {
      boolean vReturn;
      StructuredType vStructuredType = getStructuredType();
      if (vStructuredType != null) {
         String parameterName = vStructuredType.getName();
         if (parameterName.startsWith(cIdMapType)) {
            vReturn = true;
         }
         else {
            vReturn = false;
         }
      }
      else {
         vReturn = false;
      }

      return vReturn;
   }

   public Parameter getParameter () throws GenerationException {
      Parameter vReturn;
      if (_Parameter == null) {
         throw new GenerationException("Erreur application : Le _Parameter est null", "Vérifier que le constructeur avec le Parameter a été utilisé");
      }
      else {
         vReturn = _Parameter;
      }
      return vReturn;
   }

   private StructuredType getStructuredType () throws GenerationException {
      StructuredType vReturn;

      if (_Type instanceof StructuredType) {
         vReturn = (StructuredType) _Type;
      }
      else if (_Type instanceof PrimitiveType) {
         vReturn = null;
      }
      else if (_Type instanceof Enumeration) {
         vReturn = null;
      }
      else {
         throw new GenerationException("Erreur application : Le pType=" + _Type.getClass() + " n'est pas géré pour l'instant", "Reprendre la structure de cas  - ajouter un else if ?");
      }
      return vReturn;
   }

}
