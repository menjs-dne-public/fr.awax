package fr.awax.acceleo.common.only_export.dsl.environment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.ecore.util.EcoreUtil.UsageCrossReferencer;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.obeonetwork.dsl.cinematic.CinematicElement;
import org.obeonetwork.dsl.environment.Attribute;
import org.obeonetwork.dsl.environment.Enumeration;
import org.obeonetwork.dsl.environment.ObeoDSMObject;
import org.obeonetwork.dsl.environment.Reference;
import org.obeonetwork.dsl.requirement.Requirement;
import org.obeonetwork.dsl.requirement.RequirementPackage;
import org.obeonetwork.dsl.soa.Operation;
import org.obeonetwork.dsl.soa.Parameter;

import fr.awax.acceleo.common.dsl.Dsl_Enum;
import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.exception.GenerationRuntimeException;
import fr.awax.acceleo.common.only_export.SceAbstractInstance;

public class SceCommonDslEnvironment extends SceAbstractInstance {
   /** Pour la traçabilité. */
   private static final Dsl_Enum DSL = Dsl_Enum.DslEnvironment;

   public SceCommonDslEnvironment() {
      super();
   }

   private SceCommonDslEnvironment(String pPluginId) {
      super(pPluginId);
   }

   private static Map<String, SceCommonDslEnvironment> _MapInstances = new LinkedHashMap<>();

   public static SceCommonDslEnvironment getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static SceCommonDslEnvironment getInstance (String pPluginId) {
      SceCommonDslEnvironment instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new SceCommonDslEnvironment(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      // RAS
   }

}
