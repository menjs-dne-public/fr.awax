package fr.awax.acceleo.common.only_export.dsl.requirement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.ecore.util.EcoreUtil.UsageCrossReferencer;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.obeonetwork.dsl.cinematic.CinematicElement;
import org.obeonetwork.dsl.cinematic.view.ViewElement;
import org.obeonetwork.dsl.environment.Attribute;
import org.obeonetwork.dsl.environment.ObeoDSMObject;
import org.obeonetwork.dsl.environment.Reference;
import org.obeonetwork.dsl.requirement.Requirement;
import org.obeonetwork.dsl.requirement.RequirementPackage;
import org.obeonetwork.dsl.soa.Operation;
import org.obeonetwork.dsl.soa.Parameter;

import fr.awax.acceleo.common.data.XBoolean;
import fr.awax.acceleo.common.dsl.Dsl_Enum;
import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.exception.GenerationRuntimeException;
import fr.awax.acceleo.common.only_export.SceAbstractInstance;

public class SceCommonDslRequirement extends SceAbstractInstance {
   private static final String cAucuneDescription = "** Non renseigné dans le modèle **";

   /** Pour la traçabilité. */
   private static final Dsl_Enum DSL = Dsl_Enum.DslRequirement;

   public SceCommonDslRequirement() {
      super();
   }

   private SceCommonDslRequirement(String pPluginId) {
      super(pPluginId);
   }

   private static Map<String, SceCommonDslRequirement> _MapInstances = new LinkedHashMap<>();

   public static SceCommonDslRequirement getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static SceCommonDslRequirement getInstance (String pPluginId) {
      SceCommonDslRequirement instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new SceCommonDslRequirement(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      // RAS
   }

   /**
    * Obtenir la liste des exigences sur un élement de modélisation DSM (une opération SOA).
    * @param pElement L'élément d'accrochage.
    * @return La liste d'exigence(s) accroché sur l'élément.
    */
   public List<Requirement> getListRequirement_sce (ObeoDSMObject pElement) {
      List<Requirement> vReturn;
      // Use of Sirius cross referencer if possible
      Session session = SessionManager.INSTANCE.getSession(pElement);
      if (pElement == null) {
         throw new GenerationRuntimeException("Problème lors du \"getListRequirement()\" : Le pElement est null", "Revoir le traitement");
      }

      if (pElement instanceof ViewElement) {
         ViewElement vElement = (ViewElement) pElement;
      }
      if (session != null) {
         ECrossReferenceAdapter semanticCrossReferencer = session.getSemanticCrossReferencer();
         if (semanticCrossReferencer != null) {
            vReturn = getRequirementsFromSettings(semanticCrossReferencer.getInverseReferences(pElement));
         }
      }

      if (pElement.eResource() == null) {
         vReturn = null;
      }
      else {
         // Use of our own cross referencer if needed            
         vReturn = getRequirementsFromSettings(UsageCrossReferencer.find(pElement, pElement.eResource().getResourceSet()));
      }
      return vReturn;
   }

   /**
    * Obtenir les exigences sur l'opértation (exigences MAS).
    * @param pOperation L'opération à regarder.
    * @return La liste des exigences sur l'opération.
    */
   public List<Requirement> getListRequirementOfOperation_sce (Operation pOperation) {
      List<Requirement> vReturn = getListRequirement(pOperation);
      return vReturn;
   }

   /**
    * Obtenir la liste des exigences sur un élement de modélisation DSM (une opération SOA).
    * @param pElement L'élément d'accrochage.
    * @return La liste d'exigence(s) accroché sur l'élément.
    */
   public List<Requirement> getListRequirement (ObeoDSMObject pElement) {
      // Use of Sirius cross referencer if possible
      Session session = SessionManager.INSTANCE.getSession(pElement);
      if (pElement == null) {
         throw new GenerationRuntimeException("Problème lors du \"getListRequirement()\" : Le pElement est null", "Revoir le traitement");
      }

      if (session != null) {
         ECrossReferenceAdapter semanticCrossReferencer = session.getSemanticCrossReferencer();
         if (semanticCrossReferencer != null) {
            return getRequirementsFromSettings(semanticCrossReferencer.getInverseReferences(pElement));
         }
      }

      if (pElement.eResource() == null) {
         return null;
      }
      else {
         // Use of our own cross referencer if needed            
         return getRequirementsFromSettings(UsageCrossReferencer.find(pElement, pElement.eResource().getResourceSet()));
      }
   }

   private List<Requirement> getRequirementsFromSettings (Collection<Setting> settings) {
      List<Requirement> requirements = new ArrayList<Requirement>();

      for (Setting usage : settings) {
         if (RequirementPackage.Literals.REQUIREMENT__REFERENCED_OBJECT == usage.getEStructuralFeature()) {
            EObject referencingObject = usage.getEObject();
            if (referencingObject instanceof Requirement) {
               Requirement requirement = (Requirement) referencingObject;
               if (!requirements.contains(requirement)) {
                  requirements.add(requirement);
               }
            }
         }
      }
// TODO : A réactiver  --> erreur de compilation "The method sort(List<T>, Comparator<? super T>) in the type Collections is not applicable for the arguments (List<Requirement>, RequirementComparator)"
//      Collections.sort(requirements, new RequirementComparator());
      return requirements;
   }

   public String getRequirementDescriptionHtml_sce (Requirement pRequirement) {
      XBoolean vIsEmpty = new XBoolean();
      String description = getDescription(pRequirement.getStatement(), "exigence " + pRequirement.getId(), vIsEmpty);
      // SI aucune description
      if (vIsEmpty.getValue() == true) {
         description = "<span style=\"color: red;\">"
               + description
               + "</span>";
      }

      return description;
   }

   /**
    * Obtenir la valeur et savoir si la description d'un élement est vide.
    * @param pDescription La description à regarder.
    * @param pElemName L'élément sur lequel porte la description.
    * @param pOutIsEmpty (OUT) 'true' Si la description est vide - 'false' sinon.
    * @return La description, où une chaîne indiquant que la description est vide.
    */
   private String getDescription (String pDescription, String pElemName, XBoolean pOutIsEmpty) {
      String vReturn;

      if (pDescription == null) {
         pOutIsEmpty.setValue(true);
         vReturn = "Description de \"" + pElemName + "\" : " + cAucuneDescription;
      }
      else {
         pOutIsEmpty.setValue(false);
         vReturn = pDescription;
      }

      return vReturn;
   }

   /**
    * Obtenir toutes les exigences accrochées à un élément de Cinematic.
    * @param element L'élément (ex : un Panel, un widget, ...)
    * @return La liste désirée.
    */
   public List<Requirement> getRequirements_sce (CinematicElement element) {
      return getListRequirement_sce(element);
   }

   public List<Requirement> getRequirements_sce (Reference element) {
      return getListRequirement_sce(element);
   }

   public List<Requirement> getRequirements_sce (Attribute element) {
      return getListRequirement_sce(element);
   }

   public List<Requirement> getRequirements_sce (Operation element) {
      return getListRequirement_sce(element);
   }

   public List<Requirement> getRequirements_sce (Parameter element) {
      return getListRequirement_sce(element);
   }

   public List<Requirement> getRequirements_sce (ObeoDSMObject element) {
      return getListRequirement_sce(element);
   }

}
