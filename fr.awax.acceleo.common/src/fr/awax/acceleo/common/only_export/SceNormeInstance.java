package fr.awax.acceleo.common.only_export;

import java.util.LinkedHashMap;
import java.util.Map;

import org.obeonetwork.dsl.environment.DataType;
import org.obeonetwork.dsl.environment.Enumeration;
import org.obeonetwork.dsl.environment.PrimitiveType;
import org.obeonetwork.dsl.environment.StructuredType;
import org.obeonetwork.dsl.environment.Type;

import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.norme.StringNorme;
import fr.awax.acceleo.common.util.StringUtils;

public class SceNormeInstance extends SceAbstractInstance {
   public SceNormeInstance() {
      super();
   }

   private SceNormeInstance(String pPluginId) {
      super(pPluginId);
   }

   private static Map<String, SceNormeInstance> _MapInstances = new LinkedHashMap<>();

   public static SceNormeInstance getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static SceNormeInstance getInstance (String pPluginId) {
      SceNormeInstance instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new SceNormeInstance(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      // RAS
   }

   /**
    * Convertir en CamelCase.
    * @param pValue La valeur à convertir.
    * @return La valeur mise à la norme.
    */
   public final String toNormeCamelCase_sce (String pValue) {
      final StringNorme vStringNorme = new StringNorme(trim(pValue));
      final String vReturn = vStringNorme.toNormeCamelCase().toString();
      return vReturn;
   }

   /**
    * Convertir en convention pour un nom de classe.
    * @param pValue La valeur à convertir.
    * @return La valeur mise à la norme.
    */
   public final String toConvJavaClass_sce (String pValue) {
      final String vReturn = toNormeCamelCase_sce(pValue);
      return vReturn;
   }

   /**
    * Convertir en convention pour un nom de package.
    * @param pValue La valeur à convertir.
    * @return La valeur mise à la norme.
    */
   public final String toConvJavaPackage_sce (String pValue) {
      final StringNorme vStringNorme = new StringNorme(trim(pValue));
      final String vReturn = vStringNorme.toConvJavaPackage();
      return vReturn;
   }

   /**
    * Convertir en convention pour un nom d'attribut.
    * @param pValue La valeur à convertir.
    * @return La valeur mise à la norme.
    */
   public final String toConvJavaAttribute_sce (String pValue) {
      final StringNorme vStringNorme = new StringNorme(trim(pValue));
      final String vReturn = vStringNorme.toConvJavaAttribute();
      return vReturn;
   }

   /**
    * Convertir en convention pour un nom de paramètre.
    * @param pValue La valeur à convertir.
    * @return La valeur mise à la norme.
    */
   public final String toConvJavaParameter_sce (String pValue) {
      final StringNorme vStringNorme = new StringNorme(trim(pValue));
      final String vReturn = vStringNorme.toConvJavaParameter();
      return vReturn;
   }

   /**
    * Convertir en convention pour un nom de méthode GETTER.
    * @param pValue La valeur à convertir.
    * @return La valeur mise à la norme.
    */
   public final String toConvJavaGetter_sce (String pValue) {
      final StringNorme vStringNorme = new StringNorme("get " + trim(pValue));
      final String vReturn = vStringNorme.toNormeCamelCaseIgnoreFirst().toString();
      return vReturn;
   }

   /**
    * Convertir en convention pour un nom de méthode SETTER.
    * @param pValue La valeur à convertir.
    * @return La valeur mise à la norme.
    */
   public final String toConvJavaSetter_sce (String pValue) {
      final StringNorme vStringNorme = new StringNorme("set " + trim(pValue));
      final String vReturn = vStringNorme.toNormeCamelCaseIgnoreFirst().toString();
      return vReturn;
   }

   /**
    * Convertir en convention pour une description en JavaDoc.
    * @param pValue La valeur à convertir.
    * @return La valeur mise à la norme.
    */
   public final String toConvJavaDescription_sce (String pValue) {
      final StringNorme vStringNorme = new StringNorme(trim(pValue));
      String vReturn = vStringNorme.toUpperFirst().toString();
      if (vReturn.endsWith(".")) {
         // Supprimer le "point" final
         vReturn = vReturn.substring(0, vReturn.length() - ".".length());
      }
      return vReturn;
   }

   /**
    * Convertir en type Java normé (ex : "TypeDispo : Enumeration --> TypeDispoEnum).
    * @param pType Le type.
    * @return Le type formaté.
    */
   public final String toConvJavaType_sce (DataType pType) {
      String vReturn;

      if (pType instanceof Enumeration) {
         vReturn = toConvJavaTypeEnumeration_sce(pType);
      }
      else if (pType instanceof StructuredType) {
         vReturn = toConvJavaTypeStructured_sce(pType);
      }
      else if (pType instanceof PrimitiveType) {
         vReturn = pType.getName();
      }
      else {
         throw new IllegalArgumentException("Erreur gen : Cas non prévu pour pType=" + pType.getClass());
      }
      return vReturn;
   }

   /**
    * Convertir en type Java normé 'Enumeration' (ex : "TypeDispo : Enumeration --> TypeDispoEnum).
    * @param pType Le type.
    * @return Le type formaté.
    */
   public final String toConvJavaTypeEnumeration_sce (DataType pType) {
      String vReturn = toNormeCamelCase_sce(pType.getName() + " Enum");
      return vReturn;
   }

   /**
    * Convertir en type Java normé 'Structuré' (ex : "Personne : StructuredType --> Personne).
    * @param pType Le type.
    * @return Le type formaté.
    */
   public final String toConvJavaTypeStructured_sce (Type pType) {
      String vReturn = pType.getName();
      return vReturn;
   }

   public String toConvHtml_sce (String pValue) {
      String vReturn;
      if (pValue == null) {
         vReturn = "** Pas renseigné dans la modélisation ** ";
      }
      else {
         vReturn = pValue.trim();
         vReturn = StringUtils.replace(vReturn, "<", "&lt;");
         vReturn = StringUtils.replace(vReturn, ">", "&gt;");
         vReturn = StringUtils.replace(vReturn, "\n", "<br/>");
         vReturn = applyWithoutRC_sce(vReturn);
      }
      return vReturn;
   }

   public String toConvCvs_sce (String pValue) {
      String vReturn;
      if (pValue == null) {
         vReturn = pValue = "** Pas renseigné dans la modélisation ** ";
      }
      else {
         vReturn = pValue.trim();
         vReturn = pValue = applyWithoutRC_sce(pValue);
      }
      return vReturn;
   }

   /**
    * Mettre en norme "WithoutRC" (ex : "unE\n Valeur\n\n" --> "unE Valeur").
    * @param pTextBrut Le texte original.
    * @return Le texte mis en forme.
    */
   public String applyWithoutRC_sce (String pValue) {
      String vReturn;
      final StringNorme vStringNorme = new StringNorme(trim(pValue));
      if (pValue == null) {
         vReturn = "** Pas renseigné dans la modélisation ** ";
      }
      else {
         vReturn = vStringNorme.toWithoutRC().toString();
      }
      return vReturn;
   }
}
