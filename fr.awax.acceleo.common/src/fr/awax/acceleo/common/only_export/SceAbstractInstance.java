package fr.awax.acceleo.common.only_export;

import fr.awax.acceleo.common.conf.AllPropAbstract;
import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.log.LogHelper;
import fr.awax.acceleo.common.norme.StringNorme;

abstract public class SceAbstractInstance {
   /**
    * Faire un RAZ du service Acceleo.
    */
   abstract public void clear ();

   private String _PluginId;

   public String getPluginId () {
      return _PluginId;
   }

   public void setPluginId (String pPluginId) {
      _PluginId = pPluginId;
   }

   private EngineHelper _EngineHelper;

   public EngineHelper getEngineHelper () {
      return _EngineHelper;
   }

   public void setEngineHelper (EngineHelper pEngineHelper) {
      _EngineHelper = pEngineHelper;
   }

   private AllPropAbstract _AllProp;

   public AllPropAbstract getAllProp () {
      return _AllProp;
   }

   public void setAllProp (AllPropAbstract pAllProp) {
      _AllProp = pAllProp;
   }

   private LogHelper _Log;

   public LogHelper getLog () {
      return _Log;
   }

   public void setLog (LogHelper pLog) {
      _Log = pLog;
   }

   protected SceAbstractInstance() {
//      // Constructeur interdit
//      throw new IllegalStateException("Appel à ce constructeur interdit");
      this(EngineHelper.getPluginIdRunning());
   }

   protected SceAbstractInstance(String pPluginId) {
      _PluginId = pPluginId;
      if (_PluginId == null) {
         throw new IllegalStateException("Pas de pluginId en cours d'exécution");
      }
      _EngineHelper = EngineHelper.getInstance(_PluginId);
      _AllProp = _EngineHelper.getAllProperties();
      _Log = LogHelper.getInstance(_PluginId);
   }

   /**
    * Supprimer les espaces à droite et à gauche.
    * @param pValue La valeur d'origine.
    * @return La valeur sans espace non significatif.
    */
   protected String trim (String pValue) {
      final String vReturn = StringNorme.trim(pValue);
      return vReturn;
   }

}
