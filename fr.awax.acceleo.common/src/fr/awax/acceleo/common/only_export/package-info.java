/**
 * Ces fichiers sont copier dans la partie common de chaque projet de génération
 * car avec Acceleo 3 : pb si le .mtl n'est pas dans le projet en mode "plugin"
 * (fonctionne en mode sous-Eclipse de dev)
 */
/**
 * @author egarel
 *
 */
package fr.awax.acceleo.common.only_export;