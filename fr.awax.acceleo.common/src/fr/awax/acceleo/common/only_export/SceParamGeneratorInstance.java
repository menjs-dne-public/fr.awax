package fr.awax.acceleo.common.only_export;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.norme.StringNorme;
import fr.awax.acceleo.common.util.DirectoryUtils;

public class SceParamGeneratorInstance extends SceAbstractInstance {

   public SceParamGeneratorInstance() {
      super();
   }

   private SceParamGeneratorInstance(String pPluginId) {
      super(pPluginId);
   }

   private static Map<String, SceParamGeneratorInstance> _MapInstances = new LinkedHashMap<>();

   public static SceParamGeneratorInstance getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static SceParamGeneratorInstance getInstance (String pPluginId) {
      SceParamGeneratorInstance instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new SceParamGeneratorInstance(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      // RAS
   }

   /**
    * Obtenir le nom de l'application spécifiée dans la Property 'appli_info_name'.
    * @return Le nom désiré.
    */
   public String getApplicationName_sce () {
      String appliName = getAllProp().propCommon().appli_info_name.getValue().toString();
      final StringNorme vStringNorme = new StringNorme(trim(appliName));
      final String vReturn = vStringNorme.toNormeCamelCase().toString();

      return vReturn;
   }

}
