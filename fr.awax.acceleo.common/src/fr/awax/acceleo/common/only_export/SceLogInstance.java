package fr.awax.acceleo.common.only_export;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;

import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.log.LogLevelEnum;
import fr.awax.acceleo.common.util.DateUtils;

public class SceLogInstance extends SceAbstractInstance {

   public SceLogInstance() {
      super();
   }

   private SceLogInstance(String pPluginId) {
      super(pPluginId);
   }

   private static Map<String, SceLogInstance> _MapInstances = new LinkedHashMap<>();

   public static SceLogInstance getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static SceLogInstance getInstance (String pPluginId) {
      SceLogInstance instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new SceLogInstance(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      // RAS
   }

   /**
    * Savoir si l'on est en mode DEBUG (ou sup).
    * @return 'true' si l'on est en mode DEBUG (ou sup).
    */
   public final boolean isNivDebug_sce () {
      boolean vReturn;
      if (getLog().getLevel().ordinal() <= LogLevelEnum.INFO.ordinal()) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }
      return vReturn;
   }

   /**
    * Permet de faire un log de niveau INFO dans la console Eclipse.
    * @param pMessage Le message.
    */
   public final void info_sce (String pMessage) {
      getLog().info(pMessage);
   }

   /**
    * Permet de faire un log de niveau DEBUG dans un fichier ".mtl".
    * @param pMessage Le message.
    */
   public final String debugPrint_sce (String pMessage) {
      String vReturn = printLog(pMessage, LogLevelEnum.DEBUG);

      return vReturn;
   }

   /**
    * Permet de faire un log de niveau DEBUG dans la console Eclipse.
    * @param pMessage Le message.
    */
   public final void debugConsole_sce (String pMessage) {
      getLog().debug(pMessage);
   }

   /**
    * Permet de faire un log de niveau WARN dans un fichier ".mtl".
    * @param pMessage Le message.
    */
   public final String warnPrint_sce (String pMessage) {
      String vReturn = printLog(pMessage, LogLevelEnum.WARNING);

      return vReturn;
   }

   /**
    * Permet de faire un log de niveau WARN dans la console Eclipse.
    * @param pMessage Le message.
    */
   public final void warngConsole_sce (String pMessage) {
      getLog().warn(pMessage);
   }

   /**
    * Permet de faire un log de niveau ERROR dans un fichier ".mtl".
    * @param pMessage Le message.
    */
   public final String errorPrint_sce (String pMessage) {
      String vReturn = printLog(pMessage, LogLevelEnum.ERROR);

      return vReturn;
   }

   /**
    * Permet de faire un log de niveau ERROR dans la console Eclipse.
    * @param pMessage Le message.
    */
   public final void errorConsole_sce (String pMessage) {
      getLog().error(pMessage);
   }

   private String printLog (String pMessage, LogLevelEnum pLogLevelEnum) {
      String vReturn = "";
      if (getLog().getLevel().ordinal() <= pLogLevelEnum.ordinal()) {
         vReturn = pMessage;
      }
      return vReturn;
   }

   /**
    * Obtient la date en cours sous la forme YYYY-MM-DD HH:mm.
    * @return une date.
    */
   public static String today_sce () {
      String vReturn;

      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
      vReturn = simpleDateFormat.format(DateUtils.getNowDate());
      return vReturn;
   }

}
