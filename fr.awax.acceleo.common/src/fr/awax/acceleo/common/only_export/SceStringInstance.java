package fr.awax.acceleo.common.only_export;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.obeonetwork.dsl.environment.ObeoDSMObject;

import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.util.DateUtils;
import fr.awax.acceleo.common.util.MetadataUtils;

public class SceStringInstance extends SceAbstractInstance {
   /** Map contenant le nombre de fois où la clé à été rencontrée */
   private Map<String, Integer> _MapCptSee = new LinkedHashMap<String, Integer>();

   public SceStringInstance() {
      super();
   }

   private SceStringInstance(String pPluginId) {
      super(pPluginId);
   }

   private static Map<String, SceStringInstance> _MapInstances = new LinkedHashMap<>();

   public static SceStringInstance getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static SceStringInstance getInstance (String pPluginId) {
      SceStringInstance instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new SceStringInstance(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      // RAS
   }

   /**
    * Obtenir la valeur associé à la METADATA.
    * @param pObj L'objet du DSM contenant la METADATA.
    * @param pName Le nom de la METADATA.
    * @return La valeur désirée.
    */
   public String getMetadata_sce (ObeoDSMObject pObj, String pName) {
      return MetadataUtils.getMetadata(pObj, pName);
   }

   /**
    * Savoir si un fichier a déjà été rencontré lors de la génération courante.
    * 
    * @param pKey Le nom du fichier.
    * @param pValAlea4CacheDesactivate La valeur courante de la boucle pour désactiver le cache Acceleo (non utilisé dans le traitement).
    * @return 'true' si déjà rencontré.
    */
   public Boolean isNeverSee_sce (final String pKey, final ObeoDSMObject pValAlea4CacheDesactivate) {
      final Boolean vReturn;
      // Si pas encore rencontré
      Integer vCpt = _MapCptSee.get(pKey);
      if (vCpt == null) {
         // Affecter "c'est la première fois"
         vCpt = 1;
         vReturn = Boolean.TRUE;
      }
      // Si déjà rencontré
      else {
         // Affecter "une fois de plus"
         vCpt++;
         vReturn = Boolean.FALSE;
      }

      _MapCptSee.put(pKey, vCpt);

//      System.err.println("DEBUG ==> pKey=" + pKey + " - " + pValAlea4CacheDesactivate.toString() + " - " + vReturn);
      return vReturn;
   }

   /** La Map de compteur. */
   private Map<String, Integer> _MapCpt = new HashMap<>();

   /**
    * Faire une Remise A Zéro de la boucle.
    * @param pBoucleName Le nom del a boucle.
    */
   public void cptRaz_sce (String pBoucleName) {
      _MapCpt.remove(pBoucleName);
   }

   public void cptRaz_sce (String pBoucleName, Integer pValue) {
      cptLet(null, pBoucleName, pValue);
   }

   /**
    * Incrémenter la boucle.
    * @param pBoucleName Le nom del a boucle.
    * @param pFakeForCacheAcceleo Valeur bidon pour obliger le cache Acceleo a ne pas retourner toujours la même valeur.
    */
   public void cptDec_sce (ObeoDSMObject pFakeForCacheAcceleo, String pBoucleName) {
      Integer value = _MapCpt.get(pBoucleName);
      if (value == null) {
         value = 0;
      }
      else {
         value++;
      }
      cptLet(pFakeForCacheAcceleo, pBoucleName, value);
   }

   public void cptInc_sce (ObeoDSMObject pFakeForCacheAcceleo, String pBoucleName) {
      Integer value = _MapCpt.get(pBoucleName);
      if (value == null) {
         value = 0;
      }
      else {
         value--;
      }
      cptLet(pFakeForCacheAcceleo, pBoucleName, value);
   }

   private void cptLet (ObeoDSMObject pFakeForCacheAcceleo, String pBoucleName, Integer pValue) {
      _MapCpt.put(pBoucleName, pValue);
   }

   /**
    * Obtenir la valeur de la boucle.
    * @param pBoucleName Le nom del a boucle.
    * @param pFakeForCacheAcceleo Valeur bidon pour obliger le cache Acceleo a ne pas retourner toujours la même valeur.
    */
   public int cptGet_sce (ObeoDSMObject pFakeForCacheAcceleo, String pBoucleName) {
      int vReturn;
      Integer value = _MapCpt.get(pBoucleName);
      if (value == null) {
         vReturn = 0;
      }
      else {
         vReturn = value;
      }
      return vReturn;
   }

   /**
    * Obtient la date en cours sous la forme YYYY-MM-DD HH:mm.
    * @return une date.
    */
   public String today_sce () {
      String vReturn;

      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
      vReturn = simpleDateFormat.format(DateUtils.getNowDate());
      return vReturn;
   }

   /**
    * Otenir la balise "TODO GENERATED".
    * @return La balise désirée.
    */
   public String tagTodoGenerated_sce () {
      return "RAF GENERATED " + today_sce() + " : ";
   }

   /**
    * Obtenir le nombre d'espaces.
    * @param pNbIndent Le nombre d'indentations.
    * @return La chaîne d'espacement.
    */
   public String spaces_sce (Integer pNbIndent) {
      String vReturn = "";
      if (pNbIndent != null) {
         String vOneIndent = "  ";
         for (int i = 0; i < pNbIndent; i++) {
            vReturn = vReturn + vOneIndent;
         }
      }
      return vReturn;
   }

}
