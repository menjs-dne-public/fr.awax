package fr.awax.acceleo.common.only_export;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.util.DirectoryUtils;

public class ScePathInstance extends SceAbstractInstance {

   public ScePathInstance() {
      super();
   }

   private ScePathInstance(String pPluginId) {
      super(pPluginId);
   }

   private static Map<String, ScePathInstance> _MapInstances = new LinkedHashMap<>();

   public static ScePathInstance getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }

   public static ScePathInstance getInstance (String pPluginId) {
      ScePathInstance instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new ScePathInstance(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }

      return instance;
   }

   @Override
   public void clear () {
      // RAS
   }

   public String convertPackage2Path_sce (String pPackageName, String pFileName) {
      String vReturn;

      if (pPackageName == null) {
         throw new IllegalArgumentException("Paramètre invalide : pPackageName ne peut pas être 'null'");
      }
      if (pFileName == null) {
         throw new IllegalArgumentException("Paramètre invalide : pFileName ne peut pas être 'null'");
      }

      String path = pPackageName.replace('.', '/');
      while (path.endsWith("/")) {
         path = path.substring(0, path.length() - "/".length());
      }

      String fileNameWithoutExtension = DirectoryUtils.getFileWithoutExtension(pFileName);
      int rangPartieGauche = pFileName.indexOf(fileNameWithoutExtension);
      String partieGaucheFileName;
      if (rangPartieGauche > 0) {
         partieGaucheFileName = pFileName.substring(0, rangPartieGauche);
         partieGaucheFileName = partieGaucheFileName.replace('.', '/');
      }
      else {
         partieGaucheFileName = "";
      }
      String fileName = partieGaucheFileName + "/" + fileNameWithoutExtension.replace('.', '/') + DirectoryUtils.getFileExtension(pFileName);
      // Uniformiser le path
      vReturn = DirectoryUtils.uniformPath(path + "/" + fileName);
      return vReturn;
   }

   /**
    * Obtenir le chemin en absolu.
    * @param pPath Le répertoire.
    * @param pIsFullPath (Optionnel - default=false) 'true' donne le répertoire en absolu complet - 'false' en partiel.
    * @return La répertoire désiré.
    */
   public String doAbsolutePath (String pTargetAbsolutePath, String pPath, Boolean... pIsFullPath) {
      String vReturn;

      if ((pIsFullPath != null) && (pIsFullPath.length == 1)) {
         // SI génération en absolu "full"
         if (pIsFullPath[0]) {
            String vPathRoot = getAllProp().propCommon().path_root_generation.getValue().toString();
            if (isRelativePath(vPathRoot)) {
               vReturn = DirectoryUtils.getAbsolutePath(pTargetAbsolutePath + "/" + pPath);
            }
            else {
               vReturn = DirectoryUtils.getAbsolutePath(vPathRoot + "/" + pPath);
            }
         }
         // SI génération en absolu "partiel"
         else {
            vReturn = DirectoryUtils.getAbsolutePath(pPath);
         }
      }
      else {
         boolean isFullPath = !isRelativePath(pPath);
         if (isFullPath) {
            String vPathRoot = getAllProp().propCommon().path_root_generation.getValue().toString();
            if (isRelativePath(vPathRoot)) {
               vReturn = DirectoryUtils.getAbsolutePath(pTargetAbsolutePath + "/" + pPath);
            }
            else {
               vReturn = DirectoryUtils.getAbsolutePath(vPathRoot + "/" + pPath);
            }
         }
         // SI génération en absolu "partiel"
         else {
            vReturn = DirectoryUtils.getAbsolutePath(pPath);
         }

      }
      return vReturn;
   }

   /**
    * Obtenir le chemin en relatif.
    * @param pPath Le répertoire.
    * @param pIsFullPath (Optionnel - default=false) 'true' donne le répertoire en absolu complet - 'false' en partiel.
    * @return La répertoire désiré.
    */
   public String doRelativePath (String pTargetAbsolutePath, String pPath) {
      return doAbsolutePath(pTargetAbsolutePath, pPath, false);
   }

   /**
    * Savoir s'il sagit d'un path relatif (commence par "." ou "..").
    * @param pPath Le path à regarder.
    * @return 'true' si c'est un path en relatif.
    */
   private boolean isRelativePath (String pPath) {
      boolean vReturn;
      if (pPath.startsWith(".") || pPath.startsWith("..")) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }
      return vReturn;
   }

   public final String get_path_common_src_sce () {
      String vReturn = getAllProp().propCommon().dir_generation_projet_root.getValue().toString() + "/" +
            getAllProp().propCommon().path_common_src.getValue().toString() + "/";
      vReturn = DirectoryUtils.uniformPath(vReturn);
      return doAbsolutePath(null, vReturn);
   }

}
