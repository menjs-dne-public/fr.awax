package fr.awax.acceleo.common.data;

/**
 * Permet de réaliser des paramètres "Out" et "In/Out".
 * Contrairement au type 'Boolean' qui est immuable.
 * 
 * @author MinEdu
 */
public class XBoolean {
   private Boolean _Value = null;

   public XBoolean() {

   }

   public XBoolean(Boolean pValue) {
      super();
      _Value = pValue;
   }

   public Boolean getValue () {
      return _Value;
   }

   public void setValue (Boolean pValue) {
      _Value = pValue;
   }

   @Override
   public int hashCode () {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((_Value == null) ? 0 : _Value.hashCode());
      return result;
   }

   @Override
   public boolean equals (Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      XBoolean other = (XBoolean) obj;
      if (_Value == null) {
         if (other._Value != null)
            return false;
      }
      else if (!_Value.equals(other._Value))
         return false;
      return true;
   }

   @Override
   public String toString () {
      return "" + _Value;
   }

}
