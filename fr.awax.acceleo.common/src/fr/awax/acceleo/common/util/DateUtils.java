package fr.awax.acceleo.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Classe utilitaire pour manipuler des dates.
 * 
 * @author MinEdu
 */
/**
 * @author egarel
 *
 */
abstract public class DateUtils {
   public static final String cTypeXjdbc_Date = "Date";
   public static final String cTypeXjdbc_DateTime = "DateTime";

   /** Appel de la méthode permettant d'obtenir la date du jour. */
   public static final String cObtenirDateTimeJour = "DateUtils.getNowDate()";
   public static final String cObtenirDateJour = "DateUtils.getDateWithoutTime(DateUtils.getNowDate())";

   /** La date système de référence virtuelle en cas de contrôle du temps pour les tests. */
   private Date _DateReferenceVirtuelle = null;
   /** La date système de référence réelle en cas de contrôle du temps pour les tests. */
   private Date _DateReferenceReelle = null;
   /** 'true' pour que le temps avance à partir de pNow - 'false' le temps reste à pNow. */
   private boolean _IsTimeAdvanced = true;

   /** Définir l'instance pour la production. */
   private static DateUtils _Instance = new DateUtils() {
      @Override
      public void setDateReference (Date pNow) {
         throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
      }

      @Override
      public void unsetDateReference () {
         throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
      }
   };

   protected static DateUtils getInstance () {
      return _Instance;
   }

   protected static void setInstance (DateUtils pInstance) {
      _Instance = pInstance;
   }

   /**
    * Affecter la date système.
    * 
    * @param pNow
    *           La date de départ.
    */
   abstract public void setDateReference (Date pNow);

   /**
    * Cette méthode est utile uniquement pour les tests afin de maîtriser la date système.
    * @param pNow La date de départ.
    * @param pIsTimeAdvanced 'true' pour que le temps avance à partir de pNow - 'false' le temps reste à pNow.
    */
   public static void setDateReference4Tests (Date pNow, boolean pIsTimeAdvanced) {
      // Mémoriser la date de référence virtuelle
      setDateReferenceVirtuelle(pNow);
      // Mémoriser la date de référence réelle
      setDateReferenceReelle(new Date());
      // Mémoriser le désire de faire avancer le temps (ou pas)
      setIsTimeAdvanced(pIsTimeAdvanced);
   }

   /**
    * Permet d'annuler la date de référence pour les tests.
    */
   abstract public void unsetDateReference ();

   @SuppressWarnings("deprecation")
   protected void unsetDateReference_root () {
      setDateReferenceVirtuelle(null);
      setDateReferenceReelle(null);
   }

   private Date getDateReferenceVirtuelle () {
      return getInstance()._DateReferenceVirtuelle;
   }

   /**
    * Affecter la date du jour pour l'application.
    * @param pDateReferenceVirtuelle La date pour simuler un déplacement temporelle.
    * @see @getDate()
    * @see unsetDateReference_root()
    */
   private static void setDateReferenceVirtuelle (Date pDateReferenceVirtuelle) {
      getInstance()._DateReferenceVirtuelle = pDateReferenceVirtuelle;
   }

   private Date getDateReferenceReelle () {
      return getInstance()._DateReferenceReelle;
   }

   private static void setDateReferenceReelle (Date pDateReferenceReelle) {
      getInstance()._DateReferenceReelle = pDateReferenceReelle;
   }

   private static boolean isTimeAdvanced () {
      return getInstance()._IsTimeAdvanced;
   }

   private static void setIsTimeAdvanced (boolean pIsTimeAdvanced) {
      getInstance()._IsTimeAdvanced = pIsTimeAdvanced;
   }

   /**
    * Obtenir la date système.
    * 
    * @return La date sous forme d'une instance de 'Date'.
    */
   private Date getSystemDate () {
      Date vDate;
      // Si pas de contrôle du temps (pour l'instant, mais peut-être un fichier Properties)
      if (_DateReferenceVirtuelle == null) {
         // Date système normale
         vDate = new Date();
      }
      // Si contrôle du temps
      else {
         vDate = controlDuTemps();
      }

      return vDate;
   }

   /**
    * Obtenir la date système.
    * 
    * @return La date sous forme d'une instance de 'Calendar'.
    */
   private Calendar getSystemCalendar () {
      Calendar vCalendar = Calendar.getInstance();
      vCalendar.setTime(getSystemDate());
      return vCalendar;
   }

   /**
    * Permet d'obtenir la date en delta par rapport à la date de référence.
    * 
    * @return La date.
    */
   private Date controlDuTemps () {
      Date vDate;
      // Si l'on désire faire avancer le temps
      if (_IsTimeAdvanced) {
         final Date vDateActuelle = new Date();
         // Obtenir le delta depuis la date de référence
         long vDelta = vDateActuelle.getTime() - _DateReferenceReelle.getTime();
         // Définir la nouvelle date simulée à partir de la date de référence - cf. #setDateReference()
         vDate = new Date(_DateReferenceVirtuelle.getTime() + vDelta);
      }
      // Si le temps est fixe (ex : pour éviter les décalages de secondes lors de la génération)
      else {
         // Pas de delta depuis la date de référence
         long vDelta = 0L;
         // Définir la nouvelle date simulée à partir de la date de référence - cf. #setDateReference()
         vDate = new Date(_DateReferenceVirtuelle.getTime() + vDelta);
      }

      return vDate;
   }

   ////////////////////////////////////////////////

   /**
    * Obtenir la date système.
    * 
    * @return La date du jour en java.util.Date.
    */
   public static Date getNowDate () {
      return getInstance().getSystemDate();
   }

   /**
    * Obtenir la date système.
    * 
    * @return La date du jour en java.util.Calendar.
    */
   public static Calendar getNowCalendar () {
      return getInstance().getSystemCalendar();
   }

   /**
    * Obtenir une instance de 'Calendar' à partir de l'instance de 'Date'
    * spécifiée.
    * 
    * @param pDate
    *           La date.
    * @return L'instance désirée.
    *         Example
    *         // Date --> Calendar
    *         Calendar vCalendar = DateUtils.getCalendar(new java.util.Date());
    */
   public static Calendar getCalendar (Date pDate) {
      Calendar vReturn;

      if (pDate == null) {
         vReturn = null;
      }
      else {
         vReturn = GregorianCalendar.getInstance(Locale.FRANCE);
         // Date --> Calendar
         vReturn.setTime(pDate);
      }

      return vReturn;
   }

   /**
    * Obtenir la date (sans l'heure : 0h0m0s0ms).
    * 
    * @param pDayOfMonth
    *           Le jour du mois.
    * @param pMonth
    *           Le mois dans l'année.
    * @param pYear
    *           L'année.
    * @return L'instance déisrée.
    * @example
    *          // Définir le 03/07/2015
    *          Date vTheDay = DateUtils.getDateSpecified(3, 7, 2015);
    */
   public static Date getDateSpecified (int pDayOfMonth, int pMonth, int pYear) {
      return getDateTimeSpecified(pDayOfMonth, pMonth, pYear, 0, 0, 0, 0);
   }

   /**
    * Obtenir les heures (sans la date : 1er janvier 1970).
    * 
    * @param pHour
    *           Les heures.
    * @param pMinute
    *           Les minutes.
    * @param pSeconde
    *           Les secondes.
    * @param pMillis
    *           Les millisecondes.
    * @return L'instance désirée.
    */
   public static Date getTimeSpecified (int pHour, int pMinute, int pSeconde, int pMillis) {
      return getDateTimeSpecified(1, 0, 2000, pHour, pMinute, pSeconde, pMillis);
   }

   /**
    * Obtenir la date et l'heure spécifée sous la forme d'un java.util.Date.
    * 
    * @param pDayOfMonth
    *           Le jour du mois.
    * @param pMonth
    *           Le mois dans l'année.
    * @param pYear
    *           L'année.
    * @param pHour
    *           L'heure (format 24H).
    * @param pMinute
    *           Les minutes dans l'heure.
    * @param pSeconde
    *           Les secondes dans la minute.
    * @param pMillis
    *           Les millisecondes dans la seconde.
    * @return L'instance désirée.
    * @example
    *          // Obtenir 06 juillet 2015 à 07h37m53 et 235millis
    *          Date vDateIn1 = DateUtils.getDateTimeSpecified(6, 7, 2015, 7, 37,
    *          53, 253);
    */
   public static Date getDateTimeSpecified (int pDayOfMonth, int pMonth, int pYear, int pHour, int pMinute, int pSeconde, int pMillis) {

      Date vDateNow = new Date();
      Calendar vCalendarIn1 = GregorianCalendar.getInstance(Locale.FRANCE);
      vCalendarIn1.setTime(vDateNow);
      // Définir le 06/07/2015 à 07h37m53 et 235millis
      vCalendarIn1.set(Calendar.YEAR, pYear);
      vCalendarIn1.set(Calendar.MONTH, pMonth - 1);  // -1 car commence à "0"
      vCalendarIn1.set(Calendar.DAY_OF_MONTH, pDayOfMonth);
      vCalendarIn1.set(Calendar.HOUR_OF_DAY, pHour);
      vCalendarIn1.set(Calendar.MINUTE, pMinute);
      vCalendarIn1.set(Calendar.SECOND, pSeconde);
      vCalendarIn1.set(Calendar.MILLISECOND, pMillis);
      // Calendar --> Date
      Date vDateIn1 = vCalendarIn1.getTime();

      return vDateIn1;
   }

   /**
    * Convertir un timestamp en format ISO : "dd-MM-yyyy hh:mm:ss.SS".
    * 
    * @param pDateTime
    *           Le timestamp à convertir.
    * @return La chaîne au format désiré.
    * @example
    *          // Si la colonne 'cDateStart' est en 'Varchar2(24)" dans la table
    *          _PreparedStatement_findByInterval.setString(cDateStart,
    *          DateUtils.toFmtIsoDateTime(pDateStart));
    */
   public static String toFmtIsoDateTime (Date pDateTime) {
      String vReturn;
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SS");
      vReturn = simpleDateFormat.format(pDateTime);
      return vReturn;
   }

   /**
    * Convertir une date en format ISO : "dd-MM-yyyy".
    * 
    * @param pDate
    *           La date à convertir.
    * @return La chaîne au format désiré.
    *         // Si la colonne 'cDateStart' est en 'Varchar2(10)" dans la table
    *         _PreparedStatement_findByInterval.setString(cDateStart,
    *         DateUtils.toFmtIsoDate(pDateStart));
    */
   public static String toFmtIsoDate (Date pDate) {
      String vReturn;
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
      vReturn = simpleDateFormat.format(pDate);
      return vReturn;
   }

   /**
    * Obtenir la date en uniformisant la date au 1er janvier 1970.
    * 
    * @param pDate
    *           Le date et l'heure d'origine (ex : "2015-07-01 13:53:21:990").
    * @return La date tronquée avec le jour, mois et année à 0 (ex :
    *         "1970-01-01 13:53:21:990").
    */
   public static Date getTimeWithoutDate (Date pDate) {
      Date vReturn;

      // Si pas de date
      if (pDate == null) {
         vReturn = null;
      }
      else {
         // Date --> Calendar
         Calendar vCalendar = GregorianCalendar.getInstance(Locale.FRANCE);
         vCalendar.setTime(pDate);
         // Mettre la partie 'Date' à zéro
         vCalendar.set(Calendar.YEAR, 1970);
         vCalendar.set(Calendar.MONTH, 0);
         vCalendar.set(Calendar.DAY_OF_WEEK, 1);
         vCalendar.set(Calendar.DAY_OF_MONTH, 1);
         // Calendar --> Time
         vReturn = vCalendar.getTime();
      }

      return vReturn;
   }

   /**
    * Obtenir la date en uniformisant les heures à "00:00:00.0".
    * 
    * @param pDate
    *           Le date et l'heure d'origine (ex : "2015-07-01 13:53:21:990").
    * @return La date tronquée avec l'heure et les milisecondes à 0 (ex :
    *         "2015-07-01 00:00:00.000").
    */
   public static Date getDateWithoutTime (Date pDate) {
      Date vReturn;

      // Si pas de date
      if (pDate == null) {
         vReturn = null;
      }
      else {
         // Date --> Calendar
         Calendar vCalendar = GregorianCalendar.getInstance(Locale.FRANCE);
         vCalendar.setTime(pDate);
         // Mettre la partie 'Time' à zéro
         vCalendar.set(Calendar.HOUR_OF_DAY, 0);
         vCalendar.set(Calendar.MINUTE, 0);
         vCalendar.set(Calendar.SECOND, 0);
         vCalendar.set(Calendar.MILLISECOND, 0);
         // Calendar --> Time
         vReturn = vCalendar.getTime();
      }

      return vReturn;
   }

   /**
    * Obtenir la date avec les heures de début de journée (00h00m00s).
    * 
    * @param pDate
    *           La date d'origine.
    * @return La date désirée.
    */
   public static Date getDateWithStartOfDay (Date pDate) {
      // Date --> Calendar
      Calendar vDateStart = DateUtils.getCalendar(pDate);
      // Affecter le jour de début à 00h00m00s
      vDateStart.set(Calendar.HOUR_OF_DAY, 0);
      vDateStart.set(Calendar.HOUR, 0);
      vDateStart.set(Calendar.AM_PM, Calendar.AM);
      vDateStart.set(Calendar.MINUTE, 0);
      vDateStart.set(Calendar.SECOND, 0);
      vDateStart.set(Calendar.MILLISECOND, 0);

      return vDateStart.getTime();
   }

   /**
    * Obtenir la date avec les heures de fin de journée (23h59m59s).
    * 
    * @param pDate
    *           La date d'origine.
    * @return La date désirée.
    */
   public static Date getDateWithEndOfDay (Date pDate) {
      // Date --> Calendar
      Calendar vDateEnd = DateUtils.getCalendar(pDate);
      // Affecter le jour de fin   à 23h59m59s
      vDateEnd.set(Calendar.HOUR_OF_DAY, 23);
      vDateEnd.set(Calendar.HOUR, 11);
      vDateEnd.set(Calendar.AM_PM, Calendar.PM);
      vDateEnd.set(Calendar.MINUTE, 59);
      vDateEnd.set(Calendar.SECOND, 59);
      vDateEnd.set(Calendar.MILLISECOND, 999);

      return vDateEnd.getTime();
   }

   /**
    * Obtenir la date (sans l'heure) formatée suivant le format "dd/MM/yyyy".
    * 
    * @param Date
    *           pDate La date à formatée.
    * @return La date formatée.
    */
   public static String getDateFormat (Date pDate) {
      String vReturn;

      if (pDate == null) {
         vReturn = null;
      }
      else {
         SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
         vReturn = simpleDateFormat.format(pDate);
      }

      return vReturn;
   }

   /**
    * Obtenir l'heure (sans la date) formatée suivant le format "HH:mm:ss".
    * 
    * @param Date
    *           pDate La date à formatée.
    * @return La date formatée.
    */
   public static String getTimeFormat (Date pDate) {
      String vReturn;

      if (pDate == null) {
         vReturn = null;
      }
      else {
         SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
         vReturn = simpleDateFormat.format(pDate);
      }

      return vReturn;
   }

   /**
    * Obtenir la date avec l'heure formatée suivant le format
    * "dd/MM/yyyy hh:mm:ss".
    * 
    * @param Date
    *           pDate La date à formatée.
    * @return La date formatée.
    */
   public static String getDateTimeFormat (Date pDate) {
      String vReturn;

      if (pDate == null) {
         vReturn = null;
      }
      else {
         SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
         vReturn = simpleDateFormat.format(pDate);
      }

      return vReturn;
   }

   /**
    * Obtenir la date avec l'heure formatée en français (ex : "dimanche 12
    * juillert 2015 11 h 43 CET).
    * 
    * @param Date
    *           pDate La date à formatée.
    * @return La date formatée.
    */
   public static String getFullDateTimeFormat (Date pDate) {
      String vReturn;

      if (pDate == null) {
         vReturn = null;
      }
      else {
         DateFormat fullDateFormat = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);
         vReturn = fullDateFormat.format(pDate);
      }

      return vReturn;
   }

   /**
    * Obtenir un Calendar avec l'heure uniquement (utilise pour comparer les
    * heures).
    * 
    * @param pDate
    *           La date à convertir.
    * @return L'instance désirée.
    * @example
    *          // Obtenir 01 juillet 2015 à 10h37m53 et 235millis
    *          Date vDate1 = DateUtils.getDateTimeSpecified(1, 7, 2015, 10, 37,
    *          53, 253);
    *          Calendar vCalendar1 = DateUtils.getCalendar(vDate1);
    *          // Obtenir 02 juillet 2015 à 07h37m53 et 235millis
    *          Date vDate2 = DateUtils.getDateTimeSpecified(2, 7, 2015, 7, 37,
    *          53, 253);
    *          Calendar vCalendar2 = DateUtils.getCalendar(vDate2);
    * 
    *          Calendar vTimeOnly1 = DateUtils.getCalendarTimeOnly(vDate1);
    *          Calendar vTimeOnly2 = DateUtils.getCalendarTimeOnly(vDate2);
    * 
    *          // Vérifier que vTimeOnly1 > vTimeOnly2 (en termes d'heures
    *          uniquement)
    *          Assert.assertTrue(vTimeOnly1.after(vTimeOnly2));
    *          // Vérifier que vCalendar1 < vCalendar2 (en termes de date+heure)
    *          Assert.assertTrue(vCalendar1.before(vCalendar2));
    */
   public static Calendar getCalendarTimeOnly (Date pDate) {
      Date vDate = DateUtils.getTimeWithoutDate(pDate);
      Calendar vReturn = DateUtils.getCalendar(vDate);

      return vReturn;
   }

   /**
    * Ajouter (ou retrancher) des jour à la date spécifiée.
    * 
    * @param pDate
    *           (In) La date à prendre en compte.
    * @param pNbJours
    *           Le nombre de jours (ex : -1 ==> retrancher un jour).
    */
   public static Date addDay (Date pDate, int pNbJours) {
      Calendar vCalendar = getCalendar(pDate);
      addDay(vCalendar, pNbJours);

      return vCalendar.getTime();
   }

   /**
    * Ajouter (ou retrancher) des jour à la date spécifiée.
    * 
    * @param pCalendar
    *           (In/Out)La date à prendre en compte.
    * @param pNbJours
    *           Le nombre de jours (ex : -1 ==> retrancher un jour).
    */
   public static void addDay (Calendar pCalendar, int pNbJours) {
      // Ajouter le nombre de jours spécifié
      // Equivalent : pCalendar.set(Calendar.DAY_OF_YEAR, pCalendar.get(Calendar.DAY_OF_YEAR) + pNbJours)
      pCalendar.add(Calendar.DAY_OF_MONTH, pNbJours);
   }

   /**
    * Déterminer le nombre calendaire de la période.
    * 
    * @param pDateStart
    *           La date de début.
    * @param pDateEnd
    *           La date de fin (inclus).
    * @return Le nombre de jours calendaire.
    * @example
    *          Date vDateStart = DateUtils.getDateTimeSpecified(01, 07, 2015, 0,
    *          30, 0, 0);
    *          Date vDateEnd = DateUtils.getDateTimeSpecified(02, 07, 2015, 14,
    *          30, 0, 0);
    *          int vNbJoursCalendaire =
    *          DateUtils.getNbJoursCalendaire(vDateStart, vDateEnd);
    *          // --> vNbJoursCalendaire = 2
    */
   public static int getNbJoursCalendaire (Date pDateStart, Date pDateEnd) {
      int vReturn;

      Date vDateStart = getDateWithoutTime(pDateStart);
      Date vDateEnd = getDateWithoutTime(pDateEnd);

      if ((pDateStart == null) || (pDateEnd == null)) {
         throw new IllegalArgumentException("La date de début (=" + pDateStart + ") ou de fin (= " + pDateEnd + ") ne peuvent pas être 'null'");
      }
      else if (vDateStart.equals(vDateEnd)) {
         vReturn = 1;
      }
      else {
         // Calculer le deta (en milisecondes)
         long vDelta = vDateEnd.getTime() - vDateStart.getTime();
         // Milisecondes --> Secondes
         int vNbSecondes = (int) (vDelta / 1000);
         // Secondes --> Jours
         vReturn = (int) (vNbSecondes / (3600 * 24)) + 1;
      }

      return vReturn;
   }

   /**
    * Obtenir une Map des jours de la période par incrément de 1.
    * 
    * @param pDateStart
    *           La date de départ.
    * @param pDateEnd
    *           La date de fin (inclus).
    * @param pIsOrdreChronologique
    *           'true' ordre chronologique - 'false' ordre inverse.
    * @return La Map triée par date désirée avec comme clé la date (ayant les heures à 00:00:00).
    */
   public static Map<Date, Date> getMapDesJoursDeLaPeriode (Date pDateStart, Date pDateEnd, boolean pIsOrdreChronologique) {
      Map<Date, Date> vReturn = new LinkedHashMap<Date, Date>();

      // Date --> Calendar
      Calendar vDateStart = Calendar.getInstance();
      vDateStart.setTime(DateUtils.getDateWithoutTime(pDateStart));
      Calendar vDateEnd = Calendar.getInstance();
      vDateEnd.setTime(DateUtils.getDateWithoutTime(pDateEnd));

      Calendar vDateDebutIntervalle;
      Calendar vDateFinIntervalle;
      // Si classement dans ordre chronologique
      if (pIsOrdreChronologique) {
         vDateDebutIntervalle = vDateStart;
         vDateFinIntervalle = vDateEnd;
      }
      // Si classement dans ordre chronologique inverse
      else {
         vDateDebutIntervalle = vDateEnd;
         vDateFinIntervalle = vDateStart;
      }

      Date vDateWithoutTime;
      Calendar vDateCour = Calendar.getInstance();
      vDateCour.setTime(DateUtils.getDateWithoutTime(vDateDebutIntervalle.getTime()));
      // Initialiser avec les jours de la période
      // Tant que la date courante est inférieure ou égale à la borne supérieure de l'intervalle
      do {
         vDateWithoutTime = DateUtils.getDateWithoutTime(vDateCour.getTime());
         vReturn.put(vDateWithoutTime, vDateCour.getTime());
         // Ajouter (ou retrancher) un jour suivant l'ordre chronologique désiré
         avancerJourIntervalle(pIsOrdreChronologique, vDateCour);
      } while (isDateOk(pIsOrdreChronologique, vDateCour, vDateFinIntervalle));

      return vReturn;
   }

   /**
    * Obtenir une instance de Date à partir d'une 'String' formatée.
    * 
    * @param pDateFmt
    *           La date formatée (ex : "03/09/2015" ou
    *           "03/09/2015 21h03m45s099")
    * @return
    */
   public static Date parseDate (String pDateFmt) {
      Date vReturn;

      // Si pas de valeur
      if ((pDateFmt == null) || ("".equals(pDateFmt))) {
         vReturn = null;
      }
      else {
         int vDay;
         int vMonth;
         int vYear;
         int vHour = 0;
         int vMinute = 0;
         int vSeconde = 0;
         int vMillis = 0;
         // Obtenir le nombre de caractère
         int vLength = pDateFmt.length();
         int vRgStart = 0;
         // Si format "03/09/2015"
         if (vLength == 10) {
            vDay = Integer.parseInt(pDateFmt.substring(0, "JJ".length()));
            vRgStart = "JJ/".length();
            vMonth = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "MM".length()));
            vRgStart = "JJ/MM/".length();
            vYear = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "AAAA".length()));
            vReturn = DateUtils.getDateSpecified(vDay, vMonth, vYear);
         }
         // Si format "20150309_210335"
         else if (vLength == 15) {
            vYear = Integer.parseInt(pDateFmt.substring(0, "AAAA".length()));
            vRgStart = "AAAA".length();
            vMonth = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "MM".length()));
            vRgStart = "AAAAMM".length();
            vDay = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "JJ".length()));
            vRgStart = "AAAAMMJJ_".length();
            vHour = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "hh".length()));
            vRgStart = "AAAAMMJJ_hh".length();
            vMinute = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "mm".length()));
            vRgStart = "AAAAMMJJ_hhmm".length();
            vSeconde = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "ss".length()));
            vReturn = DateUtils.getDateTimeSpecified(vDay, vMonth, vYear, vHour, vMinute, vSeconde, 0);
         }
         // Si format "03/09/2015 21h03m35s099"
         else if (vLength == 23) {
            vDay = Integer.parseInt(pDateFmt.substring(0, "JJ".length()));
            vRgStart = "JJ/".length();
            vMonth = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "MM".length()));
            vRgStart = "JJ/MM/".length();
            vYear = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "AAAA".length()));
            vRgStart = "JJ/MM/AAAA ".length();
            vHour = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "HH".length()));
            vRgStart = "JJ/MM/AAAA HHh".length();
            vMinute = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "MM".length()));
            vRgStart = "JJ/MM/AAAA HHhMMm".length();
            vSeconde = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "SS".length()));
            vRgStart = "JJ/MM/AAAA HHhMMmSSs".length();
            vMillis = Integer.parseInt(pDateFmt.substring(vRgStart, vRgStart + "MMM".length()));
            vReturn = DateUtils.getDateTimeSpecified(vDay, vMonth, vYear, vHour, vMinute, vSeconde, vMillis);
         }
         else {
            try {
               // Cas format toString() de Date (ex : "Wed Dec 31 00:00:00 CET 2014")
               SimpleDateFormat vDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
//               String dateToStr = vDateFormat.format(new Date());
//               System.out.println("==> " + dateToStr);
               vReturn = vDateFormat.parse(pDateFmt);
            }
            catch (Exception vErr) {
               throw new IllegalArgumentException("Cas non prévu pour pDateFmt=" + pDateFmt + " (mettre par exemple : \"03/09/2015\" ou \"03/09/2015 21h03m35s099\")");
            }
         }
      }

      return vReturn;
   }

   private static void avancerJourIntervalle (boolean pIsOrdreChronologique, Calendar pDateCour) {
      if (pIsOrdreChronologique) {
         pDateCour.set(Calendar.DAY_OF_YEAR, pDateCour.get(Calendar.DAY_OF_YEAR) + 1);
      }
      else {
         pDateCour.set(Calendar.DAY_OF_YEAR, pDateCour.get(Calendar.DAY_OF_YEAR) - 1);
      }
   }

   private static boolean isDateOk (boolean pIsOrdreChronologique, Calendar pDateCour, Calendar pDateFinIntervalle) {
      if (pIsOrdreChronologique) {
         return ((pDateCour.before(pDateFinIntervalle)) || (pDateCour.equals(pDateFinIntervalle)));
      }
      else {
         return ((pDateCour.after(pDateFinIntervalle)) || (pDateCour.equals(pDateFinIntervalle)));
      }
   }

   /**
    * Obtenir le jour de la semaine.
    * @param pDate L'instance représentant la date courante.
    * @return Le No correspondant au jour de la semaine (ex : 1 = lundi, ... 7 = dimanche).
    */
   public static int getNumberDayOfWeek (Date pDate) {
      // Convertir Date --> Calendar
      Calendar vCalendar = getCalendar(pDate);
      return getNumberDayOfWeek(vCalendar);
   }

   /**
    * Obtenir le jour de la semaine.
    * @param pCalendar L'instance représentant la date courante.
    * @return Le No correspondant au jour (ex : 1 = lundi, ... 7 = dimanche).
    */
   public static int getNumberDayOfWeek (Calendar pCalendar) {
      int vReturn;
      vReturn = pCalendar.get(Calendar.DAY_OF_WEEK);

      // Recalage, car de base : 1 = dimanche, 2 = lundi, ...
      vReturn = vReturn - 1;
      if (vReturn == 0) {
         vReturn = 7;
      }

      return vReturn;
   }

   /**
    * Savoir si la date est un jour ouvré : du lundi au vendredi (inclus).
    * @param pDate La date à regarder.
    * @return 'true' si c'est un jour ouvré.
    */
   public static boolean isJourOuvre (Date pDate) {
      boolean vReturn;
      // Obtenir le No correspondant au jour (ex : 1 = lundi, ... 7 = dimanche)
      int vNumberDayOfWeek = DateUtils.getNumberDayOfWeek(pDate);
      // Si jour compris entre lundi et vendredi (inclus)
      if ((vNumberDayOfWeek >= 1) && (vNumberDayOfWeek <= 5)) {
         vReturn = true;
      }
      // Sinon : samedi ou dimanche
      else {
         vReturn = false;
      }
      return vReturn;
   }

   /**
    * Obtenir le jour ouvré précédent la date spécifiée.
    * @param pDate La date de départ.
    * @return La date du jour ouvré (entre lundi et vendredi) précédent.
    */
   public static Date getDatePreviousJourOuvre (Date pDate) {
      Date vReturn = null;

      Date vDatePossible = pDate;
      // Obtenir le jour de la semaine
      do {
         // Retrancher 1J
         vDatePossible = DateUtils.addDay(vDatePossible, -1);
         // Si c'est un jour ouvré
         if (isJourOuvre(vDatePossible)) {
            vReturn = vDatePossible;
         }
      } while (vReturn == null);

      return vReturn;
   }

   /**
    * Obtenir le jour ouvré suivant la date spécifiée.
    * @param pDate La date de départ.
    * @return La date du jour ouvré (entre lundi et vendredi) suivant.
    */
   public static Date getDateNextJourOuvre (Date pDate) {
      Date vReturn = null;

      Date vDatePossible = pDate;
      // Obtenir le jour de la semaine
      do {
         // Ajouter 1J
         vDatePossible = DateUtils.addDay(vDatePossible, 1);
         // Si c'est un jour ouvré
         if (isJourOuvre(vDatePossible)) {
            vReturn = vDatePossible;
         }
      } while (vReturn == null);

      return vReturn;
   }

   /**
    * Convertir en chaîne de caractère une Date : Date --> String
    * @param pDate La date.
    * @return La chaîne désirée.
    * @see #fromText(String)
    */
   public static String toText (Date pDate) {
      return toText(pDate, MasqueEnum.JJ_MM_AAAA_wwhxxmyyszzz);
   }

   public static String toText (Date pDate, MasqueEnum pMasqueEnum) {
      String vReturn;

      // Obtenir l'instance de Calendar associée
      Calendar vCalendar = getCalendar(pDate);

      int vYear = vCalendar.get(Calendar.YEAR);
      int vMonth = vCalendar.get(Calendar.MONTH) + 1; // car commence à "0"
      int vDay = vCalendar.get(Calendar.DAY_OF_MONTH);

      int vHour = vCalendar.get(Calendar.HOUR);
      int vMinute = vCalendar.get(Calendar.MINUTE);
      int vSecond = vCalendar.get(Calendar.SECOND);
      int vMilliSecond = vCalendar.get(Calendar.MILLISECOND);

      if (pMasqueEnum == MasqueEnum.JJ_MM_AAAA_wwhxxmyyszzz) {
         // Mettre au format, par exemple : 03/09/2015 21h03m45s099
         vReturn = String.format("%02d", vDay)
               + "/" + String.format("%02d", vMonth)
               + "/" + String.format("%04d", vYear)
               + " "
               + String.format("%02d", vHour) + "h"
               + String.format("%02d", vMinute) + "m"
               + String.format("%02d", vSecond) + "s"
               + String.format("%03d", vMilliSecond);
      }
      else if (pMasqueEnum == MasqueEnum.AAAAMMJJ_hhmmss) {
         // Mettre au format, par exemple : 03/09/2015 21h03m45s099
         vReturn = String.format("%04d", vYear)
               + String.format("%02d", vMonth)
               + String.format("%02d", vDay)
               + "_"
               + String.format("%02d", vHour)
               + String.format("%02d", vMinute)
               + String.format("%02d", vSecond);
      }
      else {
         throw new IllegalArgumentException("Cas non prévu pour : " + pMasqueEnum);
      }

      return vReturn;
   }

   /**
    * Convertir une chaîne de caractères formatée en instance de 'Date' : "String" --> "Date".
    * @param pStringFmt La chaîne de caractères formatées.
    * @return L'instance de 'Date' associée.
    */
   public static Date fromText (String pStringFmt) {
      return parseDate(pStringFmt);
   }

   public enum MasqueEnum {
      AAAAMMJJ_hhmmss,
      JJ_MM_AAAA_wwhxxmyyszzz
   }
}
