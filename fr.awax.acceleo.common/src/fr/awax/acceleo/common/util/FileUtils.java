package fr.awax.acceleo.common.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtils {
   /**
    * Unzip a file given its url into a folder
    * 
    * @param zipURL
    *           URL of the zip file
    * @param folder
    *           Folder where to unzip
    * @throws FileNotFoundException
    * @throws IOException
    */
   public static void unzip (URL zipURL, File folder) throws FileNotFoundException, IOException {
      // Si on a une URL
      if (zipURL != null) {
         ZipInputStream zis = new ZipInputStream(zipURL.openStream());

         ZipEntry ze = null;
         try {
            while ((ze = zis.getNextEntry()) != null) {

               File f = new File(folder.getCanonicalPath(), ze.getName());
               if (ze.isDirectory()) {
                  f.mkdirs();
                  continue;
               }
               f.getParentFile().mkdirs();
               OutputStream fos = new BufferedOutputStream(new FileOutputStream(f));
               try {
                  try {
                     final byte[] buf = new byte[8192];
                     int bytesRead;
                     while (-1 != (bytesRead = zis.read(buf)))
                        fos.write(buf, 0, bytesRead);
                  }
                  finally {
                     fos.close();
                  }
               }
               catch (final IOException ioe) {
                  f.delete();
                  throw ioe;
               }
            }
         }
         finally {
            zis.close();
         }
      }
   }

   /**
    * Lire le fichier et mettre chaque ligne dans une liste de String.
    * @param pFile Le fichier
    * @return La liste représentant le fichier.
    * @throws IOException Si une erreur se produit.
    */
   public static List<String> readFileToList (File pFile) throws IOException {
      List<String> vReturn = new ArrayList<String>();
      // Obtenir le chemin en absolu
      String vFileName = pFile.getAbsolutePath();
      try {
         BufferedReader vOut = new BufferedReader(new InputStreamReader(new FileInputStream(vFileName)));

         String vLineCour;
         // Lire le fichier ligne par ligne
         while ((vLineCour = vOut.readLine()) != null) {
            // Ajouter à la liste
            vReturn.add(vLineCour);
         }

         // Fermer le flux
         vOut.close();
      }
      catch (Exception vErr) {
         throw new IOException("Problème lors de la lecture du fichier : " + vFileName, vErr);
      }
      return vReturn;
   }

   /**
    * Lire le fichier et mettre chaque ligne dans un tableau de String.
    * @param pFile Le fichier
    * @return Le tableau représentant le fichier.
    * @throws IOException Si une erreur se produit.
    */
   public static String[] readFileToArrayString (File pFile) throws IOException {
      String[] vReturn;
      List<String> vListFile = readFileToList(pFile);
      // List<String> --> String[]
      vReturn = vListFile.toArray(new String[vListFile.size()]);

      return vReturn;
   }

   /**
    * Permet de supprimer une succession de lignes vides dans les fichiers se trouvant dans le répertoire (récursif).
    * @param pAbsolutePath Le répertoire d'entrée.
    * @param pNbOccurs Le nombre de lignes vides autorisés.
    * @throws IOException Si une Erreur se produit.
    */
   public static void deleteEmptyLines (String pAbsolutePath, int pNbOccurs, List<String> pListExtension) throws IOException {
      File vFile = new File(pAbsolutePath);
      // Traiter récursivement tous les fichiers
      computeAllFiles(vFile.listFiles(), pNbOccurs, pListExtension);
   }

   private static void computeAllFiles (File[] pListFiles, int pNbOccurs, List<String> pListExtension) throws IOException {
      if (pListFiles != null) {
         for (File file : pListFiles) {
            // SI c'est un répertoire
            if (file.isDirectory()) {
               computeAllFiles(file.listFiles(), pNbOccurs, pListExtension);
            }
            else {
               // SI l'extension correspond
               if (isExtensionFile(file, pListExtension)) {
                  computeFile(file, pNbOccurs);
               }
            }
         }
      }
   }

   /**
    * Savoir si l'extension du fichier correspond.
    * @param pFile Le fichier a regarder.
    * @param pListExtension Les extensions à prendre en compte.
    * @return 'true' si elle correspond.
    */
   private static boolean isExtensionFile (File pFile, List<String> pListExtension) {
      boolean vReturn = false;

      final String absolutePath = pFile.getAbsolutePath();
      // Parcourir les extensions autorisées
      for (String extension : pListExtension) {
         if (absolutePath.endsWith(extension)) {
            vReturn = true;
            break;
         }
      }
      return vReturn;
   }

   private static void computeFile (File pFile, int pNbOccurs) throws IOException {
      List<String> listLinesOfFile = FileUtils.readFileToList(pFile);

      // Supprimer les lignes vide
      listLinesOfFile = deleteEmptyLines_intern(listLinesOfFile, pNbOccurs);

      // Ouvrir le fichier en écriture
      BufferedWriter vOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pFile)));
      // Parcourir les lignes du fichier épuré des lignes vides
      for (String line : listLinesOfFile) {
         vOut.write(line + "\n");
      }

      // Fermer le flux
      vOut.close();
   }

   static List<String> deleteEmptyLines_intern (List<String> pListLinesOfFile, int pNbOccurs) {
      List<String> vReturn = new ArrayList<>(pListLinesOfFile);
      String lineWithoutSpaces;
      int vCptLines = 0;
      int vCptLinesDeleted = 0;
      int vNbOccursActual = 0;
      boolean vIsLines4Deleted = false;
      // Parcourir les lignes du fichier brut
      for (String line : pListLinesOfFile) {
         lineWithoutSpaces = line.trim();
         // SI c'est ligne vide (uniquement un retour chariot)
         if (lineWithoutSpaces.equals("")) {
            vNbOccursActual++;
            if (vNbOccursActual > pNbOccurs) {
               vIsLines4Deleted = true;
            }
            int vNumLine = vCptLines - vCptLinesDeleted;
            vReturn.set(vNumLine, lineWithoutSpaces);
         }
         // SINON c'est une ligne avec au moins un caractère
         else {
            if (vIsLines4Deleted) {
               int vNbOccursActualMem = vNbOccursActual - 1;
               int vNumLineDeleted;
               vNumLineDeleted = vCptLines - vCptLinesDeleted - vNbOccursActual;
               while (vNbOccursActual > 1) {
                  vNbOccursActual--;
                  if (vReturn.size() > vNumLineDeleted) {
                     String lineDeleted = vReturn.remove(vNumLineDeleted);
                  }
               }
               vCptLinesDeleted = vCptLinesDeleted + vNbOccursActualMem;
            }
            else {
               int vNumLine = vCptLines - vCptLinesDeleted;
               vReturn.set(vNumLine, line);
            }
            // RAZ
            vIsLines4Deleted = false;
            // RAZ, car ligne non vide venant d'être lue
            vNbOccursActual = 0;
         }
         vCptLines++;
      } // FIN for (String line : pListLinesOfFile)

      return vReturn;
   }

}
