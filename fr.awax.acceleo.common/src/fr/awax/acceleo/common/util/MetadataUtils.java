package fr.awax.acceleo.common.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.obeonetwork.dsl.environment.Annotation;
import org.obeonetwork.dsl.environment.MetaData;
import org.obeonetwork.dsl.environment.MetaDataContainer;
import org.obeonetwork.dsl.environment.ObeoDSMObject;

import fr.awax.acceleo.common.norme.StringNorme;

public class MetadataUtils {

   /**
    * Savoir le corps d’une METADATA attaché à 'pObeoDSMObject'.
    * @param pObeoDSMObject L'instance de la syntaxe DSL (ex: une Entity) ayant la METADATA positionnée dessus.
    * @param pMetadataName Le nom de la Metadata.
    * @return Le corps de la METADATA.
    */
   public static String getMetadataBody (final ObeoDSMObject pObeoDSMObject, final String pMetadataName) {
      String vReturn = null;
      if (pObeoDSMObject != null) {
         String vElementName = getName(pObeoDSMObject, "name");
         vReturn = getMetadataBody(pObeoDSMObject, pMetadataName, false, vElementName);
      }
      return vReturn;
   }

   public static String getMetadataBody (final ObeoDSMObject p_ObeoDSMObject, final String p_annotation, final String p_ElementName) {
      return getMetadataBody(p_ObeoDSMObject, p_annotation, false, p_ElementName);
   }

   /**
    * Récupère le corps d'une annotation, récursivement ou non.
    * 
    * @param pObeoDSMObject
    *           l'objet source de la recherche
    * @param pAnnotationName
    *           le titre de l'annotation recherchée
    * @param pRecursive
    *           true si la recherche doit être récursive, false si la recherche doit se faire seulement sur l'objet pObeoDSMObject.
    * @return le corps de l'annotation trouvée, null sinon
    */
   public static String getMetadataBody (final ObeoDSMObject pObeoDSMObject, final String pAnnotationName, final boolean pRecursive, final String pElementName) {
      if (pObeoDSMObject == null) {
         return null;
      }
      final MetaDataContainer v_metadataContainer = pObeoDSMObject.getMetadatas();
      final String v_body = getMetadataBody(v_metadataContainer, pAnnotationName, pElementName);
      if (v_body != null) {
         return v_body;
      }
      if (pRecursive) {
         final EObject v_containerObject = pObeoDSMObject.eContainer();
         if (v_containerObject instanceof ObeoDSMObject) {
            // Appeler le getter - Par ex : pMetadataContainer.getName()
            String v_ElementName = getName(v_containerObject, "name");
            return getMetadataBody((ObeoDSMObject) v_containerObject, pAnnotationName, pRecursive, v_ElementName);
         }
      }
      return null;
   }

   public static String getMetadataBody (final MetaDataContainer p_metadataContainer, final String p_annotation, final String p_ElementName) {
      if (p_metadataContainer != null) {
         for (final MetaData v_m : p_metadataContainer.getMetadatas()) {
            if (v_m instanceof Annotation) {
               final Annotation v_anno = (Annotation) v_m;

               // Si pas de valeur dans la metadata
               if (v_anno.getBody() == null) {
                  // TODO Voir si on autorise de ne pas mettre de valeur sur une annotation
//                  EclipseUiUtils.writeViewErrorLog_warning("L'annotation " + p_annotation + " pour l'élément \"" + p_ElementName + "\" possède une valeur vide (pas de body)");
               }

               // Si OK
               if ((v_anno != null) && (v_anno.getTitle() != null) && (v_anno.getBody() != null) && (v_anno.getTitle().equalsIgnoreCase(p_annotation)) && (v_anno.getBody().trim().isEmpty() == false)) {
                  return v_anno.getBody();
               }
            }
         }
      }
      return null;
   }

   /**
    * Obtenir toutes les METADATA sous la forme d'une chaîne de caractères.
    * @param pMetadataContainer Le conteneur à regarder.
    * @return La chaîne désirée.
    */
   public static List<String> getListAllMetadata (final MetaDataContainer pMetadataContainer) {
      List<String> vListMetadata = new ArrayList<String>();

      if (pMetadataContainer != null) {
         for (final MetaData v_m : pMetadataContainer.getMetadatas()) {
            if (v_m instanceof Annotation) {
               final Annotation v_anno = (Annotation) v_m;
               // Ajouter la METADATA
               vListMetadata.add(v_anno.getTitle());
            }
         }
      }

      return vListMetadata;
   }

   /**
    * Permet de savoir si le titre de l'annotation correspond (ne regarde pas la valeur).
    * @param pMetadataContainer Le conteneur de METADATA.
    * @param pTitle Le titre recherché.
    * @return 'true' si le titre a été trouvé en tant que METADATA.
    */
   public static boolean isMetadataTitle (final ObeoDSMObject pObeoDSMObject, final String pMetadataName) {
      return isMetadataTitle(pObeoDSMObject.getMetadatas(), pMetadataName);
   }

   public static boolean isMetadataTitle (final MetaDataContainer pMetadataContainer, final String ppMetadataName) {
      boolean vReturn = false;

      if (pMetadataContainer != null) {
         for (final MetaData v_m : pMetadataContainer.getMetadatas()) {
            if (v_m instanceof Annotation) {
               final Annotation v_anno = (Annotation) v_m;
               // Si le titre de l'annotation correspond
               if (v_anno.getTitle().equals(ppMetadataName)) {
                  vReturn = true;
                  break;
               }
            }
         }
      }

      return vReturn;
   }

   /**
    * Récupère le corps d'une annotation dans un MetadataContainer.
    * 
    * @param p_metadataContainer
    *           le container de métadonnées
    * @param p_annotation
    *           l'annotation recherchée
    * @return le corps de l'annotation trouvée, null sinon
    */
   public static String getMetadataBody (final MetaDataContainer p_metadataContainer, final String p_annotation) {
      return getMetadataBody(p_metadataContainer, p_annotation, "non spécifié");
   }

   /**
    * Appeler via reflect le "getName()" par exemple.
    * @param pObject L'instance.
    * @param pPropertyName Le nom de la propriété (ex : "name")
    * @return Le résultat de l'appel du getter.
    */
   private static String getName (Object pObject, String pPropertyName) {
      String vReturn;
      final StringNorme vStringNorme = new StringNorme(pPropertyName);
      final String propertyName = vStringNorme.toNormeCamelCase().toString();

      // Nom du getter
      String vGetterName = "get" + propertyName;
      Method vMethod = null;
      try {
         // Obtenir l'instance Method représentant le getter
         vMethod = pObject.getClass().getMethod(vGetterName);
         vMethod.setAccessible(true);
         // Appeler le getter - Ex : vReturn = pObject.getName()
         vReturn = (String) vMethod.invoke(pObject);
      }
      catch (SecurityException vErr) {
         throw new RuntimeException("Problème de sécurité avec la méthode " + vGetterName + " sur l'objet " + pObject, vErr);
      }
      catch (Exception vErr) {
         throw new RuntimeException("Problème pour invoquer la méthode " + vGetterName + " sur l'objet " + pObject, vErr);
      }

      return vReturn;
   }

   public static String getMetadata (ObeoDSMObject pElement, String pName) {
      if (pElement != null
            && pElement.getMetadatas() != null) {
         String name = pName.trim();
         for (MetaData metadata : pElement.getMetadatas().getMetadatas()) {
            if (metadata instanceof Annotation) {
               Annotation annotation = (Annotation) metadata;
               if (name.equals(annotation.getTitle())) {
                  return annotation.getBody();
               }
            }
         }
      }
      return null;
   }
}
