package fr.awax.acceleo.common.util.rcpe;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.IOConsoleOutputStream;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import fr.awax.acceleo.common.engine.EngineHelper;

/**
 * ConsoleView permettant de rediriger la console System.out vers la console Eclipse.
 * @author User1
 */
public final class ConsoleViewHelper {
   private PrintStream _defaultOut;
   private PrintStream _defaultErr;

   public static enum ConsoleType {
      DEFAULT,
      STDERR,
      STDOUT,
      OTHER,
      ERRORS
   }

   /** La Map contenant les instances. */
   private static Map<String, ConsoleViewHelper> _MapInstances = new HashMap<>();

   /**
    * Constructeur par défaut.
    */
   private ConsoleViewHelper() {
      // RAS
   }

   /**
    * L'instance associé au pluginId.
    * @param pPLuginId L'identifiant du plugin qui a été lancé.
    * @return L'instance désirée.
    */
   public static ConsoleViewHelper getInstance (String pPLuginId) {
      ConsoleViewHelper vReturn = _MapInstances.get(pPLuginId);
      // SI pas encore instanciée
      if (vReturn == null) {
         vReturn = new ConsoleViewHelper();
         _MapInstances.put(pPLuginId, vReturn);
      }

      return vReturn;
   }

   /**
    * Permet d'obtenir la console Eclipse à partir de son nom et la créée si elle n'existe pas.
    * @param pName One of the members of the static enum defined in Console
    * @return L'instance désirée.
    * @see #ConsoleType
    */
   private MessageConsole findOrCreateConsole (final String pName) {
      final ConsolePlugin plugin = ConsolePlugin.getDefault();
      final IConsoleManager conMan = plugin.getConsoleManager();
      final IConsole[] existing = conMan.getConsoles();

      for (final IConsole element : existing)
         if (pName.toString().compareTo(element.getName()) == 0)
            return (MessageConsole) element;

      // failed to find existing console, create one:
      final MessageConsole myConsole = new MessageConsole(pName.toString(), null);
      conMan.addConsoles(new IConsole[] { myConsole });

//      try {
//         writeToConsole(myConsole.newMessageStream(), "Initializing: " + name + "\n");
//      }
//      catch (Exception err) {
//         err.printStackTrace();
//      }

      return myConsole;
   }

   /**
    * Permet d'écrire dans la console Eclipse STDOUT.
    * @param pMessage Le message à écrire.
    */
   public void writeStdOut (String pMessage) {
      write(ConsoleType.STDOUT.toString(), pMessage);
   }

   /**
    * Permet d'écrire dans la console Eclipse STDERR.
    * @param pMessage Le message à écrire.
    */
   public void writeStdErr (String pMessage) {
      write(ConsoleType.STDERR.toString(), pMessage);
   }

   private void write (final String pName, String pMessage) {
      MessageConsole myConsole = findOrCreateConsole(pName);
      MessageConsoleStream out = myConsole.newMessageStream();
      writeToConsole(out, pMessage);
//      out.println(pMessage);
   }

   private void writeToConsole (MessageConsoleStream stream, String lineToConsole) {
      final String lineToWrite = lineToConsole;
      Display.getDefault().asyncExec(new Runnable() {
         @Override
         public void run () {
            try {
               if (false == stream.isClosed())
                  stream.write(lineToWrite);
            }
            catch (Exception err) {
               err.printStackTrace();
            }
         };
      });
   }

   /**
    * Return a new output stream for a given console. If the console does
    * not exist it will be created.
    * @param type A console type defined in Console's ConsoleType enum.
    * @return An open stream for writing to.
    */
   private OutputStream getOutputStream (final String type) {
      MessageConsole ib = findOrCreateConsole(type);
      IOConsoleOutputStream mcs = ib.newOutputStream();
      mcs.setActivateOnWrite(true);
      return mcs;
   }

   /**
    * Create consoles for STDERR and STDOUT, redirect all output
    * to the in-application console. Should only be called once.
    * 
    */
   public void init () {
      // Mémoriser
      _defaultOut = System.out;
      _defaultErr = System.err;
      // Rediriger vers l'Eclipse contenant le plug-in (= sous-Eclipse en mode dev)
      MessageConsole outConsole = findOrCreateConsole(ConsoleType.STDOUT.toString());
      IOConsoleOutputStream outStream = outConsole.newOutputStream();
      System.setOut(new PrintStream(outStream));

      MessageConsole errConsole = findOrCreateConsole(ConsoleType.STDERR.toString());
      IOConsoleOutputStream errStream = errConsole.newOutputStream();
      System.setErr(new PrintStream(errStream));
   }

   /**
    * Remettre les out et err avec les valeurs par défaut.
    */
   public void raz () {
      // SI pas encore de init()
      if (_defaultOut == null) {
         // RAS
      }
      else {
         System.setOut(_defaultOut);
         System.setErr(_defaultErr);
      }
   }

   /**
    * Alternate way to bring up the console view. Don't know
    * which one is better / the differences.
    * @param myConsole Console to show, must not be null.
    */
   public void showConsole (IConsole myConsole) {
      IWorkbench workbench = PlatformUI.getWorkbench();
      IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();

      IWorkbenchPage page = window.getActivePage();
      String id = IConsoleConstants.ID_CONSOLE_VIEW;
      IConsoleView view;
      try {
         view = (IConsoleView) page.showView(id);
         view.display(myConsole);
      }
      catch (PartInitException e) {
         e.printStackTrace();
      }
   }

   /**
    * Show the application console. If the given console is null, attempt
    * to find an existing console and use it. If the given console is null
    * and no existing consoles exist, exit without doing anything.
    * 
    * @param myConsole An existing console.
    */
   public static void displayConsole (IConsole myConsole) {

      // try to grab any old console and display it if given null
      if (myConsole == null) {
         final ConsolePlugin plugin = ConsolePlugin.getDefault();
         final IConsoleManager conMan = plugin.getConsoleManager();
         final IConsole[] existing = conMan.getConsoles();

         if (existing.length == 0)
            return;

         for (final IConsole element : existing)
            myConsole = element;
      }

      ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[] { myConsole });
      ConsolePlugin.getDefault().getConsoleManager().showConsoleView(myConsole);
   }

   /**
    * Show the console view with the given ConsoleType. Will not
    * create one if one does not already exist.
    * 
    * @param type Non-null enum of existing console (stdout/err probably safe)
    */
   public static void displayConsole (final String type) {

      final ConsolePlugin plugin = ConsolePlugin.getDefault();
      final IConsoleManager conMan = plugin.getConsoleManager();
      final IConsole[] existing = conMan.getConsoles();

      for (final IConsole element : existing)
         if (type.toString().compareTo(element.getName()) == 0) {
            ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[] { element });
            ConsolePlugin.getDefault().getConsoleManager().showConsoleView(element);
            return;
         }
   }

}
