package fr.awax.acceleo.common.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe utilitaire pour les Exceptions.
 *
 * ATTENTION, cette classe ne doit être appelée QUE par les tests junit, <br />
 * L'utiliser dans le code métier dégradera les performances.
 * @author MinEdu
 */
public class ExceptionUtils {
   /**
    * Il s'agit d'un accès statique vers la méthode {@link Throwable#getStackTraceElement()}.
    */
   private static Method m;

   static {
      try {
         m = Throwable.class.getDeclaredMethod("getStackTraceElement", int.class);
         m.setAccessible(true);
      }
      catch (Exception e) {
         e.printStackTrace();
      }
   }

   /**
    * Indique quel est nom de la méthode du stackTrace en cours.
    * 
    * @param depth
    *           La profondeur de recherche dans le stack trace.
    * @return le nom de la méthode
    */
   public static String getMethodName (final int depth) {
      try {
         StackTraceElement element = (StackTraceElement) m.invoke(new Throwable(), depth + 1);
         return element.getMethodName();
      }
      catch (Exception e) {
         e.printStackTrace();
         return null;
      }
   }

   /**
    * Obtenir la pile d'appels sous la forme d'une chaine de caractères.
    * 
    * @return La pile d'appels désirée.
    */
   public static String getStackTraceAsString () {
      return getStackTraceAsString(null);
   }

   /**
    * Obtenir la pile d'appels sous la forme d'une chaine de caractères en
    * générant une exception a partir de cet appel.
    * 
    * @param pMessage
    *           Le message.
    * @return La pile d'appels avec le message.
    */
   public static String getStackTraceAsString (String pMessage) {
      String vReturn;
      try {
         throw new RuntimeException("INFORMATION : Erreur provoquée pour voir la pile d'appels");
      }
      catch (Exception vErr) {
         vReturn = getStackTraceAsString(pMessage, vErr);
      }
      return vReturn;
   }

   /**
    * Obtenir la pile d'appels sous la forme d'une chaine de caractères.
    * 
    * @param pMessage
    *           Le message.
    * @param pException
    *           L'exception dont on désire avoir la pile d'appels.
    * @return La pile d'appels avec le message.
    */
   public static String getStackTraceAsString (String pMessage, Throwable pException) {
      String vReturn;
      StackTraceElement[] vTabStackTrace = pException.getStackTrace();
      // Si pas de message
      if (pMessage == null) {
         // Si pas de message dans l'exception (ex : "NullPointerException")
         if (pException.getMessage() == null) {
            vReturn = pException.getClass().getName();
         }
         else {
            // Si pas de message dans l'exception (ex : "NullPointerException")
            if (pException.getMessage() == null) {
               vReturn = pException.getClass().getName();
            }
            else {
               vReturn = pException.getMessage();
            }
         }
      }
      else {
         // Si pas de message dans l'exception (ex : "NullPointerException")
         if (pException.getMessage() == null) {
            vReturn = pMessage + " - " + pException.getClass().getName();
         }
         else {
            vReturn = pMessage + " - " + pException.getMessage();
         }
      }

      int vCpt = 0;
      for (StackTraceElement vStackTraceElement : vTabStackTrace) {
         // Si ce n'est pas la première ligne (car on est dans cette méthode)
         if (vCpt >= 1) {
            vReturn = vReturn + "\n   at " + vStackTraceElement.getClassName() + "." + vStackTraceElement.getMethodName() + "(" + vStackTraceElement.getFileName() + ":" + vStackTraceElement.getLineNumber() + ")";
         }
         vCpt++;
      }

      // S'il y a une rootCause
      if (pException.getCause() != null) {
         vReturn = vReturn + "\n" + getStackTraceAsString("Caused by:", pException.getCause());
      }

      return vReturn;
   }

   public static String getStackTraceAsOneLine (String pMessage) {
      return getStackTraceAsOneLine(pMessage, getStackTrace(null));
   }

   /**
    * Obtenir l'exception sur une seule ligne.
    * 
    * @param pMessage
    *           Le message.
    * @param pException
    *           L'exception dont on désire avoir la pile d'appels.
    * @return La pile d'appels avec le message sur une seule ligne.
    */
   public static String getStackTraceAsOneLine (String pMessage, Throwable pException) {
      String vReturn;
      Throwable vException;
      // Si pas d'exception spécifiée
      if (pException == null) {
         // On en génère une à partir d'ici
         vException = getStackTrace();
      }
      else {
         vException = pException;
      }
      List<String> vTabStackTrace = ExceptionUtils.getListStackTraceUsefull(ExceptionUtils.class, getStackTraceAsList(vException));
      if (pMessage == null) {
         vReturn = vException.getMessage();
      }
      else {
         vReturn = pMessage + " - " + vException.getMessage();
      }
      int vCpt = 0;
      for (String lineStackTrace : vTabStackTrace) {
         // Si ce n'est pas la première ligne (car on est dans cette méthode)
         if (vCpt >= 1) {
            vReturn = vReturn + lineStackTrace;
         }
         vCpt++;
      }

//      StackTraceElement[] vTabStackTrace = vException.getStackTrace();
//      // Si pas de message
//      if (pMessage == null) {
//         vReturn = vException.getMessage();
//      }
//      else {
//         vReturn = pMessage + " - " + vException.getMessage();
//      }
//      int vCpt = 0;
//      for (StackTraceElement vStackTraceElement : vTabStackTrace) {
//         // Si ce n'est pas la première ligne (car on est dans cette méthode)
//         if (vCpt >= 1) {
//            vReturn = vReturn + " |   at " + vStackTraceElement.getClassName() + "." + vStackTraceElement.getMethodName() + "(" + vStackTraceElement.getFileName() + ":" + vStackTraceElement.getLineNumber() + ")";
//         }
//         vCpt++;
//      }

      // S'il y a une rootCause
      if (vException.getCause() != null) {
         vReturn = vReturn + "--> " + getStackTraceAsOneLine("Caused by:", vException.getCause());
      }
      return vReturn;
   }

   /**
    * Savoir s'il s'agit d'un appel récursif.
    * @param pNbRecurse Le nombre de résursion maximum.
    * @return 'true' si l'appel est récursif.
    */
   public static boolean isRecurse (int pNbRecurse) {
      boolean vReturn = false;
      Exception vException = new ForSeeingLineOfMessageException("isRecurse()");
      StackTraceElement[] vTabStackTrace = vException.getStackTrace();
      // StackTraceElement[] --> String[]
      String[] vTabString = convert2TabString(vTabStackTrace);

      vReturn = isRecurse_intern(pNbRecurse, vTabString);

      return vReturn;
   }

   /**
    * Savoir s'il s'agit d'un appel récursif.
    * @return 'true' si l'appel est récursif.
    */
   public static boolean isRecurse () {
//      return isRecurse(2);
      boolean vReturn = false;
      Exception vException = new ForSeeingLineOfMessageException("isRecurse()");
      StackTraceElement[] vTabStackTrace = vException.getStackTrace();

      int vCpt = 0;
      Map<String, StackTraceElement> vMapMethodRencontree = new LinkedHashMap<String, StackTraceElement>();
      String vClassNameIn = null;
      String vPackageNameIn = null;
      boolean vIsSortir = false;
      // Parcourir les méthodes de la pile d'appels
      for (int vI = 0; (vI < vTabStackTrace.length) && (false == vIsSortir); vI++) {
         // Obtenir l'élément courant
         StackTraceElement vStackTraceElement = vTabStackTrace[vI];
         // Si appel de "isRecurse()"
         if (vI == 0) {
            // RAS
         }
         else {
            if (vI == 1) {
               // Mémoriser la classe d'entrée
               vClassNameIn = vStackTraceElement.getClassName();
               vPackageNameIn = getPackage(vClassNameIn);
            }

            // Constituer le nom de la méthode en absolu
            String vMethodName = vStackTraceElement.getClassName() + "." + vStackTraceElement.getMethodName() + " : " + vStackTraceElement.getLineNumber();
            // Obtenir l'élément éventuellement rencontrée dans le dépilage
            StackTraceElement vStackTraceElementFinded = vMapMethodRencontree.get(vMethodName);
            // Extraire le package
            String vPackageCour = getPackage(vStackTraceElement.getClassName());
            // Si on n'est plus dans le package d'entré
            if (false == vPackageCour.equals(vPackageNameIn)) {
               vIsSortir = true;
            }
            // Si la méthode a déjà été rencontré
            else if (vStackTraceElementFinded != null) {
               vReturn = true;
               vIsSortir = true;
            }
            else {
               // RAS
            }
            // Mémorisé la méthode du niveau courant de la pile d'appels
            vMapMethodRencontree.put(vMethodName, vStackTraceElement);
         }
      }

      return vReturn;
   }

   /**
    * Enlever les espaces à droite et à gauche.
    * @param pStringOri La chaîne originale.
    * @return Une nouvelle chaine sans les espaces à droite et à gauche.
    */
   private static String sanitized (String pStringOri) {
      String vReturn = pStringOri;
      // Enlever les espaces à droite et à gauche
      vReturn = vReturn.trim();
      return vReturn;
   }

   private static String getPackage (String pClass) {
      String vReturn;
      final String cPoint = ".";

      // Si on a un "." (ex : "mon_pac.metier.MaClasse")
      if (pClass.indexOf(cPoint) >= 0) {
         String[] vTabNom = StringUtils.split(pClass, cPoint);
         vReturn = vTabNom[0];
         String vNomCour;
         for (int vI = 1; vI < vTabNom.length - 1; vI++) {
            vNomCour = vTabNom[vI];
            vReturn = vReturn + cPoint + vNomCour;
         }
      }
      // Sinon : un seul nom de package (ex : "mon_pac")
      else {
         vReturn = pClass;
      }

      return vReturn;
   }

   static boolean isRecurse_intern (int pNbRecurse, String[] pTabString) {
      boolean vReturn = false;
      Map<String, BeanMemStatckTrace> vMapMethodRencontree = new LinkedHashMap<String, BeanMemStatckTrace>();
      boolean vIsSortir = false;
      int vNbFinded = 0;
      // Parcourir les méthodes de la pile d'appels
      for (int vRang = 0; (vRang < pTabString.length) && (false == vIsSortir); vRang++) {
         // Si appel de "isRecurse()"
         if (vRang == 0) {
            // RAS
         }
         else {
            // Constituer le nom de la méthode en absolu
            String vMethodName = sanitized(pTabString[vRang]);
            // Obtenir l'élément éventuellement rencontrée dans le dépilage
            BeanMemStatckTrace vBeanMemStatckTrace = vMapMethodRencontree.get(vMethodName);
            if (vBeanMemStatckTrace == null) {
               vNbFinded = 1;
               // Mémoriser la récursion potentielle
               vBeanMemStatckTrace = new BeanMemStatckTrace(vNbFinded, vRang);
            }
            // Si la méthode a déjà été rencontré
            else {
               // Obtenir le nombre d'occurences de la méthode trouvé
               vNbFinded = vBeanMemStatckTrace.getNbFinded();
               List<Integer> vListRangInTab = vBeanMemStatckTrace.getListRangInTab();
               for (Integer vRangInTab : vListRangInTab) {
                  if (vRangInTab == null) {
                     // RAS
                  }
                  else {
                     String vStringLastRangInTab = sanitized(pTabString[vRangInTab - 1]);
                     String vStringLinePrec = sanitized(pTabString[vRang - 1]);
                     // Si la ligne précédente est égale à celle de la dernière fois  
                     // ou les 2 dernières lignes sont égales 
                     if ((vStringLastRangInTab.equals(vStringLinePrec))
                           || (vMethodName.equals(vStringLinePrec))) {
                        // Mémoriser une récursion de plus
                        vNbFinded++;
                        vBeanMemStatckTrace.setNbFinded(vNbFinded);
                     }
                     else {
                        // RAS
                     }
                  }

                  if (vNbFinded > pNbRecurse) {
                     vReturn = true;
                     vIsSortir = true;
                  }
                  else {
                     // RAS
                  }
               } // FIN for (Integer vRangInTab : vListRangInTab) {
               vBeanMemStatckTrace.addRangInTab(vRang);
            }
            // Mémorisé la méthode du niveau courant de la pile d'appels
            vMapMethodRencontree.put(vMethodName, vBeanMemStatckTrace);
         }
      }
      return vReturn;
   }

   /**
    * Obtenir un message formaté avec la StackTrace de l'appel sur une seule ligne.
    * @param pMessage Le message.
    * @return Le message formaté.
    */
   public static String getMessageWithStackTrace (String pMessage) {
      String vReturn = pMessage + " - " + ExceptionUtils.toStringStackTraceUsefull(ExceptionUtils.class);

      return vReturn;
   }

   /**
    * Permet de convertir une Exception Java en une liste de 'String'.
    * 
    * @param p_Exception
    *           L'exception à convertir.
    * @return La liste de 'String' représentant l'erreur.
    * @example
    *          ExceptionUtils.getStackTraceAsList(ExceptionUtils.getStackTrace("Regarder la ligne de votre package"))
    */
   public static List<String> getStackTraceAsList (final Throwable p_Exception) {
      final List<String> v_getStackTraceAsList = new ArrayList<String>();
      // Obtenir la pile d'appel
      final StackTraceElement[] v_tabStackTraceElement;
      // Si pas d'exception spécifiée
      if (p_Exception == null) {
         // On en génère une à partir d'ici
         v_tabStackTraceElement = ExceptionUtils.getStackTrace().getStackTrace();
      }
      else {
         v_tabStackTraceElement = p_Exception.getStackTrace();
      }
      // Ajouter le message de l'erreur
      v_getStackTraceAsList.add("Exception mise à plat");
      v_getStackTraceAsList.add("--> " + p_Exception.toString());
      // Parcourir la pile d'appel
      for (int v_i = 0; v_i < v_tabStackTraceElement.length; v_i++) {
         // Ajouter l'élément
         v_getStackTraceAsList.add(v_tabStackTraceElement[v_i].toString());
      }

      if (p_Exception.getCause() != null) {
         final List<String> v_Listcause = getStackTraceAsList(p_Exception.getCause());
         for (String vCause : v_Listcause) {
            v_getStackTraceAsList.add(vCause);
         }
      }

      return v_getStackTraceAsList;
   }

   /**
    * Permet de couper la pile d'appels à la partie utile.
    * 
    * @param p_ClassAppelante
    *           La classe appelante.
    * @param p_lstStackTrace
    *           La liste contenant les lignes de la pile d'appels.
    * @return La chaîne formatée sur 1 ligne avec la pile d'appels utiles. Par
    *         exemple : [Exception mise en forme dans getStackTraceAsList(), -->
    *         java.lang.IllegalArgumentException: Message d'erreur de
    *         génération pour les tests unitaires,
    *         fr.pacman.commons.errorgen.ErrorMessage_Test
    *         .toString_CN(ErrorMessage_Test.java:14),
    *         sun.reflect.NativeMethodAccessorImpl.invoke0(...
    * @example
    *          ExceptionUtils.toStringStackTraceUsefull(Service_Abs.class, _affectationInstanceService)
    */
   public static String toStringStackTraceUsefull (final Class<?> p_ClassAppelante) {
      List<String> vListStackTraceUsefull = getListStackTraceUsefull(p_ClassAppelante, getStackTraceAsList(getStackTrace()));

      String vReturn = "";
      for (String elemStackTraceUsefull : vListStackTraceUsefull) {
         vReturn = vReturn + elemStackTraceUsefull;
      }
      return vReturn;
   }

   public static String toStringStackTraceUsefull (final Class<?> p_ClassAppelante, Exception pException) {
      List<String> vListStackTraceUsefull = getListStackTraceUsefull(p_ClassAppelante, getStackTraceAsList(pException));

      String vReturn = "";
      for (String elemStackTraceUsefull : vListStackTraceUsefull) {
         vReturn = vReturn + elemStackTraceUsefull;
      }
      return vReturn;
   }

   public static String toStringStackTraceUsefull (final Class<?> p_ClassAppelante, final List<String> p_lstStackTrace) {
      List<String> vListStackTraceUsefull = getListStackTraceUsefull(p_ClassAppelante, p_lstStackTrace);

      String vReturn = "";
      for (String elemStackTraceUsefull : vListStackTraceUsefull) {
         vReturn = vReturn + elemStackTraceUsefull;
      }
      return vReturn;
   }

   /**
    * Permet de couper la pile d'appels à la partie utile.
    * 
    * @param p_ClassAppelante
    *           La classe appelante.
    * @param p_lstStackTrace
    *           La liste contenant les lignes de la pile d'appels.
    * @return La chaîne formatée sur 1 ligne avec la pile d'appels utiles. Par
    */
   public static List<String> getListStackTraceUsefull (final Class<?> p_ClassAppelante, final List<String> p_lstStackTrace) {
      List<String> v_return = new ArrayList<>();
      String v_callCour;
      // Par exemple : "fr.pacman.commons.errorgen"
      final String v_packageActuel = p_ClassAppelante.getPackage().getName();

      int v_nbLignesPackageSocle = 0;
      int v_nbLignesPackageOther = 0;
      boolean v_isSortir = false;
      // Parcourir la pile d'appels
      for (int v_i = 0; ((v_isSortir == false) && (v_i < p_lstStackTrace.size())); v_i++) {
         v_return.add(" ");
         // Obtenir l'appel courant de la pile d'appels
         v_callCour = p_lstStackTrace.get(v_i);
         // Si on a un package socle (ex : "fr.socle.commons.errorgen.ErrorMessage_Test.toString_CN(ErrorMessage_Test.java:14)")
         if (v_callCour.indexOf(v_packageActuel) >= 0) {
            // Une ligne de plus
            v_nbLignesPackageSocle++;
            v_return.add(v_callCour);
         }
         // Si ce n'est pas un package socle
         else {
            // Si déjà au moins une ligne socle rencontrée
            if (v_nbLignesPackageSocle > 0) {
               v_nbLignesPackageOther++;

               if (v_nbLignesPackageOther > 2) {
                  v_isSortir = true;
                  v_return.add(v_callCour + "...");
               }
               else {
                  v_return.add(v_callCour);
               }
            }
            else {
               v_return.add(v_callCour);
            }
         }
      }

      return v_return;
   }

   public static Exception getStackTrace () {
      return new ForSeeingLineOfMessageException("");
   }

   /**
    * Pour avoir une exception 'ForSeeingLineOfMessageException'.
    * @param pMessage Le message.
    * @return L'exception désirée.
    */
   public static Exception getStackTrace (String pMessage) {
      return new ForSeeingLineOfMessageException(pMessage);
   }

   /**
    * Convertir en un tableau de String.
    * @param pTabStackTrace Le tableau en entrée.
    * @return Le tableau désiré.
    */
   private static String[] convert2TabString (StackTraceElement[] pTabStackTrace) {
      String[] vReturn = new String[pTabStackTrace.length];

      int vCpt = 0;
      for (StackTraceElement vStackTraceElement : pTabStackTrace) {
         // Constituer le nom de la méthode en absolu
         String vMethodName = vStackTraceElement.getClassName() + "." + vStackTraceElement.getMethodName() + " : " + vStackTraceElement.getLineNumber();

         // Mémoriser l'élément
         vReturn[vCpt] = vMethodName;
         vCpt++;
      }
      return vReturn;
   }

}

/**
 * Pour voir la ligne où a été écrite le message.
 * @author MinEdu
 */
class ForSeeingLineOfMessageException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public ForSeeingLineOfMessageException(String pMessage) {
      super("PROVOQUEE POUR VOIR OU A ETE ECRITE LA LIGNE DU MESSAGE. " + pMessage);
   }
}

class BeanMemStatckTrace {
   private int _NbFinded = 0;
   private List<Integer> _ListRangInTab = new ArrayList<Integer>();

   public BeanMemStatckTrace(int pNbFinded, int pRangInTab) {
      super();
      _NbFinded = pNbFinded;
      addRangInTab(pRangInTab);
   }

   public int getNbFinded () {
      return _NbFinded;
   }

   public void setNbFinded (int pNbFinded) {
      _NbFinded = pNbFinded;
   }

   /**
    * Ajouter un rang dans le tableau de StackTrace.
    * @param pRangInTab Le rang à ajouter.
    */
   public void addRangInTab (int pRangInTab) {
      _ListRangInTab.add(pRangInTab);
   }

   public Integer getLastRangInTab () {
      return (_ListRangInTab.isEmpty() ? null : _ListRangInTab.get(_ListRangInTab.size() - 1));
   }

   public List<Integer> getListRangInTab () {
      return _ListRangInTab;
   }

   @Override
   public String toString () {
      return "BeanMemStatckTrace [_NbFinded=" + _NbFinded + ", _ListRangInTab=" + _ListRangInTab + "]";
   }
}