package fr.awax.acceleo.common.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.obeonetwork.dsl.cinematic.view.AbstractViewElement;

/**
 * Méthode utilitaire sur les type 'List'.
 * @author MinEdu
 */
public class ListUtils {
   /** Identifiant d'un élément dans le masque (ex : "${elem.getType().getName()} ${elem.getName()}"). */
   private static final String cIdElemStart = "${elem.";
   private static final String cIdEndMethod = "()";
   private static final String cIdElemEnd = cIdEndMethod + "}";

   /**
    * Permet de savoir si un élément est présent dans la liste.
    * @param pElem L'élément à rechercher.
    * @param pList La liste à regarder.
    * @return 'true' si l'élément existe.
    * @example
    *          boolean isExist = ListUtils.isExist(uneList, unElem);
    */
   public static boolean isExist (List<?> pList, Object pElem) {
      boolean vReturn = false;

      for (Object vElem : pList) {
         // SI l'élément est trouvé dans la liste
         if (vElem.equals(pElem)) {
            vReturn = true;
            break;
         }
      }
      return vReturn;
   }

   /**
    * Mettre la liste sous forme de chaîne de caractères.
    * @param pListe La liste.
    * @param pMethodName4String Le nom de la méthode à appeler sur chaque élément de la liste pour avoir une 'String'.
    * @return La liste sous forme de 'String'.
    * @example
    *          String vResult = ListUtils.toString(vList, "getNom");
    */
   public static String toString (Collection<?> pListe, String pMethodName4String) {
      String vReturn;
      String vListeFmt = "";

      Method vMethod;
      // Instancier le 'Method'
      try {
         int vRang = 0;
         // Parcourir la liste
         for (Object vElem : pListe) {
            vRang++;
            vMethod = vElem.getClass().getMethod(pMethodName4String);

            String vString;
            try {
               // Appeler la méthode pour avoir une 'String'
               vString = (String) vMethod.invoke(vElem);
            }
            catch (ClassCastException e) {
               // Appeler la méthode
               Object vObject = vMethod.invoke(vElem);
               // Puis appeler "toString()"
               vMethod = vObject.getClass().getMethod("toString");
               vString = (String) vMethod.invoke(vObject);
            }
            // Si encore au moins un élément
            if (vRang < pListe.size()) {
               // Concaténer
               vListeFmt = vListeFmt + vString + ",";
            }
            // Si plus aucun un élément
            else {
               // Concaténer
               vListeFmt = vListeFmt + vString;
            }
         }
      }
      catch (NoSuchMethodException vErr) {
         throw new RuntimeException("Problème pour appeler la méthode \"" + pMethodName4String + "()\" sur un élément de la liste du type " + pListe.getClass() + " : Vérifier que le méthode existe");
      }
      catch (SecurityException vErr) {
         throw new RuntimeException("Problème de sécurité pour appeler la méthode \"" + pMethodName4String + "()\" sur un élément de la liste du type " + pListe.getClass() + " : Vérifier que le méthode est accessible");
      }
      catch (IllegalAccessException e) {
         throw new RuntimeException("Problème pour appeler la méthode \"" + pMethodName4String + "()\" sur un élément de la liste du type " + pListe.getClass() + " : Vérifier que le méthode est accessible : public");
      }
      catch (IllegalArgumentException e) {
         throw new RuntimeException("Problème pour appeler la méthode \"" + pMethodName4String + "()\" sur un élément de la liste du type " + pListe.getClass() + " : Vérifier la méthode ne possède pas de paramètre");
      }
      catch (InvocationTargetException e) {
         throw new RuntimeException("Problème pour appeler la méthode \"" + pMethodName4String + "()\" sur un élément de la liste du type " + pListe.getClass() + " : Vérifier la méthode existe sur la cible");
      }

      vReturn = "[" + vListeFmt + "]";
      return vReturn;
   }

   /**
    * Mettre la liste sous forme de chaîne de caractères.
    * @param pListe La liste.
    * @param pMethodName4String Le nom de la méthode à appeler sur chaque élément de la liste pour avoir une 'String'.
    * @return La liste sous forme de 'String'.
    * @example
    *          String vResult = ListUtils.toString(vList, "getNom");
    */
   public static String toString (List<?> pListe, String pMethodName4String) {
      String vReturn;
      String vListeFmt = "";

      Method vMethod;
      // Instancier le 'Method'
      try {
         int vRang = 0;
         // Parcourir la liste
         for (Object vElem : pListe) {
            vRang++;
            vMethod = vElem.getClass().getMethod(pMethodName4String);

            String vString;
            try {
               // Appeler la méthode pour avoir une 'String'
               vString = (String) vMethod.invoke(vElem);
            }
            catch (ClassCastException e) {
               // Appeler la méthode
               Object vObject = vMethod.invoke(vElem);
               // Puis appeler "toString()"
               vMethod = vObject.getClass().getMethod("toString");
               vString = (String) vMethod.invoke(vObject);
            }
            // Si encore au moins un élément
            if (vRang < pListe.size()) {
               // Concaténer
               vListeFmt = vListeFmt + vString + ",";
            }
            // Si plus aucun un élément
            else {
               // Concaténer
               vListeFmt = vListeFmt + vString;
            }
         }
      }
      catch (NoSuchMethodException vErr) {
         throw new RuntimeException("Problème pour appeler la méthode \"" + pMethodName4String + "()\" sur un élément de la liste du type " + pListe.getClass() + " : Vérifier que le méthode existe");
      }
      catch (SecurityException vErr) {
         throw new RuntimeException("Problème de sécurité pour appeler la méthode \"" + pMethodName4String + "()\" sur un élément de la liste du type " + pListe.getClass() + " : Vérifier que le méthode est accessible");
      }
      catch (IllegalAccessException e) {
         throw new RuntimeException("Problème pour appeler la méthode \"" + pMethodName4String + "()\" sur un élément de la liste du type " + pListe.getClass() + " : Vérifier que le méthode est accessible : public");
      }
      catch (IllegalArgumentException e) {
         throw new RuntimeException("Problème pour appeler la méthode \"" + pMethodName4String + "()\" sur un élément de la liste du type " + pListe.getClass() + " : Vérifier la méthode ne possède pas de paramètre");
      }
      catch (InvocationTargetException e) {
         throw new RuntimeException("Problème pour appeler la méthode \"" + pMethodName4String + "()\" sur un élément de la liste du type " + pListe.getClass() + " : Vérifier la méthode existe sur la cible");
      }

      vReturn = "[" + vListeFmt + "]";
      return vReturn;
   }

   /**
    * Savoir si une list contient au moins un élément.
    * @param pList La liste
    * @return 'true' si au moins un élément.
    */
   public static boolean isNotEmpty (List<?> pList) {
      return !isEmpty(pList);
   }

   /**
    * Savoir si une list est vide.
    * @param pList La liste
    * @return 'true' si vide - 'false' si au moins un élément.
    */
   public static boolean isEmpty (List<?> pList) {
      boolean vReturn;

      // Si vide
      if ((pList == null) || (pList.isEmpty())) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }

      return vReturn;
   }

   /**
    * Formater une liste en String à partir d'un masque.
    * 
    * @param pMasque Le masque (ex : "<${elem.getvalueSimple()}, ${elem.getvalueComplexe().getvalueSimple()}>").
    * @param pSeparateur Le séparateur (ex : " | ").
    * @param pList La liste à formater.
    * @return La liste formatée.
    */
   public static String formatList (String pMasque, String pSeparateur, List<?> pList) {
      String vReturn = "";

      try {
         int rangInList = 0;
         // Parcourir la liste
         for (Object elem : pList) {
            // Obtenir le nombre d'élément dans le masque (ex : 2)
            int nbElemMasque = getNbElemMasque(pMasque);
            String vMasqueFmt = pMasque;
            for (int i = 0; i < nbElemMasque; i++) {
               List<String> listMethodOfMasque = getListMethodOfElem(elem, pMasque, i);
               Object sousElem = elem;
               // Parcourir les méthodes en cascade de l'élément du masque (ex : getType(), puis getName() --> getType().getName())
               for (String methodName : listMethodOfMasque) {
                  Method vMethodOfMasque = sousElem.getClass().getMethod(methodName);
                  // Appeler la méthode (ex : "getvalueComplexe()") pour obtenir la valeur de l'élément courant de la liste 'pList'
                  sousElem = vMethodOfMasque.invoke(sousElem);
               }
               // Replacer la valeur de l'élément dans le masque
               vMasqueFmt = replaceNextElemInMasqueFmt(sousElem, vMasqueFmt);
            } // FIN for (int i = 0; i < nbElemMasque; i++)
            vReturn = (rangInList < pList.size() - 1 ? vReturn + vMasqueFmt + pSeparateur : vReturn + vMasqueFmt);
            rangInList++;
         }
      }
      catch (Exception e) {
         throw new RuntimeException("Problème lors formatList() avec pMasque=" + pMasque + " : " + e.getMessage(), e);
      }

      return vReturn;
   }

   private static String replaceNextElemInMasqueFmt (Object pSousElem, String pMasqueFmt) {
      String vReturn;

      int rangStart = pMasqueFmt.indexOf(cIdElemStart);
      int rangEnd = pMasqueFmt.indexOf(cIdElemEnd);
      if (rangStart >= 0) {
         vReturn = pMasqueFmt.substring(0, rangStart) + pSousElem + pMasqueFmt.substring(rangEnd + cIdElemEnd.length(), pMasqueFmt.length());
      }
      else {
         vReturn = pMasqueFmt;
      }

      return vReturn;
   }

   /**
    * Obtenir toute les méthodes figurant dans le masque.
    * 
    * @param pMasque Le masque (ex : "<${elem.getvalueSimple()}, ${elem.getvalueComplexe().getvalueSimple()}>").
    * @param pElemNumber Le rang de l'élément désiré.
    * @return La liste désirée.
    */
   private static List<String> getListMethodOfElem (Object pElem, String pMasque, int pElemNumber) {
      List<String> vReturn = null;

      // Obtenir le masque désiré (ex : getvalueSimple)
      String elemAllMethodName = getMasqueOfElem(pMasque, pElemNumber);
      ;
      vReturn = getListMethodOfAllMethodName(elemAllMethodName);

      return vReturn;
   }

   private static String getMasqueOfElem (String pMasque, int pElemNumber) {
      String vReturn;

      int rangStart = getRangInMasqueOfElem(pMasque, pElemNumber) + cIdElemStart.length();
      int rangEnd = pMasque.indexOf(cIdElemEnd, rangStart);
      vReturn = pMasque.substring(rangStart, rangEnd);

      return vReturn;
   }

   private static int getRangInMasqueOfElem (String pMasque, int pElemNumber) {
      int vReturn;
      int rangStart = 0;
      int cpt = -1;
      do {
         rangStart = pMasque.indexOf(cIdElemStart, rangStart);
         rangStart++;
         cpt++;
      } while ((rangStart > 0) && (pElemNumber > cpt));
      vReturn = rangStart - 1;

      return vReturn;
   }

   private static List<String> getListMethodOfAllMethodName (String pElemAllMethodName) {
      List<String> vReturn = new ArrayList<>();

      try {
         String methodName;
         int rangStart = 0;
         int rangEnd = 0;
         int rangCour = 0;
         do {
            rangCour = pElemAllMethodName.indexOf(cIdEndMethod, rangEnd);
            rangEnd = (rangCour < 0 ? pElemAllMethodName.length() : rangCour);
            // Obtenir le nom de la méthode
            methodName = pElemAllMethodName.substring(rangStart, rangEnd);
            if (methodName.startsWith("().")) {
               methodName = methodName.substring("().".length(), methodName.length());
            }
            vReturn.add(methodName);
            rangStart = rangEnd;
            rangEnd++;
         } while (rangCour > 0);
      }
      catch (Exception e) {
         throw new RuntimeException("Problème dans getListMethodOfAllMethodName() : " + e.getMessage(), e);
      }

      return vReturn;
   }

   /**
    * Obtenir le nombre d'élements du masque.
    * 
    * @param pMasque Le masque.
    * @return Le nombre désiré.
    */
   private static int getNbElemMasque (String pMasque) {
      int vReturn = -1;
      int rang = 0;
      do {
         rang = pMasque.indexOf(cIdElemStart, rang);
         rang++;
         vReturn++;
      } while (rang > 0);

      return vReturn;
   }

   /**
    * Cloner une liste (sans cloner les éléments).
    * @param pList La liste originale.
    * @return La nouvelle liste avec les mêmes éléments.
    */
   public static <TypElem> List<TypElem> clone (List<TypElem> pList) {
      List<TypElem> vReturn = new ArrayList<>();

      for (TypElem vElem : pList) {
         vReturn.add(vElem);
      }

      return vReturn;
   }

   /**
    * Permet de savoir si la liste est remplie de 'null'.
    * @param pList La liste à regarder.
    * @return 'true' si la liste est remplie de 'null'.
    */
   public static boolean isListNull (List<?> pList) {
      boolean vReturn = true;
      
      for (Object elem : pList) {
         if (elem != null) {
            vReturn = false;
            break;
         }
      }
      return vReturn;
   }

}
