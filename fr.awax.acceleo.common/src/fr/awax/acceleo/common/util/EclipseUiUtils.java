package fr.awax.acceleo.common.util;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Bundle;

import fr.awax.acceleo.common.conf.AllPropHelper;
import fr.awax.acceleo.common.log.LogLevelEnum;

/**
 * Utilitaire pour "eclipse.ui".
 * @author MinEdu
 * @Example
 *          EclipseUiUtils.writeErrorLog(Activator_Abs.getDefault().getPlugInId(), "Problème avec ...", null);
 */
public class EclipseUiUtils {
   private String _Message;
   /** 'true' : Pas encore rencontré de message masqué à cause du niveau de LOG dans le paramétrage. */
   private boolean _isFirstMessageHidden = true;
   private String _PlugInId;

   private static Map<String, EclipseUiUtils> _MapInstances = new HashMap<>();

   public static EclipseUiUtils getInstance (String pPlugInId) {
      EclipseUiUtils instance = _MapInstances.get(pPlugInId);
      if (instance == null) {
         instance = new EclipseUiUtils();
         instance._PlugInId = pPlugInId;
         _MapInstances.put(pPlugInId, instance);
      }
      return instance;
   }

   public EclipseUiUtils() {
      // RAS
   }

   /**
    * Permet d'ecrire un message "Info" dans la vue "Error Log".
    * @param pMessage Le message à écrire.
    */
   public void writeViewErrorLog_trace (String pMessage) {
      log(LogLevelEnum.TRACE, newInstanceStatus(Status.INFO, _PlugInId, Status.OK, StringUtils.getStringUtf8("TRACE  : " + pMessage), ExceptionUtils.getStackTrace()));
   }

   /**
    * Permet d'ecrire un message "Info" dans la vue "Error Log".
    * @param pMessage Le message à écrire.
    */
   public void writeViewErrorLog_info (String pMessage) {
      log(LogLevelEnum.INFO, newInstanceStatus(Status.INFO, _PlugInId, Status.OK, StringUtils.getStringUtf8("INFO : " + pMessage), ExceptionUtils.getStackTrace()));
   }

   /**
    * Permet d'ecrire un message "warning" dans la vue "Error Log".
    * @param pMessage Le message à écrire.
    */
   public void writeViewErrorLog_warning (String pMessage) {
      log(LogLevelEnum.WARNING, newInstanceStatus(Status.WARNING, _PlugInId, Status.OK, StringUtils.getStringUtf8("AVERTISSEMENT : " + pMessage), ExceptionUtils.getStackTrace()));
   }

   /**
    * Permet d'ecrire un message "OK" dans la vue "Error Log".
    * @param pMessage Le message à écrire.
    */
   public void writeViewErrorLog_debug (String pMessage) {
      log(LogLevelEnum.DEBUG, newInstanceStatus(Status.OK, _PlugInId, Status.OK, StringUtils.getStringUtf8(pMessage), ExceptionUtils.getStackTrace()));
   }

   /**
    * Permet d'ecrire une erreur dans la vue "Erreur Log" et dans une MessageBox.
    * @param pMessage Le message d'erreur.
    * @param pException L'exception à afficher.
    */
   public void writeViewErrorLog_fatal (String pMessage, Throwable pException) {
      log(LogLevelEnum.ERROR, newInstanceStatus(Status.ERROR, _PlugInId, Status.ERROR, "FATAL ERROR GENERATION SERVICE  : " + StringUtils.getStringUtf8(pMessage), pException));

      /// Afficher une Popup
//      Display.getDefault().asyncExec(new Runnable()
      PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
         @Override
         public void run () {
            MessageDialog.openError(PlatformUI.getWorkbench().getDisplay().getActiveShell(), "PROBLEM WITH " + _PlugInId, _Message);
         }
      });
   }

   /**
    * Permet d'ecrire une erreur dans la vue "Erreur Log".
    * @param pLogLevelMessage Le niveau de LOG du message.
    * @param pMessage Le message d'erreur.
    * @param pException L'exception à afficher.
    */
   public void writeViewErrorLog (LogLevelEnum pLogLevelMessage, String pMessage, Throwable pException) {
      String vTypeMessage;
      // SI c'est un message d'erreur
      if (pLogLevelMessage.ordinal() > LogLevelEnum.ERROR.ordinal()) {
         vTypeMessage = "ERROR GENERATION SERVICE : ";
      }
      else {
         vTypeMessage = pLogLevelMessage.name() + " GENERATION SERVICE : ";
      }
      // SI exécution en mode plugin Eclipse
      if (_PlugInId != null) {
         log(pLogLevelMessage, newInstanceStatus(pLogLevelMessage, _PlugInId, getSeverityFromLogLevelEnum(pLogLevelMessage), vTypeMessage + StringUtils.getStringUtf8(pMessage), pException));
      }

   }

   /**
    * Obtenir une instance de 'Status'.
    * @param pSeverity La sévérité (ex : Status.ERROR).
    * @param pPluginId L'identifiant du plug-in.
    * @param pCode Le code (ex : Status.OK)
    * @param pMessage Le message.
    * @param pException L'exception potentielle.
    * @return L'instance désirée.
    */
   private Status newInstanceStatus (LogLevelEnum pLogLevelMessage, String pPluginId, int pCode, String pMessage, Throwable pException) {
      int vSeverity = getSeverityFromLogLevelEnum(pLogLevelMessage);
      return newInstanceStatus(vSeverity, pPluginId, pCode, pMessage, pException);
   }

   private Status newInstanceStatus (int pSeverity, String pPluginId, int pCode, String pMessage, Throwable pException) {
      return new Status(pSeverity, pPluginId, pCode, pMessage, pException);
   }

   /**
    * Permet d'obtenir le niveau de séverité pour Eclipse.
    * @param pLogLevelMessage Le niveau de LOG du socle de génération.
    * @return Le niveau de séverité pour Eclipse associé à 'pLogLevelMessage'.
    */
   private int getSeverityFromLogLevelEnum (LogLevelEnum pLogLevelMessage) {
      int vReturn;
      // SI ERROR
      if (pLogLevelMessage.ordinal() >= LogLevelEnum.ERROR.ordinal()) {
         vReturn = Status.ERROR;
      }
      // SI WARNING
      else if (pLogLevelMessage.ordinal() == LogLevelEnum.WARNING.ordinal()) {
         vReturn = Status.WARNING;
      }
      else {
         vReturn = Status.INFO;
      }
      return vReturn;
   }

   /**
    * Permet de logger le message dans le bon endroit.
    * @param pStatus Le status.
    */
   private void log (LogLevelEnum pLogLevelEnum, Status pStatus) {
      Throwable vException = pStatus.getException();
      Bundle vBundle = Platform.getBundle(_PlugInId);
      LogUtils vLogUtils = LogUtils.getInstance(_PlugInId);
      // S'il faut LOGGUER
      if (vLogUtils.isLogMessage(pLogLevelEnum)) {
//         // Si pas d'exception
//         if (vException == null) {
//            // Obtenir la pile d'appels pour connaître la localisation de l'affichage du message 
//            vException = ExceptionUtils.getStackTrace();
//         }

         // Si exécution en mode plug-in
         if (pStatus.getPlugin() != null) {
            writeStatusInErrorLog(pStatus);
         }

//         // Dans tous les cas : on écrit à minima dans la console
//         final PrintStream vOut = System.out;
//         vOut.println(pStatus.getMessage());
      } // FIN if (LogUtils.isLogMessage(pLogLevelEnum))
      else {
         if (_isFirstMessageHidden) {
            _isFirstMessageHidden = false;
            Status vStatus = newInstanceStatus(Status.WARNING, _PlugInId, Status.WARNING, "Le niveau de LOG a masqué certain message --> Regarder le niveau spécifié sur la valeur " + AllPropHelper.PROP_COMMON.generation_log.getKey(), vException);
            writeStatusInErrorLog(vStatus);
         }
      }
   }

   private void writeStatusInErrorLog (Status pStatus) {
      // Obtenir le bundle pour le plug-in
      Bundle vBundle = Platform.getBundle(_PlugInId);

      // Ajouter le Log dans le fenêtre "Error Log" d'Eclipse
      ILog logViewErrorLog = Platform.getLog(vBundle);
      logViewErrorLog.log(pStatus);
   }

   /**
    * Pose une question à l'utilisateur
    * @param p_question
    *           l'intitulé de la question
    * @param p_default
    *           la valeur par défaut
    * @param p_validator
    *           un validateur de réponse
    * @return la réponse
    * @throws UnsupportedOperationException
    *            si l'utilisateur clique sur un autre bouton que OK
    * @example
    *          String v_ProjetName = ask("Information necessaire", "Saisissez le nom du projet (conseil : donner le meme nom que le projet CDO)", pProjetName.get_String(), new IInputValidator()
    *          {
    * @Override
    *           public String isValid (final String p_value)
    *           {
    *           return null;
    *           }
    *           });
    */
   public String ask (final String p_title, final String p_question, final String p_default, final IInputValidator p_validator) throws UnsupportedOperationException {
      final org.eclipse.jface.dialogs.InputDialog v_i = new org.eclipse.jface.dialogs.InputDialog(Display.getCurrent().getActiveShell(), p_title, p_question, p_default, p_validator);
      v_i.setBlockOnOpen(true);
      v_i.open();
      if (v_i.getReturnCode() == org.eclipse.jface.dialogs.Dialog.OK) {
         final String v_str = v_i.getValue();
         v_i.close();
         return v_str;
      }
      else {
         throw new UnsupportedOperationException("Impossible de continuer");
      }
   }

   /**
    * Permet d'ecrire une erreur dans la vue "Erreur Log".
    * @param pPlugInId L'identifiant du plug-in (ex : Activator.c_PLUGIN_ID)
    * @param pMessage Le message d'erreur.
    * @param pException L'exception Ã  afficher.
    */
   public void writeErrorLog (String pPlugInId, String pMessage, Exception pException) {
      _PlugInId = pPlugInId;
      log(LogLevelEnum.ERROR, newInstanceStatus(Status.ERROR, pPlugInId, Status.OK, "ERREUR SERVICE GENERATION : " + pMessage, pException));
   }

   public void writeErrorLog_info (String pPlugInId, String pMessage) {
      _PlugInId = pPlugInId;
      log(LogLevelEnum.INFO, newInstanceStatus(Status.INFO, pPlugInId, Status.OK, "ERREUR SERVICE GENERATION : " + pMessage, null));
   }
}
