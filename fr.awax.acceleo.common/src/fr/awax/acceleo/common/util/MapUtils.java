package fr.awax.acceleo.common.util;

import java.util.Iterator;
import java.util.Map;

/**
 * Classe utilitaire pour les Map.
 * 
 * @author MinEdu
 */
public class MapUtils {

   /**
    * Obtenir le 1er élément de la Map.
    * 
    * @param pMap
    *           La Map concernée.
    * @return Le 1er élément.
    */
   public static Object getFirstElem (Map<?, ?> pMap) {
      Object vReturn;

      if (pMap == null) {
         vReturn = null;
      }
      else {
         if (pMap.isEmpty()) {
            vReturn = null;
         }
         else {
            vReturn = pMap.get(getFirstKey(pMap));
         }
      }

      return vReturn;
   }

   /**
    * Obtenir le dernier élément de la Map.
    * 
    * @param pMap
    *           La Map concernée.
    * @return Le dernier élément.
    */
   public static Object getLastElem (Map<?, ?> pMap) {
      Object vReturn;

      if (pMap == null) {
         vReturn = null;
      }
      else {
         if (pMap.isEmpty()) {
            vReturn = null;
         }
         else {
            vReturn = pMap.get(getLastKey(pMap));
         }
      }

      return vReturn;
   }

   /**
    * Obtenir le 1ère clé de la Map.
    * 
    * @param pMap
    *           La Map concernée.
    * @return La 1ère clé.
    */
   public static Object getFirstKey (Map<?, ?> pMap) {
      Object vReturn;

      if (pMap == null) {
         vReturn = null;
      }
      else {
         if (pMap.isEmpty()) {
            vReturn = null;
         }
         else {
            @SuppressWarnings("rawtypes")
            Iterator vItKeys = pMap.keySet().iterator();
            if (vItKeys.hasNext()) {
               vReturn = vItKeys.next();
            }
            else {
               vReturn = null;
            }
         }
      }

      return vReturn;
   }

   /**
    * Obtenir le dernière clé de la Map.
    * 
    * @param pMap
    *           La Map concernée.
    * @return La dernière clé.
    */
   public static Object getLastKey (Map<?, ?> pMap) {
      Object vReturn;

      if (pMap == null) {
         vReturn = null;
      }
      else {
         if (pMap.isEmpty()) {
            vReturn = null;
         }
         else {
            vReturn = null;
            @SuppressWarnings("rawtypes")
            Iterator vItKeys = pMap.keySet().iterator();
            while (vItKeys.hasNext()) {
               vReturn = vItKeys.next();
            }
         }
      }

      return vReturn;
   }

   /**
    * Permet de convertir une instance de Map en une chaîne de caractères.
    * 
    * @param pMap
    *           La Map à convertir.
    * @return La chaîne de caractères désirée.
    * @example
    *          System.out.println(MapUtils.toString(vMap));
    */
   public static String toString (Map<?, ?> pMap) {
      String vContenu = "";
      String vReturn;
      final String cSeparator = ", ";

      if (pMap.size() == 0) {
         vReturn = "Map vide";
      }
      else {
         // Obtenir l'itérator des clés
         @SuppressWarnings("rawtypes")
         Iterator vItKeys = pMap.keySet().iterator();
         Object vKey;
         Object vValue;
         while (vItKeys.hasNext()) {
            // Obtenir la clé courante
            vKey = vItKeys.next();
            // Obtenir la valeur courante
            vValue = pMap.get(vKey);

            vContenu = vContenu + vKey + " = '" + vValue + "'" + cSeparator;
         }

         // Enlever le dernier séparateur
         vContenu = vContenu.substring(0, vContenu.length() - cSeparator.length());
         // Constituer la chaîne résultat
         vReturn = "[" + pMap.size() + " elem]=[" + vContenu + "]";
      }

      return vReturn;
   }
}
