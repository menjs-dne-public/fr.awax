package fr.awax.acceleo.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fr.awax.acceleo.common.conf.AllPropAbstract;
import fr.awax.acceleo.common.conf.AllPropHelper;
import fr.awax.acceleo.common.log.LogLevelEnum;

/**
 * Propose des méthodes pour gérer le log et faciliter le débuggage de la
 * génération.
 */
public class LogUtils {
//   public enum ELevel {
//      TRACE,
//      DEBUG,
//      INFO,
//      WARNING,
//      ERROR;
//   }
   private String _PlugInId;

   private LogLevelEnum level = LogLevelEnum.INFO;

   /**
    * Formate la date dans les logs.
    */
   private DateFormat dtf = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss.S");

   private static Map<String, LogUtils> _MapInstances = new HashMap<>();

   public static LogUtils getInstance (String pPlugInId) {
      LogUtils instance = _MapInstances.get(pPlugInId);
      if (instance == null) {
         instance = new LogUtils();
         instance._PlugInId = pPlugInId;
         _MapInstances.put(pPlugInId, instance);
      }
      return instance;
   }

   private LogUtils() {
      // RAS
   }

   /**
    * Affiche un message de debug sous la forme :
    * 
    * <pre>
    * DD-MM-YYYY HH:mm - DEBUG - message
    * 	Veuillez envisager les solutions suivantes :
    * 		- Solution 1
    * 		- Solution 2
    * </pre>
    * 
    * @param message
    * @param solutions
    */
   public void debug (String message, String... solutions) {
      if (level.ordinal() <= LogLevelEnum.DEBUG.ordinal()) {
         afficherConsoleEclipse(dtf.format(new Date()) + " - DEBUG - " + message);

         if (solutions.length > 0) {
            afficherConsoleEclipse("\tVeuillez envisager les solutions suivantes :");
            for (String solution : solutions) {
               afficherConsoleEclipse("\t\t - " + solution);
            }
         }
      }
   }

   /**
    * Affiche un message d'info sous la forme :
    * 
    * <pre>
    * DD-MM-YYYY HH:mm - DEBUG - message
    * 	Veuillez envisager les solutions suivantes :
    * 		- Solution 1
    * 		- Solution 2
    * </pre>
    * 
    * @param message
    * @param solutions
    */
   public void info (String message, String... solutions) {
      if (level.ordinal() <= LogLevelEnum.INFO.ordinal()) {
         afficherConsoleEclipse(dtf.format(new Date()) + " - INFO - " + message);

         if (solutions.length > 0) {
            afficherConsoleEclipse("\tVeuillez envisager les solutions suivantes :");
            for (String solution : solutions) {
               afficherConsoleEclipse("\t\t - " + solution);
            }
         }
      }
   }

   /**
    * Affiche un message de warning sous la forme :
    * 
    * <pre>
    * DD-MM-YYYY HH:mm - WARNING - message
    * 	Veuillez envisager les solutions suivantes :
    * 		- Solution 1
    * 		- Solution 2
    * </pre>
    * 
    * @param message
    * @param solutions
    */
   public void warn (String message, String... solutions) {
      if (level.ordinal() <= LogLevelEnum.WARNING.ordinal()) {
         System.err.println(dtf.format(new Date()) + " - WARNING - " + message);

         if (solutions.length > 0) {
            System.err.println("\tVeuillez envisager les solutions suivantes :");
            for (String solution : solutions) {
               System.err.println("\t\t - " + solution);
            }
         }
      }
   }

   /**
    * Permet de savoir s'il faut logguer le message à partir de la valeur spécifiée dans le Properties.
    * @param pLogLevelOfMessage Le niveau de LOG du message.
    * @return 'true' s'il faut logguer.
    */
   public boolean isLogMessage (LogLevelEnum pLogLevelOfMessage) {
      boolean vIsReturn;
      String vLogLevel;
      try {
         AllPropAbstract allProp = AllPropHelper.getInstance(_PlugInId);
         vLogLevel = allProp.propCommon().generation_log.getValue().toString();
      }
      catch (Exception e) {
         // Par défaut : on log tout
         vLogLevel = LogLevelEnum.TRACE.name();
         afficherConsoleEclipse("Problème pour obtenir les infos du projet, niveau de log mis par défaut : vLogLevel=" + vLogLevel);
         e.printStackTrace();
      }
      LogLevelEnum vLogLevelEnum = LogLevelEnum.valueOf(vLogLevel);
      if (vLogLevelEnum == null) {
         throw new IllegalArgumentException("Erreur dans le fichier de paramétrage : " + vLogLevel + " n'est pas une valeur autorisée pour '" + AllPropHelper.PROP_COMMON.generation_log + "'");
      }

      // S'il faut LOGGUER
      if (pLogLevelOfMessage.ordinal() >= vLogLevelEnum.ordinal()) {
         vIsReturn = true;
      }
      else {
         vIsReturn = false;
      }

      return vIsReturn;
   }

   private void afficherConsoleEclipse (String pMessage) {
      System.out.println(pMessage);
   }

   /**
    * Permet de savoir si l'on en dans un niveau de log DEBUG.
    * @return 'true' si mode DEBUG.
    */
   public boolean isLogLevelDebug () {
      boolean vIsReturn;
      AllPropAbstract allProp = AllPropHelper.getInstance(_PlugInId);
      String vLogLevel = allProp.propCommon().generation_log.getValue().toString();
      LogLevelEnum vLogLevelEnum = LogLevelEnum.valueOf(vLogLevel);
      // SI niveau DEBUG (ou inférieur) 
      if (vLogLevelEnum.ordinal() <= LogLevelEnum.DEBUG.ordinal()) {
         vIsReturn = true;
      }
      else {
         vIsReturn = false;
      }

      return vIsReturn;
   }
}
