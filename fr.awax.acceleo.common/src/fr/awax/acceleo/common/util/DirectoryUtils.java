package fr.awax.acceleo.common.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DirectoryUtils {
   final static String cProtocoleFile = "file:/";

   final static String cProtocolePlateformResource = "platform:/resource/";

   public static String uniformPath (final String pPath) {
      String vReturn;
      String path = pPath;
      if (path == null) {
         vReturn = null;
      }
      else {
         String vPathProtocole = getProtocole(path);
         String vPathWithoutProtocole = getWithoutProtocole(path);
         vPathWithoutProtocole = vPathWithoutProtocole.replace('\\', '/');
         vPathWithoutProtocole = vPathWithoutProtocole.replaceAll("//", "/");
         // Reconstituer le chemin complet
         vReturn = vPathProtocole + vPathWithoutProtocole;
         // SI répertoire
         if (vReturn.endsWith("/")) {
            // Laisser un seul slash à la fin
            int rang = vReturn.length() - 1;
            while (rang >= 0 && vReturn.charAt(rang) == '/') {
               rang--;
            }
            if (rang > 0) {
               vReturn = vReturn.substring(0, rang + 1) + "/";
            }
         }
      }

      return vReturn;
   }

   /**
    * Obtenir le dernier répertoire (qui peut-être un nom de projet).
    * 
    * @param pPath Le chemin à regarder (répertoire ou fichier)
    * @return Le dernier répertoire.
    */
   public static String getLastDirectory (final String pPath) {
      String vReturn;

      try {
         // Obtenir le path normaliser :
         // - soit : platform:/resource/sample4menus/Sirhen_D01.entity
         // - soit : file:/C:/WS_ProMinEdu/sample1-model/Sirhen_D01.entity
         String vPath = uniformPath(pPath);

         final String cSlash = "/";
         List<Integer> vTabPosSlash = new ArrayList<Integer>();
         int vRgCour = 0;
         // Remplir les positions du "/"
         while ((vRgCour = vPath.indexOf(cSlash, vRgCour)) >= 0) {
            // Ajouter le rang
            vTabPosSlash.add(vRgCour);
            // Passer au "/" suivant
            vRgCour++;
         }

         int vRgDeb;
         int vRgFin;
         // Si aucun "/"
         if (vTabPosSlash.size() == 0) {
            vReturn = null;
            // throw new IllegalArgumentException("Impossible de déterminer le dernier répertoire avec le chemin : " + pPath);
         }
         // Si le path se termine par un "/" avec un seul répertoire (ex : "sample1-model/")
         else if (vPath.endsWith(cSlash) && vTabPosSlash.size() == 1) {
            vRgDeb = 0;
            vRgFin = vTabPosSlash.get(vTabPosSlash.size() - 1);
            vReturn = vPath.substring(vRgDeb, vRgFin);
         }
         // Si le path se termine par un "/" (ex : "file:/C:/WS_ProMinEdu/sample1-model/")
         else if (vPath.endsWith(cSlash) && vTabPosSlash.size() >= 2) {
            vRgDeb = vTabPosSlash.get(vTabPosSlash.size() - 2) + 1;
            vRgFin = vTabPosSlash.get(vTabPosSlash.size() - 1);
            vReturn = vPath.substring(vRgDeb, vRgFin);
         }
         else {
            // SI au moins 2 "/"
            if (vTabPosSlash.size() - 2 >= 0) {
               vRgDeb = vTabPosSlash.get(vTabPosSlash.size() - 2) + 1;
               vRgFin = vTabPosSlash.get(vTabPosSlash.size() - 1);
               vReturn = vPath.substring(vRgDeb, vRgFin);
            }
            // SI un seul "/" ou aucun
            else {
               // Ex : "/createEnseignant.html"
               if (vPath.startsWith(cSlash)) {
                  vReturn = null;
               }
               // Ex : "createEnseignant.html"
               else {
                  vRgDeb = 0;
                  vRgFin = vTabPosSlash.get(0);
                  vReturn = vPath.substring(vRgDeb, vRgFin);
               }
            }
         }
      }
      catch (Exception err) {
         throw new RuntimeException("Problème avec pPath=" + pPath, err);
      }

      return vReturn;
   }

   /**
    * Obtenir le chemin sans le dernier répertoire.
    * 
    * @param pPath Le chemin d'origine.
    * @return Le chemin sans le dernier répertoire.
    */
   public static String getAbsolutePathWithoutLastDir (String pPath) {
      String vReturn;

      vReturn = replaceLastDirectory(pPath, "");
      // Si "//" à la fin
      if (vReturn.endsWith("//")) {
         vReturn = vReturn.substring(0, vReturn.length() - "//".length()) + "/";
      }

      vReturn = uniformPath(vReturn);

      return vReturn;
   }

   /**
    * Remplacer le nom du dernier répertoire par un nouveau nom.
    * 
    * @param pPath Le chmin d'origine (ex :
    *           "file:\\C:\\workspace\\sample1-model\\Sirhen_D01.entity").
    * @param pNewNameLastDirectory Le nouveau nom (ex : "sample2").
    * @return Le chemin substitué (ex :
    *         "file:\\C:\\workspace\\sample2\\Sirhen_D01.entity").
    */
   public static String replaceLastDirectory (final String pPath, final String pNewNameLastDirectory) {
      String vReturn;

      // Obtenir le path normaliser :
      // - soit : platform:/resource/sample4menus/Sirhen_D01.entity
      // - soit : file://C:/WS_ProMinEdu/sample1-model/Sirhen_D01.entity
      String vPath = uniformPath(pPath);

      // Obtenir le nom du dernier répertoire du chemin
      String vLastDirectory = getLastDirectory(vPath);

      int vRgDeb = vPath.indexOf(vLastDirectory);
      int vRgFin = vRgDeb + vLastDirectory.length();

      vReturn = vPath.substring(0, vRgDeb) + pNewNameLastDirectory + vPath.substring(vRgFin, vPath.length());
      // Obtenir le path normaliser :
      vReturn = uniformPath(vReturn);

      return vReturn;
   }

   /**
    * Obtenir le protocole du chemin ou du fichier.
    * 
    * @param pPath Le chemin (ex :
    *           "file:/C:/workspace/sample1-model/Sirhen_D01.entity").
    * @return Le protocole (ex : "file:/") ou "" inconnu.
    */
   public static String getProtocole (final String pPath) {
      String vReturn;

      vReturn = pPath;
      // Procole file (ex : "file:/C:/workspace/sample1-model/Sirhen_D01.entity")
      if (vReturn.startsWith(cProtocoleFile)) {
         vReturn = cProtocoleFile;
      }
      // Procole file (ex :
      // "platform:/resource/C:/workspace/sample1-model/Sirhen_D01.entity")
      else if (vReturn.startsWith(cProtocolePlateformResource)) {
         vReturn = cProtocolePlateformResource;
      }
      // Sinon : pas de traitement préalable
      else {
         vReturn = "";
      }

      return vReturn;

   }

   /**
    * Enlever le protocole du chemin ou du fichier.
    * 
    * @param pPath Le chemin (ex :
    *           "file:\\C:\\workspace\\sample1-model\\Sirhen_D01.entity").
    * @return Le nom du fichier sans le protocole (ex :
    *         C:/WS_ProMinEdu/sample1-model/Sirhen_D01.entity).
    */
   public static String getWithoutProtocole (final String pPath) {
      String vReturn;

      vReturn = pPath;
      if (vReturn.startsWith(cProtocoleFile)) {
         // Supprimer le protocole
         vReturn = vReturn.substring(cProtocoleFile.length(), vReturn.length());
      }
      // Procole file (ex :
      // "platform:/resource/C:/workspace/sample1-model/Sirhen_D01.entity")
      else if (vReturn.startsWith(cProtocolePlateformResource)) {
         // Supprimer le protocole
         vReturn = vReturn.substring(cProtocolePlateformResource.length(), vReturn.length());
      }
      // Sinon : pas de traitement préalable
      else {
         // RAS
      }

      return vReturn;
   }

   /**
    * Obtenir le chemin en absolu. Important - la règle est simple : - Si ça se
    * termine par un "/" ou "\" c'est un répertoire à la fin. - Sinon le dernier
    * élément est un fichier et sera supprimé du retour.
    * 
    * @param pPath Le chemin à regarder (répertoire ou fichier).
    * @return Le chemin en absolu (sans le fichier éventuel).
    */
   public static String getAbsolutePath (final String pPath) {
      String vReturn;

      if (pPath == null) {
         vReturn = null;
      }
      else if (".".equals(pPath)) {
         File vFile = new File(pPath);
         vReturn = vFile.getAbsolutePath();
         if (vReturn.endsWith(".")) {
            // Supprimer le "." de la fin
            vReturn = vReturn.substring(0, vReturn.length() - ".".length());
         }
      }
      else {
         // Obtenir le path normaliser :
         // - soit : platform:/resource/sample4menus/Sirhen_D01.entity
         // - soit : file:/C:/WS_ProMinEdu/sample1-model/Sirhen_D01.entity
         String vPath = uniformPath(pPath);
         // Obtenir le path sans le protocole
         vPath = getWithoutProtocole(vPath);

         final String cSlash = "/";
         List<Integer> vTabPosSlash = new ArrayList<Integer>();
         int vRgCour = 0;
         // Remplir les positions du "/"
         while ((vRgCour = vPath.indexOf(cSlash, vRgCour)) >= 0) {
            // Ajouter le rang
            vTabPosSlash.add(vRgCour);
            // Passer au "/" suivant
            vRgCour++;
         }

         int vRgDeb;
         int vRgFin;
         // Si aucun "/"
         if (vTabPosSlash.size() == 0) {
            throw new IllegalArgumentException("Impossible de déterminer un répertoire en absolu avec le chemin : "
                  + pPath + " (vPath=" + vPath + ")");
         }
         // Si le path se termine par un "/" (ex : "C:/WS_ProMinEdu/sample1-model/")
         else if (vPath.endsWith(cSlash)) {
            vRgDeb = 0;
            vRgFin = vPath.length();
            vReturn = vPath.substring(vRgDeb, vRgFin);
         }
         // Sinon le path se termine par un fichier (ex :
         // "C:/WS_ProMinEdu/sample1-model/Sirhen_D01.entity")
         else {
            vRgDeb = 0;
            vRgFin = vTabPosSlash.get(vTabPosSlash.size() - 1);
            vReturn = vPath.substring(vRgDeb, vRgFin + 1);
         }
      }

      return vReturn;
   }

   /**
    * Formater un répertoire avec des "/" (à la place de "\") et un "/" à la fin.
    * 
    * @param pDirectoryPath Le répertoire brut (ex : "d:\rep1\rep2")
    * @return Le chemin formaté (ex : "d:/rep1/rep2/").
    */
   public static String formatDirectory (String pDirectoryPath) {
      String vReturn;

      String vPath = pDirectoryPath.replace('\\', '/');
      vPath = vPath.replaceAll("//", "/");
      // SI se termine par un "/"
      if (vPath.endsWith("/")) {
         vReturn = vPath;
      }
      else {
         vReturn = vPath + "/";
      }

      // SI se termine par un "."
      if (vPath.endsWith(".")) {
         vReturn = vPath.substring(0, vPath.length() - ".".length()) + "/";
      }

      return vReturn;
   }

   /**
    * Supprimer récurivement un répertoire.
    * 
    * @param pAbsolutePath Le répertoire à supprimer.
    * @throws IOException
    */
   public static void deleteDirRecursif (String pAbsolutePath) throws IOException {
      Files.walk(Paths.get(pAbsolutePath)).map(Path::toFile).sorted( (o1, o2) -> -o1.compareTo(o2))
            .forEach(File::delete);
   }

   /**
    * Obtenir la liste des fichiers ayant l'extension désirée.
    * 
    * @param pPathSearch Le path de recherche.
    * @param pExtensionFile L'extension recherchée (ex : ".properties").
    * @return La liste désirée.
    */
   public static List<File> findAllFile (String pPathSearch, String pExtensionFile) {
      List<File> vReturn = new ArrayList<File>();

      File vFilePathSearch = new File(pPathSearch);
      for (File vFile : vFilePathSearch.listFiles()) {
         if (vFile.getAbsolutePath().endsWith(pExtensionFile)) {
            vReturn.add(vFile);
         }
      }

      return vReturn;
   }

   /**
    * Obtenir le nom du fichier dans le path sans son extension.
    * 
    * @param pPath Le chemin (ex :
    *           "file:\\C:\\workspace\\sample1-model\\Sirhen_D01.entity").
    * @return Le nom du fichier (ex : "Sirhen_D01") ou 'null' si pas trouvé.
    */
   public static String getFileWithoutExtension (final String pPath) {
      String vReturn;
      String vFileName = getFile(pPath);

      int rang = vFileName.length();
      char car;
      do {
         rang--;
         car = vFileName.charAt(rang);
      } while (car != '.' && rang > 0);
      int vRgPoint = rang;
      // Si on a un "."
      if (vRgPoint > 0) {
         vReturn = vFileName.substring(0, vRgPoint);
      }
      else {
         vReturn = pPath;
      }

      // Recherche un '.' restant dans le nom (ex : "sample4menus.Sirhen_D01.entity")
      rang = vReturn.length();
      do {
         rang--;
         car = vReturn.charAt(rang);
      } while (car != '.' && rang > 0);
      vRgPoint = rang;
      // Si on a un "."
      if (vRgPoint > 0) {
         vReturn = vReturn.substring(vRgPoint + ".".length(), vReturn.length());
      }
      else {
         vReturn = vReturn;
      }

      return vReturn;
   }

   public static String getFileExtension (final String pPath) {
      String vReturn;

      String vFileName = getFile(pPath);

      int rang = pPath.length();
      char car;
      do {
         rang--;
         car = pPath.charAt(rang);
      } while (car != '.' && rang > 0);
      int vRgPoint = rang;
      // Si on a un "."
      if (vRgPoint > 0) {
         vReturn = pPath.substring(vRgPoint, pPath.length());
      }
      else {
         vReturn = pPath;
      }

      return vReturn;
   }

   /**
    * Obtenir le nom du fichier dans le path.
    * 
    * @param pPath Le chemin (ex :
    *           "file:\\C:\\workspace\\sample1-model\\Sirhen_D01.entity").
    * @return Le nom du fichier (ex : "Sirhen_D01.entity") ou 'null' si pas trouvé.
    */
   public static String getFile (final String pPath) {
      String vReturn;

      String vLastDirectory = getLastDirectory(pPath);
      // Si pas trouvé
      if (vLastDirectory == null) {
         // Si pas de chemin (ex : pPath="")
         if (pPath.isEmpty()) {
            vReturn = null;
         }
         else {
            // Le chemin est uniquement un fichier (ex : pPath="Sirhen_D01.entity")
            vReturn = pPath;
         }
      }
      else {
         // Obtenir le rang du début du sous-répertoire
         // Ex : pPath = "file://C:/workspace/sample1-model/Sirhen_D01.entity"
         // vLastDirectory = "sample1-model"
         // --> vRgStart = 19
         int vRgStart = pPath.indexOf(vLastDirectory);
         vReturn = pPath.substring(vRgStart + vLastDirectory.length() + "/".length(), pPath.length());

         // Si pas de fichier
         if (vReturn.isEmpty()) {
            vReturn = null;
         }
      }

      return vReturn;
   }

   /**
    * Obtenir le chemin d'exécution.
    * 
    * @param pThis L'instance de la classe permettant de déterminer le chemin
    *           d'exécution.
    * @return Le chemin d'exécution.
    * @example: String vPath = DirectoryUtils.getExecutionRootDirectory(this);
    */
   public static String getExecutionRootDirectory (Object pThis) {
      String vReturn;

      String vPathBrut;
      String vPathInstance = pThis.getClass()
            .getResource('/' + pThis.getClass().getName().replace('.', '/') + ".class").toString();
      // Si la classe est dans un jar
      if (vPathInstance.indexOf('!') > 0) {
         int x = vPathInstance.lastIndexOf('/', vPathInstance.lastIndexOf('!'));
         File vFile = new File(vPathInstance.substring(10, x + 1).replace('/', '\\'));
         vPathBrut = uniformPath(vFile.toString());
      }
      // Si la classe est dans un répertoire
      else {
         // Ex :
         // "file:/T:/WSO_ProMinEdu/men.gen.sirhen.persistence/bin/men/gen/sirhen/persistence/files/FileAbstractEntity_TestMacro.class"
         vPathBrut = uniformPath(vPathInstance);
      }

      // Ex : "men/gen/sirhen/persistence/files/FileAbstractEntity_TestMacro.class"
      String vPackagePath = pThis.getClass().getName().replace('.', '/') + ".class";
      int vRg = vPathBrut.indexOf(vPackagePath);
      // Si trouvé
      if (vRg >= 0) {
         // Ex : "file:/T:/WSO_ProMinEdu/men.gen.sirhen.persistence/bin/"
         vReturn = vPathBrut.substring(0, vRg);
      }
      else {
         throw new IllegalArgumentException("ERREUR APPLICATION : Package vPackagePath=\"" + vPackagePath
               + "\" nontrouvé dans vPathBrut=\"" + vPathBrut + "\"");
      }

      return vReturn;
   }

   /**
    * Obtenir le chemin inverse en relatif.
    * 
    * @param pAbsolutePath Le chemin en absolu (ex : "rep1/rep2/rep3/")
    * @return Le chemin inverse en relatif (ex : "../../../").
    */
   public static String getRelatifPathBack (String pAbsolutePath) {
      String vReturn = "";
      int rang = pAbsolutePath.length();
      if (rang > 0) {
         char car;
         List<Integer> listRang = new ArrayList<>();
         do {
            rang--;
            car = pAbsolutePath.charAt(rang);
            if (car == '/') {
               listRang.add(rang);
            }
         } while (rang > 0);

         // Constituer le chemin inverse en relatif
         for (Integer rangSlash : listRang) {
            vReturn = vReturn + "../";
         }

      }

      return vReturn;
   }

   /**
    * Obtenir tous les chemins à partir du chemin spécifié.
    * 
    * @param pPath Le path à regarder (ex : "/rep1/rep2/rep3/").
    * @return La liste désirée (ex : {"/", "/rep1/", "/rep1/rep2/",
    *         "/rep1/rep2/rep3/"}).
    */
   public static List<String> getListDirectory (String pPath) {
      List<String> vReturn = new ArrayList<>();

      if ((pPath != null) && (false == "".equals(pPath))) {
         String pathCour = "";
         char car;
         int rang = 0;
         do {
            car = pPath.charAt(rang);
            rang++;
            if (car == '/') {
               pathCour = pPath.substring(0, rang);
               vReturn.add(pathCour);
            }
         } while (rang < pPath.length());
      }

      return vReturn;
   }

   /**
    * Obtenir tous les répertoires unitaires à partir du chemin spécifié.
    * 
    * @param pPath Le path à regarder (ex : "/rep1/rep2/rep3/").
    * @return La liste désirée (ex : {"/", "rep1/", "rep2/", "rep3/"}).
    */
   public static List<String> getListDirectoryUnitaire (String pPath) {
      List<String> vReturn = new ArrayList<>();

      if ((pPath != null) && (false == "".equals(pPath))) {
         String pathCour = "";
         char car;
         int rangPrec = 0;
         int rang = 0;
         do {
            car = pPath.charAt(rang);
            rang++;
            if (car == '/') {
               pathCour = pPath.substring(rangPrec, rang);
               vReturn.add(pathCour);
               rangPrec = rang;
            }
         } while (rang < pPath.length());
      }

      return vReturn;
   }

   /**
    * Permet d'uniformiser le path.
    * 
    * @param pPath Le path à regarder.
    * @return Le patth uniformisé.
    */
   public static String sanitizedPath (String pPath) {
      String vReturn = pPath;
      if (pPath == null) {
         vReturn = null;
      }
      else {
         do {
            // Remplacer "\" par "/"
            vReturn = StringUtils.replace(vReturn, "\\", "/");
         } while (vReturn.indexOf("\\") >= 0);

         do {
            // Remplacer "//" par "/"
            vReturn = StringUtils.replace(vReturn, "//", "/");
         } while (vReturn.indexOf("//") >= 0);

      }
      return vReturn;
   }

   /**
    * Obtenir le delta représentant le path en relatif en fonction de la source ou l'on est.
    * 
    * @param pPathSource Le path source ou l'on est (ex : "viewcontainer/enseignant/EcrChoisirEnseignant.html").
    * @param pPathDest Le path destination ou l'on désire aller (ex : "viewcontainer/enseignant/titulaire/EcrSaisirEnseignantTitulaire.html").
    * @return Le path destination en relatif désiré (ex : "titulaire/EcrSaisirEnseignantTitulaire.html").
    */
   public static String getDeltaRelatifPath (String pPathSource, String pPathDest) {
      String vReturn = null;

      if (pPathSource == null) {
         vReturn = null;
      }
      else if (pPathDest == null) {
         vReturn = null;
      }
      else {
         vReturn = "";
         List<String> listPathSource = getListDirectoryUnitaire(pPathSource);
         List<String> listPathDest = getListDirectoryUnitaire(pPathDest);
         String fileDest = getFile(pPathDest);
         int rang = 0;

         // SI les 2 path ont une partie commune
         if (isStartsWhithCommon(pPathSource, pPathDest)) {
            // Ex1 : vReturn = "titulaire/EcrSaisirEnseignantTitulaire.html"
            // pPathSource = "viewcontainer/enseignant/EcrChoisirEnseignant.html"
            // pPathDest   = "viewcontainer/enseignant/titulaire/EcrSaisirEnseignantTitulaire.html"
            if (listPathSource.size() <= listPathDest.size()) {
               String repSource;
               for (String repDest : listPathDest) {
                  if (rang < listPathSource.size()) {
                     repSource = listPathSource.get(rang);
                     // SI le répertoire n'existe pas dans le path source et le path destination
                     if (false == repSource.equals(repDest)) {
                        for (int i = rang; i < listPathDest.size(); i++) {
                           vReturn = vReturn + listPathDest.get(i);
                        }
                        break;
                     }
                     rang++;
                  }
                  else {
                     for (int i = rang; i < listPathDest.size(); i++) {
                        vReturn = vReturn + listPathDest.get(i);
                     }
                     break;
                  }
               }
            }
            // Ex2 : vReturn = "../EcrSaisirEnseignantTitulaire.html"
            // pPathSource = "viewcontainer/enseignant/titulaire/EcrSaisirEnseignantTitulaire.html"
            // pPathDest   = "viewcontainer/enseignant/EcrChoisirEnseignant.html"
            else {
               for (String repSource : listPathSource) {
                  if (rang < listPathDest.size()) {
                     String repDest = listPathDest.get(rang);
                     // SI le répertoire n'existe pas dans le path Dest et le path Sourceination
                     if (false == repDest.equals(repSource)) {
                        for (int i = rang; i < listPathSource.size(); i++) {
                           vReturn = vReturn + listPathSource.get(i);
                        }
                        break;
                     }
                     rang++;
                  }
                  else {
                     for (int i = rang; i < listPathSource.size(); i++) {
                        vReturn = vReturn + "../";
                     }
                     break;
                  }
               }
            }

            if (fileDest == null) {
               vReturn = sanitizedPath(vReturn);
            }
            else {
               // Ex3 : vReturn = "EcrSaisirEnseignantTitulaire.html"
               // pPathSource = "viewcontainer/enseignant/EcrChoisirEnseignant.html"
               // pPathDest   = "viewcontainer/enseignant/EcrSaisirEnseignantTitulaire.html"
               if ("".equals(vReturn)) {
                  vReturn = fileDest;
               }
               else {
                  vReturn = vReturn + "/" + fileDest;
               }
            }
         }
         // SINON les 2 path n'ont pas de partie commune, et il faut remonter
         else {
            int nbRepSource = listPathSource.size();
            vReturn = "";
            for (int i = 0; i < nbRepSource; i++) {
               vReturn = vReturn + "../";
            }

            // SI path destination en absolu
            if (pPathDest.startsWith("/")) {
               vReturn = vReturn + fileDest;
            }
            // SINON le path destination est en relatif
            else {
               vReturn = vReturn + pPathDest;
            }
         }
      }
      vReturn = sanitizedPath(vReturn);

      return vReturn;
   }

   /**
    * Permet de savoir si les 2 path ont partie commune (ou pas).
    * @param pPathSource Le path source.
    * @param pPathDest Le path destination
    * @return 'true' s'il y a une partie commune (ex : "/rep1/rep2/file1.txt" et "/rep2/file2.txt")
    *         ou pas (ex : ""/rep1/rep2/file1.txt" et "rep3/file2.txt").
    */
   private static boolean isStartsWhithCommon (String pPathSource, String pPathDest) {
      boolean vReturn;

      int rang = 0;
      char carSource;
      char carDest;
      while (rang < pPathSource.length() && rang < pPathDest.length()) {
         carSource = pPathSource.charAt(rang);
         carDest = pPathDest.charAt(rang);
         if (carSource != carDest) {
            break;
         }
         rang++;
      }

      // SI aucune partie commune
      if (rang == 0) {
         vReturn = false;
      }
      else {
         vReturn = true;
      }
      return vReturn;
   }

}
