package fr.awax.acceleo.common.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class StringUtils {
   private static final String cPlainAscii = "AaEeIiOoUu" // grave
         + "AaEeIiOoUuYy" // acute
         + "AaEeIiOoUuYy" // circumflex
         + "AaOoNn" // tilde
         + "AaEeIiOoUuYy" // umlaut
         + "Aa" // ring
         + "Cc" // cedilla
         + "OoUu"; // double acute

   private final static String cUnicode = "\u00C0\u00E0\u00C8\u00E8\u00CC\u00EC\u00D2\u00F2\u00D9\u00F9" + "\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DD\u00FD" + "\u00C2\u00E2\u00CA\u00EA\u00CE\u00EE\u00D4\u00F4\u00DB\u00FB\u0176\u0177" + "\u00C3\u00E3\u00D5\u00F5\u00D1\u00F1" + "\u00C4\u00E4\u00CB\u00EB\u00CF\u00EF\u00D6\u00F6\u00DC\u00FC\u0178\u00FF" + "\u00C5\u00E5" + "\u00C7\u00E7" + "\u0150\u0151\u0170\u0171";

   /**
    * Supprime les caractères spéciaux et les remplace par leurs équivalents.
    * 
    * @param pText
    *           la chaine de caractère à normaliser
    * @return la chaine normalisée
    */
   public static String normalize (final String pText) {
      if (pText == null) {
         return null;
      }
      final StringBuilder buffer = new StringBuilder();
      final int n = pText.length();
      for (int i = 0; i < n; i++) {
         final char carac = pText.charAt(i);
         final int pos = cUnicode.indexOf(carac);
         if (pos > -1) {
            buffer.append(cPlainAscii.charAt(pos));
         }
         else {
            buffer.append(carac);
         }
      }
      return buffer.toString();
   }

   public static String toLowerFirstLetter (String pString) {
      String vReturn;

      // Mettre en minuscule
      String vStringLower = pString.toLowerCase();

      if (vStringLower.length() > 1) {
         // Formater avec le 1er caractère en minuscule
         vReturn = vStringLower.charAt(0) + pString.substring(1, pString.length());
      }
      else {
         vReturn = vStringLower.toLowerCase();
      }

      return vReturn;
   }

   public static String toUpperFirstLetter (String pString) {
      String vReturn;

      // Mettre en majuscule
      String vStringUpper = pString.toUpperCase();

      if (vStringUpper.length() > 1) {
         // Formater avec le 1er caractère en majuscule
         vReturn = vStringUpper.charAt(0) + pString.substring(1, pString.length());
      }
      else {
         vReturn = vStringUpper.toUpperCase();
      }

      return vReturn;
   }

   /**
    * Obtenir la 'String' en UTF-8.
    * @param pStringOri La chaîne originale.
    * @return La chaîne en UTF-8.
    */
   public static String getStringUtf8 (String pStringOri) {
      try {
         return new String(pStringOri.getBytes(), "UTF-8");
      }
      catch (UnsupportedEncodingException vErr) {
         throw new IllegalArgumentException("Problème pour faire l'encodage en UTF-8", vErr);
      }
   }

   /**
    * Permet de splitter un chaîne de caractères par rapport à un séparateur.
    * 
    * @example
    *          <code>
    * String[] vTabResult = StringUtils.split("\"nom\"=\"BONO\"||\"prenom\"=\"Jean\"||\"dateNaissance\"=\"Fri Oct 25 11:43:34 CEST 2013\"", "||");
    * </code>
    * @param pString La chaîne originale.
    * @param pSep Le séparateur.
    * @return Le tableau splitté.
    */
   public static String[] split (String pString, String pSep) {
      String[] vReturn;
      List<String> vLstElement = new ArrayList<String>();

      String vElement;
      int vRgStartElement = 0;
      // Obtenir le rang de séparation entre le nom et la valeur
      int vRgSepNomEtValeur = pString.indexOf(pSep);
      while (vRgSepNomEtValeur >= 0) {
         // Obtenir la partie droite
         vElement = pString.substring(vRgStartElement, vRgSepNomEtValeur);
         // Ajouter l'élément
         vLstElement.add(vElement);

         // Obtenir le rang de l'éventuel séparateur suivant
         vRgStartElement = vRgSepNomEtValeur + pSep.length();
         vRgSepNomEtValeur = pString.indexOf(pSep, vRgSepNomEtValeur + pSep.length());
      }

      // Obtenir le dernier élément
      vElement = pString.substring(vRgStartElement, pString.length());
      // Ajouter l'élément
      vLstElement.add(vElement);

      // List --> String[]
      vReturn = vLstElement.toArray(new String[vLstElement.size()]);

      return vReturn;
   }

   /**
    * Faire une substitution dans la chaîne de caractères.
    * 
    * @param pStringOri
    *           La chaîne originale.
    * @param pOldOccu
    *           L'occurance à rechercher.
    * @param pNewOccu
    *           L'occurance à remplacer.
    * @return La chaîne avec les substitution.
    * @example
    *          String vNewString = XString.replace("début ???, ???, ??? fin",
    *          "???", "!!!");
    */
   public static String replace (String pStringOri, String pOldOccu, String pNewOccu) {
      final StringBuffer v_buf = new StringBuffer();

      // Conversion des "p_oldString" par des "p_newString"
      int v_rang1 = pStringOri.indexOf(pOldOccu);
      int v_rang2 = 0;
      while (v_rang1 >= 0) {
         v_buf.append(pStringOri.substring(v_rang2, v_rang1) + pNewOccu);
         v_rang2 = v_rang1 + pOldOccu.length();
         v_rang1 = pStringOri.indexOf(pOldOccu, v_rang2);
      }
      v_buf.append(pStringOri.substring(v_rang2, pStringOri.length()));

      return v_buf.toString();
   } // FIN "replace()"

   /**
    * Permet de savoir si une chaîne n'est pas vide ou nulle.
    * @param pValue La chaîne a regarder.
    * @return 'true' si pas vide et pas null.
    */
   public static boolean isNotEmpty (String pValue) {
      return !isEmpty(pValue);
   }

   /**
    * Permet de savoir si une chaîne est vide ou nulle.
    * @param pValue La chaîne a regarder.
    * @return 'true' si vide ou null.
    */
   public static boolean isEmpty (String pValue) {
      boolean vReturn;

      if (pValue == null || "".equals(pValue)) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }

      return vReturn;
   }

}
