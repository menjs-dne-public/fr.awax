package fr.awax.acceleo.common.util;

import java.util.UUID;

/**
 * Classe utilitaire pour manipuler un UUID (= Universally Unique IDentifier).
 */
public class UuidUtils {
   private static long _UniqueIdentifierFromInstance = 0;

   /**
    * Obtenir un identifiant unique.
    * @return La chaîne représentant l'identifiant unique.
    */
   public static String getUniqueIdentifier () {
//      return EcoreUtil.generateUUID()
      return UUID.randomUUID().toString();
   }

   /**
    * Obtenir un identifiant unique au sein de la JVM.
    * @return La chaîne représentant l'identifiant unique.
    */
   public static String getUniqueIdentifierFromInstance () {
      _UniqueIdentifierFromInstance++;
      return "" + _UniqueIdentifierFromInstance;
   }

}
