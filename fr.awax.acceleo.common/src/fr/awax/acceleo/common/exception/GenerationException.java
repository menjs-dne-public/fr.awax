package fr.awax.acceleo.common.exception;

/**
 * L'exception Runtime de génération.
 * @author MEN
 */
public class GenerationException extends Exception {

   /** Pour la sérialisation */
   private static final long serialVersionUID = 1L;
   /** La solution potentielle */
   private String _SolutionPotentielle;

   /**
    * Constructeur avec exception racine.
    * 
    * @param pMessage
    *           Le message d'erreur
    * @param pRootCause
    *           L'exception racine.
    * @param pSolutionPotentielle
    *           La solution potentielle.
    */
   public GenerationException(String pMessage, Throwable pRootCause, String pSolutionPotentielle) {
      super(pMessage, pRootCause);
      _SolutionPotentielle = pSolutionPotentielle;
   }

   public GenerationException(String pMessage, Throwable pRootCause) {
      this(pMessage, pRootCause, null);
   }

   /**
    * Constructeur.
    * 
    * @param pMessage
    *           Le message d'erreur
    * @param pSolutionPotentielle
    *           La solution potentielle.
    */
   public GenerationException(String pMessage, String pSolutionPotentielle) {
      super(pMessage);
      _SolutionPotentielle = pSolutionPotentielle;
   }

   /**
    * Obtenir la solution potentielle.
    * 
    * @return La solution potentielle.
    */
   public String getSolutionPotentielle () {
      return _SolutionPotentielle;
   }

   @Override
   public String toString () {
      return getMessage() +
            "\nSolution potentielle : " + _SolutionPotentielle;
   }
}
