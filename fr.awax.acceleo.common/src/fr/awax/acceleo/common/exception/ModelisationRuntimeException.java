package fr.awax.acceleo.common.exception;

/**
 * L'exception Runtime dans le lecture du modèle de génération.
 * 
 * @author MEN
 */
public class ModelisationRuntimeException extends RuntimeException {

   /** Pour la sérialisation */
   private static final long serialVersionUID = 1L;
   /** La solution potentielle */
   private String _SolutionPotentielle;

   /**
    * Constructeur avec exception racine.
    * 
    * @param pMessage
    *           Le message d'erreur
    * @param pCause
    *           L'exception racine.
    * @param pSolutionPotentielle
    *           La solution potentielle.
    */
   public ModelisationRuntimeException(String pMessage, Throwable pCause, String pSolutionPotentielle) {
      super(pMessage, pCause);
      _SolutionPotentielle = pSolutionPotentielle;
   }

   public ModelisationRuntimeException(String pMessage, Throwable pCause) {
      super(pMessage, pCause);
   }

   /**
    * Constructeur.
    * 
    * @param pMessage
    *           Le message d'erreur
    * @param pSolutionPotentielle
    *           La solution potentielle.
    */
   public ModelisationRuntimeException(String pMessage, String... pSolutionPotentielle) {
      super("ERREUR MODELISATION - " + pMessage);
      if ((pSolutionPotentielle != null) && (pSolutionPotentielle.length == 1)) {
         _SolutionPotentielle = pSolutionPotentielle[0];
      }
   }

   /**
    * Obtenir la solution potentielle.
    * 
    * @return La solution potentielle.
    */
   public String getSolutionPotentielle () {
      return _SolutionPotentielle;
   }

   @Override
   public String toString () {
      return getMessage() +
            "\nSolution potentielle : " + _SolutionPotentielle;
   }
}
