package fr.awax.acceleo.common.exception;

/**
 * L'exception Runtime de génération.
 * 
 * @author MEN
 */
public class GenerationRuntimeException extends RuntimeException {

   /** Pour la sérialisation */
   private static final long serialVersionUID = 1L;
   /** La solution potentielle */
   private String[] _TabSolutionPotentielle;

   /**
    * Constructeur avec exception racine.
    * 
    * @param pMessage
    *           Le message d'erreur
    * @param pCause
    *           L'exception racine.
    * @param pSolutionPotentielle
    *           La solution potentielle.
    */
   public GenerationRuntimeException(String pMessage, Throwable pCause, String pSolutionPotentielle) {
      super(pMessage, pCause);
      _TabSolutionPotentielle = new String[1];
      _TabSolutionPotentielle[0] = pSolutionPotentielle;
   }

   public GenerationRuntimeException(String pMessage, Throwable pCause, String... pTabSolutionPotentielle) {
      super(pMessage, pCause);
      _TabSolutionPotentielle = pTabSolutionPotentielle;
   }

   /**
    * Constructeur.
    * 
    * @param pMessage
    *           Le message d'erreur
    * @param pSolutionPotentielle
    *           La solution potentielle.
    */
   public GenerationRuntimeException(String pMessage, String... pTabSolutionPotentielle) {
      super(pMessage);
      _TabSolutionPotentielle = pTabSolutionPotentielle;
   }

   /**
    * Obtenir la solution potentielle.
    * 
    * @return La solution potentielle.
    */
   public String[] getTabSolutionPotentielle () {
      return _TabSolutionPotentielle;
   }

   @Override
   public String toString () {
      String vSolutionPotentielleFmt;
      if (_TabSolutionPotentielle == null) {
         vSolutionPotentielleFmt = "";
      }
      else if (_TabSolutionPotentielle.length == 1) {
         vSolutionPotentielleFmt = "";
         vSolutionPotentielleFmt = _TabSolutionPotentielle[0] + "\n";
      }
      else {
         int vCpt = 1;
         vSolutionPotentielleFmt = "\n";
         for (String vSolutionPotentielle : _TabSolutionPotentielle) {
            vSolutionPotentielleFmt = vSolutionPotentielleFmt + "   -" + vCpt + "- " + vSolutionPotentielle + "\n";
            vCpt++;
         }
      }

      String message = getMessage();
      if (getCause() != null) {
         message = message + "\nCaused by : " + getCause().getMessage();
      }

      return message +
            "--> Solution(s) potentielle(s) : " + vSolutionPotentielleFmt;
   }
}
