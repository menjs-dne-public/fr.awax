package fr.awax.acceleo.common.dsl;

/**
 * Permet de regrouper les différents DSL possibles dans une optique de mémorisation intermédiaire pour simplifier certaines "queries".
 */
public enum Dsl_Enum {
   DslRequirement("Requirement", org.obeonetwork.dsl.requirement.Repository.class),
   DslCinematic("Cinematic", org.obeonetwork.dsl.cinematic.AbstractPackage.class),
   DslSoaServices("Soa-Service", org.obeonetwork.dsl.soa.System.class),
   DslSoaDto("Soa-Dto", org.obeonetwork.dsl.soa.Interface.class),
   DslEntityConcept("EntityConcept", org.obeonetwork.dsl.environment.NamespacesContainer.class),
   DslEntity("Entity", org.obeonetwork.dsl.environment.NamespacesContainer.class),
   DslDataBase("DataBase", org.obeonetwork.dsl.database.DataBase.class),
   DslEnvironment("DslEnvironment", org.obeonetwork.dsl.environment.TypesDefinition.class);

   /** Le nom du DSL. */
   private String _Nom;
   /** La classe racine du DSL. */
   private Class<?> _ClassRootDsl;

   /**
    * Constructeur privé.
    * @param pNom Le nom du DSL.
    * @param pClassRootDsl La classe racine du DSL.
    */
   private Dsl_Enum(String pNom, Class<?> pClassRootDsl) {
      _Nom = pNom;
      _ClassRootDsl = pClassRootDsl;
   }

   public String getNom () {
      return _Nom;
   }

   public void setNom (String pNom) {
      _Nom = pNom;
   }

   public Class<?> getClassRootDsl () {
      return _ClassRootDsl;
   }

   public void setClassRootDsl (Class<?> pClassRootDsl) {
      _ClassRootDsl = pClassRootDsl;
   }

}
