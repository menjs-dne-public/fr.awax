package fr.awax.acceleo.common.conf;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * Permet de mémoriser une "propriété" de paramétrage avec un commentaire
 * expliquant son rôle.
 * 
 * @author MinEdu
 */
public class XPropElem {
   /** La clé de la propriété */
   private String _Key;
   /** La valeur de la propriété */
   private XValue _Value;
   /** Le commentaire décrivant la propriété */
   private String _Commentaire;
   /** La propriété est obligatoire. */
   private boolean _IsOblig;

   public XPropElem() {
      // RAS
   }

   /**
    * Constructeur.
    * @param pKey
    * @param pValue
    * @param pCommentaire
    * @param pIsOblig
    */
   public XPropElem(final String pKey, final String pValue, final String pCommentaire, final boolean pIsOblig) {
      setKey(pKey);
      setValue(pValue);
      setCommentaire(pCommentaire);
      setIsOblig(pIsOblig);
   }

   /**
    * Réinitialiser la valeur de l'instance.
    * @param pXPropElem Les valeurs à réinitialiser.
    */
   public void reinit (XPropElem pXPropElem) {
      setKey(pXPropElem.getKey());
      setValue(pXPropElem.getValue());
      setCommentaire(pXPropElem.getCommentaire());
      setIsOblig(pXPropElem.isOblig());
      setIsReadingFromFile(pXPropElem.isReadingFromFile());
   }

   public String getKey () {
      return _Key;
   }

   public void setKey (String pKey) {
      if (pKey == null) {
         throw new IllegalArgumentException("Le paramètre pKey est obligatoire : null impossible");
      }
      _Key = pKey;
   }

   public XValue getValue () {
      return _Value;
   }

   public void setValue (XValue pValue) {
      _Value = pValue;
   }

   public void setValue (String pValue) {
      _Value = new XValue(pValue);
   }

   public String getCommentaire () {
      return _Commentaire;
   }

   public void setCommentaire (String pCommentaire) {
      if (pCommentaire == null) {
         throw new IllegalArgumentException("Le paramètre pCommentaire est obligatoire : null impossible");
      }
      _Commentaire = pCommentaire;
   }

   public boolean isOblig () {
      return _IsOblig;
   }

   public void setIsOblig (boolean pIsOblig) {
      _IsOblig = pIsOblig;
   }

   @Override
   public int hashCode () {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((_Commentaire == null) ? 0 : _Commentaire.hashCode());
      result = prime * result + (_IsOblig ? 1231 : 1237);
      result = prime * result + ((_Key == null) ? 0 : _Key.hashCode());
      result = prime * result + ((_Value == null) ? 0 : _Value.hashCode());
      return result;
   }

   @Override
   public boolean equals (Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      XPropElem other = (XPropElem) obj;
      if (_Commentaire == null) {
         if (other._Commentaire != null)
            return false;
      }
      else if (!_Commentaire.equals(other._Commentaire))
         return false;
      if (_IsOblig != other._IsOblig)
         return false;
      if (_Key == null) {
         if (other._Key != null)
            return false;
      }
      else if (!_Key.equals(other._Key))
         return false;
      if (_Value == null) {
         if (other._Value != null)
            return false;
      }
      else if (!_Value.equals(other._Value))
         return false;
      return true;
   }

   @Override
   public String toString () {
      return "XPropElem [_Key=" + _Key + ", _Value=" + _Value + ", _Commentaire=" + _Commentaire + ", _IsOblig=" + _IsOblig + ", _IsReadingFromFile=" + _IsReadingFromFile + "]";
   }

   /** La propriété a été lue depuis un fichier. */
   private boolean _IsReadingFromFile;

   public boolean isReadingFromFile () {
      return _IsReadingFromFile;
   }

   public void setIsReadingFromFile (boolean pIsReadingFromFile) {
      _IsReadingFromFile = pIsReadingFromFile;
   }

   /**
    * Lire un élément depuis un fichier mis dans une liste.
    * @param pListLine La liste représentant le fichier.
    * @param pRang Le rang ou commencer la lecture dans la liste.
    * @param pXPropElem (Out)(*) Le POJO résultat.
    * @return Le prochain rang où reprendre la lecture.
    */
   public static int readElem (List<String> pListLine, int pRang, XPropElem pXPropElem) {
      int vReturn = -1;

      int rang = pRang;
      boolean isFinded = false;
      String line1;
      String line2;
      while (false == isFinded && isEof(pListLine, rang)) {
         // SI c'est un commentaire d'une "cle = valeur"
         if (isCommentaireSuiviDeCleValeur(pListLine, rang)) {
            isFinded = true;
            // Obtenir la ligne courante
            line1 = pListLine.get(rang);
            line2 = pListLine.get(rang + 1);
            // Affecteur le POJO
            pXPropElem.setCommentaire(extractCommentaire(line1));
            pXPropElem.setKey(extractKey(line2));
            pXPropElem.setValue(extractValue(line2));
            // Affecter le prochain rang de lecture
            vReturn = rang + 2;
         }

         rang++;
      }
      return vReturn;
   }

   /**
    * POur savoir si nous sommes en End Of File.
    * @param pListLine La liste représentant le fichier.
    * @param pRang La rang de la ligne courante.
    * @return 'true' si nous sommes en EOF.
    */
   private static boolean isEof (List<String> pListLine, int pRang) {
      return (pRang < pListLine.size());
   }

   private static String extractValue (String pLine) {
      String vReturn;
      String line = pLine.trim();
      int rang = line.indexOf("=");
      vReturn = line.substring(rang + "=".length(), line.length());
      vReturn = vReturn.trim();
      return vReturn;
   }

   private static String extractCommentaire (String pLine) {
      String vReturn;
      String line = pLine.trim();
      // Supprimer le "#"
      vReturn = line.substring("#".length(), line.length());
      // Supprimer le RC
      if (vReturn.endsWith("\n")) {
         vReturn = vReturn.substring(0, vReturn.length() - "\n".length());
      }
      vReturn = vReturn.trim();
      return vReturn;
   }

   private static String extractKey (String pLine) {
      String vReturn;
      String line = pLine.trim();
      int rang = line.indexOf("=");
      vReturn = line.substring(0, rang);
      vReturn = vReturn.trim();
      return vReturn;
   }

   private static boolean isCommentaireSuiviDeCleValeur (List<String> pListLine, int pRang) {
      boolean vReturn;

      if ((pRang + 1) < pListLine.size()) {
         // Lire les 2 lignes suivantes
         String line1 = pListLine.get(pRang);
         line1 = line1.trim();
         String line2 = pListLine.get(pRang + 1);
         line2 = line2.trim();
         // SI 1ère ligne est un commentaire mais pas la 2ème
         if (line1.startsWith("#") && isKeyValue(line2)) {
            vReturn = true;
         }
         else {
            vReturn = false;
         }
      }
      else {
         vReturn = false;
      }

      return vReturn;
   }

   private static boolean isKeyValue (String pLine) {
      boolean vReturn;

      if ("".equals(pLine)) {
         vReturn = false;
      }
      else if (false == pLine.startsWith("#")) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }
      return vReturn;
   }

   public void write (Writer out) throws IOException {
      out.write("# " + _Commentaire + "\n");
      out.write(_Key + " = " + _Value + "\n");
   }

}
