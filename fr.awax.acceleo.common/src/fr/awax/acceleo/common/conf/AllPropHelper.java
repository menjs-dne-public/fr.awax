package fr.awax.acceleo.common.conf;

import java.util.List;

import fr.awax.acceleo.common.conf.AllPropAbstract.IAllPropCommun;
import fr.awax.acceleo.common.engine.EngineHelper;

/**
 * Pour instancier les propriétés nécessaires à la génération.
 * @author egarel
 */
public class AllPropHelper extends AllPropAbstract {
   private AllPropHelper() {
      super();
   }

   public static AllPropAbstract getInstance (final String pPluginId) {
      AllPropAbstract instance = (AllPropAbstract) findInstance(pPluginId);
      if (instance == null) {
         instance = EngineHelper.getInstance(pPluginId).getAllProperties();
         if (instance == null) {
            instance = new AllPropHelper();
         }
         registerInstance(pPluginId, instance);
      }
      return instance;
   }

   @Override
   public List<XPropElem> getAllPropertiesSpecif () throws Exception {
      throw new UnsupportedOperationException("Non implémenté à ce niveau");
   }
}
