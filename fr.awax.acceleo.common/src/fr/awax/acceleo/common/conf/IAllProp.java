package fr.awax.acceleo.common.conf;

/**
 * La liste des propriétés nécessaires au plugin.
 * Remarque : Propriétés sous forme de constantes, car en 2019, une Enum pose
 * problème en mode plugin Eclipse RCP (à vérifier sur une version récente).
 * 
 * @author egarel
 */
public interface IAllProp {
   // RAS
}
