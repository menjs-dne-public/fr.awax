package fr.awax.acceleo.common.conf;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.awax.acceleo.common.log.LogLevelEnum;

public class XValue {
   public static final String cFmtDate_AAAAMMJJ_HHMMSS = "YYYYMMdd_HHmmss";

   public XValue(String pValue) {
      super();
      _Value = pValue;
   }

   private String _Value;

   public String getValue () {
      return _Value;
   }

   public void setValue (String pValue) {
      _Value = pValue;
   }

   @Override
   public String toString () {
      return _Value;
   }

   public Boolean toBoolean () {
      return Boolean.parseBoolean(_Value);
   }

   public LogLevelEnum toLogLevelEnum () {
      return LogLevelEnum.valueOf(_Value);
   }

   public Date toDate_AAAAMMJJ_HHMMSS () {
      try {
         // Ex : "20210402_152037"
         SimpleDateFormat vDateFormat = new SimpleDateFormat(cFmtDate_AAAAMMJJ_HHMMSS);
         // String --> Date
         Date vReturn = vDateFormat.parse(_Value);

         return vReturn;
      }
      catch (ParseException err) {
         throw new RuntimeException("Problème lors du toDate_AAAAMMJJ_HHMMSS() : " + err.getMessage(), err);
      }
   }

}
