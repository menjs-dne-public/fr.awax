package fr.awax.acceleo.common.conf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.awax.acceleo.common.log.LogLevelEnum;
import fr.awax.acceleo.common.util.DirectoryUtils;

/**
 * Classe racine pour gérer les propriétés sous la forme :
 * # Explication de la propriété sur une ligne
 * cleProp1 = valProp1
 * @author egarel
 */
abstract public class AllPropAbstract {
   /** Le nom du fichiers contenant les propriétés obligatoires manquantes. */
   private static final String cDefaultPropertiesFile = "missing.properties_";

   /** La Map contenant les instances par plugin. */
   private static final Map<String, AllPropAbstract> _MapInstances = new HashMap<>();
   /** Les propriétés communes à tous les plugin. */
   private final Map<String, XPropElem> _MapAllPropCommon = new LinkedHashMap<>();
   /** Les propriétés spécifiques à un plugin. */
   private final Map<String, XPropElem> _MapAllPropSpecif = new LinkedHashMap<>();
   /** Les propriétés ajoutées dans le fichier. */
   private final Map<String, XPropElem> _MapAllPropUser = new LinkedHashMap<>();
   /** Les propriétés obligatoires. */
   private final Map<String, XPropElem> _MapAllPropOblig = new HashMap<>();

   private boolean _isMapLoaded = false;

   /** Le path de lecture du fichier de Properties. */
   private String _PathReading;
   public String getPathReading () {
      return _PathReading;
   }
   public void setPathReading (String pPathReading) {
      this._PathReading = pPathReading;
   }

   private void initMap () throws Exception {
      if (false == _isMapLoaded) {
         // RAZ
         _MapAllPropCommon.clear();
         _MapAllPropSpecif.clear();
         _MapAllPropOblig.clear();

         List<XPropElem> listCommon = getAllPropertiesCommon();
         // Ecrire les propriétés communes
         for (XPropElem elem : listCommon) {
            elem.setIsReadingFromFile(false);
            // Mémoriser l'instance interne common
            _MapAllPropCommon.put(elem.getKey(), elem);
            // SI la propriété est obligatoire
            if (elem.isOblig()) {
               _MapAllPropOblig.put(elem.getKey(), elem);
            }
         }

         List<XPropElem> listSpecif = getAllPropertiesSpecif();
         // Ecrire les propriétés communes
         for (XPropElem elem : listSpecif) {
            elem.setIsReadingFromFile(false);
            // Mémoriser l'instance interne common
            _MapAllPropSpecif.put(elem.getKey(), elem);
            // SI la propriété est obligatoire
            if (elem.isOblig()) {
               _MapAllPropOblig.put(elem.getKey(), elem);
            }
         }
         _isMapLoaded = true;
      }
   }

   /**
    * Effectuer une remise à zéro.
    */
   public static void razAll () {
      // RAZ des instances
      _MapInstances.clear();
   }

   public void raz () {
      _isMapLoaded = false;
   }

   /**
    * Charger les valeurs des propriétés depuis un fichier.
    * @param pAbsoluteFileName Le nom du fichier de Properties.
    * @param pIsVerifOblig 'true' pour vérifier les propriétés obligatoires.
    */
   public void load (String pAbsoluteFileName, boolean pIsVerifOblig) {
      try {
         // Charger les Map
         initMap();

         String absoluteFileName = DirectoryUtils.uniformPath(pAbsoluteFileName);
         // Mémoriser le path de lecture
         _PathReading = DirectoryUtils.getAbsolutePathWithoutLastDir(absoluteFileName + "/");
         File absoluteFile = new File(absoluteFileName);
         if (absoluteFile.exists()) {
            // Lire le fichier et le mettre dans une liste
            List<String> listLines = readFileToList(absoluteFile);
            int rang = 0;
            int rangNext;
            // Parcourir le fichier élément 'vXPropElem' par élément
            XPropElem vXPropElem = new XPropElem();
            XPropElem vXPropElemIntern;
            while ((rangNext = XPropElem.readElem(listLines, rang, vXPropElem)) >= 0) {
               // Chercher l'instance interne de la propriété lue
               vXPropElemIntern = findXPropElem(vXPropElem.getKey());
               // SI pas trouvé
               if (vXPropElemIntern == null) {
                  // Ajouter la propriété utilisateur
                  _MapAllPropUser.put(vXPropElem.getKey(), vXPropElem);
               }
               // SI trouvé
               else {
                  // Affecter le fait que la propriété a été lu depuis un fichier
                  vXPropElem.setIsReadingFromFile(true);
                  // Obtenir l'aspect obligatoire
                  vXPropElem.setIsOblig(vXPropElemIntern.isOblig());
                  // Mémoriser les valeurs lues
                  vXPropElemIntern.reinit(vXPropElem);
               }

               // Propriété suivante
               vXPropElem = new XPropElem();
               vXPropElem.setIsReadingFromFile(false);
               rang = rangNext;
            }

            // Vérifier les paramètres obligatoires
            verifPropOblig(pIsVerifOblig);
         }
         else {
            // Ecrire le fichier de propriétés avec les commentaires et les valeurs par défaut
            saveDefault(absoluteFile);
            throw new IllegalStateException("Fichier de propriétés non trouvés --> généré avec les valeurs par défaut : " + absoluteFile.getAbsolutePath());
         }
      }
      catch (Exception err) {
         throw new RuntimeException("Problème lors du load(\"" + pAbsoluteFileName + "\") : " + err.getMessage(), err);
      }

   }

   public void load (String pAbsoluteFileName) {
      load(pAbsoluteFileName, true);
   }

   public void verifPropOblig () throws Exception {
      verifPropOblig(true);
   }

   private void verifPropOblig (boolean pIsVerifOblig) throws Exception {
      initMap();
      // Les propriétés obligatoires restants à lire (sur l'ensemble des fichiers Properties du répertoire)
      Map<String, XPropElem> vMapAllPropObligRestant = cloneMap(_MapAllPropOblig);
      XPropElem val;
      // Parcourir les propriétés common
      for (String key : _MapAllPropCommon.keySet()) {
         val = _MapAllPropCommon.get(key);
         // SI la propriété a pas été lu depuis un fichier
         if (val.isReadingFromFile()) {
            // Supprimer la propriété des obligatoires, car elle a été trouvée
            vMapAllPropObligRestant.remove(key);
         }
      }
      // Parcourir les propriétés specifiques
      for (String key : _MapAllPropSpecif.keySet()) {
         val = _MapAllPropSpecif.get(key);
         // SI la propriété a pas été lu depuis un fichier
         if (val.isReadingFromFile()) {
            // Supprimer la propriété des obligatoires, car elle a été trouvée
            vMapAllPropObligRestant.remove(key);
         }
      }

      String messagePropMissing = "";
      // SI au moins une proprité obligatoire est manquante
      if (pIsVerifOblig && (vMapAllPropObligRestant.size() > 0)) {
         for (String key : vMapAllPropObligRestant.keySet()) {
            messagePropMissing = messagePropMissing + "   - " + key + "\n";
         }

         File vFileMissingProperties = new File(_PathReading + "/" + cDefaultPropertiesFile);
         // Ecrire les propriétés manquantes (= obligatoires non trouvées) 
         // dans un autre fichier
         saveMissing(vFileMissingProperties.getAbsolutePath(), vMapAllPropObligRestant);

         throw new IllegalStateException("Au moins une propriété obligatoire est manquante : \n"
               + messagePropMissing + "\n"
               + "Les propriétés ont été générées avec les valeurs par défaut dans : " + vFileMissingProperties.getAbsolutePath());
      }
   }

   /**
    * Permet de clonner une Map.
    * @param pMap La Map à cloner (pas les valeurs).
    * @return La nouvelle instance de Map avec les mêmes valeurs
    */
   private Map<String, XPropElem> cloneMap (Map<String, XPropElem> pMap) {
      Map<String, XPropElem> vReturn = new HashMap<>();
      for (String key : pMap.keySet()) {
         vReturn.put(key, pMap.get(key));
      }
      return vReturn;
   }

   /**
    * Ajouter dynamiquement une propriété.
    * @param pXPropElem La propriété à ajouter.
    */
   public void addProp (XPropElem pXPropElem) {
      _MapAllPropUser.put(pXPropElem.getKey(), pXPropElem);
   }

   /**
    * Supprimer une propriété.
    * @param pKey La clé identifiant la propriété.
    */
   public void removeProp (String pKey) {
      try {
         XPropElem vXPropElemIntern = findXPropElem(pKey, true);
         if (vXPropElemIntern == null) {
            throw new IllegalArgumentException("Impossible de trouver la propriété \"" + pKey + "\" : suppression impossible");
         }
      }
      catch (Exception err) {
         throw new RuntimeException("Problème pour supprimer la propriété \"" + pKey + "\" : " + err.getMessage(), err);
      }
   }

   /**
    * Obtenir une propriété dynamiquement par son nom.
    * @param pKey La clé identifiant la propriété.
    * @return L'instance désirée - 'null' si pas trouvé.
    */
   public XPropElem getProp (String pKey) {
      try {
         return findXPropElem(pKey);
      }
      catch (Exception err) {
         throw new RuntimeException("Problème pour obtenir la propriété \"" + pKey + "\" : " + err.getMessage(), err);
      }
   }

   /**
    * Permet de trouvé une propriété.
    * @param pKey Le nom de la propriété recherchée.
    * @param pIsRemoveWhenFinded 'true' pour supprimer la propriété si trouvée.
    * @return L'instance représentant la propriété en interne.
    * @throws Exception
    */
   private XPropElem findXPropElem (String pKey, boolean pIsRemoveWhenFinded) throws Exception {
      XPropElem vReturn = null;
      // SI les Map ont été chargées
      if (_isMapLoaded) {
         // On cherche dans les common
         vReturn = _MapAllPropCommon.get(pKey);
         // SI pas trouvé dans les propriétés common
         if (vReturn == null) {
            // On cherche dans les spécifiques
            vReturn = _MapAllPropSpecif.get(pKey);
            // SI pas trouvé dans les propriétés spécifiques
            if (vReturn == null) {
               // On cherche dans les propriétés "user"
               vReturn = _MapAllPropUser.get(pKey);
               // SI pas trouvé dans les propriétés utilisateur
               if (vReturn == null) {
                  // RAS
               }
               else {
                  if (pIsRemoveWhenFinded) {
                     _MapAllPropUser.remove(pKey);
                  }
               }
            }
            else {
               if (pIsRemoveWhenFinded) {
                  _MapAllPropSpecif.remove(pKey);
               }
            }
         }
         else {
            if (pIsRemoveWhenFinded) {
               _MapAllPropCommon.remove(pKey);
            }
         }
      }
      else {
         List<XPropElem> listCommon = getAllPropertiesCommon();
         // Ecrire les propriétés communes
         for (XPropElem elem : listCommon) {
            if (pKey.equals(elem.getKey())) {
               vReturn = elem;
            }
         }

         // SI pas trouvé dans les common
         if (vReturn == null) {
            // On cherche dans les cpécifiques
            List<XPropElem> listSpecif = getAllPropertiesSpecif();
            // Ecrire les propriétés communes
            for (XPropElem elem : listSpecif) {
               if (pKey.equals(elem.getKey())) {
                  vReturn = elem;
               }
            }
         }
      }

      return vReturn;
   }

   private XPropElem findXPropElem (String pKey) throws Exception {
      return findXPropElem(pKey, false);
   }

   /**
    * Lire le fichier et mettre chaque ligne dans une liste de String.
    * @param pFile Le fichier
    * @return La liste représentant le fichier.
    * @throws IOException Si une erreur se produit.
    */
   public static List<String> readFileToList (File pFile) throws IOException {
      List<String> vReturn = new ArrayList<String>();
      // Obtenir le chemin en absolu
      String vFileName = pFile.getAbsolutePath();
      try {
         BufferedReader vOut = new BufferedReader(new InputStreamReader(new FileInputStream(vFileName)));

         String vLineCour;
         // Lire le fichier ligne par ligne
         while ((vLineCour = vOut.readLine()) != null) {
            // Ajouter à la liste
            vReturn.add(vLineCour);
         }

         // Fermer le flux
         vOut.close();
      }
      catch (Exception vErr) {
         throw new IOException("Problème lors de la lecture du fichier : " + vFileName, vErr);
      }
      return vReturn;
   }

   /**
    * Ecrire le fichier de Properties par dafaut.
    * @param pAbsoluteFileName Le nom du fichier.
    */
   private void saveDefault (File pAbsoluteFile) throws Exception {
      Writer out = new BufferedWriter(new OutputStreamWriter(
            new FileOutputStream(pAbsoluteFile), StandardCharsets.UTF_8.displayName()));
      // Ecrire le BOM (= 1 carac UTF-8 'feff')
      out.write('\ufeff');
      out.write("# Fichier de propriétés généré le " + (new Date()).toString() + "\n");
      out.write("#\n");
      out.write("# Propritétés communes\n");
      out.write("#\n");
      List<XPropElem> listCommon = getAllPropertiesCommon();
      // Ecrire les propriétés communes
      for (XPropElem elem : listCommon) {
         elem.write(out);
      }

      out.write("#\n");
      out.write("# Propritétés spécifiques au plugin \"" + getPlugInId() + "\"\n");
      out.write("#\n");
      List<XPropElem> listSpecif = getAllPropertiesSpecif();
      // Ecrire les propriétés communes
      for (XPropElem elem : listSpecif) {
         elem.write(out);
      }

      // Fermer
      out.flush();
      out.close();
   }

   /**
    * Sauver les valeurs les propriétés dans un fichier.
    * @throws IOException
    */
   public void save (String pAbsoluteFileName) {
      try {
         if (_isMapLoaded) {
            Writer out = new BufferedWriter(new OutputStreamWriter(
                  new FileOutputStream(pAbsoluteFileName), StandardCharsets.UTF_8.displayName()));
            // Ecrire le BOM (= 1 carac UTF-8 'feff')
            out.write('\ufeff');
            out.write("# Fichier de propriétés généré le " + (new Date()).toString() + "\n");
            out.write("#\n");
            out.write("# Propritétés communes\n");
            out.write("#\n");
            XPropElem elem;
            // Parcourir les propriétés common
            for (String key : _MapAllPropCommon.keySet()) {
               // Obtenir la propriété
               elem = _MapAllPropCommon.get(key);
               elem.write(out);
            }

            if (_MapAllPropSpecif.size() > 0) {
               out.write("#\n");
               out.write("# Propritétés spécifiques au plugin \"" + getPlugInId() + "\"\n");
               out.write("#\n");
               // Parcourir les propriétés common
               for (String key : _MapAllPropSpecif.keySet()) {
                  // Obtenir la propriété
                  elem = _MapAllPropSpecif.get(key);
                  elem.write(out);
               }
            }

            if (_MapAllPropUser.size() > 0) {
               out.write("#\n");
               out.write("# Propritétés utilisateur\n");
               out.write("#\n");
               // Parcourir les propriétés common
               for (String key : _MapAllPropUser.keySet()) {
                  // Obtenir la propriété
                  elem = _MapAllPropUser.get(key);
                  elem.write(out);
               }
            }

            // Fermer
            out.flush();
            out.close();
         }
         else {
            throw new IllegalStateException("Etat incohérent car initMap() n'a pas été appelé : sauvegarde impossible");
         }
      }
      catch (Exception err) {
         throw new RuntimeException("Problème lors du save(\"" + pAbsoluteFileName + "\") : " + err.getMessage(), err);
      }
   }

   /**
    * Sauver les valeurs les propriétés manquantes dans un fichier.
    * @param pMapAllPropOblig
    * @throws IOException
    */
   private void saveMissing (String pAbsoluteFileName, Map<String, XPropElem> pMapAllPropOblig) {
      try {
         if (_isMapLoaded) {
            Writer out = new BufferedWriter(new OutputStreamWriter(
                  new FileOutputStream(pAbsoluteFileName), StandardCharsets.UTF_8.displayName()));
            // Ecrire le BOM (= 1 carac UTF-8 'feff')
            out.write('\ufeff');
            out.write("# Fichier des propriétés manquantes généré le " + (new Date()).toString() + "\n");
            out.write("#\n");
            out.write("# A FAIRE : Remettre dans le fichier de Properties principales avec les bonnes valeurs\n");
            out.write("#\n");
            XPropElem elem;
            // Parcourir les propriétés manquantes
            for (String key : pMapAllPropOblig.keySet()) {
               // Obtenir la propriété
               elem = pMapAllPropOblig.get(key);
               elem.write(out);
            }

            // Fermer
            out.flush();
            out.close();
         }
         else {
            throw new IllegalStateException("Etat incohérent car initMap() n'a pas été appelé : sauvegarde impossible");
         }
      }
      catch (Exception err) {
         throw new RuntimeException("Problème lors du saveMissing(\"" + pAbsoluteFileName + "\") : " + err.getMessage(), err);
      }
   }

   /**
    * Trouver l'instance à partir de l'identifiant du plugin.
    * 
    * @param pPlugInId L'identifiant du plugin.
    * @return L'instance si trouvée, 'null' sinon.
    */
   protected static AllPropAbstract findInstance (final String pPlugInId) {
      return _MapInstances.get(pPlugInId);
   }

   /**
    * Mémoriser une instance.
    * 
    * @param pPlugInId L'identifiant du plugin.
    * @param pInstance L'instance à mémoriser.
    */
   protected static void registerInstance (String pPlugInId, AllPropAbstract pInstance) {
      pInstance.setPlugInId(pPlugInId);
      _MapInstances.put(pPlugInId, pInstance);
   }

   private String _PlugInId;

   public String getPlugInId () {
      return _PlugInId;
   }

   public void setPlugInId (String pPlugInId) {
      _PlugInId = pPlugInId;
   }

   public List<XPropElem> getAllPropertiesCommon () throws Exception {
      List<XPropElem> vReturn = getListXProperty(propCommon);
      return vReturn;
   }

   protected List<XPropElem> getListXProperty (IAllProp pAllProp) throws IllegalAccessException {
      List<XPropElem> vReturn = new ArrayList<>();
      Field[] allField = pAllProp.getClass().getFields();
      for (Field field : allField) {
         // SI type 'XPropElem'
         if (field.getType().equals(XPropElem.class)) {
            vReturn.add((XPropElem) field.get(pAllProp));
         }
      }
      return vReturn;
   }

   /**
    * Obtenir la liste des propriétés spécifiques.
    * @return La liste désirée.
    */
   abstract public List<XPropElem> getAllPropertiesSpecif () throws Exception;

   /** Les propriétés communes à tous les plugins en static pour accéder aux clés uniquement. */
   public static final IAllPropCommun PROP_COMMON = (new AllPropAbstract() {
      @Override
      public List<XPropElem> getAllPropertiesSpecif () throws Exception {
         throw new UnsupportedOperationException("Non implémentée à ce niveau");
      }
   }).propCommon();

   /** Les propriétés communes à tous les plugins. */
   private IAllPropCommun propCommon = new IAllPropCommun() {
      // RAS
   };

   public IAllPropCommun propCommon () {
      return propCommon;
   }

   /**
    * Toutes les propriétés communes.
    * 
    * @author egarel
    */
   public class IAllPropCommun implements IAllProp {
//      public final XPropElem generation_strategy_rc = new XPropElem("generation_strategy_rc", "\\r\\n",
//            "La strat\351gie de g\351n\351ration des fichiers au niveau du retour chariot (ex : \\r\\n)", true);
      public final XPropElem update_check = new XPropElem("update_check", "false",
            "Bloquer la génération si une nouvelle version est disponible (true ou false)", true);
      public final XPropElem update_pathfile = new XPropElem("update_pathfile",
            "C:/repo/awax.script/site_version.xml",
            "Indique le en absolu vers le site_version.xml du répertoire contenant la dernière version du plugin (ex : \"C:/repo/awax.script/site_version.xml\")", true);
      public final XPropElem appli_info_name = new XPropElem("appli_info_name", "myProject",
            "Le nom de l'application qui est prise en compte par le générateur", true);
      public final XPropElem path_root_generation = new XPropElem("path_root_generation", "../",
            "Indique le répertoire racine pour la génération (\"../\" : prend le répertoire supérieur comme racine - ou spécifier en absolu \"P:/dsi/projets/myproject/\")", true);
      public final XPropElem dir_generation_projet_root = new XPropElem(
            "dir_generation_projet_root", "myproject",
            "Le projet (= répertoire) racine qui contient le POM parent (ex1 : \"myproject\" - ex2 : \"/\" si pas de projet racine et génération depuis 'path_root_generation')", true);
      public final XPropElem path_common_src = new XPropElem(
            "path_common_src", "myproject_common/src/",
            "Le sous-répertoire des sources sur projet common (ex : \"myproject_common/src/\"", true);
      public final XPropElem generation_log = new XPropElem("generation_log", LogLevelEnum.DEBUG.toString(),
            "Affiche les logs dans la vue \"Error Logs\" (" + LogLevelEnum.getAllValuesFmt() + ")", true);

      public final XPropElem path_assets = new XPropElem("path_assets", "assets",
            "Le répertoire où il faut dézipper le 'assets'", false);

      public final XPropElem path_generation_projet_common_src = new XPropElem("path_generation_projet_common_src", "src-common",
            "Le répertoire des sources partie 'common'", false);
      public final XPropElem path_generation_projet_common_srcTest = new XPropElem("path_generation_projet_common_srcTest", "src-test-common",
            "Le répertoire des sources JUnit partie 'common'", false);
      public final XPropElem path_generation_projet_common_srcRessources = new XPropElem("generation_projet_common_srcRessources", "src-rsc-common",
            "Le répertoire des ressources partie 'common'", false);

      public final XPropElem path_generation_projet_client_src = new XPropElem("path_generation_projet_client_src", "src-client",
            "Le répertoire des sources partie 'client'", false);
      public final XPropElem path_generation_projet_client_srcTest = new XPropElem("path_generation_projet_client_srcTest", "src-test-client",
            "Le répertoire des sources JUnit partie 'client'", false);
      public final XPropElem path_generation_projet_client_srcRessources = new XPropElem("generation_projet_client_srcRessources", "src-rsc-client",
            "Le répertoire des ressources partie 'client'", false);

      public final XPropElem path_generation_projet_server_src = new XPropElem("path_generation_projet_server_src", "src-server",
            "Le répertoire des sources partie 'server'", false);
      public final XPropElem path_generation_projet_server_srcTest = new XPropElem("path_generation_projet_server_srcTest", "src-test-server",
            "Le répertoire des sources JUnit partie 'server'", false);
      public final XPropElem path_generation_projet_server_srcRessources = new XPropElem("generation_projet_server_srcRessources", "src-rsc-server",
            "Le répertoire des ressources partie 'server'", false);

   }
}
