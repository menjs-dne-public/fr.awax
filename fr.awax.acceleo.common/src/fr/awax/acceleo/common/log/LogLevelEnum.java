package fr.awax.acceleo.common.log;

public enum LogLevelEnum {
   TRACE,
   DEBUG,
   INFO,
   WARNING,
   ERROR,
   FATAL;

//   /** La variable permettant de définir le niveau de LOG. */
//   private static final String LOG_LEVEL = "logLevel";
//   private static final String LOG_LEVEL_default = LogLevelEnum.DEBUG.name();

   /**
    * Obtenir toutes les valeurs formatées.
    * @return La chaînes avec toutes les valeurs séparées par une virgule.
    */
   public static String getAllValuesFmt () {
      String vReturn = "";

      for (LogLevelEnum vLogLevelEnum : values()) {
         vReturn = vReturn + ", " + vLogLevelEnum.name();
      }
      // Enlever le ", " du début
      vReturn = vReturn.substring(", ".length(), vReturn.length());

      return vReturn;
   }

//   /**
//    * Permet de savoir s'il faut logguer le message à partir de la valeur spécifiée dans le Properties.
//    * @param pLogLevelOfMessage Le niveau de LOG du message.
//    * @return 'true' s'il faut logguer.
//    */
//   public boolean isLogMessage(LogLevelEnum pLogLevelOfMessage) {
//      boolean vIsReturn;
//      String vLogLevel;
//      try {
//         vLogLevel = System.getProperties().getProperty(LOG_LEVEL);
//         if (vLogLevel == null) {
//            System.out.println("INFO : Pas de niveau de logs spécifié (-D " + LOG_LEVEL + ") - niveau par défaut appliqué : " + LOG_LEVEL_default);
//            vLogLevel = LOG_LEVEL_default;
//         }
//      }
//      catch (Exception e) {
//         // Par défaut : on log tout
//         vLogLevel = LogLevelEnum.TRACE.name();
//         e.printStackTrace();
//      }
//      LogLevelEnum vLogLevelEnum = LogLevelEnum.valueOf(vLogLevel);
//      if (vLogLevelEnum == null) {
//         throw new IllegalArgumentException("Erreur dans le fichier de paramétrage : " + vLogLevel + " n'est pas une valeur autorisée pour '" + LogLevelEnum.getAllValuesFmt() + "'");
//      }
//
//      // S'il faut LOGGUER
//      if (pLogLevelOfMessage.ordinal() >= vLogLevelEnum.ordinal()) {
//         vIsReturn = true;
//      }
//      else {
//         vIsReturn = false;
//      }
//
//      return vIsReturn;
//   }
}
