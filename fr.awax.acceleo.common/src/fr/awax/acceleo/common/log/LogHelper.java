package fr.awax.acceleo.common.log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fr.awax.acceleo.common.conf.AllPropAbstract.IAllPropCommun;
import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.conf.AllPropHelper;

/**
 * Propose des méthodes pour gérer le log et faciliter le débuggage de la génération.
 */
@SuppressWarnings("static-access")
public class LogHelper {
//   public enum ELevel {
//      TRACE,
//      DEBUG,
//      INFO,
//      WARNING,
//      ERROR;
//   }

   public LogLevelEnum getLevel () {
      // Obtenir le niveau de log défini dans le paramétrage
      return _AllProp.generation_log.getValue().toLogLevelEnum();
   }

   private IAllPropCommun _AllProp;

   public IAllPropCommun getAllProp () {
      return _AllProp;
   }

   /**
    * Formate la date dans les logs.
    */
   private static DateFormat dtf = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss.S");

   /** La Map contenant les instances. */
   private static Map<String, LogHelper> _MapInstances = new HashMap<>();
   private String _PluginId;

   private LogHelper() {
      // RAS
   }

   /**
    * Obtenir l'instance pour le DEBUG.
    * @param pPluginId L'identifiant du plugin.
    * @return L'instance désirée.
    */
   public synchronized static LogHelper getInstance (final String pPluginId) {
      // FIXME : Mémoriser l'instance dans une Map
      LogHelper vReturn = _MapInstances.get(pPluginId);
      // SI pas encore instanciée
      if (vReturn == null) {
         vReturn = new LogHelper();
         vReturn._PluginId = pPluginId;
         vReturn._AllProp = AllPropHelper.getInstance(pPluginId).propCommon();
         _MapInstances.put(pPluginId, vReturn);
      }

      return vReturn;
   }

   public void close () {
      // RAS pour l'instant
   }

   /**
    * Affiche un message de debug sous la forme :
    * 
    * <pre>
    * DD-MM-YYYY HH:mm - DEBUG - message
    * 	Veuillez envisager les solutions suivantes :
    * 		- Solution 1
    * 		- Solution 2
    * </pre>
    * 
    * @param message
    * @param solutions
    */
   public void debug (String message, String... solutions) {
      if (getLevel().ordinal() <= LogLevelEnum.DEBUG.ordinal()) {
         afficherConsoleEclipse(dtf.format(new Date()) + " - DEBUG - " + message);

         if (solutions.length > 0) {
            afficherConsoleEclipse("\tVeuillez envisager les solutions suivantes :");
            for (String solution : solutions) {
               afficherConsoleEclipse("\t\t - " + solution);
            }
         }
      }
   }

   /**
    * Affiche un message d'info sous la forme :
    * 
    * <pre>
    * DD-MM-YYYY HH:mm - DEBUG - message
    * 	Veuillez envisager les solutions suivantes :
    * 		- Solution 1
    * 		- Solution 2
    * </pre>
    * 
    * @param message
    * @param solutions
    */
   public void info (String message, String... solutions) {
      if (getLevel().ordinal() <= LogLevelEnum.INFO.ordinal()) {
         afficherConsoleEclipse(dtf.format(new Date()) + " - INFO - " + message);

         if (solutions.length > 0) {
            afficherConsoleEclipse("\tVeuillez envisager les solutions suivantes :");
            for (String solution : solutions) {
               afficherConsoleEclipse("\t\t - " + solution);
            }
         }
      }
   }

   /**
    * Affiche un message de warning sous la forme :
    * 
    * <pre>
    * DD-MM-YYYY HH:mm - WARNING - message
    * 	Veuillez envisager les solutions suivantes :
    * 		- Solution 1
    * 		- Solution 2
    * </pre>
    * 
    * @param message
    * @param solutions
    */
   public void warn (String message, String... solutions) {
      if (getLevel().ordinal() <= LogLevelEnum.WARNING.ordinal()) {
         System.err.println(dtf.format(new Date()) + " - WARNING - " + message);

         if (solutions.length > 0) {
            System.err.println("\tVeuillez envisager les solutions suivantes :");
            for (String solution : solutions) {
               System.err.println("\t\t - " + solution);
            }
         }
      }
   }

   /**
    * Affiche un message d'erreur sous la forme :
    * 
    * <pre>
    * DD-MM-YYYY HH:mm - ERROR - message
    * 	Veuillez envisager les solutions suivantes :
    * 		- Solution 1
    * 		- Solution 2
    * </pre>
    * 
    * @param message
    * @param solutions
    */
   public void error (String message, String... solutions) {
      if (getLevel().ordinal() <= LogLevelEnum.ERROR.ordinal()) {
         System.err.println(dtf.format(new Date()) + " - ERROR - " + message);

         if (solutions.length > 0) {
            System.err.println("\tVeuillez envisager les solutions suivantes :");
            for (String solution : solutions) {
               System.err.println("\t\t - " + solution);
            }
         }
      }
   }

   /**
    * Permet de savoir s'il faut logguer le message à partir de la valeur spécifiée dans le Properties.
    * @param pLogLevelOfMessage Le niveau de LOG du message.
    * @return 'true' s'il faut logguer.
    */
   public boolean isLogMessage (LogLevelEnum pLogLevelOfMessage) {
      boolean vIsReturn;
      String vLogLevel;
      try {
         vLogLevel = _AllProp.generation_log.getValue().toString();
      }
      catch (Exception e) {
         // Par défaut : on log tout
         vLogLevel = LogLevelEnum.TRACE.name();
         afficherConsoleEclipse("Problème pour obtenir les infos du projet, niveau de log mis par défaut : vLogLevel=" + vLogLevel);
         e.printStackTrace();
      }
      LogLevelEnum vLogLevelEnum = LogLevelEnum.valueOf(vLogLevel);
      if (vLogLevelEnum == null) {
         throw new IllegalArgumentException("Erreur dans le fichier de paramétrage : " + vLogLevel + " n'est pas une valeur autorisée pour '" + _AllProp.generation_log.getValue() + "'");
      }

      // S'il faut LOGGUER
      if (pLogLevelOfMessage.ordinal() >= vLogLevelEnum.ordinal()) {
         vIsReturn = true;
      }
      else {
         vIsReturn = false;
      }

      return vIsReturn;
   }

   private void afficherConsoleEclipse (String pMessage) {
      System.out.println(pMessage);
   }

   /**
    * Permet de savoir si l'on en dans un niveau de log DEBUG.
    * @return 'true' si mode DEBUG.
    */
   public boolean isLogLevelDebug (final String pPluginId) {
      boolean vIsReturn;
      String vLogLevel = _AllProp.generation_log.getValue().toString();
      LogLevelEnum vLogLevelEnum = LogLevelEnum.valueOf(vLogLevel);
      // SI niveau DEBUG (ou inférieur) 
      if (vLogLevelEnum.ordinal() <= LogLevelEnum.DEBUG.ordinal()) {
         vIsReturn = true;
      }
      else {
         vIsReturn = false;
      }

      return vIsReturn;
   }

}
