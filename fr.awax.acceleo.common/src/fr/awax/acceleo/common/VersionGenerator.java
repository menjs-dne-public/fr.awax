package fr.awax.acceleo.common;

public class VersionGenerator {
   public static String getVersion () {
      return "La date du build du générateur Indus_SIRH - 7.6.3_20200924_080000";
   }

   /**
    * Retourne le nom de l'application.
    * 
    * @return Le nom désiré.
    */
   public static String getApplicationName () {
      return VersionGenerator.class.getPackage().getName();
   }

}
