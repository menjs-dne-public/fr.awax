package fr.awax.acceleo.common.norme;

import java.util.List;

import fr.awax.acceleo.common.util.StringUtils;

public class StringNorme {
   /** La valeur d'origine. */
   private String _Value;
   /** Le type de séparateur de mots. */
   private ChunkSepEnum _ChunkSepEnum;
   /** La liste des mots à normaliser. */
   private List<String> _ListDeMots;

   public StringNorme(String pValue) {
      this(pValue, ChunkSepEnum.normeSpace);
   }

   /**
    * Supprimer les espaces à droite et à gauche.
    * @param pValue La valeur d'origine.
    * @return La valeur sans espace non significatif.
    */
   public static String trim (String pValue) {
      String vReturn;
      if (pValue == null) {
         // Pour se protéger des NPE
         vReturn = "";
      }
      else {
         vReturn = pValue.trim();
      }
      return vReturn;
   }

   /**
    * Constructeur.
    * @param pValue Les mots à normaliser.
    * @param pChunkSepEnum Le type de séparateur.
    */
   public StringNorme(String pValue, ChunkSepEnum pChunkSepEnum) {
      super();
      _ChunkSepEnum = pChunkSepEnum;
      // Convertir les caractère spéciaux (ex : "umlaut")
      _Value = StringUtils.normalize(pValue);
      // Supprimer les expaces à droite et à gauche
      _Value = trim(_Value);
      // Initialiser la liste de mots
      _ListDeMots = _ChunkSepEnum.getListDeMots(_Value);
   }

   public String getValue () {
      return _Value;
   }

   public void setValue (String pValue) {
      _Value = pValue;
   }

   @Override
   public int hashCode () {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((_Value == null) ? 0 : _Value.hashCode());
      return result;
   }

   @Override
   public boolean equals (Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      StringNorme other = (StringNorme) obj;
      if (_Value == null) {
         if (other._Value != null)
            return false;
      }
      else if (!_Value.equals(other._Value))
         return false;
      return true;
   }

   @Override
   public String toString () {
      return _Value;
   }

   //
   // Méthodes pour NORMALISER
   //

   private String toNorme (String pSep) {
      String vReturn = "";
      for (String mot : _ListDeMots) {
         vReturn = vReturn + pSep + mot;
      }
      // Supprimer le séparateur mis à la 1ère itération
      vReturn = vReturn.substring(pSep.length(), vReturn.length());
      return vReturn;
   }

   /**
    * Passer en norme underscore avec un "_" pour séparer les mots.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toNormeUnderscore () {
      String vReturn = toNorme("_");
      return new StringNorme(vReturn, ChunkSepEnum.normeUnderscore);
   }

   /**
    * Passer en norme tiret avec un "-" pour séparer les mots.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toNormeTiret () {
      String vReturn = toNorme("-");
      return new StringNorme(vReturn, ChunkSepEnum.normeTiret);
   }

   /**
    * Passer en phrase avec un espace pour séparer les mots.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toNormeSpace () {
      String vReturn = toNorme(" ");
      return new StringNorme(vReturn, ChunkSepEnum.normeSpace);
   }

   /**
    * Enlever les retours chariots.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toWithoutRC () {
      String vReturn = "";
      for (String motCour : _ListDeMots) {
         vReturn = vReturn + " " + motCour.trim();
         // Supprimer les retours chariots éventuels
         vReturn = StringUtils.replace(vReturn, "\n", "");
         vReturn = StringUtils.replace(vReturn, "\r", "");
      }
      return new StringNorme(vReturn, ChunkSepEnum.normeSpace);
   }

   /**
    * Passer en CamelCase.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toNormeCamelCase () {
      String vReturn = "";
      for (String motCour : _ListDeMots) {
         vReturn = vReturn + StringUtils.toUpperFirstLetter(motCour);
      }
      return new StringNorme(vReturn, ChunkSepEnum.normecamelCase);
   }

   /**
    * Passer en CamelCase sauf la 1ère lettre.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toNormeCamelCaseIgnoreFirst () {
      String vReturn = "";
      int cpt = 0;
      for (String motCour : _ListDeMots) {
         if (cpt == 0) {
            vReturn = vReturn + motCour;
         }
         else {
            vReturn = vReturn + StringUtils.toUpperFirstLetter(motCour);
         }
         cpt++;
      }
      return new StringNorme(vReturn, ChunkSepEnum.normecamelCase);
   }

   /**
    * Passer en CamelCase sauf la 1ère lettre qui est mise en minuscule.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toNormeCamelCaseLowerFirst () {
      String vReturn = "";
      int cpt = 0;
      for (String motCour : _ListDeMots) {
         if (cpt == 0) {
            vReturn = vReturn + StringUtils.toLowerFirstLetter(motCour);
         }
         else {
            vReturn = vReturn + StringUtils.toUpperFirstLetter(motCour);
         }
         cpt++;
      }
      return new StringNorme(vReturn, ChunkSepEnum.normecamelCase);
   }

   /**
    * Passer la 1ère lettre en minuscule.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toLowerFirst () {
      return new StringNorme(StringUtils.toLowerFirstLetter(_Value));
   }

   /**
    * Passer la 1ère lettre en majuscule.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toUpperFirst () {
      return new StringNorme(StringUtils.toUpperFirstLetter(_Value));
   }

   /**
    * Passer tout en minuscule.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toLowerAll () {
      return new StringNorme(_Value.toLowerCase());
   }

   /**
    * Passer tout en majuscule.
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toUpperAll () {
      return new StringNorme(_Value.toUpperCase());
   }

   /**
    * Passer en mode "sans underscore".
    * @return L'instance avec la chaîne désirée.
    */
   public StringNorme toRemoveUnderscore () {
      StringNorme normeSpace = new StringNorme(_Value.replace('_', ' '));
      return normeSpace.toNormeCamelCaseIgnoreFirst();
   }

   //
   // Méthodes pour la convention d'un langage.
   //

   /**
    * Obtenir la valeur dans la convention constante Java.
    * @return La chaîne désirée.
    */
   public String toConvJavaConstant () {
      StringNorme vStringNorme = new StringNorme(trim(_Value));
      return vStringNorme.toNormeUnderscore().toUpperAll().toString();
   }

   /**
    * Obtenir la valeur dans la convention package Java.
    * @return La chaîne désirée.
    */
   public String toConvJavaPackage () {
      String value = trim(_Value);
      value = value.replace(' ', '.');
      StringNorme vStringNorme = new StringNorme(value);
      final String vReturn = vStringNorme.toLowerAll().toString();
      return vReturn;
   }

   /**
    * Obtenir la valeur dans la convention attribut Java.
    * @return La chaîne désirée.
    */
   public String toConvJavaAttribute () {
      StringNorme vStringNorme = new StringNorme(trim(_Value));
      return vStringNorme.toRemoveUnderscore().toNormeCamelCaseIgnoreFirst().toUpperFirst().toString();
   }

   /**
    * Obtenir la valeur dans la convention paramètre Java.
    * @return La chaîne désirée.
    */
   public String toConvJavaParameter () {
      StringNorme vStringNorme = new StringNorme(trim(_Value));
      return vStringNorme.toRemoveUnderscore().toNormeCamelCaseIgnoreFirst().toString();
   }

   public String toConvJavaMethod () {
      StringNorme vStringNorme = new StringNorme(trim(_Value));
      return vStringNorme.toRemoveUnderscore().toNormeCamelCaseLowerFirst().toString();
   }

}
