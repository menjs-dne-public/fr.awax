package fr.awax.acceleo.common.norme;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Type de séparation entre 2 mots.
 * @author egarel
 */
public enum ChunkSepEnum {
   /** L'espace sépare 2 mots (ex : "mot1 mot2"). */
   normeSpace,
   /** L'underscore sépare 2 mots (ex : "mot1_mot2"). */
   normeUnderscore,
   /** Le tiret sépare 2 mots (ex : "mot1-mot2"). */
   normeTiret,
   /** La CamelCase sépare 2 mots (ex : "mot1Mot2"). */
   normecamelCase;

   /**
    * Obtenir la liste de mots en fonction du type de séparation.
    * @return
    */
   public List<String> getListDeMots (String pValue) {
      List<String> vReturn = new ArrayList<String>();
      // La séparation est un espace
      if (normeSpace == this) {
         initWith_token(pValue, " ", vReturn);
      }
      // La séparation est un underscore
      else if (normeUnderscore == this) {
         initWith_token(pValue, "_", vReturn);
      }
      // La séparation est un tiret
      else if (normeTiret == this) {
         initWith_token(pValue, "-", vReturn);
      }
      else if (normecamelCase == this) {
         initWith_camelCase(pValue, vReturn);
      }
      else {
         throw new IllegalArgumentException("Cas non prévu pour ChunkSepEnum=" + name());
      }
      return vReturn;
   }

   /**
    * Initialiser la liste avec le séparateur : espace.
    * @param pValue La valeur à parcourir.
    * @param pListResult (Out)(*) La liste à remplir.
    */
   private void initWith_token (String pValue, String pSep, List<String> pListResult) {
      final StringTokenizer buffer = new StringTokenizer(pValue, pSep);
      String motCour;
      // Parcourir les mots (ex : "une valeur")
      while (buffer.hasMoreTokens()) {
         // Obtenir la valeur courante
         motCour = (String) buffer.nextElement();
         pListResult.add(motCour);
      }
   }

   /**
    * Initialiser la liste avec le séparateur : CamelCase.
    * @param pValue La valeur à parcourir.
    * @param pListResult (Out)(*) La liste à remplir.
    */
   private void initWith_camelCase (String pValue, List<String> pListResult) {
      String motCour;
      int v_rangSplitPrec = 0;
      int v_carCour;
      // Parcourir les caractères
      for (int i = 0; i < pValue.length(); i++) {
         v_carCour = pValue.charAt(i);
         // A..Z
         if ((i >= 1) && (v_carCour >= 65) && (v_carCour <= 90)) {
            // Extraire le mot courant
            motCour = pValue.substring(v_rangSplitPrec, i);
            // Ajouter à la liste de mots résultat
            pListResult.add(motCour);
            // Next ...
            v_rangSplitPrec = i;
         }
      }
      // Extraire le dernier mot
      motCour = pValue.substring(v_rangSplitPrec, pValue.length());
      pListResult.add(motCour);
   }
}
