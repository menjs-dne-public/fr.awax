package fr.awax.acceleo.common.engine;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.osgi.framework.Bundle;

import fr.awax.acceleo.common.conf.AllPropAbstract;
import fr.awax.acceleo.common.conf.ModeExecEnum;
import fr.awax.acceleo.common.log.LogLevelEnum;
import fr.awax.acceleo.common.util.DirectoryUtils;
import fr.awax.acceleo.common.util.StringUtils;

/**
 * Cette classe agrège les éléments pour le moteur de génération.
 * 
 * @author egarel
 */
public class EngineHelper {
   /** La Map contenant les instances. */
   private static Map<String, EngineHelper> _MapInstances = new HashMap<>();

   /**
    * Constructeur par défaut.
    */
   private EngineHelper() {
      // RAS
   }

   /**
    * L'instance associé au pluginId.
    * 
    * @param pPluginId L'identifiant du plugin qui a été lancé.
    * @return L'instance désirée.
    */
   public synchronized static EngineHelper getInstance (String pPluginId) {
      EngineHelper vReturn = _MapInstances.get(pPluginId);
      // SI pas encore instanciée
      if (vReturn == null) {
         vReturn = new EngineHelper();
         _MapInstances.put(pPluginId, vReturn);
      }

      return vReturn;
   }

   private ModeExecEnum _ModeExecEnum;

   public ModeExecEnum getModeExecEnum () {
      return _ModeExecEnum;
   }

   public void setModeExecEnum (ModeExecEnum pModeExecEnum) {
      _ModeExecEnum = pModeExecEnum;
   }

   private static String _PluginIdRunning;

   /**
    * Obtenir l'identifiant du plugin en cours d'exécution.
    * 
    * @return L'id désiré.
    */
   public static String getPluginIdRunning () {
      return _PluginIdRunning;
   }

   public static void setPluginIdRunning (String pPluginIdRunning) {
      _PluginIdRunning = pPluginIdRunning;
   }

   private String _PluginId;

   /**
    * Obtenir l'identifiant du plug-in (ex : Activator.PLUGIN_ID).
    * 
    * @return L'identifiant désiré.
    */
   public String getPluginId () {
      return _PluginId;
   }

   public void setPluginId (String pPluginId) {
      _PluginId = pPluginId;
   }

   private Bundle _Bundle;

   /**
    * Obtenir l'instance représentant le bundle du plugin Eclipse.
    * 
    * @return L'instance désirée.
    */
   public Bundle getBundle () {
      return _Bundle;
   }

   public void setBundle (Bundle pBundle) {
      _Bundle = pBundle;
   }

   private IProgressMonitor _ProgressMonitor;

   /**
    * @return
    * @example IProgressMonitor progressMonitor =
    *          EngineHelper.getProgressMonitor(); progressMonitor.beginTask("En
    *          cours ...", 100); progressMonitor.worked(10); ...
    *          progressMonitor.worked(100);
    */
   public IProgressMonitor getProgressMonitor () {
      return _ProgressMonitor;
   }

   public void setProgressMonitor (IProgressMonitor pProgressMonitor) {
      _ProgressMonitor = pProgressMonitor;
   }

   private AllPropAbstract _AllProperties;

   public AllPropAbstract getAllProperties () {
      return _AllProperties;
   }

   public void setAllProperties (AllPropAbstract pAllProperties) {
      _AllProperties = pAllProperties;
   }

   private String _TargetFolder;

   /**
    * Le chemin où s'exécute le bundle.
    * 
    * @return Le chemin désiré.
    */
   public String getTargetFolder () {
      return _TargetFolder;
   }

   public void setTargetFolder (String pTargetFolder) {
      _TargetFolder = pTargetFolder;
   }

   private String _FileNameOfModelInProgress;

   public String getFileNameOfModelInProgress () {
      return _FileNameOfModelInProgress;
   }

   public void setFileNameOfModelInProgress (String pFileNameOfModelInProgress) {
      _FileNameOfModelInProgress = pFileNameOfModelInProgress;
   }

   private List<Resource> _ListResource = new ArrayList<>();

   public Collection<Resource> getListResource () {
      return _ListResource;
   }

   public void addResource (Resource pResource) {
      _ListResource.add(pResource);
   }

   private ResourceSet _ResourceSet;

   public ResourceSet getResourceSet () {
      return _ResourceSet;
   }

   public void setResourceSet (ResourceSet pResourceSet) {
      _ResourceSet = pResourceSet;
   }

   public Resource getResource () {
      return _ListResource.get(0);
   }

   private URI _UriOfModelInProgress;

   public URI getUriOfModelInProgress () {
      return _UriOfModelInProgress;
   }

   public void setUriOfModelInProgress (URI pUriOfModelInProgress) {
      _UriOfModelInProgress = pUriOfModelInProgress;
   }

   private String _PathRoot;

   public String getPathRoot () {
      return _PathRoot;
   }

   public void setPathRoot (String pPathRoot) {
      _PathRoot = pPathRoot;
   }

   /**
    * L'URL pour dézipper les ASSETS (= fichiers nécessaires) afin d'utiliser le
    * fichiers générés.
    */
   private URL _AssetsZipFileUrl;

   public URL getAssetsZipFileUrl () {
      return _AssetsZipFileUrl;
   }

   public void setAssetsZipFileUrl (URL pAssetsZipFileUrl) {
      _AssetsZipFileUrl = pAssetsZipFileUrl;
   }

   public final LogLevelEnum getProp_generation_log () {
      String actualLogLevel = _AllProperties.propCommon().generation_log.getValue().toString();
      LogLevelEnum vReturn;
      try {
         vReturn = LogLevelEnum.valueOf(actualLogLevel);
      }
      catch (IllegalArgumentException err) {
         throw new IllegalArgumentException(
               StringUtils.getStringUtf8("Erreur dans la valeur spécifiée pour le niveau de log : "
                     + _AllProperties.propCommon().generation_log.getKey() + "=" + actualLogLevel
                     + " est impossible (valeurs autorisées : " + LogLevelEnum.getAllValuesFmt() + ")"),
               err);
      }

      return vReturn;
   }

   /**
    * Permet de calculer le path de génération cible à partir d'un path en relatif.
    * @param projectModelsLocationPath Le path du projet contenant les modèles de génération.
    * @param pTargetFolderProperties Le path défini dans le Properties.
    * @return Le path target pour la génération.
    */
   public String computeTargetRelatifPathRoot (String pProjectModelsLocationPath, String pTargetFolderProperties) {
      String vReturn;

      // Le path du projet de modélisation
      String projectModelsLocationPath = pProjectModelsLocationPath;
      // Ramplacer "\" par "/"
      projectModelsLocationPath = projectModelsLocationPath.replace('\\', '/');
      // Le path cible de la génération
      String targetFolderProperties = pTargetFolderProperties;
      // Ramplacer "\" par "/"
      targetFolderProperties = targetFolderProperties.replace('\\', '/');
      // SI commence par "/"
      if (targetFolderProperties.startsWith("/")) {
         throw new IllegalArgumentException("Cas non traité pour la valeur de \""
               + AllPropAbstract.PROP_COMMON.path_root_generation.getKey() + "\" : " + targetFolderProperties);
      }
      else {
         if (targetFolderProperties.startsWith("./")) {
            // C'est le path du 'pProjectModelsLocationPath'
            vReturn = projectModelsLocationPath + "/";
         }
         else if (targetFolderProperties.startsWith("../")) {
            // Compter le nombre de ".."
            int nbPointPoint = countNbPointPoint(targetFolderProperties) - 1;

            if (nbPointPoint == 1) {
               // Supprimer le "/" éventuellement à la fin
               projectModelsLocationPath = (projectModelsLocationPath.endsWith("/")
                     ? projectModelsLocationPath.substring(0, projectModelsLocationPath.length() - "/".length())
                     : projectModelsLocationPath);
               // Ex : "T:/Ws4Cours/WSS4_cours_Acceleo_init/runtime-Eclipse_TD_Acceleo_scripts_configuration/"
               String absolutePathWorkspace = DirectoryUtils.getAbsolutePath(projectModelsLocationPath);
               // Supprimer le ".."
               targetFolderProperties = targetFolderProperties.substring("..".length(), targetFolderProperties.length());
               vReturn = DirectoryUtils.getAbsolutePath(absolutePathWorkspace + "/" + targetFolderProperties + "/");
            }
            else {
//					List<String> listTargetFolderProperties = DirectoryUtils.getListDirectory(targetFolderProperties + "/");
               int rang = 0;
               int rangPrec;
               rang = targetFolderProperties.indexOf("/", rang);
               String targetFolderPropertiesCour = targetFolderProperties.substring(0, rang);
               // Chercher le 1er répertoire aute que ".."
               do {
                  rangPrec = rang;
                  rang = targetFolderProperties.indexOf("/", rang + 1);
                  targetFolderPropertiesCour = targetFolderProperties.substring(0, rang + "/".length());
               } while (targetFolderPropertiesCour.endsWith("../"));
               targetFolderPropertiesCour = targetFolderProperties.substring(rangPrec, targetFolderProperties.length());
               // Découper en List<String> le path du projet de modeling
               List<String> listProjectModelsLocationPath = DirectoryUtils.getListDirectory(projectModelsLocationPath);
               // Supprimer les ".."
               vReturn = listProjectModelsLocationPath.get(listProjectModelsLocationPath.size() - nbPointPoint) + targetFolderPropertiesCour + "/";
            }
         }
         else {
            throw new IllegalArgumentException("Cas non traité pour la valeur de \""
                  + AllPropAbstract.PROP_COMMON.path_root_generation.getKey() + "\" : " + targetFolderProperties);
         }
      }

      vReturn = sanitizedPath(vReturn);

      return vReturn;
   }

   private String sanitizedPath (String pPath) {
      return DirectoryUtils.sanitizedPath(pPath);
   }

   /**
    * Savoir le nombre de "../" dans le path.
    * @param pPath Le path à regarder.
    * @return Le nombre désiré.
    */
   private int countNbPointPoint (String pPath) {
      int vReturn;
      String[] tabPointPoint = StringUtils.split(pPath, "../");
      vReturn = tabPointPoint.length;
      return vReturn;
   }

   /**
    * Permet de calculer le path de génération cible. *
    * @param pProjectModelsLocationPath Le path du projet contenant les modèles de génération.
    * @param pTargetFolderProperties Le path défini dans le Properties.
    * @return Le path target pour la génération.
    */
   public String computeTargetPathRoot (String pProjectModelsLocationPath, String pTargetFolderProperties) {
      String vReturn;
      String targetFolderAbsolutePath;
      // Le path cible de la génération
      String targetFolderProperties = pTargetFolderProperties;
      // Ramplacer "\" par "/"
      targetFolderProperties = targetFolderProperties.replace('\\', '/');

      // Le chemin du projet contenant les modèles de génération
      // Ex :
      // "T:\Ws4Cours\WSS4_cours_Acceleo_init\runtime-Eclipse_TD_Acceleo_scripts_configuration\TD_Acceleo_model"
      // Ramplacer "\" par "/"
      pProjectModelsLocationPath = pProjectModelsLocationPath.replace('\\', '/');

      // SI chemin spécifié en absolu
      if (targetFolderProperties.startsWith("/") || targetFolderProperties.indexOf(":/") >= 0) {
         // On prend le path du Properties
         targetFolderAbsolutePath = DirectoryUtils.formatDirectory(targetFolderProperties);
      }
      // SI commence par ".."
      else if (targetFolderProperties.startsWith("../..")) {
         throw new IllegalArgumentException("Cas non traité pour la valeur de \""
               + AllPropAbstract.PROP_COMMON.path_root_generation.getKey() + "\" : " + targetFolderProperties);
      }
      // SI commence par ".."
      else if (targetFolderProperties.startsWith("..")) {
         throw new IllegalArgumentException("Cas non traité pour la valeur de \""
               + AllPropAbstract.PROP_COMMON.path_root_generation.getKey() + "\" : " + targetFolderProperties);
      }
      else {
         String absolutePathModel = pProjectModelsLocationPath;
         targetFolderAbsolutePath = absolutePathModel + targetFolderProperties;
      }

      vReturn = DirectoryUtils.formatDirectory(targetFolderAbsolutePath);

      return vReturn;
   }

   public void validate () {
      String vReturn = "";
      String vMsgFmt = "Erreur d'initialisation sur EngineHelper : ajouter les appels aux setter pour les éléments suivants :\n";

      if (_Bundle == null) {
         vReturn = vReturn + "   _Bundle\n";
      }
      if (_PluginId == null) {
         vReturn = vReturn + "   _PluginId\n";
      }
      if (_TargetFolder == null) {
         vReturn = vReturn + "   _TargetFolder\n";
      }
      if (_AllProperties == null) {
         vReturn = vReturn + "   _AllProperties\n";
      }
      if (_FileNameOfModelInProgress == null) {
         vReturn = vReturn + "   _FileNameOfModelInProgress\n";
      }

      if (false == "".equals(vReturn)) {
         throw new IllegalStateException(vMsgFmt + "\n" + vReturn);
      }
   }

//   /**
//    * Permet d'obtenir le chemin racine pour la génération.
//    * @return Le chemin désiré.
//    */
//   public String getPathRoot () {
//      String vReturn;
//      String path_root_generation = ParamGeneration.getProp_path_root_generation_sce();
//      if (".".equals(path_root_generation)) {
//         vReturn = getPathRoot(".");
//      }
//      else {
//         vReturn = path_root_generation;
//      }
//      return vReturn;
//   }

   /**
    * Obtenir le chemin racine.
    * 
    * @param pPath Le chemin d'entrée (Si "." : racine du projet de modélisation).
    * @return Le chemin désiré.
    */
   public String getPathRoot (String pPath) {
      String vReturn;
      // Si chemin courant
      if (".".equals(pPath)) {
//         IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject();
         IWorkspace workspace;
         try {
            workspace = ResourcesPlugin.getWorkspace();
         }
         catch (Exception err) {
            // RAS : erreur du type "Workspace is closed"
            workspace = null;
         }

         File vFile;
         // Si mode plug-in
         if (workspace != null) {
            vFile = workspace.getRoot().getLocation().toFile();
         }
         else {
            vFile = new File(".");
         }
         vReturn = DirectoryUtils.getWithoutProtocole(vFile.getAbsolutePath());
      }
      else {
         vReturn = pPath;
      }
      return vReturn;
   }

}
