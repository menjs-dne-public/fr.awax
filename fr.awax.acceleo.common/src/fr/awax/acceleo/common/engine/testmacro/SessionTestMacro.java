package fr.awax.acceleo.common.engine.testmacro;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.sirius.business.api.session.ReloadingPolicy;
import org.eclipse.sirius.business.api.session.SavingPolicy;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionEventBroker;
import org.eclipse.sirius.business.api.session.SessionListener;
import org.eclipse.sirius.business.api.session.SessionService;
import org.eclipse.sirius.business.api.session.SessionStatus;
import org.eclipse.sirius.business.api.session.SiriusPreferences;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreter;
import org.eclipse.sirius.ecore.extender.business.api.accessor.ModelAccessor;
import org.eclipse.sirius.tools.api.ui.RefreshEditorsPrecommitListener;
import org.eclipse.sirius.viewpoint.DView;
import org.eclipse.sirius.viewpoint.description.Viewpoint;

import fr.awax.acceleo.common.conf.ModeExecEnum;
import fr.awax.acceleo.common.engine.EngineHelper;

/**
 * Session pour les tests macro.
 * @author MinEdu
 */
public class SessionTestMacro implements Session {
   /** La Map contenant les instances. */
   private static Map<String, SessionTestMacro> _MapInstances = new HashMap<>();

   /**
    * Constructeur par défaut.
    */
   private SessionTestMacro() {
      // RAS
   }

   private String _PLuginId;

   /**
    * L'instance associé au pluginId.
    * @param pPLuginId L'identifiant du plugin qui a été lancé.
    * @return L'instance désirée.
    */
   public synchronized static SessionTestMacro getInstance (String pPLuginId) {
      SessionTestMacro vReturn = _MapInstances.get(pPLuginId);
      // SI pas encore instanciée
      if (vReturn == null) {
         vReturn = new SessionTestMacro();
         vReturn._PLuginId = pPLuginId;
         _MapInstances.put(pPLuginId, vReturn);
      }

      return vReturn;
   }

   @Override
   public void open (IProgressMonitor pMonitor) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public boolean isOpen () {
      return true;
   }

   @Override
   public String getID () {
      return ModeExecEnum.runTestMacro.name();
   }

   @Override
   public Resource getSessionResource () {
      EngineHelper vEngineHelper = EngineHelper.getInstance(_PLuginId);
      return vEngineHelper.getResource();
   }

   @Override
   public Collection<Resource> getSemanticResources () {
      EngineHelper vEngineHelper = EngineHelper.getInstance(_PLuginId);
      return vEngineHelper.getListResource();
   }

   @Override
   public Set<Resource> getReferencedSessionResources () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public Set<Resource> getAllSessionResources () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void addSemanticResource (URI pSemanticResourceURI, IProgressMonitor pMonitor) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void removeSemanticResource (Resource pSemanticResource, IProgressMonitor pMonitor, boolean pRemoveReferencingResources) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void save (IProgressMonitor pMonitor) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void save (Map<?, ?> pOptions, IProgressMonitor pMonitor) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void close (IProgressMonitor pMonitor) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public Collection<Viewpoint> getSelectedViewpoints (boolean pIncludeReferencedAnalysis) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void createView (Viewpoint pViewpoint, Collection<EObject> pSemantics, IProgressMonitor pMonitor) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void createView (Viewpoint pViewpoint, Collection<EObject> pSemantics, boolean pCreateNewRepresentations, IProgressMonitor pMonitor) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void addSelectedView (DView pView, IProgressMonitor pMonitor) throws IllegalArgumentException {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void removeSelectedView (DView pView, IProgressMonitor pMonitor) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public Collection<DView> getSelectedViews () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public Collection<DView> getOwnedViews () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void addListener (SessionListener pListener) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void removeListener (SessionListener pListener) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public ECrossReferenceAdapter getSemanticCrossReferencer () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public SessionService getServices () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public SessionStatus getStatus () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void setReloadingPolicy (ReloadingPolicy pReloadingPolicy) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public ReloadingPolicy getReloadingPolicy () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public void setSavingPolicy (SavingPolicy pSavingPolicy) {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public SavingPolicy getSavingPolicy () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public SessionEventBroker getEventBroker () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public RefreshEditorsPrecommitListener getRefreshEditorsListener () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public Collection<Resource> getSrmResources () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public TransactionalEditingDomain getTransactionalEditingDomain () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public ModelAccessor getModelAccessor () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public IInterpreter getInterpreter () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

   @Override
   public SiriusPreferences getSiriusPreferences () {
      throw new UnsupportedOperationException("Méthode non implémentée à ce niveau");
   }

}
