package fr.awax.acceleo.common.engine;

import java.util.ArrayList;
import java.util.List;

import fr.awax.acceleo.common.conf.XPropElem;

public class TrtPostGeneration {
   // Pour ne faire aucune traitement post génération. */
   public static final TrtPostGeneration NONE = new TrtPostGeneration(TrtPostGenerationEnum.none);

   public TrtPostGeneration(TrtPostGenerationEnum... pTabTrtPostGenerationEnum) {
      super();
      setTrtPostGenerationEnum(pTabTrtPostGenerationEnum);
   }

   /** Le traitement post-génération. */
   private TrtPostGenerationEnum[] _TabTrtPostGenerationEnum;

   public TrtPostGenerationEnum[] getTabTrtPostGenerationEnum () {
      return _TabTrtPostGenerationEnum;
   }

   private void setTrtPostGenerationEnum (TrtPostGenerationEnum... pTabTrtPostGenerationEnum) {
      _TabTrtPostGenerationEnum = pTabTrtPostGenerationEnum;
   }

   /** La liste des extensions a prendre en compte. */
   private List<String> _ListExtensionFile = new ArrayList<>();

   public List<String> getListExtensionFile () {
      return _ListExtensionFile;
   }

   public void addExtensionFile (String pExtensionFile) {
      _ListExtensionFile.add(pExtensionFile);
   }

   /** La liste des chemins concernés par le traitement. */
   private List<XPropElem> _ListXPropElem = new ArrayList<>();

   public List<XPropElem> getListXPropElem () {
      return _ListXPropElem;
   }

   public void addXPropElem (XPropElem pXPropElem) {
      _ListXPropElem.add(pXPropElem);
   }

}
