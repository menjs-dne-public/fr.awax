package fr.awax.acceleo.common.engine.error;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.awax.acceleo.common.engine.Clearable;
import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.exception.ModelisationRuntimeException;

/**
 * Permet de centraliser la gestion d'erreur de génération afin qu'elle
 * remonte dans le code généré.
 * 
 * <code>
 *    String v_return;
 *    ...
 *    v_return = ErrorGeneration.writeErrorGeneration("Description de l'erreur", "Solution potentielle");
 * </code>
 * 
 * @author MinEdu
 */
public class ErrorControler implements Clearable {
   /** Mémoriser les erreurs de génération */
   private static final List<ErrorMessageWrapped> _ListErrorMessage = new ArrayList<ErrorMessageWrapped>();
   /** Indiquer s'il y a eu une erreur de génération */
   private static boolean _isErreurGeneration = false;
   /** Le nombre d'erreurs survenue pendant la génération. */
   private static int _cptErreurGeneration = 0;

   /** La Map contenant les instances. */
   private static Map<String, ErrorControler> _MapInstances = new HashMap<>();

   /**
    * Constructeur par défaut.
    */
   private ErrorControler() {
      // RAS
   }

   public synchronized static ErrorControler getInstance (String pPluginId) {
      ErrorControler vReturn = _MapInstances.get(pPluginId);
      // SI pas encore instanciée
      if (vReturn == null) {
         vReturn = new ErrorControler();
         _MapInstances.put(pPluginId, vReturn);
      }

      return vReturn;
   }

   /**
    * Remise à zéro des exceptions encapsulées.
    */
   @Override
   public void clear () {
      _isErreurGeneration = false;
      _ListErrorMessage.clear();
      ErrorMessageWrapped.init();
   }

   public List<ErrorMessageWrapped> getListErrorMessage () {
      return _ListErrorMessage;
   }

   /**
    * Permet d'obtenir le message formaté sous la forme d'une 'String' et de l'écrire dans la vue "Error Logs". <br/>
    * <code>
    *    // Au lieu de soulever une exception du type : throw new GenerationRuntimeException("Impossible d'avoir plusieurs return", "Sur l'opération \"" + pOperation.getName() + "\" du service \"" + pService.getName() + "\", mettre aucun ou seul output");
    *    // Faire :
    *    String vReturn = ErrorGeneration.writeErrorGeneration("Impossible d'avoir plusieurs return", "Sur l'opération \"" + pOperation.getName() + "\" du service \"" + pService.getName() + "\", mettre aucun ou seul output");
    * </code>
    * 
    * @param pMessage Le message d'erreur.
    * @param pRootCause L'exception à l'origine de l'erreur.
    * @param pTabSolutionPotentielle Le tableau de messages indiquant la (ou les) solution(s) potentielle(s).
    * @return L'erreur formatée.
    */
   public String writeErrorGeneration (final String pMessage, Throwable pRootCause, final String... pTabSolutionPotentielle) {
      _cptErreurGeneration++;
      _isErreurGeneration = true;
      final ErrorMessageWrapped vErrorMessageWrapped;
      if (pRootCause == null) {
         vErrorMessageWrapped = new ErrorMessageWrapped(pMessage, pRootCause, pTabSolutionPotentielle);
      }
      else {
         if (pRootCause.getMessage() != null && pRootCause.getMessage().equals(pMessage)) {
            vErrorMessageWrapped = new ErrorMessageWrapped(pMessage, pRootCause, pTabSolutionPotentielle);
         }
         else {
            vErrorMessageWrapped = new ErrorMessageWrapped(pMessage + " : " + pRootCause.getMessage(), pRootCause, pTabSolutionPotentielle);
         }
      }

      // Mémoriser la 3 premières erreurs : ça suffit amplement, car X fois la même (N sur chaque table)
      if (_cptErreurGeneration <= 3) {
         _ListErrorMessage.add(vErrorMessageWrapped);
         System.err.println("Erreur lors de la génération : " + vErrorMessageWrapped);
      }
      return vErrorMessageWrapped.toString();
   }

   public String writeErrorGeneration (final Throwable pException, final String... pTabSolutionPotentielle) {
      return writeErrorGeneration(null, pException, pTabSolutionPotentielle);
   }

   public String writeErrorGeneration (final String pMessage, final String... pTabSolutionPotentielle) {
      return writeErrorGeneration(pMessage, getNewException(pMessage), pTabSolutionPotentielle);
   }

   /**
    * Soulève directement une exception pour sortir du traitement.
    * @param pMessage Le message.
    * @param pTabSolutionPotentielle La (ou les) solution(s) potentielle(s).
    * @return RAS
    * @throws ErrorGenerationException L'exception désiré.
    */
   public String writeErrorGeneration_throws (final Exception pException, final String... pTabSolutionPotentielle)
         throws ErrorGenerationException {
      throw new ErrorGenerationException(writeErrorGeneration(pException.getMessage(), pException, pTabSolutionPotentielle), pException);
   }

   public String writeErrorGeneration_throws (final String pMessage, final String... pTabSolutionPotentielle)
         throws ErrorGenerationException {
      throw new ErrorGenerationException(writeErrorGeneration(pMessage, pTabSolutionPotentielle));
   }

   public String writeErrorGeneration_throws (final String pMessage, Throwable pRootCause, final String... pTabSolutionPotentielle)
         throws ErrorGenerationException {
      throw new ErrorGenerationException(writeErrorGeneration(pMessage + " : " + pRootCause.getMessage(), pRootCause, pTabSolutionPotentielle), pRootCause);
   }

   private Throwable getNewException (final String pMessage) {
      Throwable vReturn;
      try {
         vReturn = new RuntimeException(pMessage);
      }
      catch (Exception vErr) {
         vReturn = vErr;
      }
      return vReturn;
   }

   /**
    * Permet de lancer une "ErrorGenerationException" des erreurs de
    * génération survenues. (et donc un affichage dans la vue "Error Log"
    * d'Eclipse).
    */
   public void doIfThrowErrorGenerationException () {
      // Parcourir les erreurs mémorisées
      for (ErrorMessageWrapped vErrorMessage : _ListErrorMessage) {
         doTrtErrorGeneration(vErrorMessage);
      }
   }

   public void doIfThrowErrorGenerationException_sce () {
      doIfThrowErrorGenerationException();
   }

   /**
    * Permet de savoir s'il y a eu une erreur de génération.
    * @return 'true' s'il y a eu une erreur de génération.
    */
   public boolean isErreurGeneration () {
      return _isErreurGeneration;
   }

   private void doTrtErrorGeneration (ErrorMessageWrapped pErrorMessage) {
      // Obtenir l'erreur racine encapsulant toutes les autres erreurs de génération
      final ErrorGenerationException vRootException = ErrorMessageWrapped.getRootErrorGenerationException();

      // Si on a une exception
      if (vRootException.getListExceptionSupplem().isEmpty() == false) {
         throw vRootException;
      }
   }

   /**
    * Soulever une violation d'une incohénrence dans la modélisation.
    * @param pMessageViolation
    * @param pSolutionPotentielle
    * @throws ModelisationRuntimeException
    */
   public void violationRuleModelisation (String pMessageViolation, String pSolutionPotentielle) throws ModelisationRuntimeException {
      writeErrorGeneration(pMessageViolation, pSolutionPotentielle);
//      System.out.println(pMessageViolation + " --> " + pSolutionPotentielle);
   }

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

// /**
// * Permet d'obtenir le message formaté sous la forme d'une 'String' et de l'écrire dans la vue "Error Logs". <br/>
// * @param pMessage Le message d'erreur.
// * @param pSolutionPotentielle Le message indiquant la solution potentielle
// */
//public static void writeErrorGeneration_throws (final String pMessage, final String ... pTabSolutionPotentielle) {
//   writeErrorGeneration(pMessage, pTabSolutionPotentielle);
//   throw new RuntimeException(pMessage, pTabSolutionPotentielle);
//}

}
