package fr.awax.acceleo.common.engine.error;

import java.util.ArrayList;
import java.util.List;

import fr.awax.acceleo.common.util.ExceptionUtils;

/**
 * Exception pour indiquer une erreur de génération.
 */
class ErrorGenerationException extends RuntimeException {
   /** Pour la sérialisation */
   private static final long serialVersionUID = 1L;

   public static final String c_libErrorGeneration = "[ERROR_GEN] --> ";

   /** L'exception encapsulée */
   private final Throwable _ExceptionWrapped;

   /** Solution(s) potentielle(s) à l'erreur */
   private final String[] _TabSolutionPotentielle;

   /**
    * La liste d'erreur de génération supplémentaire (cas d'une
    * 'ErrorGenerationException' en chapeau des autres pour affichage dans le
    * "Error Log")
    */
   private final List<ErrorGenerationException> _lstExceptionSupplem = new ArrayList<ErrorGenerationException>();

   /**
    * Constructeur.
    * 
    * @param pExceptionWrapped
    *           L'exception encapsulée.
    * @param pSolutionPotentielle
    *           Le message indiquant la (ou les) solution(s) potentielle(s).
    */
   public ErrorGenerationException(final Throwable pExceptionWrapped, final String... pSolutionPotentielle) {
      super(pExceptionWrapped.getMessage());
      _ExceptionWrapped = pExceptionWrapped;
      _TabSolutionPotentielle = pSolutionPotentielle;
   }

   public ErrorGenerationException(final String pMessage, final Throwable pExceptionWrapped, final String... pTabSolutionPotentielle) {
      super(pMessage, pExceptionWrapped);
      _ExceptionWrapped = pExceptionWrapped;
      _TabSolutionPotentielle = pTabSolutionPotentielle;
   }

   /**
    * Constructeur.
    * 
    * @param pMessage
    *           Le message d'erreur.
    * @param pTabSolutionPotentielle
    *           Le (ou les) message(s) indiquant la (ou les) solution(s) potentielle(s).
    */
   public ErrorGenerationException(final String pMessage, final String... pTabSolutionPotentielle) {
      super(pMessage);
      _ExceptionWrapped = null;
      _TabSolutionPotentielle = pTabSolutionPotentielle;
   }

   /**
    * Obtenir la solution potentielles.
    * 
    * @return La chaîne désirée.
    */
   public String[] getTabSolutionPotentielle () {
      return _TabSolutionPotentielle;
   }

   /**
    * Permet d'ajouter une erreur de génération.
    * 
    * @param pException
    *           L'exception à ajouter.
    * @param pTabSolutionPotentielle
    *           Le message indiquant la (ou les) solution(s) potentielle(s).
    */
   public void addException (final String p_message, final Throwable pException, final String... pTabSolutionPotentielle) {
      // Mémoriser l'erreur de génération.
      final ErrorGenerationInternException vErrorGeneration = new ErrorGenerationInternException(p_message, pException, pTabSolutionPotentielle);
      // Obtenir la dernière erreur de génération mémorisée
      final ErrorGenerationException vLastException = getLastErrorGenerationException();
      // Chaîner les "RootCause"
//      vLastException.initCause(vErrorGeneration);

      // Mémoriser l'erreur de génération.
      _lstExceptionSupplem.add(vErrorGeneration);
   }

   /**
    * Obtenir la dernière erreur de génération mémorisée
    * 
    * @return L'erreur désirée.
    */
   public ErrorGenerationException getLastErrorGenerationException () {
      ErrorGenerationException vLastException;
      // Si aucune élément
      if (_lstExceptionSupplem.isEmpty() == true) {
         vLastException = this;
      }
      // Si on a au moins 1 élément
      else {
         vLastException = _lstExceptionSupplem.get(_lstExceptionSupplem.size() - 1);
      }
      return vLastException;
   }

   /**
    * Obtenir la liste des axception ajoutées.
    * 
    * @return La liste désirée.
    * @see #addException(Exception)
    */
   List<ErrorGenerationException> getListExceptionSupplem () {
      return _lstExceptionSupplem;
   }

   @Override
   public String toString () {
      return toStringFmt1() + toStringFmt2();
   }

   /**
    * Obtenir l'erreur sous la forme d'une chaîne de caractères (sans les
    * exceptions encapsulée).
    * 
    * @return La chaîne de caractères représente l'instance.
    */
   private String toStringFmt1 () {
      String v_return = c_libErrorGeneration + getMessage();
//      String v_return = c_libErrorGeneration;

      // Si l'on a une Exception encapsulée
      if (_ExceptionWrapped != null) {
         // Obtenir la pile d'appels sous forme de liste
         List<String> vStackTraceAsList = ExceptionUtils.getStackTraceAsList(_ExceptionWrapped);
         String vStackTraceAsString = ExceptionUtils.toStringStackTraceUsefull(this.getClass(), vStackTraceAsList);
//         v_return = v_return + " [Exception encapsulée = " + _ExceptionWrapped.getClass().getName() + " : " + _ExceptionWrapped.getMessage() + " --> " + vStackTraceAsList.toString() + "] ";
         v_return = v_return + vStackTraceAsString;
      }

      // Si on a une solution potentielle
      if (_TabSolutionPotentielle != null) {
         v_return = v_return + " Essayer de : \n";
         for (String solutionPotentielle : _TabSolutionPotentielle) {
            v_return = v_return + "   - " + solutionPotentielle + "\n";
         }
      }

      return v_return;
   }

   /**
    * Obtenir l'erreur sous la forme d'une chaîne de caractères avec
    * uniquement les exceptions encapsulées.
    * 
    * @return La chaîne de caractères représente l'instance des exceptions
    *         encapsulées.
    */
   private String toStringFmt2 () {
      String v_return = "";
      // Si au moins une erreur de génération supplémentaire
      if (_lstExceptionSupplem.isEmpty() == false) {
         // Parcourir les erreurs supplémentaires
         for (int v_i = 0; v_i < _lstExceptionSupplem.size(); v_i++) {
            // Ajouter l'erreur supplémentaire courante
            v_return = v_return + "\n" + _lstExceptionSupplem.get(v_i).toStringFmt1();
         }
      }
      return v_return;
   }
}

/**
 * Permet de gérer un sous-erreur inclue dans une "ErrorGenerationException".
 * @author MinEdu
 */
class ErrorGenerationInternException extends ErrorGenerationException {

   /** Pour le mécanisme de sérialisation Java */
   private static final long serialVersionUID = 1L;

   /**
    * Constructeur.
    * @param pException L'exception.
    * @param pTabSolutionPotentielle La solution potentielle.
    */
   ErrorGenerationInternException(String pMessage, Throwable pException, String... pTabSolutionPotentielle) {
      super(pMessage, pException, pTabSolutionPotentielle);
   }

   ErrorGenerationInternException(Throwable pException, String pSolutionPotentielle) {
      super(pException, pSolutionPotentielle);
   }

}