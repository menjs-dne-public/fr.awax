package fr.awax.acceleo.common.engine.error;

import fr.awax.acceleo.common.exception.GenerationRuntimeException;
import fr.awax.acceleo.common.util.ExceptionUtils;

// import fr.pacman.commons.common.PacmanPropertiesManager;

/**
 * Représente un message d'erreur.
 */
public class ErrorMessageWrapped {
   /** Exception de génération "chapeau" */
   private static ErrorGenerationException _RootException = null;
   private GenerationRuntimeException _GenerationRuntimeException;

   public GenerationRuntimeException getGenerationRuntimeException () {
      return _GenerationRuntimeException;
   }

   /**
    * Permet de constituer l'exception encapsulant toutes les erreurs de génération de l'exécution.
    * 
    * @return La 'ErrorGenerationException' racine.
    */
   static ErrorGenerationException getRootErrorGenerationException () {
      if (_RootException == null) {
         // Nouvelle instance à cause de la RootCause que l'on ne peut pas réinitialiser
         _RootException = new ErrorGenerationException("Il y a des erreurs de génération", "regarder les erreurs dans les fichiers générés et corriger les lignes avec les balises \"" + ErrorGenerationException.c_libErrorGeneration + "\"");
      }
      return _RootException;
   }

   /**
    * Constructeur.
    * 
    * @param p_Exception
    *           L'exception.
    * @param pTabSolutionPotentielle
    *           Le (ou les) message(s) indiquant la (ou les) solution(s) potentielle(s).
    */
   ErrorMessageWrapped(final Throwable p_Exception, final String... pTabSolutionPotentielle) {
      super();
      // Si pas de solution
      if ((pTabSolutionPotentielle == null) || (pTabSolutionPotentielle.length == 0)) {
         // Mémoriser l'erreur de génération
         getRootErrorGenerationException().addException(null, p_Exception, null);
      }
      // Si on a au moins 1 solution
      else {
         // Mémoriser l'erreur de génération
         getRootErrorGenerationException().addException(null, p_Exception, pTabSolutionPotentielle);
      }
      _GenerationRuntimeException = new GenerationRuntimeException("", p_Exception, pTabSolutionPotentielle);
   }

   /**
    * Constructeur.
    * 
    * @param p_message
    *           Le message d'erreur.
    * @param pTabSolutionPotentielle
    *           Le (ou les) message(s) indiquant la (ou les) solution(s) potentielle(s).
    */
   ErrorMessageWrapped(final String p_message, Throwable pRootCause, final String... pTabSolutionPotentielle) {
//      this(new RuntimeException(p_message, pRootCause), pTabSolutionPotentielle);
      super();
      // Si pas de solution
      if ((pTabSolutionPotentielle == null) || (pTabSolutionPotentielle.length == 0)) {
         // Mémoriser l'erreur 
         getRootErrorGenerationException().addException(p_message, pRootCause, null);
      }
      // Si on a au moins 1 solution
      else {
         // Mémoriser l'erreur 
         getRootErrorGenerationException().addException(p_message, pRootCause, pTabSolutionPotentielle);
      }
      _GenerationRuntimeException = new GenerationRuntimeException(p_message, pRootCause, pTabSolutionPotentielle);
   }

   /**
    * Remise à zéro des exceptions encapsulées.
    * 
    * @return l'exception racine
    */
   static void init () {
      _RootException = null;
   }

   @Override
   public String toString () {
      String v_return = ErrorGenerationException.c_libErrorGeneration;

      // Message
      // -------
      // Formater le message avec StackTrace utile (jusqu'au dernier package PacMan)
      v_return = v_return + ExceptionUtils.toStringStackTraceUsefull(getClass(), ExceptionUtils.getStackTraceAsList(getRootErrorGenerationException().getLastErrorGenerationException()));

      // Solution potentielle
      // --------------------

//      // Si pas de solution potentielle
//      if (getRootErrorGenerationException().getLastErrorGenerationException().getTabSolutionPotentielle() == null) {
//         v_return = v_return + " ==> Pas de solution potentielle pour l'instant";
//      }
//      // Si on a une solution potentielle
//      else {
//         for (String solutionPotentielle : getRootErrorGenerationException().getLastErrorGenerationException().getTabSolutionPotentielle())
//         {
//            v_return = v_return + " ==> " + solutionPotentielle + "\n";
//         }
//      }

      return v_return;
   }

}
