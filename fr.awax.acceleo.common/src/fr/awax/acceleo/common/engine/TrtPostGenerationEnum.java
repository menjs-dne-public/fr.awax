package fr.awax.acceleo.common.engine;

/**
 * La liste des traitements possibles après la génération du code source Acceleo.
 */
public enum TrtPostGenerationEnum {
   none,
   /** Effacer les lignes vides après la génération. */
   deleteEmptyLines;
}
