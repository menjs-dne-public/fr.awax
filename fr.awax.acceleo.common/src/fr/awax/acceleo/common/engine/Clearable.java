package fr.awax.acceleo.common.engine;

public interface Clearable {
   /**
    * Permet de faire un RAZ des données 'static' de cache.
    */
   abstract public void clear ();
}
