package fr.awax.acceleo.common.engine.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.acceleo.engine.service.AbstractAcceleoGenerator;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.sirius.business.api.modelingproject.ModelingProject;
import org.eclipse.sirius.business.api.session.DefaultLocalSessionCreationOperation;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionCreationOperation;
import org.eclipse.sirius.ext.base.Option;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.handlers.HandlerUtil;
import org.obeonetwork.dsl.cinematic.CinematicRoot;
import org.obeonetwork.dsl.cinematic.util.CinematicResourceFactoryImpl;
import org.obeonetwork.dsl.database.DataBase;
import org.obeonetwork.dsl.database.impl.DatabaseFactoryImpl;
import org.obeonetwork.dsl.entity.util.EntityResourceFactoryImpl;
import org.obeonetwork.dsl.requirement.Repository;
import org.obeonetwork.dsl.requirement.util.RequirementResourceFactoryImpl;
import org.obeonetwork.dsl.soa.util.SoaResourceFactoryImpl;
import org.osgi.framework.Bundle;

import fr.awax.acceleo.common.VersionGenerator;
import fr.awax.acceleo.common.conf.AllPropAbstract;
import fr.awax.acceleo.common.conf.ModeExecEnum;
import fr.awax.acceleo.common.dsl.Dsl_Enum;
import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.engine.TrtPostGeneration;
import fr.awax.acceleo.common.engine.error.ErrorControler;
import fr.awax.acceleo.common.engine.error.ErrorMessageWrapped;
import fr.awax.acceleo.common.engine.testmacro.SessionTestMacro;
import fr.awax.acceleo.common.exception.GenerationException;
import fr.awax.acceleo.common.exception.GenerationRuntimeException;
import fr.awax.acceleo.common.log.LogHelper;
import fr.awax.acceleo.common.only_export.SceLogInstance;
import fr.awax.acceleo.common.util.DateUtils;
import fr.awax.acceleo.common.util.DateUtils.MasqueEnum;
import fr.awax.acceleo.common.util.EclipseUiUtils;
import fr.awax.acceleo.common.util.FileUtils;
import fr.awax.acceleo.common.util.rcpe.ConsoleViewHelper;

abstract public class AcceleoGeneratePluginAbstract {
   /** Instance de plugin Acceleo ayant cliqué depuis la racine d'un projet. */
   private AcceleoGeneratePluginProjectAbstract _AcceleoGeneratePluginProject;
   /** Le service de Log. */
   private SceLogInstance _Log;
   /** Les propriétés lus dans le fichier nécessaires pour le générateur. */
   private AllPropAbstract _AllProp;

//   /**
//    * Obtenir une nouvelle instance de cette classe racine.
//    * @return
//    */
//   public static AcceleoGeneratePluginAbstract newInstance () {
//      return new AcceleoGeneratePluginAbstract();
//   }

   /**
    * Dézippe les ASSETS nécessaires pour utiliser les éléments générés.
    */
   abstract public void unzipAsserts (String pTargetAbsolutePath, AllPropAbstract pAllProp);

   public void doUnzipAsserts (AllPropAbstract pAllProp, String pPathDezip) {
      String pluginId = _AllProp.getPlugInId();
      EngineHelper vEngineHelper = EngineHelper.getInstance(pluginId);
      File fileOut = new File(pPathDezip);
//      File fileOut = new File(ParamGeneration.get_directory_generation_projet_root() + "/" + ParamGenerationMaquette.getPath_generation_projet_maquette_srcMaquette_sce());
      URL assetsZipFileUrl = vEngineHelper.getAssetsZipFileUrl();
      if (assetsZipFileUrl == null) {
         _Log.info_sce("Pas de Assets spécifié - Cf. SiGestionMaquette_AcceleoGenPluginHandler.execute(...)");
      }
      else {
         try {
            FileUtils.unzip(assetsZipFileUrl, fileOut);
            _Log.info_sce("Décompression des ASSETS de \"" + assetsZipFileUrl + "\" vers \"" + fileOut.getAbsolutePath() + "\" terminé");
         }
         catch (IOException vErr) {
            throw new GenerationRuntimeException("Problème lors du unzip des Assets de \"" + assetsZipFileUrl + "\" vers \"" + fileOut.getAbsolutePath() + "\"", vErr);
         }
      }
   }

   /**
    * Lancer la génération sur un conteneur du modèle spécifiée.
    * 
    * @param pSession La session courante.
    * @param pTargetAbsolutePath Le répertoire cible de génération.
    * @param pMonitor Le moniteur de progression.
    * @param pModelRoot La racine du modèle.
    * @throws IOException Si une erreur se produit.
    */
   abstract public void runGenerationAcceleo (Dsl_Enum pDsl_Enum, Session pSession, String pTargetAbsolutePath, IProgressMonitor pMonitor, EObject pModelRoot) throws IOException;

   public Object execute_root (
         final AcceleoGeneratePluginProjectAbstract pAcceleoGeneratePluginProjectRootAbstract,
         final AllPropAbstract pAllProp,
         final ExecutionEvent pEvent, final String pJobName, final String pGenerationName,
         final String pAssetsZipPath, final TrtPostGeneration pTrtPostGeneration)
         throws ExecutionException {
      Object vReturn;
      _AllProp = pAllProp;
      _AcceleoGeneratePluginProject = pAcceleoGeneratePluginProjectRootAbstract;
      // Obtenir le "modeling project"
      ISelection selection = HandlerUtil.getCurrentSelection(pEvent);
      try {
         vReturn = execute_rootIntern(pAcceleoGeneratePluginProjectRootAbstract, pAllProp, selection, pJobName, pGenerationName, pAssetsZipPath, pTrtPostGeneration);
      }
      catch (Exception err) {
         String plugInId = pAllProp.getPlugInId();
         ErrorControler vErrorControler = ErrorControler.getInstance(plugInId);
         vErrorControler.writeErrorGeneration("Problème lors du lancement du pluing : " + plugInId, err);
//         vErrorControler.doIfThrowErrorGenerationException();
         ConsoleViewHelper console = ConsoleViewHelper.getInstance(plugInId);
         console.raz();
         List<ErrorMessageWrapped> listErrorMessage = vErrorControler.getListErrorMessage();
         String message = "";
         for (ErrorMessageWrapped vErrorMessageWrapped : listErrorMessage) {
            message = message + vErrorMessageWrapped.getGenerationRuntimeException() + "\n--------------------------------------\n";
         }
         ModelingProject vModelingProject = getSelectedModelingProject(selection);
         String messageErr = "Problème lors du generateFromPlugIn() avec " + vModelingProject + ":\n" + message + "\n" + err.toString();
         // Faire "vErr.printStackTrace()" avant d'avoir rediriger la console : affichage dans l'Eclipse principal
         err.printStackTrace();
         // Rediriger la console dans l'Eclipse contenant le plugin (ou le sous-Eclipse en mode dev)
         console.init();
         console.writeStdOut(messageErr + "\n" + err.getMessage());
//         console.showConsole(new MessageConsole("AWAX", null));
         // Afficher une fenêtre Popup
         MessageDialog.openError(HandlerUtil.getActiveShell(pEvent), pGenerationName,
               messageErr + "\n" + err.getMessage());
         throw new ExecutionException("Problème lors du lancement du pluing : " + plugInId, err);
      }

      return vReturn;
   }

   private Object execute_rootIntern (
         final AcceleoGeneratePluginProjectAbstract pAcceleoGeneratePluginProjectRootAbstract,
         final AllPropAbstract pAllProp, final ISelection pSelection, final String pJobName, final String pGenerationName,
         final String pAssetsZipPath, final TrtPostGeneration pTrtPostGeneration)
         throws Exception {
      String plugInId = pAllProp.getPlugInId();
      // Mémorise l'id du plugin qui est lancé (casse le principe d'instanciantion multi-génération en parallèle)
      EngineHelper.setPluginIdRunning(plugInId);
      EngineHelper vEngineHelper = EngineHelper.getInstance(plugInId);
      // Obtenir le bundle pour le plug-in
      Bundle vBundle = Platform.getBundle(plugInId);
      if (vBundle == null) {
         String message = MessageFormat.format("Plugin: {0} not found in {1}.", new Object[] { plugInId, EclipseUiUtils.class.getName() });
         throw new IllegalStateException(message);
      }
      String vTargetAbsolutePath;
      vEngineHelper.setBundle(vBundle);
      // Instancier le job pour le traitement de génération 
      AcceleoGenerateJob job = new AcceleoGenerateJob(pJobName, pAllProp, this, pAssetsZipPath, pSelection, pTrtPostGeneration);

      // Obtenir le "Modeling Project"
      ModelingProject vModelingProject = getSelectedModelingProject(pSelection);
      // SI mode test macro
      if (vModelingProject == null) {
         vTargetAbsolutePath = vEngineHelper.getTargetFolder();
         vEngineHelper.setFileNameOfModelInProgress(vEngineHelper.getUriOfModelInProgress().toString());
      }
      // SI mode plugin Eclipse (pas test macro)
      else {
         // Obtenir le projet dans le RCP-Eclipse
         IProject project = vModelingProject.getProject();
         // Calculer le targetPath racine de génération
         IPath projectLocation = project.getLocation();
         // Obtenir le chemin en absolu dans lequel se trouve les modèles pour la génération
         // Ex : "T:/WSS5_Industrialisation_SI-RH/WSS_runtime4Plugin_D02/d02_barem_models/"
         vTargetAbsolutePath = job.initTargetPath(vEngineHelper.getAllProperties(), projectLocation);
      }
      // Affecter le path de géération cible
      job.setTargetAbsolutePath(vTargetAbsolutePath);

      // Instancier le Log
      _Log = SceLogInstance.getInstance(plugInId);

      // SI mode test macro
      if (vEngineHelper.getModeExecEnum() == ModeExecEnum.runTestMacro) {
         // Lancement en synchrhone
         job.excecute(new NullProgressMonitor());
      }
      // SINON mode plugin
      else {
         // Lancement en asynchrone
         job.schedule();
      }

      return null;
   }

   public void generateFromPlugIn (Session pSession, AllPropAbstract pAllProp,
         String pTargetAbsolutePath, IProgressMonitor pMonitor, final String pAssetsZipFile) throws IOException, GenerationException {
      String plugInId = pAllProp.getPlugInId();
      // Initialiser les Assets et les lire les Properties
      init(pAllProp, pSession, pTargetAbsolutePath, pMonitor, pAssetsZipFile);

      EngineHelper vEngineHelper = EngineHelper.getInstance(plugInId);
      Collection<Resource> vListResource;
      ResourceSet vResourceSet;
      if (pSession == null) {
         vResourceSet = vEngineHelper.getResourceSet();
         vListResource = vEngineHelper.getListResource();
      }
      else {
         vResourceSet = pSession.getSessionResource().getResourceSet();
         vListResource = pSession.getSemanticResources();
      }

      List<Repository> listRepositoryRoot = getRootRepository(vListResource);
      // Si l'on a un modèle Requirement
      if (listRepositoryRoot != null) {
         // Chargement Requirement
         vResourceSet.getPackageRegistry().put(
               org.obeonetwork.dsl.requirement.RequirementPackage.eINSTANCE.getNsURI(),
               org.obeonetwork.dsl.requirement.RequirementPackage.eINSTANCE);
         vResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("requirement",
               new RequirementResourceFactoryImpl());

         Map<String, Repository> mapMemRequirement = new HashMap<>();
         // Si l'on a au moins un modèle Requirement
         for (Repository repositoryRoot : listRepositoryRoot) {
            Repository vRepositoryTmp = mapMemRequirement.get(repositoryRoot.getName());
            if (vRepositoryTmp == null) {
               mapMemRequirement.put(repositoryRoot.getName(), repositoryRoot);
            }
            else {
               throw new IllegalStateException("GENERATION BLOQUEE !! problème certainement dans l'AIRD qui référence 2 modèles DSL Requirement ayant le même nom : " + repositoryRoot.getName());
            }
            // Lancer la génération
            runGenerationAcceleo(Dsl_Enum.DslRequirement, pSession, pTargetAbsolutePath, pMonitor, repositoryRoot);
         }
      }

      List<DataBase> listDatabaseRoot = getRootDataBase(vListResource);
      if (listDatabaseRoot.size() > 0) {
         // Chargement Cinematic
         vResourceSet.getPackageRegistry().put(org.obeonetwork.dsl.database.DatabasePackage.eINSTANCE.getNsURI(),
               org.obeonetwork.dsl.database.DatabasePackage.eINSTANCE);
         vResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("database",
               new DatabaseFactoryImpl());

         Map<String, DataBase> mapMemDataBase = new HashMap<>();
         // Si l'on a au moins un modèle Cinématic
         for (DataBase databaseRoot : listDatabaseRoot) {
            DataBase vDataBaseTmp = mapMemDataBase.get(databaseRoot.getName());
            if (vDataBaseTmp == null) {
               mapMemDataBase.put(databaseRoot.getName(), databaseRoot);
            }
            else {
               throw new IllegalStateException("GENERATION BLOQUEE !! problème certainement dans l'AIRD qui référence 2 modèles DSL DataBase ayant le même nom : " + databaseRoot.getName());
            }
            // Lancer la génération
            runGenerationAcceleo(Dsl_Enum.DslDataBase, pSession, pTargetAbsolutePath, pMonitor, databaseRoot);
         }
      }

      List<org.obeonetwork.dsl.entity.Root> listEntityRoot = getRootEntity(vListResource);
      // Si l'on a un modèle Entity
      if (listEntityRoot.size() > 0) {
         // Chargement Entity
         vResourceSet.getPackageRegistry().put(org.obeonetwork.dsl.entity.EntityPackage.eINSTANCE.getNsURI(),
               org.obeonetwork.dsl.entity.EntityPackage.eINSTANCE);
         vResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("entity",
               new EntityResourceFactoryImpl());

         Map<String, org.obeonetwork.dsl.entity.Root> mapMemRootEntity = new HashMap<>();
         // Si l'on a au moins un modèle Etity
         for (org.obeonetwork.dsl.entity.Root entityRoot : listEntityRoot) {
            org.obeonetwork.dsl.entity.Root vEntityRoot = mapMemRootEntity.get(entityRoot.getName());
            if (vEntityRoot == null) {
               mapMemRootEntity.put(entityRoot.getName(), entityRoot);
            }
            else {
               throw new IllegalStateException("GENERATION BLOQUEE !! problème certainement dans l'AIRD qui référence 2 modèles DSL Entity ayant le même nom : " + entityRoot.getName());
            }
            // Lancer la génération
            runGenerationAcceleo(Dsl_Enum.DslEntity, pSession, pTargetAbsolutePath, pMonitor, entityRoot);
         }
      }

      List<org.obeonetwork.dsl.soa.System> listSoaRoot = getRootSoa(vListResource);
      // Si l'on a un modèle SOA
      if (listSoaRoot.size() > 0) {
         // Chargement SOA
         vResourceSet.getPackageRegistry().put(org.obeonetwork.dsl.soa.SoaPackage.eINSTANCE.getNsURI(),
               org.obeonetwork.dsl.soa.SoaPackage.eINSTANCE);
         vResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("soa",
               new SoaResourceFactoryImpl());

         Map<String, org.obeonetwork.dsl.soa.System> mapMemRootSoa = new HashMap<>();
         // Si l'on a au moins un modèle SOA
         for (org.obeonetwork.dsl.soa.System soaRoot : listSoaRoot) {
            org.obeonetwork.dsl.soa.System vEntityRoot = mapMemRootSoa.get(soaRoot.getName());
            if (vEntityRoot == null) {
               mapMemRootSoa.put(soaRoot.getName(), soaRoot);
            }
            else {
               throw new IllegalStateException("GENERATION BLOQUEE !! problème certainement dans l'AIRD qui référence 2 modèles DSL SOA ayant le même nom : " + soaRoot.getName());
            }
            // Lancer la génération
            runGenerationAcceleo(Dsl_Enum.DslSoaServices, pSession, pTargetAbsolutePath, pMonitor, soaRoot);
         }
      }

      List<CinematicRoot> listCinematicRoot = getRootCinematic(vListResource);
      // Si l'on a un modèle Cinématic
      if (listCinematicRoot.size() > 0) {
         // Chargement Cinematic
         vResourceSet.getPackageRegistry().put(org.obeonetwork.dsl.cinematic.CinematicPackage.eINSTANCE.getNsURI(),
               org.obeonetwork.dsl.cinematic.CinematicPackage.eINSTANCE);
         vResourceSet.getPackageRegistry().put(org.obeonetwork.dsl.cinematic.flow.FlowPackage.eINSTANCE.getNsURI(),
               org.obeonetwork.dsl.cinematic.flow.FlowPackage.eINSTANCE);
         vResourceSet.getPackageRegistry().put(
               org.obeonetwork.dsl.cinematic.toolkits.ToolkitsPackage.eINSTANCE.getNsURI(),
               org.obeonetwork.dsl.cinematic.toolkits.ToolkitsPackage.eINSTANCE);
         vResourceSet.getPackageRegistry().put(org.obeonetwork.dsl.cinematic.view.ViewPackage.eINSTANCE.getNsURI(),
               org.obeonetwork.dsl.cinematic.view.ViewPackage.eINSTANCE);
         vResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("cinematic",
               new CinematicResourceFactoryImpl());
         // Si l'on a au moins un modèle Cinematic
         for (CinematicRoot cinematicRoot : listCinematicRoot) {
            // Lancer la génération
            runGenerationAcceleo(Dsl_Enum.DslCinematic, pSession, pTargetAbsolutePath, pMonitor, cinematicRoot);
         }
      }

//      // RAZ à la fin de la génération
//      clear(pTabServiceUses);

      // Ecrire les erreurs de génération
      ErrorControler vErrorControler = ErrorControler.getInstance(plugInId);
      vErrorControler.doIfThrowErrorGenerationException();
   }

   /**
    * Permet d'initialiser le plugin de génération.
    * 
    * @param pTabServiceUses
    *           Le tableau contenant les instances de services utilisés.
    * @param pPropertiesEngine
    *           L'instance représentant le moteur de Properties.
    * @param pSession
    *           La session du RCP Eclipse.
    * @param pTargetAbsolutePath
    *           Le répertoire destination.
    * @param pMonitor
    *           Le moniteur du thread de génération.
    * @param pAssetsZipFile
    *           Les éléments "pré-requis" sur lesquels les éléments générés
    *           vont s'appuyer (ex : "images, binaires, ...)")
    * @throws GenerationException
    * @throws IOException
    */
   public void init (AllPropAbstract pAllProp, Session pSession,
         String pTargetAbsolutePath, IProgressMonitor pMonitor, final String pAssetsZipFile) throws GenerationException, IOException {
//      // Si le répertoire n'existe pas
//      if (false == targetFolder.getLocation().toFile().exists()) {
//         targetFolder.getLocation().toFile().mkdirs();
//      }
      String plugInId = pAllProp.getPlugInId();
      ErrorControler vErrorControler = ErrorControler.getInstance(plugInId);
      EngineHelper vEngineHelper = EngineHelper.getInstance(plugInId);

      // RAZ du service d'erreur de génération
      vErrorControler.clear();

//      Method vMethod;
//      try {
//         // Obtenir le Properties projet
//         vMethod = pPropertiesEngine.getMethod("getInstance");
//         // Invoquer le "getInstance()"
//         _PropertiesProjectEngineSingleton = (PropertiesProjectEngineSingleton) vMethod.invoke(pPropertiesEngine);
//      }
//      catch (Exception err) {
//         throw new RuntimeException("Problème lors de l'instanciation de moteur de Properties", err);
//      }
      String update_check = pAllProp.propCommon().update_check.getValue().toString();
      // SI la mise à jour obligatoire
      if ((update_check != null) && ("true".equalsIgnoreCase(update_check))) {
         // Obtenir la version actuelle du plugin
         // Ex : "La date du build du générateur Indus_SIRH - 1.7.4_20190626_080000"
         String versionActual = doDateExtract(VersionGenerator.getVersion());
         // Obtenir la dernière version déposée sur le réseau
         // Ex : "La date du build du générateur Indus_SIRH - 1.7.4_20200625_090000";
         String versionLast = getLastVersionPlugIn();
         // SI la version est identique
         if (isVersionEquals(versionActual, versionLast)) {
            // Lancer la génération
            doGenerate(pAllProp, pMonitor, pTargetAbsolutePath, pAssetsZipFile);
         }
         else {
            String messageError = "Lancement de la génération bloqué par le paramètre \"" + pAllProp.propCommon().update_check.getKey() + "\"";
            String messageSol1 = "Mettre à jour le plugin de génération depuis le path spécifié dans la propriété \"" + pAllProp.propCommon().update_pathfile.getKey() + "\" : " + pAllProp.propCommon().update_pathfile.getValue().toString();
            String messageSol2 = "Mettre la valeur à \"false\" dans la propriété \"" + pAllProp.propCommon().update_check.getKey() + "\"";
            vErrorControler.writeErrorGeneration_throws(messageError, messageSol1, messageSol2);
         }
      }
      else {
         doGenerate(pAllProp, pMonitor, pTargetAbsolutePath, pAssetsZipFile);
      }
   }

   private void doGenerate (final AllPropAbstract pAllProp, final IProgressMonitor pMonitor, final String pTargetAbsolutePath, final String pAssetsZipFile) throws IOException {
      String pluginId = _AllProp.getPlugInId();
      EngineHelper vEngineHelper = EngineHelper.getInstance(pluginId);
      Bundle vBundle = Platform.getBundle(pluginId);
      if (vBundle == null) {
         throw new IllegalArgumentException("Impossible de trouvé le bundle \"" + pluginId
               + "\" --> Vérifier le <plugin id=...> dans la feature.xml et dans le MANIFEST.MF sur le \"Bundle-SymbolicName: "
               + pluginId + ";singleton:=true\"");
      }

      if (pAssetsZipFile != null) {
         URL fileURL = FileLocator.find(vBundle, new Path(pAssetsZipFile), null);
         if (fileURL == null) {
            throw new IllegalStateException("Problème sur le plugin - Fichier pour les ASSETS non trouvé : " + pAssetsZipFile + " (" + (FileLocator.getBundleFile(vBundle)).getAbsolutePath() + ")");
         }
         // Affecter le répertoire du fichier assert
         vEngineHelper.setAssetsZipFileUrl(fileURL);

         // Dézipper les assets
         unzipAsserts(pTargetAbsolutePath, pAllProp);
      }

      // Vérifier que le paramétrage est correct
      verifParam();

      // Activation du cache Acceleo (sinon provoque des retours abérants pour "isNeverSee()")
//      org.eclipse.acceleo.common.preference.AcceleoPreferences.switchQueryCache(true);
      org.eclipse.acceleo.common.preference.AcceleoPreferences.switchQueryCache(false);

      // Désactiver le feed-back (qui pollue) de la génération Acceleo
      org.eclipse.acceleo.common.preference.AcceleoPreferences.switchDebugMessages(false);
      org.eclipse.acceleo.common.preference.AcceleoPreferences.switchOKNotifications(false);
      org.eclipse.acceleo.common.preference.AcceleoPreferences.switchTraceability(false);
      org.eclipse.acceleo.common.preference.AcceleoPreferences.switchForceDeactivationNotifications(true);

      if (pMonitor != null) {
         pMonitor.subTask("Chargement en cours ...");
         pMonitor.worked(1);
      }
   }

   /**
    * Permet de vérifier que le fichier de paramétrage est correct.
    */
   private void verifParam () {
      String pluginId = _AllProp.getPlugInId();
      EngineHelper vEngineHelper = EngineHelper.getInstance(pluginId);
      // Tester que le niveau de LOG spécifié est correct
      vEngineHelper.getProp_generation_log();
   }

   /**
    * Obtenir la dernière version du plugin mis à disposition.
    * @return La version (ex : "20190626_080000").
    */
   private String getLastVersionPlugIn () {
      ErrorControler vErrorControler = ErrorControler.getInstance(_AllProp.getPlugInId());
      // Obtenir le chemin en absolu vers le fichier contenant la dernière version du plugin (ex : "P:/dsi/realisation/update/plugin_genom/site_version.xml")
      String update_pathfile = _AllProp.propCommon().update_pathfile.getValue().toString();
      String vReturn = null;
      try {
         // Lire le fichier qui doit contenir une seule ligne, par exemple :
         // <version>1.7.4_20190626_080000</version>
         InputStream flux = new FileInputStream(update_pathfile);
         InputStreamReader lecture = new InputStreamReader(flux);
         BufferedReader buff = new BufferedReader(lecture);
         String ligne;
         String lastVersionBrute = "";
         int nbLignes = 0;
         // Parcourir le fichier ligne par ligne
         while ((ligne = buff.readLine()) != null) {
            // SI ce n'est pas une ligne vide
            if (false == "".equals(ligne)) {
               lastVersionBrute = ligne;
               nbLignes++;
            }
         }
         buff.close();

         // SI le nombre de ligne est correct
         if (nbLignes == 1) {
            // Ex : lastVersionBrute = "<version>1.7.4_20190626_080000</version>"
            int length = lastVersionBrute.length();
            int masqueLength = MasqueEnum.AAAAMMJJ_hhmmss.name().length();
            final String tagFin_version = "</version>";
            int tagFin_Length = tagFin_version.length();
            // Ex : vReturn = "20190626_080000"
            vReturn = lastVersionBrute.substring(length - masqueLength - tagFin_Length, length - tagFin_Length);
         }
         else {
            vErrorControler.writeErrorGeneration_throws("Problème lors de la lecture du fichier '" + update_pathfile + "' : il devrait contenir une seule ligne (ex : \"<version>1.7.4_20190626_080000</version>\") et non pas " + nbLignes + " lignes", "Vérifier la présence et le contenu du fichier '" + update_pathfile + "'");
         }
      }
      catch (Exception err) {
         vErrorControler.writeErrorGeneration_throws("Problème lors de la lecture du fichier '" + update_pathfile + "' : " + err.getMessage(), err, "Vérifier la présence et le contenu du fichier '" + update_pathfile + "'");
      }
      return vReturn;
   }

   /**
    * Extraire la date de la version.
    * @param pVersion La version du plugin (ex : "La date du build du générateur Indus_SIRH - 1.7.4_20190626_080000").
    * @return La date (ex : "20190626_080000").
    */
   private String doDateExtract (String pVersion) {
      int length = pVersion.length();
      int masqueLength = DateUtils.MasqueEnum.AAAAMMJJ_hhmmss.name().length();
      return pVersion.substring(length - masqueLength, length);
   }

   private boolean isVersionEquals (String pVersionActual, String pVersionLast) {
      boolean vReturn;

      Date dateVersionActual = DateUtils.fromText(pVersionActual);
      Date dateVersionLast = DateUtils.fromText(pVersionLast);

      vReturn = dateVersionActual.getTime() == dateVersionLast.getTime();
      return vReturn;
   }

   /**
    * Obtenir la racine d'une modélisation "Requirement".
    * 
    * @param pListResource La session courante.
    * @return La racine désirée.
    */
   protected List<org.obeonetwork.dsl.requirement.Repository> getRootRepository (Collection<Resource> pListResource) {
      List<org.obeonetwork.dsl.requirement.Repository> result = new ArrayList<>();
      for (Resource semanticResource : pListResource) {
         for (EObject object : semanticResource.getContents()) {
            if (object instanceof org.obeonetwork.dsl.requirement.Repository) {
               result.add((org.obeonetwork.dsl.requirement.Repository) object);
            }
         }
      }
      return result;
   }

   /**
    * Obtenir la racine d'une modélisation "DataBase".
    * 
    * @param pSession La session courante.
    * @return La racine désirée.
    */
   protected List<org.obeonetwork.dsl.database.DataBase> getRootDataBase (Collection<Resource> pListResource) {
      List<org.obeonetwork.dsl.database.DataBase> result = new ArrayList<>();

      for (Resource semanticResource : pListResource) {
         for (EObject object : semanticResource.getContents()) {
            if (object instanceof org.obeonetwork.dsl.database.DataBase) {
               result.add((org.obeonetwork.dsl.database.DataBase) object);
            }
         }
      }
      return result;
   }

   /**
    * Obtenir la racine d'une modélisation "Entity".
    * 
    * @param session
    *           La session courante.
    * @return La racine désirée.
    */
   protected List<org.obeonetwork.dsl.entity.Root> getRootEntity (Collection<Resource> pListResource) {
      List<org.obeonetwork.dsl.entity.Root> result = new ArrayList<>();
      for (Resource semanticResource : pListResource) {
         for (EObject object : semanticResource.getContents()) {
            if (object instanceof org.obeonetwork.dsl.entity.Root) {
               result.add((org.obeonetwork.dsl.entity.Root) object);
            }
         }
      }
      return result;
   }

   /**
    * Obtenir la racine d'une modélisation "SOA".
    * 
    * @param session
    *           La session courante.
    * @return La racine désirée.
    */
   protected List<org.obeonetwork.dsl.soa.System> getRootSoa (Collection<Resource> pListResource) {
      List<org.obeonetwork.dsl.soa.System> result = new ArrayList<>();
      for (Resource semanticResource : pListResource) {
         for (EObject object : semanticResource.getContents()) {
            if (object instanceof org.obeonetwork.dsl.soa.System) {
               result.add((org.obeonetwork.dsl.soa.System) object);
            }
         }

      }
      return result;
   }

   /**
    * Obtenir la racine d'une modélisation "Cinematic".
    * 
    * @param session
    *           La session courante.
    * @return La racine désirée.
    */
   protected List<CinematicRoot> getRootCinematic (Collection<Resource> pListResource) {
      List<CinematicRoot> result = new ArrayList<>();
      for (Resource semanticResource : pListResource) {
         for (EObject object : semanticResource.getContents()) {
            if (object instanceof CinematicRoot) {
               result.add((CinematicRoot) object);
            }
         }

      }
      return result;
   }

   protected ModelingProject getSelectedModelingProject (ISelection pSelection) {
      // Selection contains one and only one modeling project (EnabledWhen clause in plugin.xml)
      if (pSelection instanceof IStructuredSelection) {
         Object selectedElement = ((IStructuredSelection) pSelection).getFirstElement();
         if (selectedElement instanceof IProject) {
            Option<ModelingProject> optionModelingProject = ModelingProject
                  .asModelingProject((IProject) selectedElement);
            if (optionModelingProject.some()) {
               return optionModelingProject.get();
            }
         }
         if (selectedElement instanceof IFile) {
            IFile selectedFile = (IFile) selectedElement;
            IProject projectSelected = selectedFile.getProject();
            Option<ModelingProject> optionModelingProject = ModelingProject
                  .asModelingProject(projectSelected);
            if (optionModelingProject.some()) {
               return optionModelingProject.get();
            }
         }
         if (selectedElement instanceof StructuredSelection) {
//            return new ModelingProject();
            // Mode test macro
         }
      }
      return null;
   }

   public Session getModelingSession (String pPlugInId, ModelingProject modelingProject, IProgressMonitor monitor) throws CoreException {
      Session session;
      if (modelingProject == null) {
//         SessionCreationOperation vSessionCreationOperation = new DefaultLocalSessionCreationOperation(EngineHelper.getUriOfModelInProgress(), monitor);
//         vSessionCreationOperation.execute(); // FIXME : Provoque une erreur !
//         session = vSessionCreationOperation.getCreatedSession();
         session = SessionTestMacro.getInstance(pPlugInId);
      }
      else {
         session = modelingProject.getSession();
         // SI la session n'existe pas encore
         if (session == null) {

            Option<URI> sessionModelURI = modelingProject.getMainRepresentationsFileURI(monitor, true, false);
            if (sessionModelURI.some()) {
               SessionCreationOperation sessionCreationOperation = new DefaultLocalSessionCreationOperation(
                     sessionModelURI.get(), monitor);
               try {
                  sessionCreationOperation.execute();
               }
               catch (CoreException e) {
                  return null;
               }
               session = sessionCreationOperation.getCreatedSession();
            }
         }
      }
      return session;
   }

   /** La liste des générateurs qui sont lancés. */
   private List<AbstractAcceleoGenerator> _ListGeneratorRunning = new ArrayList<AbstractAcceleoGenerator>();

   /**
    * Ajouter un générateur qui est lancé.
    * 
    * @param pGeneratorRunning
    *           L'instance représentant le générateur.
    */
   protected void addGeneratorRunning (AbstractAcceleoGenerator pGeneratorRunning) {
      _ListGeneratorRunning.add(pGeneratorRunning);
   }

   public void runGenerationAcceleo_root (Dsl_Enum pDsl_Enum, Session pSession, String pTargetAbsolutePath, IProgressMonitor pMonitor, Object pModelRoot) throws IOException {
      if (pModelRoot instanceof org.eclipse.emf.ecore.EObject) {
         // RAS
      }
      else if (pModelRoot instanceof org.eclipse.emf.common.util.URI) {
         // RAS
      }
      else {
         throw new UnsupportedOperationException("Erreur dans la structure du générateur : Lancement uniquement depuis une instance de org.eclipse.emf.ecore.EOject ou org.eclipse.emf.common.util.URI");
      }
//      getPropertiesProjectEngineSingleton().setProjectModelClicked(pTargetFolder);
   }

   public File getAbsolutePathRoot () {
      String plugInId = _AllProp.getPlugInId();
      EngineHelper vEngineHelper = EngineHelper.getInstance(plugInId);
      String pathRoot = vEngineHelper.getPathRoot();

      File vPathRoot = new File(pathRoot);
      return vPathRoot;
   }

}
