package fr.awax.acceleo.common.engine.ui;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.sirius.business.api.modelingproject.ModelingProject;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.ext.base.Option;

import fr.awax.acceleo.common.conf.AllPropAbstract;
import fr.awax.acceleo.common.conf.XPropElem;
import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.engine.TrtPostGeneration;
import fr.awax.acceleo.common.engine.TrtPostGenerationEnum;
import fr.awax.acceleo.common.engine.error.ErrorControler;
import fr.awax.acceleo.common.engine.error.ErrorMessageWrapped;
import fr.awax.acceleo.common.exception.GenerationException;
import fr.awax.acceleo.common.log.LogHelper;
import fr.awax.acceleo.common.only_export.SceLogInstance;
import fr.awax.acceleo.common.util.DirectoryUtils;
import fr.awax.acceleo.common.util.FileUtils;
import fr.awax.acceleo.common.util.rcpe.ConsoleViewHelper;

public class AcceleoGenerateJob extends Job {
   private AcceleoGeneratePluginAbstract _AcceleoGeneratePluginRoot;

   private String _AssetsZipPath;
   /** La sélection de l'utilisateur du plugin. */
   private ISelection _Selection;
   /** Les traitements à lancer après la génération. */
   private TrtPostGeneration _TrtPostGeneration;
   /** Les propriétés pour la génération. */
   private AllPropAbstract _AllProp;
   /** L'identifiant du plugin. */
   private String _PlugInId;
   /** Le service de Log. */
   private SceLogInstance _Log;

   public AcceleoGenerateJob(final String pPlugInId, final AllPropAbstract pAllProp,
         final AcceleoGeneratePluginAbstract pAcceleoGeneratePluginRoot, final String pAssetsZipPath,
         ISelection pSelection, final TrtPostGeneration pTrtPostGeneration) {
      super(pPlugInId);
      _AcceleoGeneratePluginRoot = pAcceleoGeneratePluginRoot;
      _AssetsZipPath = pAssetsZipPath;
      _Selection = pSelection;
      _TrtPostGeneration = pTrtPostGeneration;
      _AllProp = pAllProp;
      _PlugInId = _AllProp.getPlugInId();
      _Log = SceLogInstance.getInstance(_PlugInId);
   }

   /** Le path de génération en absolu. */
   private String _TargetAbsolutePath;

   public String getTargetAbsolutePath () {
      if (_TargetAbsolutePath == null) {
         throw new RuntimeException("La _TargetAbsolutePath ne peut pas être null");
      }
      return _TargetAbsolutePath;
   }

   public void setTargetAbsolutePath (String pTargetAbsolutePath) {
      _TargetAbsolutePath = pTargetAbsolutePath;
   }

   @Override
   protected IStatus run (IProgressMonitor pMonitor) {
      return excecute(pMonitor);
   }

   public IStatus excecute (IProgressMonitor pMonitor) {
      ModelingProject vModelingProject = null;
      EngineHelper vEngineHelper = EngineHelper.getInstance(_PlugInId);

      try {
         // Mémoriser le ProgressMonitor
         vEngineHelper.setProgressMonitor(pMonitor);
         // Obtenir le "Modeling Project"
         vModelingProject = _AcceleoGeneratePluginRoot.getSelectedModelingProject(_Selection);
//         PropertiesProjectEngineSingleton vPropertiesProjectEngineSingleton = vEngineHelper.getPropertiesProjectEngineSingleton();
//         // Mémoriser l'instance de paramétrage associé au générateur
//         ParamGeneration.setInstance(vPropertiesProjectEngineSingleton);
//         // RAZ du Singleton de Properties
//         vPropertiesProjectEngineSingleton.raz();

         // Vérifier que la session est chargée
         Session vSession = _AcceleoGeneratePluginRoot.getModelingSession(_PlugInId, vModelingProject, pMonitor);

         // Mémoriser le fichier de modélisation
         Option<URI> mainRepresentationsFileURI = vModelingProject.getMainRepresentationsFileURI(pMonitor);
         vEngineHelper.setFileNameOfModelInProgress(mainRepresentationsFileURI.toString());

         // Valider l'initialisation
         vEngineHelper.validate();

         // generateFromPlugIn(..., vSession,
         // vModelingProject.getProject().getFolder("myproject"), ...;
         _AcceleoGeneratePluginRoot.generateFromPlugIn(vSession, _AllProp, getTargetAbsolutePath(), pMonitor,
               _AssetsZipPath);

         // Lancer les traitements post-génération de code Acceleo
         for (TrtPostGenerationEnum vTrtPostGenerationEnum : _TrtPostGeneration.getTabTrtPostGenerationEnum()) {
            // SI l'on désire supprimer les lignes vides des fichiers générés
            if (vTrtPostGenerationEnum == TrtPostGenerationEnum.deleteEmptyLines) {
               deletedEmptyLines(vModelingProject);
            }
         }
         // Fermer le log associer au plugInId
         LogHelper vLogHelper = LogHelper.getInstance(_PlugInId);
         vLogHelper.close();
         // A ce niveau la génération du plugin est terminée
         EngineHelper.setPluginIdRunning(null);
         _Log.info_sce("Génération \"" + _PlugInId + "\" terminée");
      }
      catch (Exception vErr) {
         ErrorControler vErrorControler = ErrorControler.getInstance(_PlugInId);
         List<ErrorMessageWrapped> listErrorMessage = vErrorControler.getListErrorMessage();
         String message = "";
         for (ErrorMessageWrapped vErrorMessageWrapped : listErrorMessage) {
            message = message + vErrorMessageWrapped.getGenerationRuntimeException()
                  + "\n--------------------------------------\n";
         }
         String messageErr = "Problème lors du generateFromPlugIn() avec " + vModelingProject + ":\n" + message
               + "\n" + vErr.toString();
         // Faire "vErr.printStackTrace()" avant d'avoir rediriger la console : affichage
         // dans l'Eclipse principal
         vErr.printStackTrace();

         // La console dans Eclipse
         ConsoleViewHelper consoleEclipse = ConsoleViewHelper.getInstance(_PlugInId);
         consoleEclipse.raz();
         // Rediriger la console dans l'Eclipse contenant le plugin (ou le sous-Eclipse
         // en mode dev)
         consoleEclipse.init();
         consoleEclipse.writeStdOut(messageErr + "\n" + vErr.getMessage());
         // Faire "vErr.printStackTrace()" après avoir rediriger la console : affichage
         // dans le sous-Eclipse
         vErr.printStackTrace();
         throw new RuntimeException(messageErr, vErr);
      }

      return Status.OK_STATUS;
   }

   public String initTargetPath (AllPropAbstract pAllProp, IPath projectLocation) throws Exception {
      String vTargetAbsolutePath;
      String plugInId = pAllProp.getPlugInId();
      LogHelper vLogHelper = LogHelper.getInstance(plugInId);
      EngineHelper vEngineHelper = EngineHelper.getInstance(plugInId);
      String pathLocationModels = projectLocation.toFile().getAbsolutePath();
      pAllProp.setPathReading(pathLocationModels);
      List<File> listAllFileProperties = DirectoryUtils.findAllFile(pathLocationModels, ".properties");
      for (File vFileProperties : listAllFileProperties) {
         // Lire les Properties pour la génération
         pAllProp.load(vFileProperties.getAbsolutePath(), false);
      }
      // Vérifier les paramètres obligatoires
      pAllProp.verifPropOblig();
//      AllPropAbstract vAllProperties = vEngineHelper.getAllProperties();
      String vPathRoot = pAllProp.propCommon().path_root_generation.getValue().toString();
      // SI PATH en relatif
      if (isPathRelatif(vPathRoot)) {
         vTargetAbsolutePath = computeTargetPathRelatifRoot(projectLocation, vPathRoot);
      }
      // SINON path en absolu
      else {
         vTargetAbsolutePath = computeTargetPathRoot(projectLocation, vPathRoot);
      }
//      // Mémoriser le target genération
//      ParamGeneration.setProp_path_root_generation_sce(vTargetAbsolutePath);
      // Créer les répertoires (au cas où)
      (new File(vTargetAbsolutePath)).mkdirs();
      vLogHelper.info("vTargetAbsolutePath = " + vTargetAbsolutePath);
      // Mémoriser le chemin où s'exécute le bundle
      vEngineHelper.setTargetFolder(vTargetAbsolutePath);
      return vTargetAbsolutePath;
   }

   /**
    * Savoir si le path est en relatif
    * @param pPathRoot Le path à regarder.
    * @return 'true' si le path est relatif.
    */
   private boolean isPathRelatif (String pPathRoot) {
      boolean vReturn;

      // SI path en relatif
      if (pPathRoot.startsWith(".")) {
         vReturn = true;
      }
      else {
         vReturn = false;
      }
      return vReturn;
   }

   /**
    * Déterminer le chemin en absolu racine à partir d'un path en relatif pour la génération de code.
    * @param pProjectModelsLocation L'instance représent le projet de modélisation.
    * @param pTargetFolderProperties Le path de génération indiqué dans le Properties.
    * @return Le path en absolu.
    */
   private String computeTargetPathRelatifRoot (IPath pProjectModelsLocation, String pTargetFolderProperties) {
      String plugInId = _AllProp.getPlugInId();
      EngineHelper vEngineHelper = EngineHelper.getInstance(plugInId);
      String vReturn = vEngineHelper.computeTargetRelatifPathRoot(pProjectModelsLocation.toFile().getAbsolutePath(),
            pTargetFolderProperties);
      vEngineHelper.setPathRoot(vReturn);

      return vReturn;
   }

   /**
    * Déterminer le chemin en absolu racine à partir d'un path en absolu pour la génération de code.
    * @param pProjectModelsLocation L'instance représent le projet de modélisation.
    * @param pTargetFolderProperties Le path de génération indiqué dans le Properties.
    * @return Le path en absolu.
    */
   private String computeTargetPathRoot (IPath pProjectModelsLocation, String pTargetFolderProperties) {
      String plugInId = _AllProp.getPlugInId();
      EngineHelper vEngineHelper = EngineHelper.getInstance(plugInId);
      String vReturn = vEngineHelper.computeTargetPathRoot(pProjectModelsLocation.toFile().getAbsolutePath(),
            pTargetFolderProperties);
      vEngineHelper.setPathRoot(vReturn);

      return vReturn;
   }

   public void deletedEmptyLines (ModelingProject vModelingProject) throws IOException {
      String plugInId = _AllProp.getPlugInId();
      EngineHelper vEngineHelper = EngineHelper.getInstance(plugInId);
      LogHelper vLogHelper = LogHelper.getInstance(plugInId);
      String pathRoot = vEngineHelper.getPathRoot();
      String absolutePath_models = pathRoot + vModelingProject.getProject().getFullPath().toString();
      // Lancer le filtrage sur les fichiers générés (retours chariots superflux)
      String absolutePath_generation;
      String allPath = "";

      final int cNbOccurs = 1;
      // Parcourir les propriétés ajoutées
      for (XPropElem vXPropElem : _TrtPostGeneration.getListXPropElem()) {
         absolutePath_generation = pathRoot + "/"
               + _AllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
               + vXPropElem.getValue().toString();
         allPath = allPath + "\n" + absolutePath_generation;
         FileUtils.deleteEmptyLines(absolutePath_generation, cNbOccurs, _TrtPostGeneration.getListExtensionFile());
      }

      // Projet client
      absolutePath_generation = pathRoot + "/"
            + _AllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
            + _AllProp.propCommon().path_generation_projet_client_src.getValue().toString();
      allPath = allPath + "\n" + absolutePath_generation;
      FileUtils.deleteEmptyLines(absolutePath_generation, cNbOccurs, _TrtPostGeneration.getListExtensionFile());

      absolutePath_generation = pathRoot + "/"
            + _AllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
            + _AllProp.propCommon().path_generation_projet_client_srcTest.getValue().toString();
      allPath = allPath + "\n" + absolutePath_generation;
      FileUtils.deleteEmptyLines(absolutePath_generation, cNbOccurs, _TrtPostGeneration.getListExtensionFile());

      absolutePath_generation = pathRoot + "/"
            + _AllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
            + _AllProp.propCommon().path_generation_projet_client_srcRessources.getValue().toString();
      allPath = allPath + "\n" + absolutePath_generation;
      FileUtils.deleteEmptyLines(absolutePath_generation, cNbOccurs, _TrtPostGeneration.getListExtensionFile());

      // Projet common
      absolutePath_generation = pathRoot + "/"
            + _AllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
            + _AllProp.propCommon().path_generation_projet_common_src.getValue().toString();
      allPath = allPath + "\n" + absolutePath_generation;
      FileUtils.deleteEmptyLines(absolutePath_generation, cNbOccurs, _TrtPostGeneration.getListExtensionFile());

      absolutePath_generation = pathRoot + "/"
            + _AllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
            + _AllProp.propCommon().path_generation_projet_common_srcTest.getValue().toString();
      allPath = allPath + "\n" + absolutePath_generation;
      FileUtils.deleteEmptyLines(absolutePath_generation, cNbOccurs, _TrtPostGeneration.getListExtensionFile());

      absolutePath_generation = pathRoot + "/"
            + _AllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
            + _AllProp.propCommon().path_generation_projet_common_srcRessources.getValue().toString();
      allPath = allPath + "\n" + absolutePath_generation;
      FileUtils.deleteEmptyLines(absolutePath_generation, cNbOccurs, _TrtPostGeneration.getListExtensionFile());

      // Projet server
      absolutePath_generation = pathRoot + "/"
            + _AllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
            + _AllProp.propCommon().path_generation_projet_server_src.getValue().toString();
      allPath = allPath + "\n" + absolutePath_generation;
      FileUtils.deleteEmptyLines(absolutePath_generation, cNbOccurs, _TrtPostGeneration.getListExtensionFile());

      absolutePath_generation = pathRoot + "/"
            + _AllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
            + _AllProp.propCommon().path_generation_projet_server_srcTest.getValue().toString();
      allPath = allPath + "\n" + absolutePath_generation;
      FileUtils.deleteEmptyLines(absolutePath_generation, cNbOccurs, _TrtPostGeneration.getListExtensionFile());

      absolutePath_generation = pathRoot + "/"
            + _AllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
            + _AllProp.propCommon().path_generation_projet_server_srcRessources.getValue().toString();
      allPath = allPath + "\n" + absolutePath_generation;
      FileUtils.deleteEmptyLines(absolutePath_generation, cNbOccurs, _TrtPostGeneration.getListExtensionFile());
      vLogHelper.info("Filtrage des retours chariots superflux terminé (génération depuis les modèles "
            + absolutePath_models + ") sur les fichiers générés sur les chemins : " + allPath);
   }
}
