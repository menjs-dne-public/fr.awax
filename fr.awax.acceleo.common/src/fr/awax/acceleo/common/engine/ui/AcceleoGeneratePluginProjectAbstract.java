package fr.awax.acceleo.common.engine.ui;

import java.io.File;
import java.io.IOException;

import org.eclipse.acceleo.engine.service.AbstractAcceleoGenerator;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.sirius.business.api.session.Session;

import fr.awax.acceleo.common.conf.AllPropAbstract;
import fr.awax.acceleo.common.dsl.Dsl_Enum;
import fr.awax.acceleo.common.engine.TrtPostGeneration;

abstract public class AcceleoGeneratePluginProjectAbstract
      extends AbstractHandler
//      implements AcceleoGeneratePluginProjectRoot_Itf
{

   /** Instance vers la classe mère. */
   private static AcceleoGeneratePluginAbstract _AcceleoGeneratePlugin;

   public Object execute_root (
         final AllPropAbstract pAllProp,
         AcceleoGeneratePluginAbstract pAcceleoGeneratePlugin,
         final ExecutionEvent pEvent, final String pJobName, final String pGenerationName,
         final String pAssetsZipPath, TrtPostGeneration pTrtPostGeneration)
         throws ExecutionException {
//      // Forcer la lecture du Properties
//      pAllProp.load(); // MEC R1, car pas le path pour lire le Properties
      _AcceleoGeneratePlugin = pAcceleoGeneratePlugin;
      // Exécuter le traitement pour la génération
      return pAcceleoGeneratePlugin.execute_root(this, pAllProp, pEvent, pJobName, pGenerationName, pAssetsZipPath, pTrtPostGeneration);
   }

   public void addGeneratorRunning (AbstractAcceleoGenerator pGeneratorRunning) {
      _AcceleoGeneratePlugin.addGeneratorRunning(pGeneratorRunning);
   }

   public void runGenerationAcceleo_root (Dsl_Enum pDsl_Enum, Session pSession, String pTargetFolder, IProgressMonitor pMonitor, Object pModelRoot) throws IOException {
      _AcceleoGeneratePlugin.runGenerationAcceleo_root(pDsl_Enum, pSession, pTargetFolder, pMonitor, pModelRoot);
   }

   public File getAbsolutePathRoot () {
      return _AcceleoGeneratePlugin.getAbsolutePathRoot();
   }
}
