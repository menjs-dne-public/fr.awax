package fr.awax.acceleo.common.engine;

import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.FileInfoMatcherDescription;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IPathVariableManager;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceFilterDescription;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.junit.Assert;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;

import fr.awax.acceleo.common.conf.AllPropHelper;


public class EngineHelperTest {
   private static final String cPlufinId = "plugin.id.EngineHelperTest";

   /**
    * Cas dans le projet des modèles de génération : "./".
    */
   @Test
   public void computeTargetRelatifPathRoot_CN0() {
      EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
      String actual = vEngineHelper.computeTargetRelatifPathRoot(
            "T:\\Ws4Cours\\WSS4_cours_Acceleo_init\\runtime-Eclipse_TD_Acceleo_scripts_configuration\\TD_Acceleo_model",
            "./TD_Acceleo_gen");
      Assert.assertEquals("T:/Ws4Cours/WSS4_cours_Acceleo_init/runtime-Eclipse_TD_Acceleo_scripts_configuration/TD_Acceleo_model/", actual);
   }
   
   /**
    * Cas dans le projet des modèles de génération : "../".
    */
   @Test
   public void computeTargetRelatifPathRoot_CN1() {
	   EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
	   String actual = vEngineHelper.computeTargetRelatifPathRoot(
			   "T:\\Ws4Cours\\WSS4_cours_Acceleo_init\\runtime-Eclipse_TD_Acceleo_scripts_configuration\\TD_Acceleo_model",
			   "../TD_Acceleo_gen");
	   Assert.assertEquals("T:/Ws4Cours/WSS4_cours_Acceleo_init/runtime-Eclipse_TD_Acceleo_scripts_configuration/TD_Acceleo_gen/", actual);
   }
   
   /**
    * Cas dans le projet des modèles de génération : "../../".
    */
   @Test
   public void computeTargetRelatifPathRoot_CN2a() {
	   EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
	   String actual = vEngineHelper.computeTargetRelatifPathRoot(
			   "T:\\Ws4Cours\\WSS4_cours_Acceleo_init\\runtime-Eclipse_TD_Acceleo_scripts_configuration\\TD_Acceleo_model",
			   "../../");
	   Assert.assertEquals("T:/Ws4Cours/WSS4_cours_Acceleo_init/", actual);
   }

   @Test
   public void computeTargetRelatifPathRoot_CN2b() {
	   EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
	   String actual = vEngineHelper.computeTargetRelatifPathRoot(
			   "T:\\Ws4Cours\\WSS4_cours_Acceleo_init\\runtime-Eclipse_TD_Acceleo_scripts_configuration\\TD_Acceleo_model",
			   "../");
	   Assert.assertEquals("T:/Ws4Cours/WSS4_cours_Acceleo_init/runtime-Eclipse_TD_Acceleo_scripts_configuration/", actual);
   }
   
   /**
    * Cas dans le projet des modèles de génération : "../../".
    */
   @Test
   public void computeTargetRelatifPathRoot_CN2c() {
	   EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
	   String actual = vEngineHelper.computeTargetRelatifPathRoot(
			   "T:\\Ws4Cours\\WSS4_cours_Acceleo_init\\runtime-Eclipse_TD_Acceleo_scripts_configuration\\TD_Acceleo_model",
			   "../../TD_Acceleo_gen");
	   Assert.assertEquals("T:/Ws4Cours/WSS4_cours_Acceleo_init/TD_Acceleo_gen/", actual);
   }
   
   /**
    * Cas dans le projet des modèles de génération : ".".
    */
   @Test
   public void computeTargetPathRoot_CN1() {
	   EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
	   String actual = vEngineHelper.computeTargetPathRoot(
			   "T:\\Ws4Cours\\WSS4_cours_Acceleo_init\\runtime-Eclipse_TD_Acceleo_scripts_configuration\\TD_Acceleo_model",
			   ".");
	   Assert.assertEquals("T:/Ws4Cours/WSS4_cours_Acceleo_init/runtime-Eclipse_TD_Acceleo_scripts_configuration/TD_Acceleo_model/", actual);
   }

   /**
    * Cas chemin en absolu
    */
   @Test
   public void computeTargetPathRoot_CN2() {
      EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
      String actual = vEngineHelper.computeTargetPathRoot(
            "T:\\Ws4Cours\\WSS4_cours_Acceleo_init\\runtime-Eclipse_TD_Acceleo_scripts_configuration\\TD_Acceleo_model",
            "D:/projet-gen");
      Assert.assertEquals("D:/projet-gen/", actual);
   }

   /**
    * Cas en relatif "..".
    */
   @Test(expected=IllegalArgumentException.class)
   public void computeTargetPathRoot_CN3() {
      EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
      String actual = vEngineHelper.computeTargetPathRoot(
            "T:\\Ws4Cours\\WSS4_cours_Acceleo_init\\runtime-Eclipse_TD_Acceleo_scripts_configuration\\TD_Acceleo_model",
            "..");
      Assert.assertEquals("T:/Ws4Cours/WSS4_cours_Acceleo_init/runtime-Eclipse_TD_Acceleo_scripts_configuration/", actual);
   }
   
   /**
    * Cas d'exception en relatif "../..".
    */
   @Test(expected=IllegalArgumentException.class)
   public void computeTargetPathRoot_CE() {
      EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
      String actual = vEngineHelper.computeTargetPathRoot(
            "T:\\Ws4Cours\\WSS4_cours_Acceleo_init\\runtime-Eclipse_TD_Acceleo_scripts_configuration\\TD_Acceleo_model",
            "../..");
   }
   
   @Test
   public void validate_CN() {
      EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
      
      // Mémoriser le Bundle 
      vEngineHelper.setBundle(getSampleBundle());
      // Mémoriser l'identifiant du plug-in (ex : Activator.PLUGIN_ID)
      vEngineHelper.setPluginId(cPlufinId);
      // Mémoriser les propriétés du projet de génération + les communes.
      vEngineHelper.setAllProperties(AllPropHelper.getInstance(cPlufinId));
      // Mémoriser le fichier de modélisation
      vEngineHelper.setFileNameOfModelInProgress("theModel.entity");
      // Mémoriser la Target
//      vEngineHelper.setTargetFolder(getSampleTargetFolder());
      vEngineHelper.setTargetFolder(new File(".").getAbsolutePath());
      
      // Valider
      vEngineHelper.validate();
      // Ne doit pas provoquer d'erreur
   }

   @Test(expected=IllegalStateException.class)
   public void validate_CE1() {
      EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
      // Mémoriser le Bundle 
      vEngineHelper.setBundle(getSampleBundle());
      // Mémoriser l'identifiant du plug-in (ex : Activator.PLUGIN_ID)
      vEngineHelper.setPluginId(cPlufinId);
      // Mémoriser les propriétés du projet de génération + les communes.
      vEngineHelper.setAllProperties(AllPropHelper.getInstance(cPlufinId));
      // Mémoriser le fichier de modélisation
      vEngineHelper.setFileNameOfModelInProgress("theModel.entity");
      // On oublie la Target
      vEngineHelper.setTargetFolder(null);
      
      // Valider
      vEngineHelper.validate();
   }

   @Test(expected=IllegalStateException.class)
   public void validate_CE2() {
      EngineHelper vEngineHelper = EngineHelper.getInstance(cPlufinId);
      // Mémoriser le Bundle 
      vEngineHelper.setBundle(getSampleBundle());
      // Mémoriser l'identifiant du plug-in (ex : Activator.PLUGIN_ID)
      vEngineHelper.setPluginId(cPlufinId);
      // Mémoriser les propriétés du projet de génération + les communes.
      vEngineHelper.setAllProperties(AllPropHelper.getInstance(cPlufinId));
      // Mémoriser le fichier de modélisation
      vEngineHelper.setFileNameOfModelInProgress(null);
      // Mémoriser la Target
      vEngineHelper.setTargetFolder(new File(".").getAbsolutePath());

      // Valider
      vEngineHelper.validate();
   }

   private IContainer getSampleTargetFolder() {
      return new IContainer() {
         
         @Override
         public boolean isConflicting(ISchedulingRule pRule) {
            return false;
         }
         
         @Override
         public boolean contains(ISchedulingRule pRule) {
            return false;
         }
         
         @Override
         public <T> T getAdapter(Class<T> pAdapter) {
            return null;
         }
         
         @Override
         public void touch(IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void setTeamPrivateMember(boolean pIsTeamPrivate) throws CoreException {
         }
         
         @Override
         public void setSessionProperty(QualifiedName pKey, Object pValue) throws CoreException {
         }
         
         @Override
         public void setResourceAttributes(ResourceAttributes pAttributes) throws CoreException {
         }
         
         @Override
         public void setReadOnly(boolean pReadOnly) {
         }
         
         @Override
         public void setPersistentProperty(QualifiedName pKey, String pValue) throws CoreException {
         }
         
         @Override
         public long setLocalTimeStamp(long pValue) throws CoreException {
            return 0;
         }
         
         @Override
         public void setLocal(boolean pFlag, int pDepth, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void setHidden(boolean pIsHidden) throws CoreException {
         }
         
         @Override
         public void setDerived(boolean pIsDerived, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void setDerived(boolean pIsDerived) throws CoreException {
         }
         
         @Override
         public void revertModificationStamp(long pValue) throws CoreException {
         }
         
         @Override
         public void refreshLocal(int pDepth, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void move(IProjectDescription pDescription, boolean pForce, boolean pKeepHistory, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void move(IProjectDescription pDescription, int pUpdateFlags, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void move(IPath pDestination, int pUpdateFlags, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void move(IPath pDestination, boolean pForce, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public boolean isVirtual() {
            return false;
         }
         
         @Override
         public boolean isTeamPrivateMember(int pOptions) {
            return false;
         }
         
         @Override
         public boolean isTeamPrivateMember() {
            return false;
         }
         
         @Override
         public boolean isSynchronized(int pDepth) {
            return false;
         }
         
         @Override
         public boolean isReadOnly() {
            return false;
         }
         
         @Override
         public boolean isPhantom() {
            return false;
         }
         
         @Override
         public boolean isLocal(int pDepth) {
            return false;
         }
         
         @Override
         public boolean isLinked(int pOptions) {
            return false;
         }
         
         @Override
         public boolean isLinked() {
            return false;
         }
         
         @Override
         public boolean isHidden(int pOptions) {
            return false;
         }
         
         @Override
         public boolean isHidden() {
            return false;
         }
         
         @Override
         public boolean isDerived(int pOptions) {
            return false;
         }
         
         @Override
         public boolean isDerived() {
            return false;
         }
         
         @Override
         public boolean isAccessible() {
            return false;
         }
         
         @Override
         public IWorkspace getWorkspace() {
            return null;
         }
         
         @Override
         public int getType() {
            return 0;
         }
         
         @Override
         public Object getSessionProperty(QualifiedName pKey) throws CoreException {
            return null;
         }
         
         @Override
         public Map<QualifiedName, Object> getSessionProperties() throws CoreException {
            return null;
         }
         
         @Override
         public ResourceAttributes getResourceAttributes() {
            return null;
         }
         
         @Override
         public URI getRawLocationURI() {
            return null;
         }
         
         @Override
         public IPath getRawLocation() {
            return null;
         }
         
         @Override
         public IPath getProjectRelativePath() {
            return null;
         }
         
         @Override
         public IProject getProject() {
            return null;
         }
         
         @Override
         public String getPersistentProperty(QualifiedName pKey) throws CoreException {
            return null;
         }
         
         @Override
         public Map<QualifiedName, String> getPersistentProperties() throws CoreException {
            return null;
         }
         
         @Override
         public IPathVariableManager getPathVariableManager() {
            return null;
         }
         
         @Override
         public IContainer getParent() {
            return null;
         }
         
         @Override
         public String getName() {
            return null;
         }
         
         @Override
         public long getModificationStamp() {
            return 0;
         }
         
         @Override
         public IMarker getMarker(long pId) {
            return null;
         }
         
         @Override
         public URI getLocationURI() {
            return null;
         }
         
         @Override
         public IPath getLocation() {
            return null;
         }
         
         @Override
         public long getLocalTimeStamp() {
            return 0;
         }
         
         @Override
         public IPath getFullPath() {
            return null;
         }
         
         @Override
         public String getFileExtension() {
            return null;
         }
         
         @Override
         public int findMaxProblemSeverity(String pType, boolean pIncludeSubtypes, int pDepth) throws CoreException {
            return 0;
         }
         
         @Override
         public IMarker[] findMarkers(String pType, boolean pIncludeSubtypes, int pDepth) throws CoreException {
            return null;
         }
         
         @Override
         public IMarker findMarker(long pId) throws CoreException {
            return null;
         }
         
         @Override
         public boolean exists() {
            return false;
         }
         
         @Override
         public void deleteMarkers(String pType, boolean pIncludeSubtypes, int pDepth) throws CoreException {
         }
         
         @Override
         public void delete(int pUpdateFlags, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void delete(boolean pForce, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public IResourceProxy createProxy() {
            return null;
         }
         
         @Override
         public IMarker createMarker(String pType) throws CoreException {
            return null;
         }
         
         @Override
         public void copy(IProjectDescription pDescription, int pUpdateFlags, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void copy(IProjectDescription pDescription, boolean pForce, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void copy(IPath pDestination, int pUpdateFlags, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void copy(IPath pDestination, boolean pForce, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void clearHistory(IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void accept(IResourceVisitor pVisitor, int pDepth, int pMemberFlags) throws CoreException {
         }
         
         @Override
         public void accept(IResourceVisitor pVisitor, int pDepth, boolean pIncludePhantoms) throws CoreException {
         }
         
         @Override
         public void accept(IResourceProxyVisitor pVisitor, int pDepth, int pMemberFlags) throws CoreException {
         }
         
         @Override
         public void accept(IResourceProxyVisitor pVisitor, int pMemberFlags) throws CoreException {
         }
         
         @Override
         public void accept(IResourceVisitor pVisitor) throws CoreException {
         }
         
         @Override
         public void setDefaultCharset(String pCharset, IProgressMonitor pMonitor) throws CoreException {
         }
         
         @Override
         public void setDefaultCharset(String pCharset) throws CoreException {
         }
         
         @Override
         public IResource[] members(int pMemberFlags) throws CoreException {
            return null;
         }
         
         @Override
         public IResource[] members(boolean pIncludePhantoms) throws CoreException {
            return null;
         }
         
         @Override
         public IResource[] members() throws CoreException {
            return null;
         }
         
         @Override
         public IFolder getFolder(IPath pPath) {
            return null;
         }
         
         @Override
         public IResourceFilterDescription[] getFilters() throws CoreException {
            return null;
         }
         
         @Override
         public IFile getFile(IPath pPath) {
            return null;
         }
         
         @Override
         public String getDefaultCharset(boolean pCheckImplicit) throws CoreException {
            return null;
         }
         
         @Override
         public String getDefaultCharset() throws CoreException {
            return null;
         }
         
         @Override
         public IResource findMember(IPath pPath, boolean pIncludePhantoms) {
            return null;
         }
         
         @Override
         public IResource findMember(String pPath, boolean pIncludePhantoms) {
            return null;
         }
         
         @Override
         public IResource findMember(IPath pPath) {
            return null;
         }
         
         @Override
         public IResource findMember(String pPath) {
            return null;
         }
         
         @Override
         public IFile[] findDeletedMembersWithHistory(int pDepth, IProgressMonitor pMonitor) throws CoreException {
            return null;
         }
         
         @Override
         public boolean exists(IPath pPath) {
            return false;
         }
         
         @Override
         public IResourceFilterDescription createFilter(int pType, FileInfoMatcherDescription pMatcherDescription, int pUpdateFlags, IProgressMonitor pMonitor) throws CoreException {
            return null;
         }
      };
   }

   private Bundle getSampleBundle() {
      return new Bundle() {
         
         @Override
         public int compareTo(Bundle pO) {
            return 0;
         }
         
         @Override
         public void update(InputStream pInput) throws BundleException {
         }
         
         @Override
         public void update() throws BundleException {
         }
         
         @Override
         public void uninstall() throws BundleException {
         }
         
         @Override
         public void stop(int pOptions) throws BundleException {
         }
         
         @Override
         public void stop() throws BundleException {
         }
         
         @Override
         public void start(int pOptions) throws BundleException {
         }
         
         @Override
         public void start() throws BundleException {
         }
         
         @Override
         public Class<?> loadClass(String pName) throws ClassNotFoundException {
            return null;
         }
         
         @Override
         public boolean hasPermission(Object pPermission) {
            return false;
         }
         
         @Override
         public Version getVersion() {
            return null;
         }
         
         @Override
         public String getSymbolicName() {
            return null;
         }
         
         @Override
         public int getState() {
            return 0;
         }
         
         @Override
         public Map<X509Certificate, List<X509Certificate>> getSignerCertificates(int pSignersType) {
            return null;
         }
         
         @Override
         public ServiceReference<?>[] getServicesInUse() {
            return null;
         }
         
         @Override
         public Enumeration<URL> getResources(String pName) throws IOException {
            return null;
         }
         
         @Override
         public URL getResource(String pName) {
            return null;
         }
         
         @Override
         public ServiceReference<?>[] getRegisteredServices() {
            return null;
         }
         
         @Override
         public String getLocation() {
            return null;
         }
         
         @Override
         public long getLastModified() {
            return 0;
         }
         
         @Override
         public Dictionary<String, String> getHeaders(String pLocale) {
            return null;
         }
         
         @Override
         public Dictionary<String, String> getHeaders() {
            return null;
         }
         
         @Override
         public Enumeration<String> getEntryPaths(String pPath) {
            return null;
         }
         
         @Override
         public URL getEntry(String pPath) {
            return null;
         }
         
         @Override
         public File getDataFile(String pFilename) {
            return null;
         }
         
         @Override
         public long getBundleId() {
            return 0;
         }
         
         @Override
         public BundleContext getBundleContext() {
            return null;
         }
         
         @Override
         public Enumeration<URL> findEntries(String pPath, String pFilePattern, boolean pRecurse) {
            return null;
         }
         
         @Override
         public <A> A adapt(Class<A> pType) {
            return null;
         }
      };
   }

} // FIN classe

