package fr.awax.acceleo.common.engine.error;

import org.junit.Assert;
import org.junit.Test;

public class ErrorControlerTest
{
   private static final String cPlufinId = "plugin.id.EngineHelperTest";

   @Test
   public void writeErrorGeneration_CN1()
   {
      ErrorControler instance = ErrorControler.getInstance(cPlufinId);
      // RAZ
      instance.clear();

      String actual = instance.writeErrorGeneration("Exemple d'erreur No1", "Solution No1", "Solution No2");
      String expected = 
            "[ERROR_GEN] -->  Exception mise à plat --> [ERROR_GEN] --> Exemple d'erreur No1 Exception mise à plat --> java.lang.RuntimeException: Exemple d'erreur No1 fr.awax.acceleo.common.engine.error.ErrorControler.getNewException(ErrorControler.java:137) fr.awax.acceleo.common.engine.error.ErrorControler.writeErrorGeneration(ErrorControler.java:109) fr.awax.acceleo.common.engine.error.ErrorControlerTest.writeErrorGeneration_CN1(ErrorControlerTest.java:17) sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)... Essayer de : \n" +
                  "   - Solution No1\n" +
                  "   - Solution No2\n" +
                  " fr.awax.acceleo.common.engine.error.ErrorGenerationException.addException(ErrorGenerationException.java:83) fr.awax.acceleo.common.engine.error.ErrorMessageWrapped.<init>(ErrorMessageWrapped.java:75) fr.awax.acceleo.common.engine.error.ErrorControler.writeErrorGeneration(ErrorControler.java:89) fr.awax.acceleo.common.engine.error.ErrorControler.writeErrorGeneration(ErrorControler.java:109) fr.awax.acceleo.common.engine.error.ErrorControlerTest.writeErrorGeneration_CN1(ErrorControlerTest.java:17) sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)...";
      Assert.assertEquals(expected, actual);

      try
      {
         // Lancer le traitement d'exception
         instance.doIfThrowErrorGenerationException();
      }
      catch (Exception vErr)
      {
         vErr.printStackTrace();
         Assert.assertEquals("Il y a des erreurs de génération", vErr.getMessage());
      }
   }

   @Test
   public void writeErrorGeneration_CN2()
   {
      ErrorControler instance = ErrorControler.getInstance(cPlufinId);
      // RAZ
      instance.clear();

      String actual = instance.writeErrorGeneration("Exemple d'erreur No1 / 3", "Solution No1.1", "Solution No1.2");
      String expected = 
            "[ERROR_GEN] -->  Exception mise à plat --> [ERROR_GEN] --> Exemple d'erreur No1 / 3 Exception mise à plat --> java.lang.RuntimeException: Exemple d'erreur No1 / 3 fr.awax.acceleo.common.engine.error.ErrorControler.getNewException(ErrorControler.java:137) fr.awax.acceleo.common.engine.error.ErrorControler.writeErrorGeneration(ErrorControler.java:109) fr.awax.acceleo.common.engine.error.ErrorControlerTest.writeErrorGeneration_CN2(ErrorControlerTest.java:44) sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)... Essayer de : \n" +
                  "   - Solution No1.1\n" +
                  "   - Solution No1.2\n" +
                  " fr.awax.acceleo.common.engine.error.ErrorGenerationException.addException(ErrorGenerationException.java:83) fr.awax.acceleo.common.engine.error.ErrorMessageWrapped.<init>(ErrorMessageWrapped.java:75) fr.awax.acceleo.common.engine.error.ErrorControler.writeErrorGeneration(ErrorControler.java:89) fr.awax.acceleo.common.engine.error.ErrorControler.writeErrorGeneration(ErrorControler.java:109) fr.awax.acceleo.common.engine.error.ErrorControlerTest.writeErrorGeneration_CN2(ErrorControlerTest.java:44) sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)...";
      Assert.assertEquals(expected, actual);
      
      actual = instance.writeErrorGeneration("Exemple d'erreur No2 / 3", getSampleException(), "Solution No2.1", "Solution No2.2");
      expected = 
            "[ERROR_GEN] -->  Exception mise à plat --> [ERROR_GEN] --> Exemple d'erreur No2 / 3 : Excemple d'exception Exception mise à plat --> java.lang.RuntimeException: Excemple d'exception fr.awax.acceleo.common.engine.error.ErrorControlerTest.getSampleException(ErrorControlerTest.java:104) fr.awax.acceleo.common.engine.error.ErrorControlerTest.writeErrorGeneration_CN2(ErrorControlerTest.java:52) sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)... Essayer de : \n" +
                  "   - Solution No2.1\n" +
                  "   - Solution No2.2\n" +
                  " fr.awax.acceleo.common.engine.error.ErrorGenerationException.addException(ErrorGenerationException.java:83) fr.awax.acceleo.common.engine.error.ErrorMessageWrapped.<init>(ErrorMessageWrapped.java:75) fr.awax.acceleo.common.engine.error.ErrorControler.writeErrorGeneration(ErrorControler.java:92) fr.awax.acceleo.common.engine.error.ErrorControlerTest.writeErrorGeneration_CN2(ErrorControlerTest.java:52) sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)...";
      Assert.assertEquals(expected, actual);

      actual = instance.writeErrorGeneration("Exemple d'erreur No3 / 3", "Solution No3.1", "Solution No3.2");
      expected = 
            "[ERROR_GEN] -->  Exception mise à plat --> [ERROR_GEN] --> Exemple d'erreur No3 / 3 Exception mise à plat --> java.lang.RuntimeException: Exemple d'erreur No3 / 3 fr.awax.acceleo.common.engine.error.ErrorControler.getNewException(ErrorControler.java:137) fr.awax.acceleo.common.engine.error.ErrorControler.writeErrorGeneration(ErrorControler.java:109) fr.awax.acceleo.common.engine.error.ErrorControlerTest.writeErrorGeneration_CN2(ErrorControlerTest.java:60) sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)... Essayer de : \n" +
                  "   - Solution No3.1\n" +
                  "   - Solution No3.2\n" +
                  " fr.awax.acceleo.common.engine.error.ErrorGenerationException.addException(ErrorGenerationException.java:83) fr.awax.acceleo.common.engine.error.ErrorMessageWrapped.<init>(ErrorMessageWrapped.java:75) fr.awax.acceleo.common.engine.error.ErrorControler.writeErrorGeneration(ErrorControler.java:89) fr.awax.acceleo.common.engine.error.ErrorControler.writeErrorGeneration(ErrorControler.java:109) fr.awax.acceleo.common.engine.error.ErrorControlerTest.writeErrorGeneration_CN2(ErrorControlerTest.java:60) sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source) sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)...";
      Assert.assertEquals(expected, actual);

      try
      {
         // Lancer le traitement d'exception
         instance.doIfThrowErrorGenerationException();
      }
      catch (Exception vErr)
      {
         vErr.printStackTrace();
         Assert.assertEquals("Il y a des erreurs de génération", vErr.getMessage());
      }
   }

   @Test(expected=ErrorGenerationException.class)
   public void writeErrorGeneration_CN3()
   {
      ErrorControler instance = ErrorControler.getInstance(cPlufinId);
      // RAZ
      instance.clear();

      try
      {
         // Lancer le traitement d'exception
         String actual = instance.writeErrorGeneration_throws("Exemple d'erreur No3", "Solution potentielle No1", "Solution potentielle No2");
      }
      catch (Exception vErr)
      {
         vErr.printStackTrace();
         throw vErr;
      }
   }
   
   private Throwable getSampleException()
   {
      Throwable vReturn;
      try
      {
         throw new RuntimeException("Excemple d'exception");
      }
      catch (Exception vErr)
      {
         vReturn = vErr;
      }
      return vReturn;
   }

}
