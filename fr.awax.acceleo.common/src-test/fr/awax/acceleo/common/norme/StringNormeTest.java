package fr.awax.acceleo.common.norme;

import org.junit.Assert;
import org.junit.Test;

public class StringNormeTest {

   /**
    * Tester avec l'espace en séparateur de mots.
    */
   @Test
   public void testSepSpace_CN1 () {
      String valueLowerWithSpace = "il fait beau";
      StringNorme vStringNorme = new StringNorme(valueLowerWithSpace);

      StringNorme actual = vStringNorme.toNormeCamelCase();
      Assert.assertEquals("IlFaitBeau", actual.toString());
      
      actual = vStringNorme.toNormeCamelCase().toLowerFirst();
      Assert.assertEquals("ilFaitBeau", actual.toString());

      // Tester un retour à la phrase du départ
      actual = vStringNorme.toNormeCamelCase().toNormeSpace().toLowerAll();
      Assert.assertEquals(valueLowerWithSpace, actual.toString());
   }
   
   /**
    * Tester avec l'espace en séparateur de mots avec des retours chariots.
    */
   @Test
   public void testSepSpace_CN2 () {
      String valueLowerWithSpace = "il fait beau   \n\net chaud";
      StringNorme vStringNorme = new StringNorme(valueLowerWithSpace);
      
      StringNorme actual = vStringNorme.toWithoutRC();
      Assert.assertEquals("il fait beau et chaud", actual.toString());
      
   }
   
   /**
    * Tester avec le underscore en séparateur de mots.
    */
   @Test
   public void testSepUnderscore () {
      String valueLowerWithSpace = "il fait beau";
      StringNorme vStringNorme = new StringNorme(valueLowerWithSpace);
      
      StringNorme actual = vStringNorme.toNormeUnderscore();
      Assert.assertEquals("il_fait_beau", actual.toString());
      
      actual = vStringNorme.toNormeUnderscore().toUpperAll();
      Assert.assertEquals("IL_FAIT_BEAU", actual.toString());
      
      // Tester un retour à la phrase du départ
      actual = vStringNorme.toNormeUnderscore().toNormeSpace().toLowerAll();
      Assert.assertEquals(valueLowerWithSpace, actual.toString());
   }
   
   /**
    * Tester avec le tiret en séparateur de mots.
    */
   @Test
   public void testSepTiret () {
      String valueLowerWithSpace = "il fait beau";
      StringNorme vStringNorme = new StringNorme(valueLowerWithSpace);
      
      StringNorme actual = vStringNorme.toNormeTiret();
      Assert.assertEquals("il-fait-beau", actual.toString());
      
      actual = vStringNorme.toNormeTiret().toUpperAll();
      Assert.assertEquals("IL-FAIT-BEAU", actual.toString());
      
      // Tester un retour à la phrase du départ
      actual = vStringNorme.toNormeTiret().toNormeSpace().toLowerAll();
      Assert.assertEquals(valueLowerWithSpace, actual.toString());
   }

   /**
    * Tester avec la CamelCase avec une chaîne 'null'.
    */
   @Test
   public void testSepCamelCase_CN0 () {
      StringNorme vStringNorme = new StringNorme(null, ChunkSepEnum.normecamelCase);
      
      StringNorme actual = vStringNorme.toNormeUnderscore();
      Assert.assertEquals("", actual.toString());

      actual = vStringNorme.toNormeSpace();
      Assert.assertEquals("", actual.toString());
      
      actual = vStringNorme.toNormeSpace().toUpperFirst();
      Assert.assertEquals("", actual.toString());

      actual = vStringNorme.toNormeSpace().toLowerAll();
      Assert.assertEquals("", actual.toString());
      
      actual = vStringNorme.toNormeSpace().toUpperAll();
      Assert.assertEquals("", actual.toString());
   }

   /**
    * Tester avec la CamelCase en séparateur de mots (sans underscore).
    */
   @Test
   public void testSepCamelCase_CN1 () {
      StringNorme vStringNorme = new StringNorme("ilFaitBeau", ChunkSepEnum.normecamelCase);

      StringNorme actual = vStringNorme.toNormeUnderscore();
      Assert.assertEquals("il_Fait_Beau", actual.toString());

      actual = vStringNorme.toNormeSpace();
      Assert.assertEquals("il Fait Beau", actual.toString());
      
      actual = vStringNorme.toNormeSpace().toUpperFirst();
      Assert.assertEquals("Il Fait Beau", actual.toString());

      actual = vStringNorme.toNormeSpace().toLowerAll();
      Assert.assertEquals("il fait beau", actual.toString());
      
      actual = vStringNorme.toNormeSpace().toUpperAll();
      Assert.assertEquals("IL FAIT BEAU", actual.toString());
   }
   
   /**
    * Tester avec la CamelCase en séparateur de mots (avec underscore).
    */
   @Test
   public void testSepCamelCase_CN2 () {
      StringNorme vStringNorme = new StringNorme("ilFaitBeau_aujourdhui", ChunkSepEnum.normecamelCase);
      
      StringNorme actual = vStringNorme.toRemoveUnderscore().toNormeCamelCase();
      Assert.assertEquals("IlFaitBeauAujourdhui", actual.toString());
      actual = vStringNorme.toRemoveUnderscore().toNormeCamelCaseIgnoreFirst();
      Assert.assertEquals("ilFaitBeauAujourdhui", actual.toString());

      actual = vStringNorme.toRemoveUnderscore().toNormeCamelCaseLowerFirst();
      Assert.assertEquals("ilFaitBeauAujourdhui", actual.toString());
      
      actual = vStringNorme.toNormeUnderscore();
      Assert.assertEquals("il_Fait_Beau_aujourdhui", actual.toString());
      
      actual = vStringNorme.toNormeSpace();
      Assert.assertEquals("il Fait Beau_aujourdhui", actual.toString());
      
      actual = vStringNorme.toNormeSpace().toUpperFirst();
      Assert.assertEquals("Il Fait Beau_aujourdhui", actual.toString());
      
      actual = vStringNorme.toNormeSpace().toLowerAll();
      Assert.assertEquals("il fait beau_aujourdhui", actual.toString());
      
      actual = vStringNorme.toNormeSpace().toUpperAll();
      Assert.assertEquals("IL FAIT BEAU_AUJOURDHUI", actual.toString());
   }

   /**
    * Tester avec la convention Java.
    */
   @Test
   public void testConvJava_CN1 () {
      String valueLowerWithSpace = "il fait beau_aujourdhui";
      StringNorme vStringNorme = new StringNorme(valueLowerWithSpace);
      
      String actual = vStringNorme.toConvJavaConstant();
      Assert.assertEquals("IL_FAIT_BEAU_AUJOURDHUI", actual);
      
      actual = vStringNorme.toConvJavaAttribute();
      Assert.assertEquals("IlFaitBeauAujourdhui", actual);
      
      actual = vStringNorme.toConvJavaParameter();
      Assert.assertEquals("ilFaitBeauAujourdhui", actual);
      
      actual = vStringNorme.toConvJavaMethod();
      Assert.assertEquals("ilFaitBeauAujourdhui", actual);

      actual = vStringNorme.toConvJavaPackage();
      Assert.assertEquals("il.fait.beau_aujourdhui", actual);
      
   }
   
   /**
    * Tester avec la convention Java.
    */
   @Test
   public void testConvJava_CN2 () {
      String valueLowerWithSpace = "Il fait beau_aujourdhui";
      StringNorme vStringNorme = new StringNorme(valueLowerWithSpace);
      
      String actual = vStringNorme.toConvJavaConstant();
      Assert.assertEquals("IL_FAIT_BEAU_AUJOURDHUI", actual);
      
      actual = vStringNorme.toConvJavaAttribute();
      Assert.assertEquals("IlFaitBeauAujourdhui", actual);
      
      actual = vStringNorme.toConvJavaParameter();
      Assert.assertEquals("IlFaitBeauAujourdhui", actual);
      
      actual = vStringNorme.toConvJavaMethod();
      Assert.assertEquals("ilFaitBeauAujourdhui", actual);
      
      actual = vStringNorme.toConvJavaPackage();
      Assert.assertEquals("il.fait.beau_aujourdhui", actual);
      
   }

}
