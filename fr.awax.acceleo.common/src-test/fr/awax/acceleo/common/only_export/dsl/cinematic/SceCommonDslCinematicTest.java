package fr.awax.acceleo.common.only_export.dsl.cinematic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.obeonetwork.dsl.cinematic.toolkits.Widget;
import org.obeonetwork.dsl.cinematic.view.AbstractViewElement;
import org.obeonetwork.dsl.cinematic.view.Layout;
import org.obeonetwork.dsl.cinematic.view.ViewContainer;
import org.obeonetwork.dsl.cinematic.view.ViewElement;
import org.obeonetwork.dsl.cinematic.view.ViewFactory;

public class SceCommonDslCinematicTest {
   
   private Map<ComplexiteWidget_Enum, Widget> _MapWidget = new HashMap<>();
   private Widget createWidget (ComplexiteWidget_Enum pWidgetType) {
      Widget vReturn;
      Widget vWidget = _MapWidget.get(pWidgetType);
      if (vWidget == null) {
         vWidget = org.obeonetwork.dsl.cinematic.toolkits.ToolkitsFactory.eINSTANCE.createWidget();
         _MapWidget.put(pWidgetType, vWidget);
      }
      vReturn = vWidget;
      
      return vReturn;
   }
   
   private Layout createLayout (String pViewElementName, int pX, int pY) {
      ViewElement vViewElt = ViewFactory.eINSTANCE.createViewElement();
      vViewElt.setName(pViewElementName);
      Layout vLayout = ViewFactory.eINSTANCE.createLayout();
      vLayout.setViewElement(vViewElt);
      vLayout.setX(pX);
      vLayout.setY(pY);
      vLayout.setWidth(187);
      return vLayout;
   }

   private ViewContainer createViewContainer(String pContainerName, ComplexiteWidget_Enum pWidgetType) {
      Widget vWidget = createWidget(pWidgetType);
      ViewContainer vViewContainer = ViewFactory.eINSTANCE.createViewContainer();
      vViewContainer.setName(pContainerName);
      vViewContainer.setWidget(vWidget);
      return vViewContainer;
   }

   private ViewElement createViewElement (String pViewElementName, ComplexiteWidget_Enum pWidgetType) {
      Widget vWidget = createWidget(pWidgetType);
      ViewElement vViewElement = ViewFactory.eINSTANCE.createViewElement();
      vViewElement.setName(pViewElementName);
      vViewElement.setWidget(vWidget);
      return vViewElement;
   }

   private Layout createLayoutTable(String pTableName, int pX, int pY) {
      Widget vWidgetTable = createWidget(ComplexiteWidget_Enum.widgetTable);
      ViewContainer vViewContainerTable = ViewFactory.eINSTANCE.createViewContainer();
      vViewContainerTable.setName(pTableName);
      vViewContainerTable.setWidget(vWidgetTable);
      Layout vLayTable = ViewFactory.eINSTANCE.createLayout();
      vLayTable.setViewElement(vViewContainerTable);
      vLayTable.setX(pX);
      vLayTable.setY(pY);
      vLayTable.setWidth(477);
      return vLayTable;
   }

   private Layout createVirtualLayout (int pX, int pY) {
      Layout vVirtualLayout = ViewFactory.eINSTANCE.createLayout();
      vVirtualLayout.setX(pX);
      vVirtualLayout.setY(pY);
      vVirtualLayout.setWidth(441);
      return vVirtualLayout;
   }
   
   /**
    * Obtenir un exemple de Layout :
    * |-tblLieu----------------------------------|
    * | |--------------------------------------| |
    * | | |---------| |----------------------| | |
    * | | | LblLieu | | rdoDirectionCentrale | | |
    * | | |---------| |----------------------| | |
    * | |--------------------------------------| |
    * | |-------------| |
    * | | rdoAcademie | |
    * | |-------------| |
    * |------------------------------------------|
    * @return L'instance désirée.
    */
   private Layout getSampleLayout1 () {
      // Layout pour le ViewElement 'Table'
      Layout vLay_tblLieu = createLayoutTable("tblLieu", 4, 302);

      // Layout pour 1er ViewElement : 'lblLieu'
      Layout vLay_lblLieu = createLayout("lblLieu", 10, 5);
      // Layout pour 2ème ViewElement : 'rdoDirectionCentrale'
      Layout vLay_rdoDirectionCentrale = createLayout("rdoDirectionCentrale", 220, 2);

      // VirtualLayout avec 'lblLieu' et 'rdoDirectionCentrale'
      Layout vVirtualLayout = createVirtualLayout(10, 2);
      // Ajouter les 2 Layout-ViewElement dans le VirtualLayout
      vVirtualLayout.getOwnedLayouts().add(vLay_lblLieu);
      vVirtualLayout.getOwnedLayouts().add(vLay_rdoDirectionCentrale);

      // Layout pour 2ème ViewElement : 'rdoAcademie'
      Layout vLay_rdoAcademie = createLayout("rdoAcademie", 220, 56);

      // Conteneur principal
      vLay_tblLieu.getOwnedLayouts().add(vVirtualLayout);
      vLay_tblLieu.getOwnedLayouts().add(vLay_rdoAcademie);

      return vLay_tblLieu;
   }

   @Test
   public void getNbColonnes_CN () {
      SceCommonDslCinematic vSceCommonDslCinematic = SceCommonDslCinematic.getInstance("idPlugin4Test");
      // Obtenir un exemple
      Layout vLayout = getSampleLayout1();
      List<Integer> actual = vSceCommonDslCinematic.getNbColonnes(vLayout);

      // Vérifier
      Assert.assertEquals(2, actual.size());
   }

   @Test
   public void getLayoutByGrid_CN1 () {
      SceCommonDslCinematic vSceCommonDslCinematic = SceCommonDslCinematic.getInstance("idPlugin4Test");
      // Obtenir un exemple
      Layout vLayout = getSampleLayout1();
      List<List<AbstractViewElement>> actual = vSceCommonDslCinematic.getLayoutByGrid_sce(vLayout);

      // Vérifier
      Assert.assertEquals(2, actual.size());
      Assert.assertEquals(2, actual.get(0).size());
      Assert.assertEquals(2, actual.get(1).size());
   }
   
   /**
    * Obtenir un exemple de Layout :
    * |-tblSampleComplexe---------------------------------------------------|
    * | |-----------------------------------------------------------------| |
    * | | |-------------| |-------------| |-------------| |-------------| | |
    * | | | lblSample1a | | lblSample2a | | lblSample3a | | chkSample4a | | |
    * | | |-------------| |-------------| |-------------| |-------------| | |
    * | |                 |-------------|                 |-------------| | |
    * | |                 | lblSample2b |                 | chkSample4b | | |
    * | |                 |-------------|                 |-------------| | |
    * | |                                 |-------------|                 | |
    * | |                                 | lblSample3c |                 | |
    * | |                                 |-------------|                 | |
    * | | |-------------| |-------------| |-------------| |-------------| | |
    * | | | lblSample1d | | lblSample2d | | lblSample3d | | chkSample4d | | |
    * | | |-------------| |-------------| |-------------| |-------------| | |
    * |---------------------------------------------------------------------|
    * @return L'instance désirée.
    */
   private Layout getSampleLayout2 () {
      // Layout pour le ViewElement 'Table'
      Layout vLay_tblSampleComplexe = createLayoutTable("tblSampleComplexe_layout", 4, 302);

      // 1ère ligne
      Layout vLay_lblSample1a = createLayout("lblSample1a", 34, 26);
      Layout vLay_lblSample2a = createLayout("lblSample2a", 244, 26);
      Layout vLay_lblSample3a = createLayout("lblSample3a", 478, 26);
      Layout vLay_lblSample4a = createLayout("chkSample4a", 724, 26);
      // VirtualLayout pour la 1ère ligne 
      Layout vligne1 = createVirtualLayout(34, 26);
      vligne1.getOwnedLayouts().add(vLay_lblSample1a);
      vligne1.getOwnedLayouts().add(vLay_lblSample2a);
      vligne1.getOwnedLayouts().add(vLay_lblSample3a);
      vligne1.getOwnedLayouts().add(vLay_lblSample4a);

      // 2ème ligne
      Layout vLay_lblSample2b = createLayout("lblSample2b", 238, 92);
      Layout vLay_lblSample4b = createLayout("chkSample4b", 724, 92);
      // VirtualLayout pour la 2ème ligne 
      Layout vligne2 = createVirtualLayout(238, 92);
      vligne2.getOwnedLayouts().add(vLay_lblSample2b);
      vligne2.getOwnedLayouts().add(vLay_lblSample4b);

      // 3ème ligne
      Layout vLay_lblSample3c = createLayout("lblSample3c", 472, 164);
      // Pas de VirtualLayout pour la 2ème ligne 

      // 4ème ligne
      Layout vLay_lblSample1d = createLayout("lblSample1d", 28, 242);
      Layout vLay_lblSample2d = createLayout("lblSample2d", 238, 242);
      Layout vLay_lblSample3d = createLayout("lblSample3d", 472, 242);
      Layout vLay_lblSample4d = createLayout("chkSample4d", 724, 242);
      // VirtualLayout pour la 4ème ligne 
      Layout vligne4 = createVirtualLayout(28, 242);
      vligne4.getOwnedLayouts().add(vLay_lblSample1d);
      vligne4.getOwnedLayouts().add(vLay_lblSample2d);
      vligne4.getOwnedLayouts().add(vLay_lblSample3d);
      vligne4.getOwnedLayouts().add(vLay_lblSample4d);

      // Conteneur principal
      vLay_tblSampleComplexe.getOwnedLayouts().add(vligne1);
      vLay_tblSampleComplexe.getOwnedLayouts().add(vligne2);
      vLay_tblSampleComplexe.getOwnedLayouts().add(vLay_lblSample3c);
      vLay_tblSampleComplexe.getOwnedLayouts().add(vligne4);
      
      return vLay_tblSampleComplexe;
   }
   
   @Test
   public void getLayoutByGrid_CN2 () {
      SceCommonDslCinematic vSceCommonDslCinematic = SceCommonDslCinematic.getInstance("idPlugin4Test");
      // Obtenir un exemple
      Layout vLayout = getSampleLayout2();
      List<List<AbstractViewElement>> actual = vSceCommonDslCinematic.getLayoutByGrid_sce(vLayout);
      
      // DEBUG
//      for (List<AbstractViewElement> listComposant : actual) {
//         for (AbstractViewElement elem : listComposant) {
//            if (elem == null) {
//               System.out.print("null - ");
//            }
//            else {
//               System.out.print(elem.getName() + " - ");
//            }
//         }
//         System.out.println("");
//      }

      // Vérifier
      Assert.assertEquals(4, actual.size());
      Assert.assertEquals(4, actual.get(0).size());
      Assert.assertEquals(4, actual.get(1).size());
      Assert.assertEquals(4, actual.get(2).size());
      Assert.assertEquals(4, actual.get(3).size());
   }


   private ViewContainer getSampleViewContainer1 () {
      ViewContainer screen = createViewContainer("Ecr01ForTest", ComplexiteWidget_Enum.widgetPage);
      
      // Panel1
      ViewContainer panel1 = createViewContainer("Panel1", ComplexiteWidget_Enum.widgetPanel);
      ViewElement lblNom = createViewElement("lblNom", ComplexiteWidget_Enum.widgetLabel);
      ViewElement txtNom = createViewElement("txtNom", ComplexiteWidget_Enum.widgetText);
      ViewElement lblPrenom = createViewElement("lblPrenom", ComplexiteWidget_Enum.widgetLabel);
      ViewElement txtPrenom = createViewElement("txtPrenom", ComplexiteWidget_Enum.widgetText);
      panel1.getOwnedElements().add(lblNom);
      panel1.getOwnedElements().add(txtNom);
      panel1.getOwnedElements().add(lblPrenom);
      panel1.getOwnedElements().add(txtPrenom);
      
      // Panel2
      ViewContainer panel2 = createViewContainer("Panel2", ComplexiteWidget_Enum.widgetPanel);
      ViewElement lblRemarque = createViewElement("lblRemarque", ComplexiteWidget_Enum.widgetLabel);
      ViewElement txaRemarque = createViewElement("txaRemarque", ComplexiteWidget_Enum.widgetTextarea);
      panel1.getOwnedElements().add(lblRemarque);
      panel1.getOwnedElements().add(txaRemarque);
      
      // Ajouter les conteneurs
      screen.getOwnedElements().add(panel1);
      screen.getOwnedElements().add(panel2);
      
      return screen;
   }

   @Test
   public void getListWidgetTabOrder_CN () {
      SceCommonDslCinematic vSceCommonDslCinematic = SceCommonDslCinematic.getInstance("idPlugin4Test");
      // Obtenir un exemple
      ViewContainer vViewContainer = getSampleViewContainer1();
      List<ViewElement> actual = vSceCommonDslCinematic.getListWidgetTabOrder(vViewContainer);
      
      // DEBUG
//      System.out.println(actual.size());
//      for (AbstractViewElement elem : actual) {
//         if (elem == null) {
//            System.out.print("null - ");
//         }
//         else {
//            System.out.print(elem.getName() + " - ");
//         }
//      }

      // Vérifier
      Assert.assertEquals(6, actual.size());
   }

}
