package fr.awax.acceleo.common.conf;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("static-access")
public class AllPropAppli1Test {
   public static final String cFmtDate_AAAAMMJJ_HHMMSS = XValue.cFmtDate_AAAAMMJJ_HHMMSS;

   private static final String cPropUser1 = "myProp1";
   private static final String cValueString1 = "new value 1";
   private static final String cValueDate1 = "20210402_152037";
   private static final String cFileNameProperties = "allPropAppli1.properties";

   /**
    * Contituer un fichier Properties avec une propriété "utilisateur".
    */
   private void initFileProp4Test () {
      AllPropAppli1.razAll();
      AllPropAppli1 allProp1 = AllPropAppli1.getInstance("pluingid0");
      // Charger avec les propriétés par défaut
      allProp1.load(cFileNameProperties, false);
      // Modifier la valeur par défaut
      allProp1.propCommon().update_pathfile.setValue(cValueString1);
      Assert.assertEquals(cValueString1, allProp1.propCommon().update_pathfile.getValue().toString());

      // Ajouter une propriété utilisateur
      allProp1.addProp(new XPropElem(cPropUser1, cValueDate1, "Exemple de propriété utilisateur", false));
      
      // Sauver
      allProp1.save(cFileNameProperties);
   }
   
   @Test
   public void contexteStatic_CN () {
      String actual = AllPropHelper.PROP_COMMON.update_check.getKey();
      Assert.assertEquals("update_check", actual);
   }
   
   /**
    * Lire un fichier de propriété avec une propriété "utilisateur".
    */
   @Test
   public void load_CN () {
      // Given
      initFileProp4Test();
      
      // 1ère instance
      //
      AllPropAppli1 allProp1 = AllPropAppli1.getInstance("pluingid1");
      Assert.assertEquals("update_check", allProp1.propCommon().update_check.getKey());
      // Charger avec les propriétés par défaut
      allProp1.load(cFileNameProperties);
      // Vérifier
      Assert.assertEquals(true, allProp1.propCommon().update_check.getValue().toBoolean());
      Assert.assertEquals("update_check", allProp1.propCommon().update_check.getKey());
      allProp1.propSpecif().optim_bdd_DB2.getValue().toString();
      Assert.assertEquals(true, allProp1.propSpecif().optim_bdd_DB2.getValue().toBoolean());
      // Obtenir une propriété en dynamique (ex : propriété utilisateur)
      // --> La propriété a été ajouté à la main par l'utilisateur
      XPropElem propUser = allProp1.getProp(cPropUser1);
      Assert.assertEquals(getDate(cValueDate1), propUser.getValue().toDate_AAAAMMJJ_HHMMSS());

      // 2ème instance
      //
      AllPropAppli1 allProp2 = AllPropAppli1.getInstance("pluingid2");
      // Charger avec les propriétés par défaut
      allProp2.load(cFileNameProperties);
      // Modifier la valeur par défaut
      allProp2.propCommon().update_pathfile.setValue("new value 2");
      // Vérifier que les 2 instances ont bien leurs valeurs propres
      Assert.assertEquals(cValueString1, allProp1.propCommon().update_pathfile.getValue().toString());
      Assert.assertEquals("new value 2", allProp2.propCommon().update_pathfile.getValue().toString());
   }

   /**
    * Lire un fichier de propriété avec 2 propriétés manquantes.
    */
   @Test(expected=RuntimeException.class)
   public void load_CE () {
      // Given
      initFileProp4Test();
      
      // Obtenir l'instance
      //
      AllPropAppli1 allProp1 = AllPropAppli1.getInstance("pluingid1");
      allProp1.load(cFileNameProperties);
      // Supprimer 1 propriété common
      allProp1.removeProp(allProp1.propCommon().dir_generation_projet_root.getKey());
      // Supprimer 1 propriété spécifique
      allProp1.removeProp(allProp1.propSpecif().path_src_gen_persistance.getKey());
      
      // Sauver le fichier
      allProp1.save(cFileNameProperties);
      
      // Recharger le fichier
      allProp1.load(cFileNameProperties);
   }

   private Date getDate (String pDate_AAAMMJJ_HHMMSS) {
      try {
         // Ex : "20210402_152037"
         SimpleDateFormat vDateFormat = new SimpleDateFormat(cFmtDate_AAAAMMJJ_HHMMSS);
         // String --> Date
         Date vReturn = vDateFormat.parse(pDate_AAAMMJJ_HHMMSS);

         return vReturn;
      }
      catch (ParseException err) {
         throw new RuntimeException("Problème lors du getDate() : " + err.getMessage(), err);
      }
   }

}

//
// Classe spécifique à une application pour les TUs
//
class AllPropAppli1 extends AllPropAbstract {
   private AllPropAppli1() {
      super();
   }

   public static AllPropAppli1 getInstance (final String pPlugInId) {
      AllPropAppli1 instance = (AllPropAppli1) findInstance(pPlugInId);
      if (instance == null) {
         instance = new AllPropAppli1();
         registerInstance(pPlugInId, instance);
      }
      return instance;
   }

   @Override
   public List<XPropElem> getAllPropertiesSpecif () throws Exception {
      List<XPropElem> vReturn = getListXProperty(propSpecif);
      return vReturn;
   }

   /** Les propriétés communes à tous les plugins. */
   private AllPropSpecif propSpecif = new AllPropSpecif() {
      // RAS
   };

   public AllPropSpecif propSpecif () {
      return propSpecif;
   }

   /**
    * Les propriétés spécifiques.
    * @author egarel
    */
   public class AllPropSpecif implements IAllProp {
      public final XPropElem path_src_gen_persistance = new XPropElem("path_src_gen_persistance", "true",
            "Le répertoire du source-folder de la couche persistance", true);
      public final XPropElem optim_bdd_DB2 = new XPropElem("optim_bdd_DB2", "true",
            "Optimisation (ou pas) pour une BdD DB2", false);

   }

}

