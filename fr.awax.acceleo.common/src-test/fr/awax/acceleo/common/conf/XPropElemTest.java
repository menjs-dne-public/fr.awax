package fr.awax.acceleo.common.conf;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class XPropElemTest {

   /**
    * Lire élément par élément le fichier de Properties.
    */
   @Test
   public void readElem_CN1 () {
      List<String> file = new ArrayList<>();
      file.add("# Généré le ...\n");
      file.add("# Un commentaire à passer\n");
      file.add("# Description clé1\n");
      file.add("cle1 = val1\n");
      file.add("# Description clé2\n");
      file.add("cle2 = val2\n");
      file.add("\n");
      file.add("# Un commentaire à passer\n");
      file.add("# Description clé3\n");
      file.add("cle3 = val3\n");

      XPropElem vXPropElem = new XPropElem();
      // Propriété No1
      int rang = 0;
      int rangNext = XPropElem.readElem(file, rang, vXPropElem);
      Assert.assertEquals(4, rangNext);
      Assert.assertEquals("cle1", vXPropElem.getKey());
      Assert.assertEquals("val1", vXPropElem.getValue().getValue());
      Assert.assertEquals("Description clé1", vXPropElem.getCommentaire());
      
      // Propriété suivante
      rang = rangNext;
      rangNext = XPropElem.readElem(file, rang, vXPropElem);
      Assert.assertEquals(6, rangNext);
      Assert.assertEquals("cle2", vXPropElem.getKey());
      Assert.assertEquals("val2", vXPropElem.getValue().getValue());
      Assert.assertEquals("Description clé2", vXPropElem.getCommentaire());
      
      // Propriété suivante
      rang = rangNext;
      rangNext = XPropElem.readElem(file, rang, vXPropElem);
      Assert.assertEquals(10, rangNext);
      Assert.assertEquals("cle3", vXPropElem.getKey());
      Assert.assertEquals("val3", vXPropElem.getValue().getValue());
      Assert.assertEquals("Description clé3", vXPropElem.getCommentaire());

      // Propriété suivante
      rang = rangNext;
      rangNext = XPropElem.readElem(file, rang, vXPropElem);
      Assert.assertEquals(-1, rangNext);
   }

}
