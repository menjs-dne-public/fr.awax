package fr.awax.acceleo.common.util;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class DirectoryUtilsTest {

   @Test
   public void getLastDirectory_CN0a () {
      String vResult = DirectoryUtils.getLastDirectory("sample1-model/");

      Assert.assertEquals("sample1-model", vResult);
   }

   @Test
   public void getLastDirectory_CN0b () {
      String vResult = DirectoryUtils.getLastDirectory("/sample1-model/");

      Assert.assertEquals("sample1-model", vResult);
   }

   @Test
   public void getLastDirectory_CN0 () {
      String vResult = DirectoryUtils.getLastDirectory("sample1-model/");

      Assert.assertEquals("sample1-model", vResult);
   }

   @Test
   public void getLastDirectory_CN1 () {
      String vResult = DirectoryUtils.getLastDirectory("file:\\C:\\WS_ProMinEdu\\sample1-model\\");

      Assert.assertEquals("sample1-model", vResult);
   }

   @Test
   public void getLastDirectory_CN2 () {
      String vResult = DirectoryUtils.getLastDirectory("file:\\C:\\WS_ProMinEdu\\sample1-model\\Sirhen_D01.entity");

      Assert.assertEquals("sample1-model", vResult);
   }

   @Test
   public void getLastDirectory_CN3 () {
      String vResult = DirectoryUtils.getLastDirectory("platform:\\resource\\sample4menus\\Sirhen_D01.entity");

      Assert.assertEquals("sample4menus", vResult);
   }

   @Test
   public void getLastDirectory_CN4 () {
      String vResult = DirectoryUtils.getLastDirectory("/sample4menus/Sirhen_D01.entity");

      Assert.assertEquals("sample4menus", vResult);
   }
   
   @Test
   public void getLastDirectory_CN5 () {
      String vResult = DirectoryUtils.getLastDirectory("services/createEnseignant.html");
      
      Assert.assertEquals("services", vResult);
   }

   @Test
   public void getLastDirectory_CN6a () {
      String vResult = DirectoryUtils.getLastDirectory("createEnseignant.html");
      
      Assert.assertNull(vResult);
   }
   
   @Test
   public void getLastDirectory_CN6b () {
      String vResult = DirectoryUtils.getLastDirectory("/createEnseignant.html");
      
      Assert.assertNull(vResult);
   }

//   @Test(expected=IllegalArgumentException.class)
   public void getLastDirectory_CE1 () {
      String vResult = DirectoryUtils.getLastDirectory("Sirhen_D01.entity");

      Assert.assertNull(vResult);
   }

   @Test
   public void uniformPath_CN1 () {
      String vResult = DirectoryUtils.uniformPath(DirectoryUtils.cProtocoleFile + "C:\\workspace\\sample1-model\\");

      Assert.assertEquals(DirectoryUtils.cProtocoleFile + "C:/workspace/sample1-model/", vResult);
   }

   @Test
   public void uniformPath_CN2 () {
      String vResult = DirectoryUtils
            .uniformPath(DirectoryUtils.cProtocoleFile + "C:\\workspace\\sample1-project\\" + "/model/");

      Assert.assertEquals(DirectoryUtils.cProtocoleFile + "C:/workspace/sample1-project/model/", vResult);
   }

   @Test
   public void getFileWithoutExtension_CN1 () {
      String vResult = DirectoryUtils.getFileWithoutExtension("/sample4menus/Sirhen_D01.entity");

      Assert.assertEquals("Sirhen_D01", vResult);
   }

   @Test
   public void getFileWithoutExtension_CN2 () {
      String vResult = DirectoryUtils.getFileWithoutExtension("sample4menus.Sirhen_D01.entity");

      Assert.assertEquals("Sirhen_D01", vResult);
   }

   @Test
   public void getFileWExtension_CN1 () {
      String vResult = DirectoryUtils.getFileExtension("/sample4menus/Sirhen_D01.entity");

      Assert.assertEquals(".entity", vResult);
   }

   @Test
   public void getFile_CN1 () {
      String vResult = DirectoryUtils.getFile("/sample4menus/Sirhen_D01.entity");

      Assert.assertEquals("Sirhen_D01.entity", vResult);
   }

   @Test
   public void getFile_CN2 () {
      String vResult = DirectoryUtils.getFile("/sample4menus/");

      Assert.assertNull(vResult);
   }

   @Test
   public void getFile_CN3 () {
      String vResult = DirectoryUtils.getFile("Sirhen_D01.entity");

      Assert.assertEquals("Sirhen_D01.entity", vResult);
   }

   @Test
   public void getAbsolutePath_CN1 () {
      String vResult = DirectoryUtils.getAbsolutePath("file:\\C:\\WS_ProMinEdu\\sample1-model\\Sirhen_D01.entity");

      Assert.assertEquals("C:/WS_ProMinEdu/sample1-model/", vResult);
   }

   @Test
   public void getAbsolutePath_CN2 () {
      String vResult = DirectoryUtils.getAbsolutePath("C:\\WS_ProMinEdu\\sample1-model\\Sirhen_D01.entity");

      Assert.assertEquals("C:/WS_ProMinEdu/sample1-model/", vResult);
   }

   @Test
   public void getProtocole_CN1 () {
      String vResult = DirectoryUtils
            .getProtocole(DirectoryUtils.cProtocoleFile + "C:\\WS_ProMinEdu\\sample1-model\\Sirhen_D01.entity");

      Assert.assertEquals(DirectoryUtils.cProtocoleFile, vResult);
   }

   @Test
   public void getProtocole_CN2 () {
      String vResult = DirectoryUtils.getProtocole(
            DirectoryUtils.cProtocolePlateformResource + "C:\\WS_ProMinEdu\\sample1-model\\Sirhen_D01.entity");

      Assert.assertEquals(DirectoryUtils.cProtocolePlateformResource, vResult);
   }

   @Test
   public void getWithoutProtocole_CN1 () {
      String vResult = DirectoryUtils.getWithoutProtocole(
            DirectoryUtils.cProtocoleFile + "C:\\WS_ProMinEdu\\sample1-model\\Sirhen_D01.entity");

      Assert.assertEquals("C:\\WS_ProMinEdu\\sample1-model\\Sirhen_D01.entity", vResult);
   }

   @Test
   public void getWithoutProtocole_CN2 () {
      String vResult = DirectoryUtils.getWithoutProtocole(
            DirectoryUtils.cProtocolePlateformResource + "C:\\WS_ProMinEdu\\sample1-model\\Sirhen_D01.entity");

      Assert.assertEquals("C:\\WS_ProMinEdu\\sample1-model\\Sirhen_D01.entity", vResult);
   }

   @Test
   public void replaceLastDirectory_CN1 () {
      String vResult = DirectoryUtils.replaceLastDirectory("C:\\WS_ProMinEdu\\sample1-model\\Sirhen_D01.entity",
            "sample2");

      Assert.assertEquals("C:/WS_ProMinEdu/sample2/Sirhen_D01.entity", vResult);
   }

   @Test
   public void replaceLastDirectory_CN2 () {
      String vResult = DirectoryUtils.replaceLastDirectory("C:\\WS_ProMinEdu\\sample1-model\\", "sample2");

      Assert.assertEquals("C:/WS_ProMinEdu/sample2/", vResult);
   }

   @Test
   public void getApplicationRootDirectory_CN1 () {
      String vResult = DirectoryUtils.getExecutionRootDirectory(this);

      Assert.assertEquals("file:/T:/Ws_Pro_DNE/WSS_AWAX/fr.awax.acceleo.common/bin/", vResult);
   }

   @Test
   public void getRelectivePathBack_CN0 () {
      String vResult = DirectoryUtils.getRelatifPathBack("");

      Assert.assertEquals("", vResult);
   }

   @Test
   public void getRelectivePathBack_CN1 () {
      String vResult = DirectoryUtils.getRelatifPathBack("rep1/rep2/rep3/");

      Assert.assertEquals("../../../", vResult);
   }

   @Test
   public void getListDirectory_CN0 () {
      List<String> vResult = DirectoryUtils.getListDirectory("");

      Assert.assertEquals(0, vResult.size());
   }

   @Test
   public void getListDirectory_CN0b () {
      List<String> vResult = DirectoryUtils.getListDirectory("/");

      Assert.assertEquals(1, vResult.size());
      Assert.assertEquals("/", vResult.get(0));
   }

   @Test
   public void getListDirectory_CN1 () {
      List<String> vResult = DirectoryUtils.getListDirectory("/rep1/rep2/rep3/");

      Assert.assertEquals(4, vResult.size());
      int i = 0;
      Assert.assertEquals("/", vResult.get(i++));
      Assert.assertEquals("/rep1/", vResult.get(i++));
      Assert.assertEquals("/rep1/rep2/", vResult.get(i++));
      Assert.assertEquals("/rep1/rep2/rep3/", vResult.get(i++));
   }

   @Test
   public void getListDirectory_CN2 () {
      List<String> vResult = DirectoryUtils.getListDirectory("rep1/rep2/rep3/");

      Assert.assertEquals(3, vResult.size());
      int i = 0;
      Assert.assertEquals("rep1/", vResult.get(i++));
      Assert.assertEquals("rep1/rep2/", vResult.get(i++));
      Assert.assertEquals("rep1/rep2/rep3/", vResult.get(i++));
   }

   @Test
   public void getListDirectoryUnitaire_CN0 () {
      List<String> vResult = DirectoryUtils.getListDirectoryUnitaire("");

      Assert.assertEquals(0, vResult.size());
   }

   @Test
   public void getListDirectoryUnitaire_CN0b () {
      List<String> vResult = DirectoryUtils.getListDirectoryUnitaire("/");

      Assert.assertEquals(1, vResult.size());
      Assert.assertEquals("/", vResult.get(0));
   }

   @Test
   public void getListDirectoryUnitaire_CN1 () {
      List<String> vResult = DirectoryUtils.getListDirectoryUnitaire("/rep1/rep2/rep3/");

      Assert.assertEquals(4, vResult.size());
      int i = 0;
      Assert.assertEquals("/", vResult.get(i++));
      Assert.assertEquals("rep1/", vResult.get(i++));
      Assert.assertEquals("rep2/", vResult.get(i++));
      Assert.assertEquals("rep3/", vResult.get(i++));
   }

   @Test
   public void getListDirectoryUnitaire_CN2 () {
      List<String> vResult = DirectoryUtils.getListDirectoryUnitaire("rep1/rep2/rep3/");

      Assert.assertEquals(3, vResult.size());
      int i = 0;
      Assert.assertEquals("rep1/", vResult.get(i++));
      Assert.assertEquals("rep2/", vResult.get(i++));
      Assert.assertEquals("rep3/", vResult.get(i++));
   }

   @Test
   public void sanitizedPath_CN () {
      String actual = DirectoryUtils.sanitizedPath(null);
      Assert.assertNull(actual);

      actual = DirectoryUtils.sanitizedPath("T:\\rep1/rep2\\\\rep3//rep4///");
      Assert.assertEquals("T:/rep1/rep2/rep3/rep4/", actual);
   }

   @Test
   public void getDeltaRelatifPath () {
      String actual = DirectoryUtils.getDeltaRelatifPath(null, null);
      Assert.assertNull(actual);

      actual = DirectoryUtils.getDeltaRelatifPath("viewcontainer/enseignant/", "viewcontainer/enseignant/titulaire/");
      Assert.assertEquals("titulaire/", actual);

      actual = DirectoryUtils.getDeltaRelatifPath("viewcontainer/enseignant/EcrChoisirEnseignant.html", "viewcontainer/enseignant/titulaire/EcrSaisirEnseignantTitulaire.html");
      Assert.assertEquals("titulaire/EcrSaisirEnseignantTitulaire.html", actual);

      actual = DirectoryUtils.getDeltaRelatifPath("viewcontainer/enseignant/titulaire/EcrSaisirEnseignantTitulaire.html", "viewcontainer/enseignant/EcrChoisirEnseignant.html");
      Assert.assertEquals("../EcrChoisirEnseignant.html", actual);
      
      actual = DirectoryUtils.getDeltaRelatifPath("viewcontainer/enseignant/EcrSaisirEnseignantTitulaire.html", "viewcontainer/enseignant/EcrChoisirEnseignant.html");
      Assert.assertEquals("EcrChoisirEnseignant.html", actual);
      
      actual = DirectoryUtils.getDeltaRelatifPath("viewcontainer/enseignant/", "viewcontainer/enseignant/");
      Assert.assertEquals("", actual);

      actual = DirectoryUtils.getDeltaRelatifPath("pages/root/EcrAjouterEnseignant.html", "services/createEnseignant.html");
      Assert.assertEquals("../../services/createEnseignant.html", actual);
   }
}
