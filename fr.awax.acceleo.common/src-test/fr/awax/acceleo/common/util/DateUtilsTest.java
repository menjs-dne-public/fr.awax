package fr.awax.acceleo.common.util;

import java.rmi.activation.UnknownObjectException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class DateUtilsTest {

   // getNowDate()
   //

   @Test
   public void getNowDate_CN1() {
      Date vDate = DateUtils.getNowDate();
      
      // Vérifier qu'il y a une instance
      Assert.assertNotNull(vDate);
   }

   /**
    * Tester un changement
    */
   @Test
   public void getNowDate_CN2() {
      // Obtenir l'instance permettant de changer la date système par simulation
       DateUtils vDateUtils = DateUtils4Tests.getInstance4Tests();
       // Définir la date et l'heure
       Date vNowExpected = DateUtils.getDateTimeSpecified(2015, 9, 1, 10, 0, 0, 0);
       // Modifier la date
       vDateUtils.setDateReference(vNowExpected);
       // Obtenir la date
       Date vNowActual = DateUtils.getNowDate();
      
      // Vérifier qu'il y a une instance
      Assert.assertEquals(vNowExpected, vNowActual);
   }
   
   // getCalendarTimeOnly()
   //

   /**
    * Tester que la comparaison en termes d'heures fonctionne.
    */
   @Test
   public void getCalendarTimeOnly_CN1() {
      // Obtenir 01 juillet 2015 à 10h37m53 et 235millis
      Date vDate1 = DateUtils.getDateTimeSpecified(1, 7, 2015, 10, 37, 53, 253);
      Calendar vCalendar1 = DateUtils.getCalendar(vDate1);
      // Obtenir 02 juillet 2015 à 07h37m53 et 235millis
      Date vDate2 = DateUtils.getDateTimeSpecified(2, 7, 2015, 7, 37, 53, 253);
      Calendar vCalendar2 = DateUtils.getCalendar(vDate2);

      Calendar vTimeOnly1 = DateUtils.getCalendarTimeOnly(vDate1);
      Calendar vTimeOnly2 = DateUtils.getCalendarTimeOnly(vDate2);

      // Vérifier que vTimeOnly1 > vTimeOnly2 (en termes d'heures uniquement)
      Assert.assertTrue(vTimeOnly1.after(vTimeOnly2));
      // Vérifier que vCalendar1 < vCalendar2 (en termes de date+heure)
      Assert.assertTrue(vCalendar1.before(vCalendar2));
   }

   /**
    * Tester que la comparaison en termes d'heures fonctionne - 2ème exemple.
    */
   @Test
   public void getCalendarTimeOnly_CN2() {
      // Obtenir 02 juillet 2015 à 10h37m53 et 235millis
      Date vDate1 = DateUtils.getDateTimeSpecified(2, 7, 2015, 10, 37, 53, 253);
      Calendar vCalendar1 = DateUtils.getCalendar(vDate1);
      // Obtenir 01 juillet 2015 à 07h37m53 et 235millis
      Date vDate2 = DateUtils.getDateTimeSpecified(1, 7, 2015, 7, 37, 53, 253);
      Calendar vCalendar2 = DateUtils.getCalendar(vDate2);

      Calendar vTimeOnly1 = DateUtils.getCalendarTimeOnly(vDate1);
      Calendar vTimeOnly2 = DateUtils.getCalendarTimeOnly(vDate2);

      // Vérifier que vTimeOnly1 > vTimeOnly2 (en termes d'heures uniquement)
      Assert.assertTrue(vTimeOnly1.after(vTimeOnly2));
      // Vérifier que vCalendar1 < vCalendar2 (en termes de date+heure)
      Assert.assertTrue(vCalendar1.after(vCalendar2));
   }

   /**
    * Tester que la comparaison en termes d'heures fonctionne - 3ème exemple.
    */
   @Test
   public void getCalendarTimeOnly_CN3() {
      // Obtenir 30 novembre 1970 à 11h30m59 et 999millis
      Date vDate1 = DateUtils.getDateTimeSpecified(30, 11, 1970, 11, 30, 59, 999);
      Calendar vCalendar1 = DateUtils.getCalendar(vDate1);
      // Obtenir 02 juillet 2015 à 11h43m53 et 235millis
      Date vDate2 = DateUtils.getDateTimeSpecified(2, 7, 2015, 11, 43, 53, 253);
      Calendar vCalendar2 = DateUtils.getCalendar(vDate2);

      Calendar vTimeOnly1 = DateUtils.getCalendarTimeOnly(vDate1);
      Calendar vTimeOnly2 = DateUtils.getCalendarTimeOnly(vDate2);

      // Vérifier que vTimeOnly2 > vTimeOnly1 (en termes d'heures uniquement)
      Assert.assertTrue(vTimeOnly2.after(vTimeOnly1));
      // Vérifier que vCalendar2 > vCalendar1 (en termes de date+heure)
      Assert.assertTrue(vCalendar2.after(vCalendar1));
   }

   /**
    * Tester que la comparaison en termes d'heures fonctionne - 4ème exemple.
    */
   @Test
   public void getCalendarTimeOnly_CN4() {
      // Obtenir 30 novembre 1970 à 11h30m59 et 999millis
      // 01/01/0001 09:30:59
      Date vDate1 = DateUtils.getDateTimeSpecified(01, 01, 0001, 11, 30, 59, 999);
      Calendar vCalendar1 = DateUtils.getCalendar(vDate1);
      // Obtenir 02 juillet 2015 à 11h43m53 et 235millis
      // 01/01/0001 11:43:18
      Date vDate2 = DateUtils.getDateTimeSpecified(01, 01, 0001, 11, 43, 18, 253);
      Calendar vCalendar2 = DateUtils.getCalendar(vDate2);

      Calendar vTimeOnly1 = DateUtils.getCalendarTimeOnly(vDate1);
      Calendar vTimeOnly2 = DateUtils.getCalendarTimeOnly(vDate2);

      // Vérifier que vTimeOnly2 > vTimeOnly1 (en termes d'heures uniquement)
      Assert.assertTrue(vTimeOnly2.after(vTimeOnly1));
      // Vérifier que vCalendar2 > vCalendar1 (en termes de date+heure)
      Assert.assertTrue(vCalendar2.after(vCalendar1));
   }

   // getDateTimeSpecified() 
   //
   @Test
   public void getDateTimeSpecified() {
      // Obtenir 30 novembre 1970 à 14h30m00 et 0millis
      Date vDate1 = DateUtils.getDateTimeSpecified(30, 11, 1970, 14, 30, 0, 0);
      String vDate1Fmt = DateUtils.getDateTimeFormat(vDate1);

      // Vérifier
      Assert.assertEquals("30/11/1970 14:30:00", vDate1Fmt);
   }

   @Test
   public void getTimeFormat_CN1() {
      // Obtenir 30 novembre 1970 à 14h30m00 et 0millis
      Date vDate1 = DateUtils.getDateTimeSpecified(30, 11, 1970, 14, 30, 0, 0);
      String vDate1Fmt = DateUtils.getTimeFormat(vDate1);

      // Vérifier
      Assert.assertEquals("14:30:00", vDate1Fmt);
   }

   @Test
   public void getNbJoursCalendaire_CN1() {
      Date vDateStart = DateUtils.getDateTimeSpecified(01, 07, 2015, 0, 30, 0, 0);
      Date vDateEnd = DateUtils.getDateTimeSpecified(01, 07, 2015, 14, 30, 0, 0);
      int vNbJoursCalendaire = DateUtils.getNbJoursCalendaire(vDateStart, vDateEnd);

      // Vérifier
      Assert.assertEquals(1, vNbJoursCalendaire);
   }

   @Test
   public void getNbJoursCalendaire_CN2() {
      Date vDateStart = DateUtils.getDateTimeSpecified(01, 07, 2015, 0, 30, 0, 0);
      Date vDateEnd = DateUtils.getDateTimeSpecified(02, 07, 2015, 14, 30, 0, 0);
      int vNbJoursCalendaire = DateUtils.getNbJoursCalendaire(vDateStart, vDateEnd);

      // Vérifier
      Assert.assertEquals(2, vNbJoursCalendaire);
   }

   @Test
   public void getNbJoursCalendaire_CN3() {
      Date vDateStart = DateUtils.getDateTimeSpecified(01, 07, 2015, 0, 30, 0, 0);
      Date vDateEnd = DateUtils.getDateTimeSpecified(20, 07, 2015, 14, 30, 0, 0);
      int vNbJoursCalendaire = DateUtils.getNbJoursCalendaire(vDateStart, vDateEnd);

      // Vérifier
      Assert.assertEquals(20, vNbJoursCalendaire);
   }

   @Test
   public void getDateWithStartOfDay_CN1() {
      // 01/07/2015 15h30m00
      Date vDate = DateUtils.getDateTimeSpecified(01, 07, 2015, 15, 30, 0, 0);
      Date vDateStart = DateUtils.getDateWithStartOfDay(vDate);

      // Vérifier
      Assert.assertEquals(DateUtils.getDateTimeSpecified(01, 07, 2015, 0, 0, 0, 0), vDateStart);
   }

   @Test
   public void getDateWithEndOfDay_CN1() {
      // 01/07/2015 15h30m00
      Date vDate = DateUtils.getDateTimeSpecified(01, 07, 2015, 15, 30, 0, 0);
      Date vDateEnd = DateUtils.getDateWithEndOfDay(vDate);

      // Vérifier
      Assert.assertEquals(DateUtils.getDateTimeSpecified(01, 07, 2015, 23, 59, 59, 999), vDateEnd);
   }

   @Test
   public void getMapDesJoursDeLaPeriode_CN1a() {
      Date vDateStart = DateUtils.getDateWithoutTime(DateUtils.getDateTimeSpecified(01, 07, 2015, 0, 0, 0, 0));
      Date vDateEnd = DateUtils.getDateWithoutTime(DateUtils.getDateTimeSpecified(05, 07, 2015, 0, 0, 0, 0));

      // Obtenir la Map des jours de la période dans l'ordre chronologique
      Map<Date, Date> vMapCalendrier = DateUtils.getMapDesJoursDeLaPeriode(vDateStart, vDateEnd, true);

      // Vérifier la taille de la Map
      Assert.assertEquals(5, vMapCalendrier.size());

      // Obtenir la première date
      Date vDate = (Date) MapUtils.getFirstElem(vMapCalendrier);
      // Vérifier que le premier élément de la Map correspond au premier jour de la période
      Assert.assertEquals(vDateStart, vDate);

      // Obtenir la dernière date
      vDate = (Date) MapUtils.getLastElem(vMapCalendrier);
      // Vérifier que le dernier élément de la Map correspond au dernier jour de la période
      Assert.assertEquals(vDateEnd, vDate);
   }

   @Test
   public void getMapDesJoursDeLaPeriode_CN1b() {
      Date vDateStart = DateUtils.getDateWithoutTime(DateUtils.getDateTimeSpecified(30, 7, 2015, 0, 0, 0, 0));
      Date vDateEnd = DateUtils.getDateWithoutTime(DateUtils.getDateTimeSpecified(2, 8, 2015, 0, 0, 0, 0));

      // Obtenir la Map des jours de la période dans l'ordre chronologique
      Map<Date, Date> vMapCalendrier = DateUtils.getMapDesJoursDeLaPeriode(vDateStart, vDateEnd, true);

      // Vérifier la taille de la Map
      Assert.assertEquals(4, vMapCalendrier.size());

      // Obtenir la première date
      Date vDate = (Date) MapUtils.getFirstElem(vMapCalendrier);
      // Vérifier que le premier élément de la Map correspond au premier jour de la période
      Assert.assertEquals(vDateStart, vDate);

      // Obtenir la dernière date
      vDate = (Date) MapUtils.getLastElem(vMapCalendrier);
      // Vérifier que le dernier élément de la Map correspond au dernier jour de la période
      Assert.assertEquals(vDateEnd, vDate);
   }

   @Test
   public void getMapDesJoursDeLaPeriode_CN1c() {
      Date vDateStart = DateUtils.getDateWithoutTime(DateUtils.getDateTimeSpecified(6, 7, 2015, 0, 0, 0, 0));
      Date vDateEnd = DateUtils.getDateWithoutTime(DateUtils.getDateTimeSpecified(5, 8, 2015, 0, 0, 0, 0));

      // Obtenir la Map des jours de la période dans l'ordre chronologique
      Map<Date, Date> vMapCalendrier = DateUtils.getMapDesJoursDeLaPeriode(vDateStart, vDateEnd, true);

      // Vérifier la taille de la Map
      Assert.assertEquals(31, vMapCalendrier.size());

      // Obtenir la première date
      Date vDate = (Date) MapUtils.getFirstElem(vMapCalendrier);
      // Vérifier que le premier élément de la Map correspond au premier jour de la période
      Assert.assertEquals(vDateStart, vDate);

      // Obtenir la dernière date
      vDate = (Date) MapUtils.getLastElem(vMapCalendrier);
      // Vérifier que le dernier élément de la Map correspond au dernier jour de la période
      Assert.assertEquals(vDateEnd, vDate);
   }

   @Test
   public void getMapDesJoursDeLaPeriode_CN2a() {
      Date vDateStart = DateUtils.getDateWithoutTime(DateUtils.getDateTimeSpecified(01, 07, 2015, 0, 0, 0, 0));
      Date vDateEnd = DateUtils.getDateWithoutTime(DateUtils.getDateTimeSpecified(05, 07, 2015, 0, 0, 0, 0));

      // Obtenir la Map des jours de la période dans l'ordre chronologique inverse
      Map<Date, Date> vMapCalendrier = DateUtils.getMapDesJoursDeLaPeriode(vDateStart, vDateEnd, false);

      // Vérifier la taille de la Map
      Assert.assertEquals(5, vMapCalendrier.size());

      // Obtenir la première date
      Date vDate = (Date) MapUtils.getFirstElem(vMapCalendrier);
      // Vérifier que le premier élément de la Map correspond au dernier jour de la période
      Assert.assertEquals(vDateEnd, vDate);

      // Obtenir la dernière date
      vDate = (Date) MapUtils.getLastElem(vMapCalendrier);
      // Vérifier que le dernier élément de la Map correspond au premier jour de la période
      Assert.assertEquals(vDateStart, vDate);
   }

   @Test
   public void getMapDesJoursDeLaPeriode_CN2b() {
      Date vDateStart = DateUtils.getDateWithoutTime(DateUtils.getDateTimeSpecified(30, 7, 2015, 0, 0, 0, 0));
      Date vDateEnd = DateUtils.getDateWithoutTime(DateUtils.getDateTimeSpecified(2, 8, 2015, 0, 0, 0, 0));

      // Obtenir la Map des jours de la période dans l'ordre chronologique inverse
      Map<Date, Date> vMapCalendrier = DateUtils.getMapDesJoursDeLaPeriode(vDateStart, vDateEnd, false);

      // Obtenir la première date
      Date vDate = (Date) MapUtils.getFirstElem(vMapCalendrier);
      // Vérifier que le premier élément de la Map correspond au dernier jour de la période
      Assert.assertEquals(vDateEnd, vDate);

      // Obtenir la dernière date
      vDate = (Date) MapUtils.getLastElem(vMapCalendrier);
      // Vérifier que le dernier élément de la Map correspond au premier jour de la période
      Assert.assertEquals(vDateStart, vDate);
   }

   @Test
   public void addDay_CN1() {
      Date vDateOri = DateUtils.getDateSpecified(1, 7, 2015);
      // Enlever 1 jour
      Date vDateResult = DateUtils.addDay(vDateOri, -1);

      // Vérifier
      Assert.assertNotEquals(vDateOri, vDateResult);
      Assert.assertEquals(DateUtils.getDateSpecified(30, 6, 2015), vDateResult);
   }
   
   @Test
   public void parseDate_CN0() {
      Date vDateExpected = null;
      // Appeler la méthode
      Date vDateActual = DateUtils.parseDate(null);
      // Vérifier
      Assert.assertEquals(vDateExpected, vDateActual);

      vDateExpected = null;
      // Appeler la méthode
      vDateActual = DateUtils.parseDate("");
      // Vérifier
      Assert.assertEquals(vDateExpected, vDateActual);
   }

   @Test
   public void parseDate_CN1() {
      Date vDateExpected = DateUtils.getDateSpecified(3, 9, 2015);
      
      Date vDateActual = DateUtils.parseDate("03/09/2015");
      
      // Vérifier
      Assert.assertEquals(vDateExpected, vDateActual);
   }

   @Test
   public void parseDate_CN2() {
      Date vDateExpected = DateUtils.getDateTimeSpecified(3, 9, 2015, 21, 3, 35, 99);
      
      Date vDateActual = DateUtils.parseDate("03/09/2015 21h03m35s099");
      
      // Vérifier
      Assert.assertEquals(vDateExpected, vDateActual);
   }
   

   @Test
   public void getNumberDayOfWeek_CN1() {
      // 06/07/2015 est un lundi
      Date vDate = DateUtils.getDateSpecified(6, 7, 2015);

      int vJourActual = DateUtils.getNumberDayOfWeek(vDate);

      // Vérifier que le jour obtenu est un lundi
      Assert.assertEquals(1, vJourActual);
   }
   
   @Test
   public void getNumberDayOfWeek_CN2() {
      // 12/07/2015 est un dimanche
      Date vDate = DateUtils.getDateSpecified(12, 7, 2015);

      int vJourActual = DateUtils.getNumberDayOfWeek(vDate);

      // Vérifier que le jour obtenu est un lundi
      Assert.assertEquals(7, vJourActual);
   }

   @Test
   public void isJourOuvre_CN1() {
      // 12/07/2015 est un dimanche
      Date vDate = DateUtils.getDateSpecified(12, 7, 2015);
      
      boolean vIsJourOuvre = DateUtils.isJourOuvre(vDate);
      
      // Vérifier que ce n'est pas un jour ouvré
      Assert.assertTrue(false == vIsJourOuvre);
   }
   
   @Test
   public void isJourOuvre_CN2() {
      // 13/07/2015 est un lundi
      Date vDate = DateUtils.getDateSpecified(13, 7, 2015);
      
      boolean vIsJourOuvre = DateUtils.isJourOuvre(vDate);
      
      // Vérifier que c'est un jour ouvré
      Assert.assertTrue(vIsJourOuvre);
   }

   @Test
   public void isJourOuvre_CN3() {
      // 17/07/2015 est un vendredi
      Date vDate = DateUtils.getDateSpecified(17, 7, 2015);
      
      boolean vIsJourOuvre = DateUtils.isJourOuvre(vDate);
      
      // Vérifier que c'est un jour ouvré
      Assert.assertTrue(vIsJourOuvre);
   }

   @Test
   public void isJourOuvre_CN4() {
      // 18/07/2015 est un samedi
      Date vDate = DateUtils.getDateSpecified(18, 7, 2015);
      
      boolean vIsJourOuvre = DateUtils.isJourOuvre(vDate);
      
      // Vérifier que ce n'est pas un jour ouvré
      Assert.assertTrue(false == vIsJourOuvre);
   }
   
   @Test
   public void getDatePreviousJourOuvre_CN1() {
      // 12/07/2015 est un dimanche
      Date vDate = DateUtils.getDateSpecified(12, 7, 2015);
      Date vDateJourOuvreExpected = DateUtils.getDateSpecified(10, 7, 2015);
            
      Date vDateJourOuvreActual = DateUtils.getDatePreviousJourOuvre(vDate);
      
      // Vérifier 
      Assert.assertEquals(vDateJourOuvreExpected, vDateJourOuvreActual);
   }

   @Test
   public void getDateNextJourOuvre_CN1() {
      // 11/07/2015 est un samedi
      Date vDate = DateUtils.getDateSpecified(11, 7, 2015);
      Date vDateJourOuvreExpected = DateUtils.getDateSpecified(13, 7, 2015);
            
      Date vDateJourOuvreActual = DateUtils.getDateNextJourOuvre(vDate);
      
      // Vérifier 
      Assert.assertEquals(vDateJourOuvreExpected, vDateJourOuvreActual);
   }
   
   /**
    * Tester la sérialisation en 'String' d'une Date.
    */
   @Test
   public void fromText_CN1 () {
      // 12 juilltet 2015 à 8H03m05s et 1ms
      Date vDateExpected = DateUtils.getDateTimeSpecified(9, 7, 2015, 8, 3, 5, 1);
      String vStringExpected = "09/07/2015 08h03m05s001";

      // Date --> String
      String vStringActual = DateUtils.toText(vDateExpected);
      // String --> Date
      Date vDateActual = DateUtils.fromText(vStringActual);
      
      // Vérifier
      Assert.assertEquals(vStringExpected, vStringActual);
      Assert.assertEquals(vDateExpected, vDateActual);
   }
   @Test
   public void fromText_CN2 () {
      // 12 juilltet 2015 à 8H03m05s et 1ms
      Date vDateExpected = DateUtils.getDateTimeSpecified(9, 7, 2015, 8, 3, 5, 0);
      String vStringExpected = "20150709_080305";
      
      // Date --> String
      String vStringActual = DateUtils.toText(vDateExpected, DateUtils.MasqueEnum.AAAAMMJJ_hhmmss);
      // String --> Date
      Date vDateActual = DateUtils.fromText(vStringActual);
      
      // Vérifier
      Assert.assertEquals(vStringExpected, vStringActual);
      Assert.assertEquals(vDateExpected, vDateActual);
   }
}

/**
 * Classe utilitaire pour les tests.
 * @author MinEdu
 * @example
 *     // Obtenir l'instance permettant de changer la date système par simulation
 *     DateUtils vDateUtils = DateUtils4Tests.getInstance4Tests();
 *     // Définir la date et l'heure
 *     Date vNowExpected = DateUtils.getDateTimeSpecified(2015, 9, 1, 10, 0, 0, 0);
 *     // Modifier la date
 *     vDateUtils.setDateReference(vNowExpected);
 *     // Obtenir la date
 *     Date vNowActual = DateUtils.getNowDate();
 *    
 *    // Vérifier qu'il y a une instance
 *    Assert.assertEquals(vNowExpected, vNowActual);
 */
class DateUtils4Tests extends DateUtils {
   /** Définir l'instance pour les tests. */
   private static DateUtils4Tests _Instance = new DateUtils4Tests();

   /**
    * Obtenir une instance pour appeler des méthodes dédiées aux tests.
    * @return L'instance désirée.
    */
   public static DateUtils4Tests getInstance4Tests() {
      _Instance.setInstance(_Instance);
      return _Instance;
   }
   
   /**
    * Affecter la date système.
    * @param pNow La date de départ.
    */
   @Override
   public void setDateReference(Date pNow) {
//      throw new UnsupportedOperationException("setDateReference_root non trouvé : vérifier l'implem");
      super.setDateReference4Tests(pNow, true);
   }

   /**
    * Permet d'annuler la date de référence pour les tests.
    */
   public void unsetDateReference() {
      super.unsetDateReference_root();
   }

}
