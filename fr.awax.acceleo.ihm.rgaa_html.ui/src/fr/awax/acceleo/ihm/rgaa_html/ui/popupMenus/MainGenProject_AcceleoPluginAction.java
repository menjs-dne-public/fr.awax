package fr.awax.acceleo.ihm.rgaa_html.ui.popupMenus;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.engine.TrtPostGeneration;
import fr.awax.acceleo.common.engine.TrtPostGenerationEnum;
import fr.awax.acceleo.common.engine.error.ErrorControler;
import fr.awax.acceleo.common.engine.ui.AcceleoGeneratePluginProjectAbstract;
import fr.awax.acceleo.ihm.rgaa_html.main.MainGenIhmRgaaHtml;
import fr.awax.acceleo.ihm.rgaa_html.ui.Activator;
import fr.awax.acceleo.ihm.rgaa_htmlconf.AllPropGenIhmHtmlRgaa;

/**
 * Classe sollicitée à un évènement clic sur à la racine d'un 'Project'.
 * cf. plugin.xml
 */
public class MainGenProject_AcceleoPluginAction extends AcceleoGeneratePluginProjectAbstract {
   /** Le ZIP contenant tous les ASSETS (= répertoires et fichiers nécessaires) pour utiliser les fichiers générés */
   private static final String cAssertZipFile = "assets/assets.zip";

   @Override
   public Object execute (final ExecutionEvent event) throws ExecutionException {
      String pluginId = Activator.PLUGIN_ID;
      EngineHelper vEngineHelper = EngineHelper.getInstance(pluginId);
      // Mémoriser l'identifiant du générateur
      vEngineHelper.setPluginId(pluginId);
      // Obtenir les propriétés pour le plugin de génération
      AllPropGenIhmHtmlRgaa allProp = AllPropGenIhmHtmlRgaa.getInstance(pluginId);
      allProp.raz();
//      allProp.load(); ICIR1 - Cf. AcceleoGeneratePluginProjectAbstract
      // Instancier le gestion de feedback d'erreur(s)
      ErrorControler vErrorControler = ErrorControler.getInstance(allProp.getPlugInId());
      vErrorControler.clear();

      // Mémoriser les Properties de l'application
      vEngineHelper.setAllProperties(allProp);
      
      AcceleoGeneratePluginHtmlRgaa vAcceleoGeneratePlugin = new AcceleoGeneratePluginHtmlRgaa();
      
      // Définir le traitement post-génération
      TrtPostGeneration vTrtPostGeneration = new TrtPostGeneration(TrtPostGenerationEnum.deleteEmptyLines);
      vTrtPostGeneration.addExtensionFile(".htm");
      vTrtPostGeneration.addExtensionFile(".html");
      vTrtPostGeneration.addXPropElem(allProp.propSpecif().path_maquette_html);
      
      return super.execute_root(allProp, vAcceleoGeneratePlugin, event, "Gen " + MainGenIhmRgaaHtml.class.getName(), "Generation " + vEngineHelper.getPluginId(), cAssertZipFile, vTrtPostGeneration);
   }

}
