package fr.awax.acceleo.ihm.rgaa_html.ui.popupMenus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.acceleo.engine.utils.AcceleoLaunchingUtil;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.business.api.session.Session;

import fr.awax.acceleo.common.conf.AllPropAbstract;
import fr.awax.acceleo.common.dsl.Dsl_Enum;
import fr.awax.acceleo.common.engine.ui.AcceleoGeneratePluginAbstract;
import fr.awax.acceleo.common.only_export.ScePathInstance;
import fr.awax.acceleo.common.util.DirectoryUtils;
import fr.awax.acceleo.ihm.rgaa_html.main.MainGenIhmRgaaHtml;
import fr.awax.acceleo.ihm.rgaa_htmlconf.AllPropGenIhmHtmlRgaa;

public class AcceleoGeneratePluginHtmlRgaa extends AcceleoGeneratePluginAbstract {

	@Override
	public void unzipAsserts(String pTargetAbsolutePath, AllPropAbstract pAllProp) {
		ScePathInstance vScePathInstance = ScePathInstance.getInstance();
		AllPropGenIhmHtmlRgaa vAllProp = (AllPropGenIhmHtmlRgaa) pAllProp;
		String path = vAllProp.propCommon().dir_generation_projet_root.getValue().toString() + "/"
				+ vAllProp.propSpecif().path_maquette_html.getValue().toString() + "/"
				+ vAllProp.propCommon().path_assets.getValue().toString() + "/";
		path = DirectoryUtils.uniformPath(path);
		path = vScePathInstance.doAbsolutePath(pTargetAbsolutePath, path);
		super.doUnzipAsserts(pAllProp, path);
	}

	@Override
	public void runGenerationAcceleo(Dsl_Enum pDsl_Enum, Session pSession, String pTargetAbsolutePath,
			IProgressMonitor pMonitor, EObject pModelRoot) throws IOException {
		super.runGenerationAcceleo_root(pDsl_Enum, pSession, pTargetAbsolutePath, pMonitor, pModelRoot);
		MainGenIhmRgaaHtml gen0 = new MainGenIhmRgaaHtml(pModelRoot, getAbsolutePathRoot(), new ArrayList<Object>());
//      MainGenIhmRgaaHtml gen0 = new MainGenIhmRgaaHtml(pModelRoot, new File(pTargetAbsolutePath), new ArrayList<Object>());
		// Mémoriser le générateur lancé
		super.addGeneratorRunning(gen0);
		String generationID = AcceleoLaunchingUtil.computeUIProjectID(pDsl_Enum.getNom(),
				"fr.awax.acceleo.ihm.rgaa_html.main.MainGenIhmRgaaHtml",
				pSession.getSessionResource().getURI().toString(), pTargetAbsolutePath, new ArrayList<String>());
//      gen0.clear(); // NE PAS FAIRE LE CLEAR A CE NIVEAU (fait en automatique à la fin de la génération)
		gen0.setGenerationID(generationID);
		gen0.doGenerate(BasicMonitor.toMonitor(pMonitor));
	}

}
