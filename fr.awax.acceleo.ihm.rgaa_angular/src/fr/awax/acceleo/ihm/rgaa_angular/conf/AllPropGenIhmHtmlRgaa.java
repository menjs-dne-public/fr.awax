package fr.awax.acceleo.ihm.rgaa_angular.conf;

import java.util.List;

import fr.awax.acceleo.common.conf.AllPropAbstract;
import fr.awax.acceleo.common.conf.IAllProp;
import fr.awax.acceleo.common.conf.XPropElem;

public class AllPropGenIhmHtmlRgaa extends AllPropAbstract {
   private AllPropGenIhmHtmlRgaa() {
      super();
   }

   public static AllPropGenIhmHtmlRgaa getInstance (final String pPlugInId) {
      AllPropGenIhmHtmlRgaa instance = (AllPropGenIhmHtmlRgaa) findInstance(pPlugInId);
      if (instance == null) {
         instance = new AllPropGenIhmHtmlRgaa();
         registerInstance(pPlugInId, instance);
      }
      return instance;
   }

   @Override
   public List<XPropElem> getAllPropertiesSpecif () throws Exception {
      List<XPropElem> vReturn = getListXProperty(propSpecif);
      return vReturn;
   }

   /** Les propriétés communes à tous les plugins. */
   private AllPropSpecif propSpecif = new AllPropSpecif() {
      // RAS
   };

   public AllPropSpecif propSpecif () {
      return propSpecif;
   }
   
   /**
    * Les propriétés spécifiques.
    * @author egarel
    */
   public class AllPropSpecif implements IAllProp {
      // TODO Mettre ici les propriétés spécifiques
      public final XPropElem path_maquette_html = new XPropElem("path_maquette_html", "maquette_html",
            "Le répertoire pour la maquette HTML RGAA compliant", true);
//      public final XPropElem optim_bdd_DB2 = new XPropElem("optim_bdd_DB2", "true",
//            "Optimisation (ou pas) pour une BdD DB2", false);

   }

}
