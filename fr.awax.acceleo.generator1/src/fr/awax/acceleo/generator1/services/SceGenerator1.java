package fr.awax.acceleo.generator1.services;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.awax.acceleo.common.engine.EngineHelper;
import fr.awax.acceleo.common.only_export.SceAbstractInstance;
import fr.awax.acceleo.common.only_export.ScePathInstance;
import fr.awax.acceleo.common.util.DirectoryUtils;
import fr.awax.acceleo.generator1.conf.AllPropGenerator1;

public class SceGenerator1 extends SceAbstractInstance {
   private ScePathInstance _ScePathInstance;

   public SceGenerator1() {
      super();
   }
   public SceGenerator1(String pPluginId) {
      super(pPluginId);
      _ScePathInstance = ScePathInstance.getInstance(pPluginId);
   }
   
   private static Map<String, SceGenerator1> _MapInstances = new LinkedHashMap<>();
   public static SceGenerator1 getInstance () {
      return getInstance(EngineHelper.getPluginIdRunning());
   }
   public static SceGenerator1 getInstance (String pPluginId) {
      SceGenerator1 instance = _MapInstances.get(pPluginId);
      if (instance == null) {
         instance = new SceGenerator1(pPluginId);
         _MapInstances.put(pPluginId, instance);
      }
      
      return instance;
   }

   @Override
   public void clear () {
      // RAS
   }
   
   public AllPropGenerator1 getAllProp () {
      return (AllPropGenerator1) super.getAllProp();
   }
   

   public final String get_path_generator1_sce () {
      String vReturn =
            getAllProp().propSpecif().path_generator1.getValue().toString() + "/" +
                  getAllProp().propSpecif().path_generator1.getValue().toString() + "/";
      vReturn = DirectoryUtils.uniformPath(vReturn);
      return _ScePathInstance.doRelativePath(null, vReturn);
   }
   
}
