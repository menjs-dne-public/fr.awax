package fr.awax.acceleo.generator1.conf;

import java.util.List;

import fr.awax.acceleo.common.conf.AllPropAbstract;
import fr.awax.acceleo.common.conf.IAllProp;
import fr.awax.acceleo.common.conf.XPropElem;

public class AllPropGenerator1 extends AllPropAbstract {
   private AllPropGenerator1() {
      super();
   }

   public static AllPropGenerator1 getInstance (final String pPlugInId) {
      AllPropGenerator1 instance = (AllPropGenerator1) findInstance(pPlugInId);
      if (instance == null) {
         instance = new AllPropGenerator1();
         registerInstance(pPlugInId, instance);
      }
      return instance;
   }

   @Override
   public List<XPropElem> getAllPropertiesSpecif () throws Exception {
      List<XPropElem> vReturn = getListXProperty(propSpecif);
      return vReturn;
   }

   /** Les propriétés communes à tous les plugins. */
   private AllPropSpecif propSpecif = new AllPropSpecif() {
      // RAS
   };

   public AllPropSpecif propSpecif () {
      return propSpecif;
   }
   
   /**
    * Les propriétés spécifiques.
    * @author egarel
    */
   public class AllPropSpecif implements IAllProp {
      /** Le path racine pour 'generator1'. */
      public final XPropElem path_generator1 = new XPropElem("path_generator1", "/the_path/",
            "Le répertoire racine pour le générateur 1", true);
      // TODO Mettre ici les propriétés spécifiques

   }

}
